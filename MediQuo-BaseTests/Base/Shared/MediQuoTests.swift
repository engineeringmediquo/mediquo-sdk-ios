//
//  MediQuoTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Nimble
import XCTest

@testable import MediQuo_Base
@testable import mediquo_videocall_lib

class MediQuoTests: XCTestCase {

    override class func setUp() {
        super.setUp()
        VideoCall.environment = .testing
    }

    func testJwtNotNil() {
        let mediQuo = MediQuo.instance
        expect(mediQuo.jwt).toNot(beNil())
    }

    func testInitializeSuccess() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.initialize(apiKey: TestConstants.Keys.apiKey) { result in
                if case .success = result {
                    expect(true).to(beTrue())
                    done()
                }
                if case let .failure(error) = result {
                    expect(true).to(beFalse())
                    expect(error).toNot(beNil())
                }
            }
        }
    }

    func testInitializeSuccessWithStyles() {
        var style: MediQuoStyleType = MediQuoStyle()
        style.prefersLargeTitles = true
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.initialize(apiKey: TestConstants.Keys.apiKey, style: style) { result in
                if case .success = result {
                    expect(true).to(beTrue())
                    done()
                }
                if case let .failure(error) = result {
                    expect(true).to(beFalse())
                    expect(error).toNot(beNil())
                }
            }
        }
    }

    func testInitializeFailure() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.initialize(apiKey: "") { result in
                if case .success = result {
                    expect(true).to(beFalse())
                }
                if case let .failure(error) = result {
                    expect(false).to(beFalse())
                    expect(error).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testAuthenticateSuccess() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.authenticate(clientCode: TestConstants.Keys.clientCode) { (status: SdkStatus) in
                switch status {
                case .ready:
                    expect(true).to(beTrue())
                    done()
                case .unauthorized(let error):
                    expect(true).to(beFalse())
                    expect(error).toNot(beNil())
                }
            }
        }
    }

    func testAuthenticateFailure() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.authenticate(clientCode: "") { (status: SdkStatus) in
                switch status {
                case .ready:
                    expect(true).to(beTrue())
                case .unauthorized(let error):
                    expect(false).to(beFalse())
                    expect(error).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testRegisterFirebase() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.registerFirebase(token: "TEST") { response in
                expect(true).to(beTrue())
                done()
            }
        }
    }

    func testGetPendingMessages() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.getPendingMessages { response in
                expect(true).to(beTrue())
                done()
            }
        }
    }

    func testGetProfessionalsList() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getProfessionalsList()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetMedicalHistory() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getMedicalHistory()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetAllergies() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getAllergies()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetAllergyForm() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getAllergyForm()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetDiseases() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getDiseases()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetDiseaseForm() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getDiseaseForm()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetMedications() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getMedications()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetMedicationForm() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getMedicationForm()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    // MARK: - Reports

    func testGetReports() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getReports()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetReportDetail() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getReportDetail("1234")
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetReportsListEmpty() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getReportsListEmpty()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetReportDetailPDF() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getReportDetailPDF(data: nil, with: nil)
            expect(viewController).toNot(beNil())
            done()
        }
    }

    // MARK: - Recipes

    func testGetRecipes() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getRecipes()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetRecipesListEmpty() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getRecipesListEmpty()
            expect(viewController).toNot(beNil())
            done()
        }
    }

    func testGetRecipesDetailPDF() {
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            let viewController = MediQuo.getRecipeDetailPDF(data: nil, with: nil)
            expect(viewController).toNot(beNil())
            done()
        }
    }

    // MARK: - Video Call

    func testDidReceiveRemoteNotificationCallRequested() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        let userInfo: [AnyHashable: Any] = ["data": "{ \"type\": \"call_requested\"}"]
        MediQuo.didReceiveRemoteNotification(userInfo: userInfo, completion: {})
        expect(true).to(beTrue())
    }

    func testDidReceiveRemoteNotificationCallRejected() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        let userInfo: [AnyHashable: Any] = ["data": "{ \"type\": \"call_rejected\"}"]
        MediQuo.didReceiveRemoteNotification(userInfo: userInfo, completion: {})
        expect(true).to(beTrue())
    }

    func testDidReceiveRemoteNotificationFailured() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        let userInfo: [AnyHashable: Any] = ["TEST": ""]
        MediQuo.didReceiveRemoteNotification(userInfo: userInfo, completion: {})
        expect(true).to(beTrue())
    }

    func testDidReceiveRemoteNotificationWithCompletionCallRequested() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["data": "{ \"module\": \"TEST\", \"type\": \"call_requested\", \"call\": {\"uuid\": \"TEST\", \"room_id\": 1}}"]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.didReceiveRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }

    func testDidReceiveRemoteNotificationWithCompletionCallRejected() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["data": "{ \"module\": \"TEST\", \"type\": \"call_rejected\", \"call\": {\"uuid\": \"TEST\", \"room_id\": 1}}"]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.didReceiveRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }

    func testDidReceiveRemoteNotificationWithCompletionFailured() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["TEST": ""]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.didReceiveRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }

    func testWillPresentRemoteNotificationCallRequested() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["data": "{ \"type\": \"call_requested\"}"]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.willPresentRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }

    func testWillPresentRemoteNotificationCallRejected() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["data": "{ \"type\": \"call_rejected\"}"]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.willPresentRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }

    func testWillPresentRemoteNotificationFailured() {
        UIApplication.shared.keyWindow?.rootViewController = mediquo_videocall_lib.MainViewController()
        var isFirstTime: Bool = false
        let userInfo: [AnyHashable: Any] = ["TEST": ""]
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            MediQuo.willPresentRemoteNotification(userInfo: userInfo) { response in
                if !isFirstTime {
                    isFirstTime = true
                    expect(true).to(beTrue())
                    done()
                }
            }
        }
    }
}
