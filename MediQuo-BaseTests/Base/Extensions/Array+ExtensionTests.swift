//
//  Array+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ArrayExtensionTests: XCTestCase {

    func testEquals() {
        let first: [String] = ["TEST", "BASE"]
        let second: [String] = ["TEST", "BASE"]
        expect(first).to(equal(second))
        expect(first.equals(as: second)).to(beTrue())
    }

    func testSumPositives() {
        let values: [Bool] = [true, false, true, true]
        expect(values.sumPositives()).to(equal(3))
    }
}
