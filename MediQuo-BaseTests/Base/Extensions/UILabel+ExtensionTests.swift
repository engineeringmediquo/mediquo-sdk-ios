//
//  UILabelExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Nimble
import XCTest

@testable import MediQuo_Base

class UILabelExtensionTests: XCTestCase {

    func testSettingAttributedString() {
        let value = UILabel()
        value.settingAttributedString(text: "TEST", color: .blue, lineSpacing: 5, alignment: .center)
        expect(value.text).to(equal("TEST"))
        expect(value.attributedText).toNot(beNil())
        expect(value.textAlignment).to(equal(.center))
    }
}
