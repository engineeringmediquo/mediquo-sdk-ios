//
//  String+ExtensionTests.swift
//  mediquo-professionals-libTests
//
//  Created by Pere Daniel Prieto on 07/11/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import Nimble
import XCTest

@testable import MediQuo_Base

class StringExtensionTests: XCTestCase {
    func testComputeAge_DifferentMonth_CurrentMonthHigher() {
        let birthDate = "1985-03-21"
        // Mock date: November 7th 2019 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1573120800)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(34))
    }

    func testComputeAge_DifferentMonth_CurrentMonthLower() {
        let birthDate = "1985-03-21"
        // Mock date: February 7th 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1549533600)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(33))
    }

    func testComputeAge_SameMonth_CurrentDayHigher() {
        let birthDate = "1985-03-21"
        // Mock date: March 22nd 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1553248800)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(34))
    }

    func testComputeAge_SameMonth_CurrentDayEqual() {
        let birthDate = "1985-03-21"
        // Mock date: March 21st 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1553162400)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(34))
    }

    func testComputeAge_SameMonth_CurrentDayLower() {
        let birthDate = "1985-03-21"
        // Mock date: March 11th 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1552298400)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(33))
    }

    func testComputeAge_SameDay_CurrentHourHigher() {
        let birthDate = "1985-03-21"
        // Mock date: March 21st 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1553162400)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(34))
    }

    func testComputeAge_SameDay_CurrentHourLower() {
        let birthDate = "1985-03-21"
        // Mock date: March 21st 2019 at 10:00:00
        let mockCurrentDate = Date(timeIntervalSince1970: 1553148000)
        expect(birthDate.computeAgeFromDate(toDate: mockCurrentDate)).to(equal(34))
    }

    func testIsValidEmail() {
        let value: String = "david.martin@mediquo.com"
        expect(value.isValidEmail).to(beTrue())
    }

    func testIsValidPhone() {
        let value: String = "691672677"
        expect(value.isValidPhone).to(beTrue())
    }

    func testGetSecuredString() {
        expect(MediQuo.instance.jwt).toNot(beNil())
    }
}
