//
//  NSMutableAttributedString+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class NSMutableAttributedStringExtensionTests: XCTestCase {

    func testNSMutableAttributedStringBold() {
        let value: NSMutableAttributedString = NSMutableAttributedString()
        value.bold("TEST")
        expect(true).to(beTrue())
    }

    func testNSMutableAttributedStringNormal() {
        let value: NSMutableAttributedString = NSMutableAttributedString()
        value.normal("TEST")
        expect(true).to(beTrue())
    }

    func testNSMutableAttributedStringUnderline() {
        let value: NSMutableAttributedString = NSMutableAttributedString()
        value.underline("TEST")
        expect(true).to(beTrue())
    }

    func testNSMutableAttributedStringItalic() {
        let value: NSMutableAttributedString = NSMutableAttributedString()
        value.italic("TEST")
        expect(true).to(beTrue())
    }
}
