//
//  NSAttributedString+ExtensionTests.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 26/08/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class NSAttributedStringExtensionTests: XCTestCase {

    func testSize() {
        let attributedString = NSAttributedString(string: "TEST")
        let size = attributedString.size(maxWidth: 16)
        expect(size.width).to(beCloseTo(CGFloat(16), within: 1.0))
    }
}
