//
//  Collection+SafeTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class CollectionSafeExtensionTests: XCTestCase {

    func testCollectionSafe() throws {
        let values: [String?] = ["TEST", nil, "TEST2"]
        let value = try XCTUnwrap(values[safe: 0])
        expect(value).to(equal("TEST"))
    }

    func testCollectionSafeNilPosition() throws {
        let values: [String?] = ["TEST", nil, "TEST2"]
        let value = try XCTUnwrap(values[safe: 1])
        expect(value).to(beNil())
    }
}
