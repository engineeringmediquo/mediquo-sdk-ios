//
//  NavigationControllerExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class NavigationControllerExtensionTests: XCTestCase {

    func testNavigationBack() {
        let navigation: UINavigationController = UINavigationController()
        navigation.back(to: UIViewController())
        expect(true).to(beTrue())
    }
}
