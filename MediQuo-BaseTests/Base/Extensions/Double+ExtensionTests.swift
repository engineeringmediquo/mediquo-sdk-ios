//
//  Double+ExtensionTests.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 26/08/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class DoubleExtensionTests: XCTestCase {

    func testCleanValueAbsolute() {
        let value = Double(1.0)
        let cleanValue: String = value.cleanValue
        expect(cleanValue).to(equal("1"))
    }

    func testCleanValueDecimal() {
        let value = Double(1.001)
        let cleanValue: String = value.cleanValue
        expect(cleanValue).to(equal("1.00"))
    }
}
