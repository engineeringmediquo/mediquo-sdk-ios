//
//  Bundle+IdentifierTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class BundleIdentifierExtensionTests: XCTestCase {

    func testLocalStorageKey() {
        let key = "TEST"
        let bundle = Bundle.localStorageKey(with: key)
        expect(bundle).to(equal("com.mediquo.sdk.TEST"))
    }
}
