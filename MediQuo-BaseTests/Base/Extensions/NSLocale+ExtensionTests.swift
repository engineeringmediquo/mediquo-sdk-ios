//
//  NSLocale+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class NSLocaleExtensionTests: XCTestCase {

    func testIs12hClockFormat() {
        let is12hClockFormat = NSLocale.is12hClockFormat
        if is12hClockFormat {
            expect(NSLocale.is12hClockFormat).to(beTrue())
        } else {
            expect(NSLocale.is12hClockFormat).to(beFalse())
        }
    }
}
