//
//  UIColor+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Nimble
import XCTest

@testable import MediQuo_Base

class UIColorExtensionTests: XCTestCase {

    func testColorHexSuccess() {
        let value = UIColor(hex: "#FFFFFFFF")
        expect(value).toNot(beNil())
    }

    func testColorHexFailure() {
        let value = UIColor(hex: "#TEST")
        expect(value).to(beNil())
    }

    func testColorHexFailureFormat() {
        let value = UIColor(hex: "TEST")
        expect(value).to(beNil())
    }
}
