//
//  CoreLog+ProfessionalsTests.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 26/08/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class CoreLogExtensionTests: XCTestCase {

    func testCoreLogForBusiness() {
        expect(CoreLog.business.identifier).to(equal("com.mediquo.base.lib"))
        expect(CoreLog.business.category).to(equal("business"))
    }

    func testCoreLogForUI() {
        expect(CoreLog.ui.identifier).to(equal("com.mediquo.base.lib"))
        expect(CoreLog.ui.category).to(equal("ui"))
    }

    func testCoreLogForFirebase() {
        expect(CoreLog.firebase.identifier).to(equal("com.mediquo.base.lib"))
        expect(CoreLog.firebase.category).to(equal("firebase"))
    }

    func testCoreLogForRemote() {
        expect(CoreLog.remote.identifier).to(equal("com.mediquo.base.lib"))
        expect(CoreLog.remote.category).to(equal("remote"))
    }

    func testCoreLogForSocket() {
        expect(CoreLog.socket.identifier).to(equal("com.mediquo.base.lib"))
        expect(CoreLog.socket.category).to(equal("socket"))
    }
}
