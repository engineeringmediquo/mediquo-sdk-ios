//
//  UIDevice+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Device
import Nimble
import XCTest

@testable import MediQuo_Base

class UIDeviceExtensionTests: XCTestCase {

    func testModelId() {
        let value = UIDevice.init()
        expect(value.modelId).toNot(beNil())
    }

    func testHasNotch() {
        let value = UIDevice.init()
        expect(value.hasNotch).toNot(beNil())
    }
}
