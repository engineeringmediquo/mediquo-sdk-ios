//
//  StorageManagerTests.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 11/07/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class StorageManagerTests: XCTestCase {

    @StorageDatasource
    private var securedData: ModelBuilderDataTest?

    @StorageDatasource(isSecured: false)
    private var nonSecuredData: ModelBuilderDataTest?

    override func setUp() {
        super.setUp()
        self.securedData?.model = [ModelBuilderTest(name: "mediQuo"), ModelBuilderTest(name: "mediQuo Pro")]
        self.nonSecuredData?.model = [ModelBuilderTest(name: "mediQuo"), ModelBuilderTest(name: "mediQuo Pro")]
    }

    // MARK: Secured

    func testSaveSecuredObjectSuccess() {
        if let storedData: [ModelBuilderTest] = self.securedData?.model {
            expect(storedData).notTo(beNil())
            expect(storedData).notTo(beEmpty())
            expect(storedData.count).to(equal(2))
        }
    }

    func testDeleteSecuredObject() {
        defer {
            let storedData: [ModelBuilderTest]? = self.securedData?.model
            expect(storedData).to(beNil())
        }
        self.securedData?.model = nil
    }

    // MARK: Non secured

    func testSaveNonSecuredObjectSuccess() {
        if let storedData: [ModelBuilderTest] = self.nonSecuredData?.model {
            expect(storedData).notTo(beNil())
            expect(storedData).notTo(beEmpty())
            expect(storedData.count).to(equal(2))
        }
    }

    func testDeleteNonSecuredObject() {
        defer {
            let storedData: [ModelBuilderTest]? = self.nonSecuredData?.model
            expect(storedData).to(beNil())
        }
        self.nonSecuredData?.model = nil
    }
}

private struct ModelBuilderDataTest: Codable {
    var model: [ModelBuilderTest]?
}

private struct ModelBuilderTest: Codable {
    let name: String
}
