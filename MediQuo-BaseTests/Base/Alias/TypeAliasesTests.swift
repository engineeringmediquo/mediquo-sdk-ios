//
//  TypeAliasesTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 30/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class TypeAliasesExtensionTests: XCTestCase {

    func testResultTypeAlias() {
        expect(ResultTypeAlias<Bool>.self).to(beAnInstanceOf(Result<Bool, BaseError>.Type.self))
    }

    func testRemoteCompletionTypeAlias() {
        expect(RemoteCompletionTypeAlias<TypeAliasesModelTest>.self).to(beAnInstanceOf(((ResultTypeAlias<TypeAliasesModelTest>) -> Void).Type.self))
    }

    func testEmptyCompletionTypeAlias() {
        expect(EmptyCompletionTypeAlias.self).to(beAnInstanceOf(((Result<Void, Error>) -> Void).Type.self))
    }
}

public struct TypeAliasesModelTest: Codable {}
