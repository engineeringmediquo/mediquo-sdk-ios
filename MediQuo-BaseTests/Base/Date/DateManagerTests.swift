//
//  DateManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 30/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class DateManagerTests: XCTestCase {

    func testConvertToDateFromRemote() {
        let stringDate = "2020-12-30 18:45:00"
        let date: Date? = DateManager.convertToDateFromRemote(stringDate)
        expect(date).toNot(beNil())
        expect(date).to(beCloseTo(Date(timeIntervalSince1970: 1609350300), within: 1000000))
    }

    func testConvertToDateFromRemoteISO() {
        let stringDate = "2020-12-30T18:45:00-0000"
        let date: Date? = DateManager.convertToDateFromRemoteISO(stringDate)
        expect(date).toNot(beNil())
        expect(date).to(beCloseTo(Date(timeIntervalSince1970: 1609353900)))
    }

    func testConvertToString() {
        let resultString = DateManager.convertToString(Date(), dateStyle: Optional.none, timeStyle: Optional.none)
        expect(resultString).toNot(beNil())
    }

    func testConvertToStringVariant() {
        let stringDate = "2020-12-30 18:45:00"
        let date: Date = DateManager.convertToDateFromRemote(stringDate) ?? Date()
        let resultString = DateManager.convertToString(date, with: nil)
        expect(resultString).toNot(beNil())
        expect(resultString).to(equal(stringDate))
    }

    func testConvertToStringForInbox() {
        let stringDate = "2020-12-30 18:45:00"
        let date: Date = DateManager.convertToDateFromRemote(stringDate) ?? Date()
        let resultString = DateManager.convertToStringForInbox(date)
        expect(resultString).toNot(beNil())
    }
}
