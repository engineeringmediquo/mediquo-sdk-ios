//
//  Codable+ExtensionTests.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 22/07/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class CodableExtensionTests: XCTestCase {

    private var model: CodableTestModel?

    override func setUp() {
        super.setUp()
        model = CodableTestModel(name: "Test")
    }

    func testStringDict() {
        let dict = model?.dictionary
        if let expectValue: String = dict?["name"] as? String {
            expect(dict).notTo(beNil())
            expect(expectValue).to(equal("Test"))
        }
    }

    func testCodableDict() {
        if let dict = model?.dictionary,
            let expctableResult = model?.name {
            do {
                let resultModel: CodableTestModel = try CodableTestModel.decode(from: dict)
                expect(expctableResult).to(equal(resultModel.name))
            } catch {
                fail(error.localizedDescription)
            }
        } else {
            fail("test model don't builded properly")
        }
    }

    func testCodableDictJson() {
        if let dict = model?.dictionary,
            let expctableResult = model?.name {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: [])
                let resultModel: CodableTestModel = try CodableTestModel.decode(data)
                expect(expctableResult).to(equal(resultModel.name))
            } catch {
                fail(error.localizedDescription)
            }
        } else {
            fail("test model don't builded properly")
        }
    }
}

private struct CodableTestModel: Codable {
    let name: String
}
