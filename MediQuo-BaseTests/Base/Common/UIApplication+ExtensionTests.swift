//
//  UIApplication+ExtensionTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class UIApplicationExtensionTests: XCTestCase {

    func testTopViewController() {
        let viewController: UIViewController = UIViewController()
        expect(UIApplication.topViewController(viewController)).toNot(beNil())
    }

    func testNavigationTopViewController() {
        let viewController: UIViewController = UIViewController()
        let navigationController: UINavigationController = UINavigationController(rootViewController: viewController)
        expect(UIApplication.topViewController(navigationController)).toNot(beNil())
    }

    func testTabBarTopViewController() {
        let viewController: UIViewController = UIViewController()
        let tabBarController: UITabBarController = UITabBarController()
        tabBarController.addChild(viewController)
        tabBarController.selectedViewController = viewController
        expect(UIApplication.topViewController(tabBarController)).toNot(beNil())
    }
}
