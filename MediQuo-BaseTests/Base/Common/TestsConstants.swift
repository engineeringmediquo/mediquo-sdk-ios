//
//  TestsConstants.swift
//  mediquo-professionals-libTests
//
//  Created by David Martin on 01/08/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import Foundation

enum TestConstants {
    enum Keys {
        static let apiKey: String = "qB3f8hmUKqrQUjfa"
        static let clientCode: String = "davidSDK"
        static let jwt: String = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vY2hhdC1kZXYubWVkaXF1by5jb20vc2RrL3YxL2F1dGhlbnRpY2F0ZSIsImlhdCI6MTYxMjE5MTI1NCwibmJmIjoxNjEyMTkxMjU0LCJqdGkiOiJUOTVIRzVUaEFRQUd0dll3Iiwic3ViIjoiYWE5NzJiYjYtNzdjZS00ZmM2LWJiN2YtNWNmODA0Zjg4MjZiIiwicHJ2IjoiOWVhNDBmMDk5MzU4OWE3OGQ1MmFjZThjNTNjMzA1OTM1MjBlZDQyNyJ9.F0Wnh_pNUD00mge69JxXhKTOP-PNA5SkwH6zcDgr9As"
    }

    enum WaitTime: TimeInterval {
        case short = 15
        case medium = 30
        case large = 60
    }
}
