//
//  RemoteManagerTests.swift
//  mediquo-professionals-libTests
//
//  Created by Edgar Paz Moreno on 08/07/2019.
//  Copyright © 2019 Mediquo. All rights reserved.
//

import XCTest
import Nimble
@testable import MediQuo_Base

// marvel api demo
let apiKey = "83ced18ccf293865ccbb569258f41382"
let apihash = "35f9374d58cc319449891dd916474e64"// md5(ts+privateKey+publicKey)

class RemoteManagerTests: XCTestCase {
    var configuration: RemoteConfiguration?
    var session: RemoteManager?

    override func setUp() {
        super.setUp()
        configuration = RemoteConfiguration(baseUrl: "https://gateway.marvel.com")
        if let config = configuration {
            session = RemoteManager(config)
        }
    }

    override func tearDown() {
        configuration = nil
        session = nil
    }

    func testGet() {
        if let mainSession = session {
            waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
                let request = RemoteTestRequest(apikey: apiKey, hash: apihash, ts: "1")
                let endpoint = "/v1/public/characters"
                mainSession.get(endpoint, parameters: request, completion: { (result: ResultTypeAlias<RemoteTestResponse?>) in
                    if case let .success(response) = result {
                        expect(response?.code).to(be(200))
                        done()
                    }
                })
            }
        }
    }
}

struct RemoteTestRequest: Codable {
    let apikey: String
    let hash: String
    let ts: String
}
struct RemoteTestResponse: Codable {
    let code: Int
}
