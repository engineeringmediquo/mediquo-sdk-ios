//
//  RemoteBasesUrlTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Nimble
import XCTest

@testable import MediQuo_Base

class RemoteBasesUrlTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    override func tearDown() {
        super.tearDown()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testSdkUrlToDebug() {
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)

        let url = RemoteBasesUrl.sdkApi
        expect(url).to(equal("https://sdk.dev.mediquo.com"))
    }

    func testSdkUrlToRelease() {
        let environment: EnvironmentType = .release
        self.manager.set(environment: environment)

        let url = RemoteBasesUrl.sdkApi
        expect(url).to(equal("https://sdk.mediquo.com"))
    }

    func testSocketUrlToDebug() {
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)

        let url = RemoteBasesUrl.socketApi
        expect(url).to(equal("https://chat-server-dev.mediquo.com"))
    }

    func testSocketUrlToRelease() {
        let environment: EnvironmentType = .release
        self.manager.set(environment: environment)

        let url = RemoteBasesUrl.socketApi
        expect(url).to(equal("https://chat-server.mediquo.com"))
    }
}
