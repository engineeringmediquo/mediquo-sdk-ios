//
//  ComponentsTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 30/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ComponentsTests: XCTestCase {
    func testInjections() {
        Components.resolve()
        expect(Resolver.shared.factoryDict).toNot(beNil())
        expect(Resolver.shared.factoryDict.count).to(beGreaterThan(0))
    }

    func testAuthModuleInject() {
        expect(Resolver.shared.resolve(AuthManager.self)).toNot(beNil())
        expect(Resolver.shared.resolve(AuthUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(AuthRepository.self)).toNot(beNil())
    }

    func testInstallationModuleInject() {
        expect(Resolver.shared.resolve(InstallationManager.self)).toNot(beNil())
        expect(Resolver.shared.resolve(InstallationUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(InstallationRepository.self)).toNot(beNil())
    }

    func testFirebaseModuleInject() {
        expect(Resolver.shared.resolve(RegisterFirebaseManager.self)).toNot(beNil())
        expect(Resolver.shared.resolve(RegisterFirebaseUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(RegisterFirebaseRepository.self)).toNot(beNil())
    }

    func testProfileModuleInject() {
        expect(Resolver.shared.resolve(ProfileManager.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfileUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfileRepository.self)).toNot(beNil())
    }

    func testProfessionalsListModuleInject() {
        expect(Resolver.shared.resolve(ProfessionalsListManager.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfessionalsListViewModel.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfessionalsListUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfessionalsListRepository.self)).toNot(beNil())
    }

    func testRoomModuleInject() {
        expect(Resolver.shared.resolve(RoomUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(RoomRepository.self)).toNot(beNil())
    }

    func testProfessionalProfileModuleInject() {
        expect(Resolver.shared.resolve(ProfessionalProfileViewModel.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfessionalProfileUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ProfessionalProfileRepository.self)).toNot(beNil())
    }

    func testMedicalHistoryModuleInject() {
        expect(Resolver.shared.resolve(MedicalHistoryViewModel.self)).toNot(beNil())
        expect(Resolver.shared.resolve(MedicalHistoryUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(MedicalHistoryRepository.self)).toNot(beNil())
    }

    func testReportsModuleInject() {
        expect(Resolver.shared.resolve(ReportsViewModel.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ReportsUseCase.self)).toNot(beNil())
        expect(Resolver.shared.resolve(ReportsRepository.self)).toNot(beNil())
    }
}
