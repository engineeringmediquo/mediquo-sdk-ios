//
//  SocketManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class SocketManagerTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    let socket = MediQuoSocketManager()

    private var roomId: Int = 0
    private let userHash: String = "043114c4-d4d1-4f57-b983-52e2bd7589a9"

    func testJoinRoom() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    expect(roomId).toNot(beNil())
                    self.socket.leave(by: roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testSendTextMessage() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    let model = MessageModel(text: "TEST", roomId: roomId, messageId: "1234", fromUserHash: "1234")

                    self.socket.send(message: model)
                    self.socket.leave(by: roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testSendImageMessage() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    let meta = MessageModel.Metadata.image(url: URL(string: "https://actualidadjoven.es/wp-content/uploads/2016/10/simp1.jpg")!,
                                                           thumb: URL(string: "https://actualidadjoven.es/wp-content/uploads/2016/10/simp1.jpg")!,
                                                           width: 10, height: 10, size: 100)
                    let model = MessageModel(image: UIImage(), roomId: roomId, messageId: messageId,
                                             fromUserHash: "1234", fileName: "image.jpg",
                                             metadata: meta)
                    self.socket.send(message: model)
                    self.socket.leave(by: roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testSendFileMessage() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    let meta = MessageModel.Metadata.file(name: "file.pdf", url: URL(string: "https://actualidadjoven.es/wp-content/uploads/2016/10/simp1.jpg")!, size: 10)
                    let model = MessageModel(image: UIImage(), roomId: roomId, messageId: messageId,
                                             fromUserHash: "1234", fileName: "file.pdf",
                                             metadata: meta)
                    self.socket.send(message: model)
                    self.socket.leave(by: roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testReceiveTextMessage() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onReceive()

                self.socket.message.subscribe { message in
                    expect(message).toNot(beNil())
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    let model = MessageModel(text: "TEST", roomId: roomId, messageId: messageId, fromUserHash: "1234")
                    self.socket.send(message: model)
                    self.socket.leave(by: roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testFetchMessages() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    self.roomId = roomId
                    let model = MessageModel(text: "TEST", roomId: roomId, messageId: messageId, fromUserHash: "1234")
                    self.socket.send(message: model)

                    self.socket.message.subscribe { message in
                        expect(message).toNot(beNil())
                    }
                    self.socket.fetch(by: roomId)
                }
                self.socket.onFetchMessages()

                self.socket.messages.subscribe { messages in
                    guard let messages = messages else { return }
                    expect(messages).toNot(beNil())
                    expect(messages.count).to(beGreaterThan(0))
                    self.socket.leave(by: self.roomId)
                    self.socket.closeAll()
                    done()
                }
            }
        }
    }

    func testFetchMessagesWithPagination() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    self.roomId = roomId
                    let model = MessageModel(text: "TEST", roomId: roomId, messageId: messageId, fromUserHash: "1234")
                    self.socket.send(message: model)

                    self.socket.message.subscribe { message in
                        expect(message).toNot(beNil())
                    }
                    self.socket.fetch(by: roomId)
                }
                self.socket.onFetchMessages()

                self.socket.messages.subscribe { messages in
                    guard let messages = messages else { return }
                    expect(messages).toNot(beNil())
                    expect(messages.count).to(beGreaterThan(0))
                    self.socket.leave(by: self.roomId)
                    self.socket.closeAll()
                    done()

                    guard let messageId = messages.first?.messageId else { return }
                    self.socket.fetch(by: self.roomId, pivotId: messageId, mode: .older)
                }
            }
        }
    }

    func testUpdateMessage() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            let messageId: String = String(Int.random(in: 0..<10_000_000))
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onUpdate()

                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    self.roomId = roomId
                    let model = MessageModel(text: "TEST", roomId: roomId, messageId: messageId, fromUserHash: "1234")
                    self.socket.send(message: model)

                    self.socket.messageStatus.subscribe { status in
                        expect(status).toNot(beNil())
                        self.socket.closeAll()
                        done()
                    }

                    self.socket.update(message: model, status: .read)
                }
            }
        }
    }

    func testTyping() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onTyping()

                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    self.roomId = roomId
                    self.socket.typing(by: roomId)
                    done()
                }
            }
        }
    }

    func testStopTyping() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onTyping()

                self.socket.onEnterRoom()

                self.socket.roomId.subscribe { roomId in
                    guard let roomId = roomId else { return }
                    self.roomId = roomId
                    self.socket.stopTyping(by: roomId)
                    done()
                }
            }
        }
    }

    func testOnRoomAccepted() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onRoomAccepted()

                self.socket.onEnterRoom()

                self.socket.isRoomAccepted.subscribe { isRoomAccepted in
                    guard let isRoomAccepted = isRoomAccepted else { return }
                    done()
                }
            }
        }
    }

    func testOnRoomRejected() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.medium.rawValue) { (done) in
            self.authManager.authenticate { result in
                defer {
                    self.socket.join(by: self.userHash)
                }
                self.socket.onRoomRejected()

                self.socket.onEnterRoom()

                self.socket.isRoomRejected.subscribe { isRoomRejected in
                    guard let isRoomRejected = isRoomRejected else { return }
                    done()
                }
            }
        }
    }
}
