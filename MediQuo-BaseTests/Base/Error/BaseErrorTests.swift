//
//  BaseErrorTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 30/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class BaseErrorTests: XCTestCase {

    // MARK: - RepositoryError

    func testRepositoryErrorNoResponseData() {
        let error = BaseError.RepositoryError.noResponseData
        expect(error.description).to(equal("noResponseData"))
    }

    func testRepositoryErrorLocalStorageUpToDate() {
        let error = BaseError.RepositoryError.localStorageUpToDate
        expect(error.description).to(equal("localStorageUpToDate"))
    }
    
    func testRepositoryErrorReason() {
        let error = BaseError.RepositoryError.reason("TEST")
        expect(error.description).to(equal("Reason: TEST"))
    }

    // MARK: - RemoteError

    func testRemoteErrorHttpUrlResponse() {
        let response = HTTPURLResponse()
        let error = BaseError.RemoteError.httpUrlResponse(response)
        expect(error.description).toNot(beNil())
    }

    func testRemoteErrorCode() {
        let error = BaseError.RemoteError.code(200)
        expect(error.description).to(equal("Code(200)"))
    }

    func testRemoteErrorReason() {
        let error = BaseError.RemoteError.reason("TEST")
        expect(error.description).to(equal("Reason: TEST"))
    }

    // MARK: - SocketError

    func testSocketErrorSocketIsDisconnect() {
        let error = BaseError.SocketError.socketIsDisconnect
        expect(error.description).to(equal("SocketIsDisconnect"))
    }

    func testSocketErrorWrongDataResponse() {
        let error = BaseError.SocketError.wrongDataResponse("TEST")
        expect(error.description).to(equal("WrongDataResponse: TEST"))
    }

    func testSocketErrorReason() {
        let error = BaseError.SocketError.reason("TEST")
        expect(error.description).to(equal("Reason: TEST"))
    }

    // MARK: General description

    func testRepositoryDescriptionError() {
        expect(BaseError.repositoryError(.reason("TEST")).description).to(equal("RepositoryError - Reason: TEST"))
    }

    func testRemoteDescriptionError() {
        expect(BaseError.remoteError(.reason("TEST")).description).to(equal("RemoteError - Reason: TEST"))
    }

    func testSocketDescriptionError() {
        expect(BaseError.socketError(.reason("TEST")).description).to(equal("SocketError - Reason: TEST"))
    }

    // MARK: Equals

    func testErrorCompare() {
        let repositoryError = BaseError.repositoryError(.reason("TEST"))
        let remoteError = BaseError.remoteError(.reason("TEST"))
        expect(repositoryError).toNot(equal(remoteError))
    }
}
