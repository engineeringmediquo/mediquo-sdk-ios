//
//  ErrorManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 30/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ErrorManagerTests: XCTestCase {

    func testErrorManagerProcessHttpResponse() {
        let response = HTTPURLResponse()
        ErrorManager.process(response)
        expect(true).to(beTrue())
    }

    func testErrorManagerProcessBadHttpResponse() {
        let response = HTTPURLResponse(url: URL(string: "http://www.mediquo.com")!, statusCode: 503, httpVersion: nil, headerFields: nil) ?? HTTPURLResponse()
        ErrorManager.process(response)
        expect(true).to(beTrue())
    }

    func testErrorManagerProcessString() {
        ErrorManager.process(error: "TEST")
        expect(true).to(beTrue())
    }
}
