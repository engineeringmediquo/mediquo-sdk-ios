//
//  RoomRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class RoomRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: RoomRepository

    func testRoomSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.fetch(by: "2852") { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testReadSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.read(by: "2852")
                done()
            }
        }
    }
}
