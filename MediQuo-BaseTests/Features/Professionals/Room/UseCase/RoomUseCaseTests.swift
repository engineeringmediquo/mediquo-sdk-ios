//
//  RoomUseCaseTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class RoomUseCaseTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var useCase: RoomUseCase

    func testRoomSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(by: "2852") { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testReadSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.read(by: "2852")
                done()
            }
        }
    }
}
