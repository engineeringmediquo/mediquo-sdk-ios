//
//  ChatRoomViewModelMock.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 27/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import Foundation

class ChatRoomViewModelMock: ChatRoomViewModel {
    private(set) var onInitChatRoom = false

    override func initChatRoom(userHash: String?) {
        self.onInitChatRoom = true
    }
}
