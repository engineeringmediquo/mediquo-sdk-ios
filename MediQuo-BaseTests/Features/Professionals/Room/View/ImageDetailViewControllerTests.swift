//
//  ImageDetailViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 3/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class ImageDetailViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testViewControllerHasLoaded() {
        let viewController = ImageDetailViewController()
        expect(viewController).toNot(beNil())
    }
}
