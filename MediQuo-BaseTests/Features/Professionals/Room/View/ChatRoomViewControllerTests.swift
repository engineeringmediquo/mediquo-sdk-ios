//
//  ChatRoomViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ChatRoomViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    private let messagesMock: [MessageModel] = [MessageModel(text: "TEST", roomId: 1, messageId: "1234", fromUserHash: "1234")]
    private let profileMock: ProfessionalModel = ProfessionalModel(id: 1, hash: "TEST", status: .online, avatar: nil, name: "TEST", title: "TEST", overview: nil,
                                                                   room: nil, speciality: nil, collegiateNumber: nil, isAvailable: nil, license: nil, specialityId: nil)

    let spy: ChatRoomViewModelMock = ChatRoomViewModelMock()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testViewControllerViewDidLoad() {
        guard let sut = self.loadView() else { return }
        sut.viewDidLoad()
        expect(sut.navigationItem.largeTitleDisplayMode).to(equal(.never))
    }

    func testViewControllerHasLoaded() {
        let viewController = ChatRoomViewController()
        expect(viewController).toNot(beNil())
    }

    func testViewControllerOnInitChatRoom() {
        guard let sut = self.loadView() else { return }
        sut.viewDidAppear(true)
        sut.bindNotifications()
        NotificationCenter.default.post(name: Notification.Name.Socket.ready, object: nil)
        expect(self.spy.onInitChatRoom).to(beTrue())
    }

    func testViewControllerInitChatRoom() {
        let viewController = ChatRoomViewController()
        viewController.viewModel.initChatRoom(userHash: "1234")
        expect(viewController.viewModel.messages).toNot(beNil())
    }

    func testViewControllerOnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.messages.value).toNot(beNil())
        expect(self.spy.profile.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.messages).toNot(beNil())
        expect(self.spy.profile).toNot(beNil())
    }

    func testViewControllerFetch() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.fetchMessages()
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerFetchMore() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.fetchMoreMessages(pivotId: "1234")
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerFetchMoreWithPivotOlder() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.fetchMoreMessages(pivotId: "1234", mode: .older)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerFetchMoreWithPivotNewer() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.fetchMoreMessages(pivotId: "1234", mode: .newer)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerSendMessageText() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.send(message: "TEST")
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerSendMessageImage() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.send(image: nil, image: UIImage(), uuid: "1234")
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerSendMessageFile() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.send(file: URL(string: "https://www.mediquo.com/wp-content/themes/mediquo-new/dist/images/logotipo-medi-quo_c2d1de74.svg")!)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerFormattedHeaderDate() {
        guard let sut = self.loadView() else { return }
        let string = sut.viewModel.formattedHeaderDate(for: MessageCellType(model: nil, sender: SenderModel(senderId: "", displayName: "")))
        expect(string).toNot(beNil())
    }

    func testViewControllerFormattedFooterDate() {
        guard let sut = self.loadView() else { return }
        let string = sut.viewModel.formattedFooterDate(for: MessageCellType(model: nil, sender: SenderModel(senderId: "", displayName: "")))
        expect(string).toNot(beNil())
    }

    func testViewControllerDidTapMessage() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.didTapMessage(at: IndexPath(item: 0, section: 0), in: sut.messagesCollectionView)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerDidTapImage() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.didTapImage(at: IndexPath(item: 0, section: 0), in: sut.messagesCollectionView) { _  in }
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerTyping() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.typing(isTyping: true)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerStopTyping() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.typing(isTyping: false)
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerExecuteDeeplink() {
        guard let sut = self.loadView() else { return }
        sut.execute(deeplink: NotificationSchema(roomId: 1))
        expect(self.spy.messages).toNot(beNil())
    }

    func testViewControllerSetPresentationNotification() {
        guard let sut = self.loadView() else { return }
        sut.setNotificationPresentation(by: 1) { (options: UNNotificationPresentationOptions) in
            expect(options).to(equal(([.alert, .badge, .sound])))
        }
    }
}

extension ChatRoomViewControllerTests {
    private func loadView() -> ChatRoomViewController? {
        let viewController = ChatRoomViewController()
        viewController.viewModel = self.spy
        viewController.viewModel.messages.value = self.messagesMock
        viewController.viewModel.profile.value = self.profileMock
        return viewController
    }
}

