//
//  ProfessionalsListManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ProfessionalsListManagerTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var manager: ProfessionalsListManager

    @StorageDatasource
    var jwtModel: JwtModel?

    func testPendingMessagesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.manager.getPendingMessagesCount { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
