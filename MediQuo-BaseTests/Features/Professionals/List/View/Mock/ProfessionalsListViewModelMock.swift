//
//  ProfessionalsListViewModelMock.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import Foundation

class ProfessionalsListViewModelMock: ProfessionalsListViewModel {
    private(set) var onFetchCalled = false

    override func fetch() {
        self.onFetchCalled = true
    }
}
