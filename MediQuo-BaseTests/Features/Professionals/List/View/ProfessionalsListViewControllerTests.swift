//
//  ProfessionalsListViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class ProfessionalsListViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    private let mockModels: [ProfessionalModel] = [ProfessionalModel(id: 1, hash: "1234", status: .online, avatar: nil, name: "Pepe", title: "TEST",
                                                                     overview: "TEST",
                                                                     room: RoomModel(id: 1, roomId: 1, lastMessage: nil, pendingMessages: nil, status: nil, title: nil, avatar: nil, meta: nil),
                                                                     speciality: nil, collegiateNumber: nil, isAvailable: nil, license: nil, specialityId: nil)]

    let spy: ProfessionalsListViewModelMock = ProfessionalsListViewModelMock()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testViewControllerOnFetch() {
        guard let sut = self.loadView() else { return }
        sut.viewDidAppear(true)
        expect(self.spy.onFetchCalled).to(beTrue())
    }

    func testViewControllerOnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.professionals.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.professionals).toNot(beNil())
    }

    func testViewControllerNavigateToRow() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.navigateTo(position: 0)
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerDeeplink() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.executeDeeplink(deeplink: NotificationSchema(roomId: 1))
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerPendingDeeplink() {
        guard let sut = self.loadView() else { return }
        sut.viewModel.executePendingDeeplink()
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerExecuteDeeplink() {
        guard let sut = self.loadView() else { return }
        sut.execute(deeplink: NotificationSchema(roomId: 1))
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerSetPresentationNotification() {
        guard let sut = self.loadView() else { return }
        sut.setNotificationPresentation(by: 1) { (options: UNNotificationPresentationOptions) in
            expect(options).to(equal(([])))
        }
    }
}

extension ProfessionalsListViewControllerTests {
    private func loadView() -> ProfessionalsListViewController? {
        let viewController = MediQuo.getProfessionalsList()
        viewController.viewModel = self.spy
        viewController.viewModel.professionals.value = self.mockModels
        return viewController
    }
}
