//
//  ProfessionalsListRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ProfessionalsListRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: ProfessionalsListRepository

    func testListSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.fetch { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testPendingMessagesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.getPendingMessagesCount { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
