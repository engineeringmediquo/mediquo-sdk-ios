//
//  ProfessionalProfileViewModelMock.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import Foundation

class ProfessionalProfileViewModelMock: ProfessionalProfileViewModel {
    private(set) var onFetchCalled = false

    override func fetch(by userHash: String) {
        self.onFetchCalled = true
    }
}
