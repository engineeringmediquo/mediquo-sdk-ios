//
//  ProfessionalProfileViewTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 12/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class ProfessionalProfileViewTests: XCTestCase {
    private let mockModel: ProfessionalModel = ProfessionalModel(id: 1, hash: "1234", status: .online, avatar: nil, name: "Pepe", title: "TEST",
                                                                 overview: "TEST",
                                                                 room: RoomModel(id: 1, roomId: 1, lastMessage: nil, pendingMessages: nil, status: nil, title: nil,
                                                                                 avatar: nil, meta: nil),
                                                                 speciality: nil, collegiateNumber: nil, isAvailable: nil, license: nil, specialityId: nil)

    func testViewLoad() {
        let view = ProfessionalProfileView()
        view.awakeFromNib()
        view.updateConstraints()
        view.configure(by: self.mockModel)
        expect(view).toNot(beNil())
    }
}
