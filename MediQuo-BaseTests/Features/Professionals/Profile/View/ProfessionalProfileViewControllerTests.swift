//
//  ProfessionalProfileViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class ProfessionalProfileViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    private let mockModel: ProfessionalModel = ProfessionalModel(id: 1, hash: "1234", status: .online, avatar: nil, name: "Pepe", title: "TEST",
                                                                 overview: "TEST",
                                                                 room: RoomModel(id: 1, roomId: 1, lastMessage: nil, pendingMessages: nil, status: nil, title: nil,
                                                                                 avatar: nil, meta: nil),
                                                                 speciality: nil, collegiateNumber: nil, isAvailable: nil, license: nil, specialityId: nil)

    let spy: ProfessionalProfileViewModelMock = ProfessionalProfileViewModelMock()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testViewControllerOnFetch() {
        guard let sut = self.loadView() else { return }
        sut.viewDidLoad()
        expect(self.spy.onFetchCalled).to(beTrue())
    }

    func testViewControllerFetch() {
        let viewController = ProfessionalProfileViewController()
        viewController.userHash = "043114c4-d4d1-4f57-b983-52e2bd7589a9"
        viewController.bindViewModels()
        viewController.viewDidLoad()
        expect(viewController.viewModel.professional).toNot(beNil())
    }

    func testViewControllerOnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.professional.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModel() {
        guard let sut = self.loadView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerExecuteDeeplink() {
        guard let sut = self.loadView() else { return }
        sut.execute(deeplink: NotificationSchema(roomId: 1))
        expect(self.spy.professional).toNot(beNil())
    }

    func testViewControllerSetPresentationNotification() {
        guard let sut = self.loadView() else { return }
        sut.setNotificationPresentation(by: 1) { (options: UNNotificationPresentationOptions) in
            expect(options).to(equal(([.alert, .sound])))
        }
    }
}

extension ProfessionalProfileViewControllerTests {
    private func loadView() -> ProfessionalProfileViewController? {
        let viewController = ProfessionalProfileViewController()
        viewController.viewModel = self.spy
        viewController.userHash = "043114c4-d4d1-4f57-b983-52e2bd7589a9"
        viewController.viewModel.professional.value = self.mockModel
        return viewController
    }
}
