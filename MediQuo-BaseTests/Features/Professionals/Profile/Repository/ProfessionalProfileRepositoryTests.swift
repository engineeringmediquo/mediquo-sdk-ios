//
//  ProfessionalProfileRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ProfessionalProfileRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: ProfessionalProfileRepository

    func testFetchSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.fetch(by: "043114c4-d4d1-4f57-b983-52e2bd7589a9") { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
