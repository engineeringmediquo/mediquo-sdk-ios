//
//  EnvironmentManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class EnvironmentManagerTests: XCTestCase {

    private var manager: EnvironmentManager = EnvironmentManager()

    func testGetEnvironment() {
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)

        let storedEnv: EnvironmentType = self.manager.get()
        expect(storedEnv).to(equal(.testing))
    }
}
