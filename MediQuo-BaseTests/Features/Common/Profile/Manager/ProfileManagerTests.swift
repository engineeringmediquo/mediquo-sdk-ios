//
//  ProfileManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ProfileManagerTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    private var manager: ProfileManager

    func testSaveProfileSuccess() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.manager.fetch { result in
                let profile = self.manager.getProfile()
                expect(profile).toNot(beNil())
                done()
            }
        }
    }
}
