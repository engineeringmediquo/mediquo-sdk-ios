//
//  ProfileRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ProfileRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: ProfileRepository

    func testSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.fetch { result in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
