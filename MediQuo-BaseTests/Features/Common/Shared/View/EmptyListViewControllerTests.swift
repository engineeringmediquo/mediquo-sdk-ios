//
//  EmptyListViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 22/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class EmptyListViewControllerTests: XCTestCase {

    func testViewControllerHasLoaded() {
        let viewController = EmptyListViewController()
        expect(viewController).toNot(beNil())
    }
}
