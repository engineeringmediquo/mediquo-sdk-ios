//
//  WireframeTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class WireframeTests: XCTestCase {

    func testWireframeChat() {
        let viewController = MediQuo.getProfessionalsList()
        Wireframe.navigate(to: .chat("1234", false), from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeProfile() {
        let viewController = ProfessionalProfileViewController()
        Wireframe.navigate(to: .profile("1234"), from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeAllergies() {
        let viewController = MediQuo.getAllergies()
        Wireframe.navigate(to: .allergies, from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeAllergyForm() {
        let viewController = MediQuo.getAllergyForm()
        Wireframe.navigate(to: .allergiesForm(nil, nil), from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeDiseases() {
        let viewController = MediQuo.getDiseases()
        Wireframe.navigate(to: .diseases, from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeDiseaseForm() {
        let viewController = MediQuo.getDiseaseForm()
        Wireframe.navigate(to: .diseasesForm(nil, nil), from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeMedications() {
        let viewController = MediQuo.getMedications()
        Wireframe.navigate(to: .medications, from: viewController.navigationController)
        expect(true).to(beTrue())
    }

    func testWireframeMedicationForm() {
        let viewController = MediQuo.getMedicationForm()
        Wireframe.navigate(to: .medicationsForm(nil, nil), from: viewController.navigationController)
        expect(true).to(beTrue())
    }
}
