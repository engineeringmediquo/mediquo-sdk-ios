//
//  AuthManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class AuthManagerTests: XCTestCase {

    @Inject
    private var manager: AuthManager

    func testSaveApiKeySuccess() {
        let apiKey: String = TestConstants.Keys.apiKey
        self.manager.set(apiKey: TestConstants.Keys.apiKey)

        let storedApiKey = self.manager.getApiKey()
        expect(storedApiKey).to(equal(apiKey))
    }

    func testSaveClientCodeSuccess() {
        let clientCode: String = TestConstants.Keys.clientCode
        self.manager.set(clientCode: TestConstants.Keys.clientCode)

        let storedClientCode = self.manager.getClientCode()
        expect(storedClientCode).to(equal(clientCode))
    }

    func testAuthenticateSuccessful() {
        self.manager.set(apiKey: TestConstants.Keys.apiKey)
        self.manager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.manager.authenticate { result in
                let storedJwt = self.manager.getJwt()
                expect(storedJwt).toNot(beNil())
                done()
            }
        }
    }
}
