//
//  AuthRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 28/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class AuthRepositoryTests: XCTestCase {
    @Inject
    private var repository: AuthRepository

    func testSaveApiKeySuccess() {
        let apiKey: String = TestConstants.Keys.apiKey
        self.repository.set(apiKey: TestConstants.Keys.apiKey)

        let storedApiKey = self.repository.getApiKey()
        expect(storedApiKey).to(equal(apiKey))
    }

    func testSaveClientCodeSuccess() {
        let clientCode: String = TestConstants.Keys.clientCode
        self.repository.set(clientCode: TestConstants.Keys.clientCode)

        let storedClientCode = self.repository.getClientCode()
        expect(storedClientCode).to(equal(clientCode))
    }

    func testAuthenticateSuccessful() {
        self.repository.set(apiKey: TestConstants.Keys.apiKey)
        self.repository.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.repository.authenticate { result in
                let storedJwt = self.repository.getJwt()
                expect(storedJwt).toNot(beNil())
                done()
            }
        }
    }
}
