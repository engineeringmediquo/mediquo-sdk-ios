//
//  InstallationRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class InstallationRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: InstallationRepository

    func testInstallationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        let request: InstallationRequest = InstallationRequest(installationUUID: "TEST", languageCode: "es", countryCode: "es",
                                                               platformName: "ios", platformVersion: "14.2", sdkVersion: SDK.version.rawValue, deviceModel: "iphone", deviceId: "1234")
        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.repository.install(request: request) { result in
                expect(true).to(beTrue())
                done()
            }
        }
    }
}
