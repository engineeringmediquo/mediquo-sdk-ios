//
//  SetupManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class InstallationManagerTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var manager: InstallationManager

    func testInstallationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.manager.install { result in
                expect(true).to(beTrue())
                done()
            }
        }
    }
}
