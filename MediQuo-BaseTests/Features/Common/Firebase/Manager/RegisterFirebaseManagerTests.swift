//
//  RegisterFirebaseManagerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class RegisterFirebaseManagerTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var manager: RegisterFirebaseManager

    func testRegisterFirebaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.manager.register(request: RegisterFirebaseRequest(token: "TEST", deviceId: "TEST ID")) { result in
                // 🙈 TODO: Do setup call here
                done()
            }
        }
    }
}
