//
//  ReportsUseCaseTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class ReportsUseCaseTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var useCase: ReportsUseCase

    func testFetchReports() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch { (result: Result<DataResponse<[ReportModel]>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testFetchReport() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(by: "1234") { (result: Result<DataResponse<ReportModel>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testDownloadReport() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.download(by: "1234") { (result: Result<Data?, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
