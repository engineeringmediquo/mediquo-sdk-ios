//
//  RecipesRepositoryTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class RecipesRepositoryTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var repository: RecipesRepository

    func testFetchRecipes() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.fetch { (result: Result<DataResponse<[RecipeModel]>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testDownloadRecipe() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.repository.download(by: "1234") { (result: Result<Data?, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }
}
