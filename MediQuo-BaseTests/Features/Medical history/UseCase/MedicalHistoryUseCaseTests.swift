//
//  MedicalHistoryUseCaseTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class MedicalHistoryUseCaseTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var useCase: MedicalHistoryUseCase

    func testFetchAllergiesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.allergies) { (result: Result<DataResponse<[Allergy]>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testCreateAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.create(.allergies, with: "TEST") { (result: Result<DataResponse<Allergy>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testUpdateAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.update(.allergies, by: 1, with: "TEST") { (result: Result<DataResponse<Allergy>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testDeleteAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.allergies) { (result: Result<DataResponse<[Allergy]>, BaseError>) in
                    if case let .success(response) = result {
                        response.data.forEach { model in
                            if let id = model.id {
                                self.useCase.delete(.allergies, with: id) { (result: Result<VoidResponse, BaseError>) in
                                    expect(result).toNot(beNil())
                                }
                            }
                        }
                        done()
                    }
                }
            }
        }
    }

    func testFetchDiseasesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.diseases) { (result: Result<DataResponse<[Disease]>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testCreateDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.create(.diseases, with: "TEST") { (result: Result<DataResponse<Disease>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testUpdateDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.update(.diseases, by: 1, with: "TEST") { (result: Result<DataResponse<Disease>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testDeleteDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.diseases) { (result: Result<DataResponse<[Disease]>, BaseError>) in
                    if case let .success(response) = result {
                        response.data.forEach { model in
                            if let id = model.id {
                                self.useCase.delete(.diseases, with: id) { (result: Result<VoidResponse, BaseError>) in
                                    expect(result).toNot(beNil())
                                }
                            }
                        }
                        done()
                    }
                }
            }
        }
    }

    func testFetchMedicationsSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.medications) { (result: Result<DataResponse<[Medication]>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testCreateMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.create(.medications, with: "TEST") { (result: Result<DataResponse<Medication>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testUpdateMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.update(.medications, by: 1, with: "TEST") { (result: Result<DataResponse<Medication>, BaseError>) in
                    expect(result).toNot(beNil())
                    done()
                }
            }
        }
    }

    func testDeleteMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.useCase.fetch(.medications) { (result: Result<DataResponse<[Medication]>, BaseError>) in
                    if case let .success(response) = result {
                        response.data.forEach { model in
                            if let id = model.id {
                                self.useCase.delete(.medications, with: id) { (result: Result<VoidResponse, BaseError>) in
                                    expect(result).toNot(beNil())
                                }
                            }
                        }
                        done()
                    }
                }
            }
        }
    }
}
