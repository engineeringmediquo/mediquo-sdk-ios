//
//  MedicalHistoryViewModelMock.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import Foundation

class MedicalHistoryViewModelMock: MedicalHistoryViewModel {
    private(set) var onFetchAllergies = false
    private(set) var onCreateAllergies = false
    private(set) var onUpdateAllergies = false
    private(set) var onDeleteAllergies = false

    private(set) var onFetchDiseases = false
    private(set) var onCreateDiseases = false
    private(set) var onUpdateDiseases = false
    private(set) var onDeleteDiseases = false

    private(set) var onFetchMedications = false
    private(set) var onCreateMedications = false
    private(set) var onUpdateMedications = false
    private(set) var onDeleteMedications = false

    override func fetchAllergies() {
        self.onFetchAllergies = true
    }

    override func createAllergy(name: String) {
        self.onCreateAllergies = true
    }

    override func updateAllergy(id: Int, name: String?) {
        self.onUpdateAllergies = true
    }

    override func deleteAllergy(id: Int) {
        self.onDeleteAllergies = true
    }

    override func fetchDiseases() {
        self.onFetchDiseases = true
    }

    override func createDisease(name: String) {
        self.onCreateDiseases = true
    }

    override func updateDisease(id: Int, name: String?) {
        self.onUpdateDiseases = true
    }

    override func deleteDisease(id: Int) {
        self.onDeleteDiseases = true
    }

    override func fetchMedications() {
        self.onFetchMedications = true
    }

    override func createMedication(name: String) {
        self.onCreateMedications = true
    }

    override func updateMedication(id: Int, name: String?) {
        self.onUpdateMedications = true
    }

    override func deleteMedication(id: Int) {
        self.onDeleteMedications = true
    }
}
