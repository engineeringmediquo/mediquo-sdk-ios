//
//  MedicalHistoryViewModelTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import XCTest
import Nimble

@testable import MediQuo_Base

class MedicalHistoryViewModelTests: XCTestCase {
    @Inject
    var authManager: AuthManager

    @Inject
    var viewModel: MedicalHistoryViewModel

    func testFetchAllergiesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.models.subscribe { models in
                    expect(models).toNot(beNil())
                }
                self.viewModel.fetchAllergies()
                done()
            }
        }
    }

    func testCreateAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.createAllergy(name: "TEST")
            }
        }
    }

    func testUpdateAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.updateAllergy(id: 1, name: "TEST")
            }
        }
    }

    func testDeleteAllergySuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                }
                self.viewModel.deleteAllergy(id: 1)
                done()
            }
        }
    }

    func testFetchDiseasesSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.models.subscribe { models in
                    expect(models).toNot(beNil())
                }
                self.viewModel.fetchDiseases()
                done()
            }
        }
    }

    func testCreateDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.createDisease(name: "TEST")
            }
        }
    }

    func testUpdateDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.updateDisease(id: 1, name: "TEST")
            }
        }
    }

    func testDeleteDiseaseSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                }
                self.viewModel.deleteDisease(id: 1)
                done()
            }
        }
    }

    func testFetchMedicationsSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.models.subscribe { models in
                    expect(models).toNot(beNil())
                }
                self.viewModel.fetchMedications()
                done()
            }
        }
    }

    func testCreateMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.createMedication(name: "TEST")
            }
        }
    }

    func testUpdateMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                    done()
                }
                self.viewModel.updateMedication(id: 1, name: "TEST")
            }
        }
    }

    func testDeleteMedicationSuccessful() {
        self.authManager.set(apiKey: TestConstants.Keys.apiKey)
        self.authManager.set(clientCode: TestConstants.Keys.clientCode)

        waitUntil(timeout: TestConstants.WaitTime.short.rawValue) { (done) in
            self.authManager.authenticate { result in
                self.viewModel.updated.subscribe { updated in
                    expect(updated).toNot(beNil())
                }
                self.viewModel.deleteMedication(id: 1)
                done()
            }
        }
    }
}
