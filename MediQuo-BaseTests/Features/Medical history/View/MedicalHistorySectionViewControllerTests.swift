//
//  MedicalHistorySectionViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class MedicalHistorySectionViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    private let mockAllergiesModels: [Allergy] = [Allergy(id: 1, severity: nil, name: "TEST", description: nil,
                                                 customerHash: nil, createdAt: nil, updatedAt: nil, deletedAt: nil)]

    private let mockDiseasesModels: [Disease] = [Disease(id: 1, name: "TEST", description: "TEST", customerHash: nil, diagnosisDate: nil,
                                                         resolutionDate: nil, createdAt: nil, updatedAt: nil, deletedAt: nil)]

    private let mockMedicationsModels: [Medication] = [Medication(id: 1, name: "TEST", posology: nil, description: "TEST",
                                                                  customerHash: nil, createdAt: nil, updatedAt: nil, deletedAt: nil)]

    let spy: MedicalHistoryViewModelMock = MedicalHistoryViewModelMock()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    // MARK: - Allergies

    func testViewControllerOnFetchAllergies() {
        guard let sut = self.loadAllergyView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.onFetchAllergies).to(beTrue())
    }

    func testViewControllerOnBindViewModelAllergies() {
        guard let sut = self.loadAllergyView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.models.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModelAllergies() {
        guard let sut = self.loadAllergyView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.models).toNot(beNil())
    }

    // MARK: - Diseases

    func testViewControllerOnFetchDiseases() {
        guard let sut = self.loadDieaseView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.onFetchDiseases).to(beTrue())
    }

    func testViewControllerOnBindViewModelDiseases() {
        guard let sut = self.loadDieaseView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.models.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModelDiseases() {
        guard let sut = self.loadDieaseView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.models).toNot(beNil())
    }

    // MARK: - Medications

    func testViewControllerOnFetchMedications() {
        guard let sut = self.loadMedicationView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.onFetchMedications).to(beTrue())
    }

    func testViewControllerOnBindViewModelMedications() {
        guard let sut = self.loadMedicationView() else { return }
        sut.viewWillAppear(true)
        expect(self.spy.models.value).toNot(beNil())
    }

    func testViewControllerOnUnBindViewModelMedications() {
        guard let sut = self.loadMedicationView() else { return }
        sut.viewWillDisappear(true)
        expect(self.spy.models).toNot(beNil())
    }
}

extension MedicalHistorySectionViewControllerTests {
    private func loadAllergyView() -> MedicalHistorySectionViewController? {
        let viewController = MediQuo.getAllergies()
        viewController.viewModel = self.spy
        viewController.viewModel.models.value = self.mockAllergiesModels
        return viewController
    }

    private func loadDieaseView() -> MedicalHistorySectionViewController? {
        let viewController = MediQuo.getDiseases()
        viewController.viewModel = self.spy
        viewController.viewModel.models.value = self.mockDiseasesModels
        return viewController
    }

    private func loadMedicationView() -> MedicalHistorySectionViewController? {
        let viewController = MediQuo.getMedications()
        viewController.viewModel = self.spy
        viewController.viewModel.models.value = self.mockMedicationsModels
        return viewController
    }
}
