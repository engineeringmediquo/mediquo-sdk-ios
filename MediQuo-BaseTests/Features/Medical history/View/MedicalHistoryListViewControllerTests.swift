//
//  MedicalHistoryListViewControllerTests.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

@testable import MediQuo_Base
import XCTest
import Nimble

class MedicalHistoryListViewControllerTests: XCTestCase {
    private var manager: EnvironmentManager = EnvironmentManager()

    override func setUp() {
        super.setUp()
        let environment: EnvironmentType = .testing
        self.manager.set(environment: environment)
    }

    func testViewControllerHasLoaded() {
        let viewController = MediQuo.getMedicalHistory()
        viewController.viewDidLoad()
        expect(viewController).toNot(beNil())
    }

    func testViewControllerWillAppearHasLoaded() {
        let viewController = MediQuo.getMedicalHistory()
        viewController.viewWillAppear(true)
        expect(viewController).toNot(beNil())
    }
}
