fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
### deploy_pod
```
fastlane deploy_pod
```
Deploys the podspec file to the private MediQuo Specs repository

Usage example: fastlane deploy_pod version:'0.0.1'
### test
```
fastlane test
```
Runs framework's unit tests in the specified device.

Usage example: fastlane test device:'iPhone 8'
### build_framework
```
fastlane build_framework
```
Builds the framework to make sure there are no breaking changes. No tests are executed.

Usage example: fastlane build_framework

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
