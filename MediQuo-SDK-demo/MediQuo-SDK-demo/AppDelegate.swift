//
//  AppDelegate.swift
//  MediQuo-SDK-demo
//
//  Created by David Martin on 19/1/21.
//

import Firebase
import FirebaseMessaging
import MediQuo_Base
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self

        var style: MediQuoStyleType = MediQuoStyle()
        style.prefersLargeTitles = true
        style.primaryColor = .purple
        style.primaryContrastColor = .white
        style.secondaryColor = .magenta
        style.accentColor = .systemPink
        style.professionalsListDelegate = ListDelegate()
        style.bookFont = UIFont(name: "Devil Breeze Book", size: 16)
        style.mediumFont = UIFont(name: "Devil Breeze Medium", size: 16)
        style.boldFont = UIFont(name: "Devil Breeze Bold", size: 16)

        MediQuoSDK._mqInit(apiKey: "WDASG0A9sxVfpiLR", style: style) { result in
            if case .success = result {
                CoreLog.business.info("Initialize has been done successfully")
            }
            if case let .failure(error) = result {
                CoreLog.business.info("Initialize has errors %@", error.description)
            }
        }

        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_: Messaging, didReceiveRegistrationToken fcmToken: String) {
        MediQuoSDK.registerFirebase(token: fcmToken) { result in
            if result {
                CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
            } else {
                CoreLog.firebase.error("Can't register Firebase registration token")
            }
        }
    }

    func messaging(message: Messaging, didReceive _: MessagingRemoteMessage) {
        CoreLog.firebase.info("Push received", message)
    }
}

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        if let fcmToken = Messaging.messaging().fcmToken {
            CoreLog.firebase.error("didRegisterForRemoteNotificationsWithDeviceToken: %@", fcmToken)
            MediQuoSDK.registerFirebase(token: fcmToken) { result in
                if result {
                    CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
                } else {
                    CoreLog.firebase.error("Can't register Firebase registration token")
                }
            }
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        CoreLog.firebase.error("didFailToRegisterForRemoteNotificationsWithError: %@", error.localizedDescription)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        MediQuoSDK.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        MediQuoSDK.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)

//        MediQuo.didReceiveRemoteNotificationWithViewController(userInfo: userInfo) { [weak self] viewController in
//            guard let self = self, let viewController = viewController else { return }
//            let navigationController = UINavigationController(rootViewController: viewController)
//            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = navigationController
//        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        MediQuoSDK.willPresentRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}
