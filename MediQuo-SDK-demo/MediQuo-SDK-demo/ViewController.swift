//
//  ViewController.swift
//  MediQuo-SDK-demo
//
//  Created by David Martin on 19/1/21.
//

import MediQuo_Base
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        MediQuoSDK.authenticate(clientCode: "d0bca716-c49f-4659-8669-588685ca235d") { [weak self] status in
            guard let self = self, case SdkStatus.ready = status else { return }
            let viewController = MediQuoSDK.getProfessionalsList()
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.modalPresentationStyle = .overFullScreen
            self.present(navigationController, animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bindNotifications()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Tracking

extension ViewController {
    private func bindNotifications() {
        // Chat
        NotificationCenter.default.addObserver(self, selector: #selector(self.onChatView(notification:)),
                                               name: Notification.Name.Event.Chat.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMessageSend(notification:)),
                                               name: Notification.Name.Event.Chat.messageSend, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMessageReceived(notification:)),
                                               name: Notification.Name.Event.Chat.messageReceived, object: nil)

        // Professional profile
        NotificationCenter.default.addObserver(self, selector: #selector(self.onProfessionalProfileView(notification:)),
                                               name: Notification.Name.Event.ProfessionalProfile.view, object: nil)

        // Call
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCallStarted),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.Call.Started, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCallEnded),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.Call.Ended, object: nil)

        // VideoCall
        NotificationCenter.default.addObserver(self, selector: #selector(self.onVideoCallStarted),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.VideoCall.Started, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onVideoCallEnded),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.VideoCall.Ended, object: nil)

        // Medical history
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMedicalHistoryView),
                                               name: Notification.Name.Event.MedicalHistory.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAllergiesView),
                                               name: Notification.Name.Event.Allergies.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onIllenessesView),
                                               name: Notification.Name.Event.Illnesses.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onMedicationsView),
                                               name: Notification.Name.Event.Medications.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReportsView),
                                               name: Notification.Name.Event.Reports.view, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRecipesView),
                                               name: Notification.Name.Event.Recipes.view, object: nil)
    }

    @objc func onChatView(notification: Notification) {
        guard let professionalHash: String = notification.userInfo?[Notification.Name.Key.professionalHash] as? String,
              let specialityId: Int = notification.userInfo?[Notification.Name.Key.specialityId] as? Int else {
            return
        }
        print("[Tracking] Chat view - ProfessionalHash: \(professionalHash) - SpecialityId: \(specialityId)")
    }

    @objc func onMessageSend(notification: Notification) {
        guard let professionalHash: String = notification.userInfo?[Notification.Name.Key.professionalHash] as? String,
              let specialityId: Int = notification.userInfo?[Notification.Name.Key.specialityId] as? Int else {
            return
        }
        print("[Tracking] Message send - ProfessionalHash: \(professionalHash) - SpecialityId: \(specialityId)")
    }

    @objc func onMessageReceived(notification: Notification) {
        guard let professionalHash: String = notification.userInfo?[Notification.Name.Key.professionalHash] as? String,
              let specialityId: Int = notification.userInfo?[Notification.Name.Key.specialityId] as? Int else {
            return
        }
        print("[Tracking] Message received - ProfessionalHash: \(professionalHash) - SpecialityId: \(specialityId)")
    }

    @objc func onProfessionalProfileView(notification: Notification) {
        guard let professionalHash: String = notification.userInfo?[Notification.Name.Key.professionalHash] as? String else {
            return
        }
        print("[Tracking] Professional profile view - ProfessionalHash: \(professionalHash)")
    }

    @objc func onCallStarted() {
        print("[Tracking] Call started")
    }

    @objc func onCallEnded() {
        print("[Tracking] Call ended")
    }

    @objc func onVideoCallStarted() {
        print("[Tracking] VideoCall started")
    }

    @objc func onVideoCallEnded() {
        print("[Tracking] VideoCall ended")
    }

    @objc func onMedicalHistoryView() {
        print("[Tracking] Medical history view")
    }

    @objc func onAllergiesView() {
        print("[Tracking] Allergies view")
    }

    @objc func onIllenessesView() {
        print("[Tracking] Illenesses view")
    }

    @objc func onMedicationsView() {
        print("[Tracking] Medications view")
    }

    @objc func onReportsView() {
        print("[Tracking] Reports view")
    }

    @objc func onRecipesView() {
        print("[Tracking] RecipesView")
    }
}
