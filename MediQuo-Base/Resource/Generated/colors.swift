// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#42cece"></span>
  /// Alpha: 100% <br/> (0x42ceceff)
  internal static let accent = ColorName(rgbaValue: 0x42ceceff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#fafafa"></span>
  /// Alpha: 100% <br/> (0xfafafaff)
  internal static let bubbleBackgroundIncomingColor = ColorName(rgbaValue: 0xfafafaff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e0e0e0"></span>
  /// Alpha: 100% <br/> (0xe0e0e0ff)
  internal static let bubbleBackgroundOutgoingColor = ColorName(rgbaValue: 0xe0e0e0ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e0e0e0"></span>
  /// Alpha: 100% <br/> (0xe0e0e0ff)
  internal static let contrast = ColorName(rgbaValue: 0xe0e0e0ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let dateBackgroundColor = ColorName(rgbaValue: 0xffffffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4e4e4e"></span>
  /// Alpha: 100% <br/> (0x4e4e4eff)
  internal static let dateTextColor = ColorName(rgbaValue: 0x4e4e4eff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#bdbdbd"></span>
  /// Alpha: 100% <br/> (0xbdbdbdff)
  internal static let gray = ColorName(rgbaValue: 0xbdbdbdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#47e676"></span>
  /// Alpha: 100% <br/> (0x47e676ff)
  internal static let green = ColorName(rgbaValue: 0x47e676ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e0e0e0"></span>
  /// Alpha: 100% <br/> (0xe0e0e0ff)
  internal static let lightGray = ColorName(rgbaValue: 0xe0e0e0ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4e4e4e"></span>
  /// Alpha: 100% <br/> (0x4e4e4eff)
  internal static let messageTextIncomingColor = ColorName(rgbaValue: 0x4e4e4eff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4e4e4e"></span>
  /// Alpha: 100% <br/> (0x4e4e4eff)
  internal static let messageTextOutgoingColor = ColorName(rgbaValue: 0x4e4e4eff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4a1ea7"></span>
  /// Alpha: 100% <br/> (0x4a1ea7ff)
  internal static let primary = ColorName(rgbaValue: 0x4a1ea7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#9e9e9e"></span>
  /// Alpha: 100% <br/> (0x9e9e9eff)
  internal static let secondary = ColorName(rgbaValue: 0x9e9e9eff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#42cece"></span>
  /// Alpha: 100% <br/> (0x42ceceff)
  internal static let turquoise = ColorName(rgbaValue: 0x42ceceff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4e4e4e"></span>
  /// Alpha: 100% <br/> (0x4e4e4eff)
  internal static let ultraDarkGray = ColorName(rgbaValue: 0x4e4e4eff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let components = RGBAComponents(rgbaValue: rgbaValue).normalized
    self.init(red: components[0], green: components[1], blue: components[2], alpha: components[3])
  }
}

private struct RGBAComponents {
  let rgbaValue: UInt32

  private var shifts: [UInt32] {
    [
      rgbaValue >> 24, // red
      rgbaValue >> 16, // green
      rgbaValue >> 8,  // blue
      rgbaValue        // alpha
    ]
  }

  private var components: [CGFloat] {
    shifts.map {
      CGFloat($0 & 0xff)
    }
  }

  var normalized: [CGFloat] {
    components.map { $0 / 255.0 }
  }
}

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
