// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum InfoPlist {
    /// La app necesita utilizar la cámara para la transmisión de imágenes o video
    internal static let nsCameraUsageDescription = L10n.tr("InfoPlist", "NSCameraUsageDescription")
    /// La aplicación necesita acceder a tus contactos
    internal static let nsContactsUsageDescription = L10n.tr("InfoPlist", "NSContactsUsageDescription")
    /// La app necesita saber tu localización para mostrarte servicios cercanos
    internal static let nsLocationWhenInUseUsageDescription = L10n.tr("InfoPlist", "NSLocationWhenInUseUsageDescription")
    /// La app necesita utilizar el micrófono para la transmisión de voz
    internal static let nsMicrophoneUsageDescription = L10n.tr("InfoPlist", "NSMicrophoneUsageDescription")
    /// La app necesita acceder a tus fotos para que las puedas enviar por el chat
    internal static let nsPhotoLibraryUsageDescription = L10n.tr("InfoPlist", "NSPhotoLibraryUsageDescription")
  }
  internal enum Localizable {
    /// Aceptar
    internal static let accept = L10n.tr("Localizable", "accept")
    /// Añadir
    internal static let add = L10n.tr("Localizable", "add")
    /// Cámara
    internal static let camera = L10n.tr("Localizable", "camera")
    /// Cancel
    internal static let cancel = L10n.tr("Localizable", "cancel")
    /// Conectando...
    internal static let connecting = L10n.tr("Localizable", "connecting")
    /// Consultas
    internal static let consultations = L10n.tr("Localizable", "consultations")
    /// Eliminar
    internal static let delete = L10n.tr("Localizable", "delete")
    /// Descripción
    internal static let description = L10n.tr("Localizable", "description")
    /// Documento
    internal static let document = L10n.tr("Localizable", "document")
    /// Editar
    internal static let edit = L10n.tr("Localizable", "edit")
    /// Galería
    internal static let gallery = L10n.tr("Localizable", "gallery")
    /// Imagen
    internal static let image = L10n.tr("Localizable", "image")
    /// Ok
    internal static let ok = L10n.tr("Localizable", "ok")
    /// Privacidad
    internal static let privacy = L10n.tr("Localizable", "privacy")
    /// Guardar
    internal static let save = L10n.tr("Localizable", "save")
    /// Escribiendo...
    internal static let typing = L10n.tr("Localizable", "typing")
    /// Ayer
    internal static let yesterday = L10n.tr("Localizable", "yesterday")
    internal enum Chat {
      internal enum Disconnected {
        /// En este momento no estoy disponible, puedes dejarme un mensaje y te responderé cuando vuelva a conectarme.
        internal static let message = L10n.tr("Localizable", "chat.disconnected.message")
      }
      internal enum Header {
        /// Para valorar mejor tu caso, el profesional podrá acceder a tu historia clínica.
        internal static let legal = L10n.tr("Localizable", "chat.header.legal")
      }
      internal enum Message {
        internal enum Deleted {
          /// Se ha eliminado este mensaje
          internal static let alien = L10n.tr("Localizable", "chat.message.deleted.alien")
          /// Has eliminado este mensaje
          internal static let own = L10n.tr("Localizable", "chat.message.deleted.own")
        }
        internal enum Not {
          /// %@ MENSAJE SIN LEER
          internal static func read(_ p1: Any) -> String {
            return L10n.tr("Localizable", "chat.message.not.read", String(describing: p1))
          }
        }
      }
      internal enum Messages {
        internal enum Not {
          /// %@ MENSAJES SIN LEER
          internal static func read(_ p1: Any) -> String {
            return L10n.tr("Localizable", "chat.messages.not.read", String(describing: p1))
          }
        }
      }
    }
    internal enum Close {
      internal enum Account {
        /// Puedes volver a iniciar sesión siempre que quieras con tu número de teléfono
        internal static let message = L10n.tr("Localizable", "close.account.message")
      }
    }
    internal enum Createreportcontroller {
      internal enum Create {
        /// Generar informe
        internal static let button = L10n.tr("Localizable", "createreportcontroller.create.button")
      }
      internal enum Exit {
        /// Salir
        internal static let button = L10n.tr("Localizable", "createreportcontroller.exit.button")
      }
      internal enum Message {
        /// Te recomendamos generar un informe de la consulta para poder guardarlo en el historial del paciente
        internal static let label = L10n.tr("Localizable", "createreportcontroller.message.label")
      }
    }
    internal enum Mainviewcontoller {
      internal enum CentralToast {
        internal enum Customer {
          internal enum Camara {
            /// El profesional ha deshabilitado su cámara
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.customer.camara.label")
          }
          internal enum Micro {
            /// El profesional ha deshabilitado su micrófono
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.customer.micro.label")
          }
          internal enum MicroAndCamara {
            /// El profesional ha deshabilitado su cámara y micrófono
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.customer.microAndCamara.label")
          }
        }
        internal enum Professional {
          internal enum Camara {
            /// El paciente ha deshabilitado su cámara
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.professional.camara.label")
          }
          internal enum Micro {
            /// El paciente ha deshabilitado su micrófono
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.professional.micro.label")
          }
          internal enum MicroAndCamara {
            /// El paciente ha deshabilitado su cámara y micrófono
            internal static let label = L10n.tr("Localizable", "mainviewcontoller.centralToast.professional.microAndCamara.label")
          }
        }
      }
    }
    internal enum MedicalHistory {
      /// Historial médico
      internal static let title = L10n.tr("Localizable", "medicalHistory.title")
      internal enum Allergy {
        /// No hay alergias añadidas
        internal static let empty = L10n.tr("Localizable", "medicalHistory.allergy.empty")
        /// Alergias
        internal static let title = L10n.tr("Localizable", "medicalHistory.allergy.title")
        internal enum Add {
          /// Cacahuete desde 2006
          internal static let placeholder = L10n.tr("Localizable", "medicalHistory.allergy.add.placeholder")
          /// Añadir alergia
          internal static let title = L10n.tr("Localizable", "medicalHistory.allergy.add.title")
        }
        internal enum Edit {
          /// Editar alergia
          internal static let title = L10n.tr("Localizable", "medicalHistory.allergy.edit.title")
        }
      }
      internal enum Disease {
        /// No hay enfermedades añadidas
        internal static let empty = L10n.tr("Localizable", "medicalHistory.disease.empty")
        /// Enfermedades
        internal static let title = L10n.tr("Localizable", "medicalHistory.disease.title")
        internal enum Add {
          /// Hipertenso desde 2004
          internal static let placeholder = L10n.tr("Localizable", "medicalHistory.disease.add.placeholder")
          /// Añadir enfermedad
          internal static let title = L10n.tr("Localizable", "medicalHistory.disease.add.title")
        }
        internal enum Edit {
          /// Editar enfermedad
          internal static let title = L10n.tr("Localizable", "medicalHistory.disease.edit.title")
        }
      }
      internal enum Medication {
        /// No hay medicamentos añadidos
        internal static let empty = L10n.tr("Localizable", "medicalHistory.medication.empty")
        /// Medicamentos
        internal static let title = L10n.tr("Localizable", "medicalHistory.medication.title")
        internal enum Add {
          /// Paracetamol desde 2000
          internal static let placeholder = L10n.tr("Localizable", "medicalHistory.medication.add.placeholder")
          /// Añadir medicamento
          internal static let title = L10n.tr("Localizable", "medicalHistory.medication.add.title")
        }
        internal enum Edit {
          /// Editar medicamento
          internal static let title = L10n.tr("Localizable", "medicalHistory.medication.edit.title")
        }
      }
    }
    internal enum Mediquo {
      /// Volver
      internal static let back = L10n.tr("Localizable", "mediquo.back")
      /// Consulta
      internal static let consultation = L10n.tr("Localizable", "mediquo.consultation")
      /// Fecha
      internal static let date = L10n.tr("Localizable", "mediquo.date")
      /// Genial
      internal static let great = L10n.tr("Localizable", "mediquo.great")
      /// Profesional
      internal static let professional = L10n.tr("Localizable", "mediquo.professional")
      /// Ver
      internal static let see = L10n.tr("Localizable", "mediquo.see")
      internal enum Accept {
        internal enum And {
          internal enum Pay {
            /// Aceptar y pagar
            internal static let label = L10n.tr("Localizable", "mediquo.accept.and.pay.label")
          }
        }
      }
      internal enum Activate {
        internal enum Now {
          /// Activar ahora
          internal static let button = L10n.tr("Localizable", "mediquo.activate.now.button")
        }
      }
      internal enum Add {
        /// Añadir
        internal static let label = L10n.tr("Localizable", "mediquo.add.label")
        internal enum Appointment {
          internal enum To {
            /// Añadir cita al calendario
            internal static let calendar = L10n.tr("Localizable", "mediquo.add.appointment.to.calendar")
          }
        }
        internal enum Credit {
          internal enum Card {
            /// Añadir tarjeta
            internal static let title = L10n.tr("Localizable", "mediquo.add.credit.card.title")
          }
        }
      }
      internal enum After {
        /// Más tarde
        internal static let button = L10n.tr("Localizable", "mediquo.after.button")
      }
      internal enum Appoinment {
        internal enum Received {
          internal enum Proposal {
            /// Revisa la propuesta del profesional
            internal static let review = L10n.tr("Localizable", "mediquo.appoinment.received.proposal.review")
          }
        }
      }
      internal enum Appointment {
        /// Revisar
        internal static let check = L10n.tr("Localizable", "mediquo.appointment.check")
        /// ¡Cita confirmada!
        internal static let confirmed = L10n.tr("Localizable", "mediquo.appointment.confirmed")
        /// Cita
        internal static let label = L10n.tr("Localizable", "mediquo.appointment.label")
        /// ¡Cita pagada!
        internal static let paid = L10n.tr("Localizable", "mediquo.appointment.paid")
        /// Pagar
        internal static let pay = L10n.tr("Localizable", "mediquo.appointment.pay")
        /// Cita agendada
        internal static let scheduled = L10n.tr("Localizable", "mediquo.appointment.scheduled")
        /// Hoy a las %@
        internal static func today(_ p1: Any) -> String {
          return L10n.tr("Localizable", "mediquo.appointment.today", String(describing: p1))
        }
        internal enum Accepted {
          /// Aceptada
          internal static let label = L10n.tr("Localizable", "mediquo.appointment.accepted.label")
          internal enum Paid {
            /// Aceptada y pagada
            internal static let label = L10n.tr("Localizable", "mediquo.appointment.accepted.paid.label")
          }
        }
        internal enum Creditcard {
          /// El método de pago ha caducado
          internal static let expired = L10n.tr("Localizable", "mediquo.appointment.creditcard.expired")
        }
        internal enum Detail {
          /// Para modificar o cancelar la consulta, habla con el profesional y llegar un acuerdo a través del chat. El o la profesional puede modificar o cancelar la consulta.
          internal static let suggestion = L10n.tr("Localizable", "mediquo.appointment.detail.suggestion")
          /// Detalles de la consulta
          internal static let title = L10n.tr("Localizable", "mediquo.appointment.detail.title")
        }
        internal enum Event {
          internal enum Calendar {
            /// Evento añadido al calendario personal
            internal static let added = L10n.tr("Localizable", "mediquo.appointment.event.calendar.added")
            /// Accede a tu cita directamente desde el enlace de acceso.\nRecuerde notificar a %@ si desea cambiar la hora o el día de su cita.\nRecuerde realizar el pago a través de este enlace %@ antes de la consulta.\n\nDesarrollado por mediquo
            internal static func description(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.appointment.event.calendar.description", String(describing: p1), String(describing: p2))
            }
            /// Cita con %@
            internal static func title(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.appointment.event.calendar.title", String(describing: p1))
            }
          }
        }
        internal enum Expired {
          /// El tiempo para realizar la cita ha expirado
          internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.expired.subtitle")
          /// Cita expirada
          internal static let title = L10n.tr("Localizable", "mediquo.appointment.expired.title")
        }
        internal enum Finished {
          /// Se ha marcado la cita como hecha
          internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.finished.subtitle")
          /// Cita finalizada
          internal static let title = L10n.tr("Localizable", "mediquo.appointment.finished.title")
        }
        internal enum How {
          internal enum Was {
            /// ¿Cómo fue la cita?
            internal static let label = L10n.tr("Localizable", "mediquo.appointment.how.was.label")
          }
        }
        internal enum In {
          internal enum Progress {
            /// Cita en curso
            internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.in.progress.subtitle")
            /// Estás en cita
            internal static let title = L10n.tr("Localizable", "mediquo.appointment.in.progress.title")
          }
        }
        internal enum Pending {
          /// Contacta con el profesional
          internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.pending.subtitle")
          /// Pago pendiente
          internal static let title = L10n.tr("Localizable", "mediquo.appointment.pending.title")
          internal enum Accept {
            /// Pendiente de aceptar
            internal static let label = L10n.tr("Localizable", "mediquo.appointment.pending.accept.label")
            internal enum Payment {
              /// Pendiente de aceptar y pagar
              internal static let label = L10n.tr("Localizable", "mediquo.appointment.pending.accept.payment.label")
            }
          }
          internal enum Activity {
            /// Tienes citas o pagos pendientes
            internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.pending.activity.subtitle")
            /// Actividad pendiente
            internal static let title = L10n.tr("Localizable", "mediquo.appointment.pending.activity.title")
          }
          internal enum Payment {
            /// Pendiente de pagar
            internal static let label = L10n.tr("Localizable", "mediquo.appointment.pending.payment.label")
          }
        }
        internal enum Received {
          /// Consulta recibida
          internal static let label = L10n.tr("Localizable", "mediquo.appointment.received.label")
        }
        internal enum Terms {
          internal enum And {
            /// Al aceptar la cita, usted acepta las <u><b>Términos y condiciones</b></u> del servicio
            internal static let conditions = L10n.tr("Localizable", "mediquo.appointment.terms.and.conditions")
          }
        }
        internal enum Unpaid {
          /// Tienes una cita sin pagar
          internal static let error = L10n.tr("Localizable", "mediquo.appointment.unpaid.error")
          /// Tienes una cita impagada
          internal static let subtitle = L10n.tr("Localizable", "mediquo.appointment.unpaid.subtitle")
        }
      }
      internal enum Appointments {
        internal enum List {
          /// Citas y pagos pendientes
          internal static let title = L10n.tr("Localizable", "mediquo.appointments.list.title")
        }
      }
      internal enum Back {
        internal enum To {
          /// Volver al chat
          internal static let chat = L10n.tr("Localizable", "mediquo.back.to.chat")
        }
      }
      internal enum Calendar {
        internal enum Access {
          /// Permite el acceso a tu calendario. Sin este permiso, no podrás añadir las citas a tu calendario personal.
          internal static let message = L10n.tr("Localizable", "mediquo.calendar.access.message")
        }
      }
      internal enum Call {
        internal enum In {
          internal enum Progress {
            /// %@ ha iniciado una llamada.
            internal static func message(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.call.in.progress.message", String(describing: p1))
            }
          }
        }
      }
      internal enum Cancel {
        internal enum Appointment {
          /// La cita ha sido cancelada
          internal static let subtitle = L10n.tr("Localizable", "mediquo.cancel.appointment.subtitle")
          /// Cita cancelada
          internal static let title = L10n.tr("Localizable", "mediquo.cancel.appointment.title")
        }
      }
      internal enum Card {
        internal enum Number {
          /// Número de tarjeta
          internal static let hint = L10n.tr("Localizable", "mediquo.card.number.hint")
        }
      }
      internal enum Change {
        /// Cambiar
        internal static let label = L10n.tr("Localizable", "mediquo.change.label")
      }
      internal enum Charge {
        internal enum Accepted {
          /// El dinero ha sido transferido
          internal static let subtitle = L10n.tr("Localizable", "mediquo.charge.accepted.subtitle")
          /// ¡Pago realizado!
          internal static let title = L10n.tr("Localizable", "mediquo.charge.accepted.title")
        }
        internal enum Cancelled {
          /// Se ha cancelado el pago
          internal static let subtitle = L10n.tr("Localizable", "mediquo.charge.cancelled.subtitle")
          /// Solicitud cancelada
          internal static let title = L10n.tr("Localizable", "mediquo.charge.cancelled.title")
        }
        internal enum Confirmed {
          /// ¡Pago confirmado!
          internal static let title = L10n.tr("Localizable", "mediquo.charge.confirmed.title")
        }
        internal enum Declined {
          /// Has rechazado el pago
          internal static let subtitle = L10n.tr("Localizable", "mediquo.charge.declined.subtitle")
          /// Solicitud rechazada
          internal static let title = L10n.tr("Localizable", "mediquo.charge.declined.title")
        }
        internal enum Expired {
          /// Solicitud no atendida
          internal static let subtitle = L10n.tr("Localizable", "mediquo.charge.expired.subtitle")
          /// Pago expirado
          internal static let title = L10n.tr("Localizable", "mediquo.charge.expired.title")
        }
        internal enum Pending {
          /// Has recibido una solicitud
          internal static let subtitle = L10n.tr("Localizable", "mediquo.charge.pending.subtitle")
          /// Solicitud de pago
          internal static let title = L10n.tr("Localizable", "mediquo.charge.pending.title")
        }
      }
      internal enum Chat {
        /// Escribiendo…
        internal static let writing = L10n.tr("Localizable", "mediquo.chat.writing")
        internal enum Error {
          internal enum Size {
            internal enum Popup {
              /// El adjunto que estás enviando supera el máximo de 50MB.
              internal static let subtitle = L10n.tr("Localizable", "mediquo.chat.error.size.popup.subtitle")
              /// Peso máximo
              internal static let title = L10n.tr("Localizable", "mediquo.chat.error.size.popup.title")
            }
          }
        }
      }
      internal enum Confirm {
        internal enum Appointment {
          internal enum Accepted {
            /// Se ha aceptado la cita del %@ a las %@
            internal static func message(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.confirm.appointment.accepted.message", String(describing: p1), String(describing: p2))
            }
          }
        }
        internal enum Charge {
          internal enum Accepted {
            /// Se ha realizado el pago de %@ a %@.\n¡Muchas gracias por utilizar nuestro servicio!
            internal static func message(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.confirm.charge.accepted.message", String(describing: p1), String(describing: p2))
            }
          }
        }
        internal enum Payment {
          internal enum Accepted {
            /// Se ha realizado el pago de la cita del %@ a las %@
            internal static func message(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.confirm.payment.accepted.message", String(describing: p1), String(describing: p2))
            }
          }
        }
      }
      internal enum Connection {
        internal enum Error {
          /// Parece que estás teniendo problemas de conexión.\nPor favor inténtalo de nuevo
          internal static let label = L10n.tr("Localizable", "mediquo.connection.error.label")
        }
      }
      internal enum Data {
        internal enum Card {
          /// Datos de la tarjeta
          internal static let label = L10n.tr("Localizable", "mediquo.data.card.label")
        }
      }
      internal enum Decline {
        /// Rechazar
        internal static let label = L10n.tr("Localizable", "mediquo.decline.label")
      }
      internal enum Declined {
        internal enum Consultation {
          /// Has declinado la consulta
          internal static let subtitle = L10n.tr("Localizable", "mediquo.declined.consultation.subtitle")
          /// Consulta declinada
          internal static let title = L10n.tr("Localizable", "mediquo.declined.consultation.title")
        }
      }
      internal enum Empty {
        internal enum Appointments {
          /// Por el momento no tienes actividad pendiente con tus profesionales
          internal static let subtitle = L10n.tr("Localizable", "mediquo.empty.appointments.subtitle")
          /// No hay citas o pagos pendientes
          internal static let title = L10n.tr("Localizable", "mediquo.empty.appointments.title")
        }
      }
      internal enum Groups {
        /// es
        internal static let locale = L10n.tr("Localizable", "mediquo.groups.locale")
        internal enum Alert {
          internal enum Dialog {
            internal enum Close {
              internal enum Image {
                internal enum Content {
                  /// Close image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.alert.dialog.close.image.content.description")
                }
              }
            }
          }
        }
        internal enum Attachment {
          internal enum View {
            internal enum Image {
              internal enum Content {
                /// Attachment image
                internal static let description = L10n.tr("Localizable", "mediquo.groups.attachment.view.image.content.description")
              }
            }
            internal enum Upload {
              internal enum Image {
                internal enum Content {
                  /// Upload image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.attachment.view.upload.image.content.description")
                }
              }
            }
          }
        }
        internal enum Chat {
          internal enum Message {
            internal enum Not {
              /// %@ MENSAJE NO LEÍDO
              internal static func read(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.groups.chat.message.not.read", String(describing: p1))
              }
            }
          }
          internal enum Messages {
            internal enum Not {
              /// %@ MENSAJES NO LEÍDOS
              internal static func read(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.groups.chat.messages.not.read", String(describing: p1))
              }
            }
          }
        }
        internal enum Empty {
          /// Estamos trabajando para que puedas disfrutar de todos los grupos y temas que ofrece nuestra comunidad
          internal static let subtitle = L10n.tr("Localizable", "mediquo.groups.empty.subtitle")
          /// Pronto disfrutarás de grupos
          internal static let title = L10n.tr("Localizable", "mediquo.groups.empty.title")
        }
        internal enum Error {
          internal enum Checker {
            internal enum Is {
              /// Debes completar este campo
              internal static let `required` = L10n.tr("Localizable", "mediquo.groups.error.checker.is.required")
            }
          }
        }
        internal enum Group {
          internal enum Chat {
            internal enum Background {
              internal enum Image {
                internal enum Content {
                  /// Background image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.chat.background.image.content.description")
                }
              }
            }
            internal enum Blocked {
              /// Activar ahora
              internal static let activate = L10n.tr("Localizable", "mediquo.groups.group.chat.blocked.activate")
              /// Grupo Premium
              internal static let premium = L10n.tr("Localizable", "mediquo.groups.group.chat.blocked.premium")
              /// Hazte premium y desbloquea el contenido de este grupo
              internal static let unblock = L10n.tr("Localizable", "mediquo.groups.group.chat.blocked.unblock")
            }
            internal enum Input {
              /// Escribe aquí…
              internal static let hint = L10n.tr("Localizable", "mediquo.groups.group.chat.input.hint")
              /// Enviar
              internal static let send = L10n.tr("Localizable", "mediquo.groups.group.chat.input.send")
              internal enum Attach {
                internal enum Content {
                  /// Attach to group chat
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.chat.input.attach.content.description")
                }
              }
            }
            internal enum Toolbar {
              internal enum Follow {
                internal enum Image {
                  internal enum Content {
                    /// Follow image
                    internal static let description = L10n.tr("Localizable", "mediquo.groups.group.chat.toolbar.follow.image.content.description")
                  }
                }
              }
              internal enum Image {
                internal enum Content {
                  /// Group image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.chat.toolbar.image.content.description")
                }
              }
              internal enum Last {
                internal enum Updated {
                  /// últ. vez hoy a las %@s
                  internal static func at(_ p1: Any) -> String {
                    return L10n.tr("Localizable", "mediquo.groups.group.chat.toolbar.last.updated.at", String(describing: p1))
                  }
                }
              }
            }
          }
          internal enum Detail {
            /// Salir del Grupo
            internal static let exit = L10n.tr("Localizable", "mediquo.groups.group.detail.exit")
            /// ACTIVAR NOTIFICACIONES
            internal static let follow = L10n.tr("Localizable", "mediquo.groups.group.detail.follow")
            /// NOTIFICACIONES ACTIVAS
            internal static let following = L10n.tr("Localizable", "mediquo.groups.group.detail.following")
            internal enum Admin {
              /// %@
              internal static func speciality(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.groups.group.detail.admin.speciality", String(describing: p1))
              }
              internal enum Image {
                internal enum Content {
                  /// Admin image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.detail.admin.image.content.description")
                }
              }
            }
            internal enum Exit {
              internal enum Dialog {
                /// ¿Estás seguro que quieres salir del grupo?
                internal static let message = L10n.tr("Localizable", "mediquo.groups.group.detail.exit.dialog.message")
                /// Salir del grupo
                internal static let title = L10n.tr("Localizable", "mediquo.groups.group.detail.exit.dialog.title")
                internal enum Button {
                  /// SALIR
                  internal static let text = L10n.tr("Localizable", "mediquo.groups.group.detail.exit.dialog.button.text")
                }
              }
            }
            internal enum Header {
              /// Moderador
              internal static let admin = L10n.tr("Localizable", "mediquo.groups.group.detail.header.admin")
            }
            internal enum Image {
              internal enum Content {
                /// Group image
                internal static let description = L10n.tr("Localizable", "mediquo.groups.group.detail.image.content.description")
              }
            }
            internal enum Last {
              internal enum Message {
                internal enum And {
                  /// Último mensaje %@ - %@ Mensajes
                  internal static func total(_ p1: Any, _ p2: Any) -> String {
                    return L10n.tr("Localizable", "mediquo.groups.group.detail.last.message.and.total", String(describing: p1), String(describing: p2))
                  }
                }
              }
            }
            internal enum Members {
              internal enum And {
                /// %@ Miembros · %@ Mensajes
                internal static func messages(_ p1: Any, _ p2: Any) -> String {
                  return L10n.tr("Localizable", "mediquo.groups.group.detail.members.and.messages", String(describing: p1), String(describing: p2))
                }
              }
            }
            internal enum Toolbar {
              /// Info. del grupo
              internal static let title = L10n.tr("Localizable", "mediquo.groups.group.detail.toolbar.title")
            }
          }
          internal enum Item {
            /// Moderador: %@
            internal static func admin(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.groups.group.item.admin", String(describing: p1))
            }
            /// %@ seguidores
            internal static func followers(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.groups.group.item.followers", String(describing: p1))
            }
            /// %@ miembros
            internal static func members(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.groups.group.item.members", String(describing: p1))
            }
            internal enum Follow {
              internal enum Image {
                internal enum Content {
                  /// Follow image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.item.follow.image.content.description")
                }
              }
            }
            internal enum Followers {
              internal enum Image {
                internal enum Content {
                  /// Followers image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.item.followers.image.content.description")
                }
              }
            }
            internal enum Image {
              internal enum Content {
                /// Group image
                internal static let description = L10n.tr("Localizable", "mediquo.groups.group.item.image.content.description")
              }
            }
            internal enum Last {
              internal enum Message {
                internal enum Recived {
                  /// Documento
                  internal static let document = L10n.tr("Localizable", "mediquo.groups.group.item.last.message.recived.document")
                  /// Imagen
                  internal static let image = L10n.tr("Localizable", "mediquo.groups.group.item.last.message.recived.image")
                }
              }
            }
            internal enum Members {
              internal enum And {
                /// %@/%@
                internal static func max(_ p1: Any, _ p2: Any) -> String {
                  return L10n.tr("Localizable", "mediquo.groups.group.item.members.and.max", String(describing: p1), String(describing: p2))
                }
              }
              internal enum Image {
                internal enum Content {
                  /// Members image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.item.members.image.content.description")
                }
              }
            }
            internal enum Messages {
              internal enum Image {
                internal enum Content {
                  /// Messages image
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.item.messages.image.content.description")
                }
              }
            }
            internal enum Reply {
              internal enum Messages {
                /// Foto
                internal static let photo = L10n.tr("Localizable", "mediquo.groups.group.item.reply.messages.photo")
              }
            }
            internal enum Unread {
              internal enum Messages {
                /// 99+
                internal static let max = L10n.tr("Localizable", "mediquo.groups.group.item.unread.messages.max")
              }
            }
            internal enum User {
              internal enum Status {
                ///    Miembro   
                internal static let joined = L10n.tr("Localizable", "mediquo.groups.group.item.user.status.joined")
                ///    Pendiente   
                internal static let pending = L10n.tr("Localizable", "mediquo.groups.group.item.user.status.pending")
              }
            }
          }
          internal enum Menu {
            /// Activar notificaciones
            internal static let follow = L10n.tr("Localizable", "mediquo.groups.group.menu.follow")
            /// Desactivar notificaciones
            internal static let unfollow = L10n.tr("Localizable", "mediquo.groups.group.menu.unfollow")
          }
          internal enum Message {
            internal enum Item {
              /// ✘ Mensaje eliminado
              internal static let deleted = L10n.tr("Localizable", "mediquo.groups.group.message.item.deleted")
              internal enum Date {
                /// yyyyMMddHHmmss
                internal static let format = L10n.tr("Localizable", "mediquo.groups.group.message.item.date.format")
              }
              internal enum Speciality {
                internal enum Content {
                  /// Message speciality
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.message.item.speciality.content.description")
                }
              }
              internal enum Status {
                internal enum Content {
                  /// Message status
                  internal static let description = L10n.tr("Localizable", "mediquo.groups.group.message.item.status.content.description")
                }
              }
            }
            internal enum Separator {
              /// Hoy
              internal static let today = L10n.tr("Localizable", "mediquo.groups.group.message.separator.today")
              /// Ayer
              internal static let yesterday = L10n.tr("Localizable", "mediquo.groups.group.message.separator.yesterday")
              internal enum Day {
                /// d
                internal static let format = L10n.tr("Localizable", "mediquo.groups.group.message.separator.day.format")
                internal enum Of {
                  /// %@ de %@
                  internal static func month(_ p1: Any, _ p2: Any) -> String {
                    return L10n.tr("Localizable", "mediquo.groups.group.message.separator.day.of.month", String(describing: p1), String(describing: p2))
                  }
                }
              }
              internal enum Month {
                /// MMMM
                internal static let format = L10n.tr("Localizable", "mediquo.groups.group.message.separator.month.format")
              }
            }
          }
          internal enum Request {
            internal enum Cancel {
              internal enum Dialog {
                /// ¿Estás seguro que quieres cancelar la solicitud?
                internal static let message = L10n.tr("Localizable", "mediquo.groups.group.request.cancel.dialog.message")
                /// Cancelar solicitud
                internal static let title = L10n.tr("Localizable", "mediquo.groups.group.request.cancel.dialog.title")
                internal enum Button {
                  /// CANCELAR SOLICITUD
                  internal static let text = L10n.tr("Localizable", "mediquo.groups.group.request.cancel.dialog.button.text")
                }
              }
            }
            internal enum Join {
              internal enum Dialog {
                /// ENVIAR SOLICITUD
                internal static let send = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.send")
                /// Escribe tus motivos
                internal static let subtitle = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.subtitle")
                /// ¿Por qué quieres\nunirte al grupo?
                internal static let title = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.title")
                internal enum Close {
                  internal enum Image {
                    internal enum Content {
                      /// Close image
                      internal static let description = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.close.image.content.description")
                    }
                  }
                }
                internal enum Logo {
                  internal enum Image {
                    internal enum Content {
                      /// Logo image
                      internal static let description = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.logo.image.content.description")
                    }
                  }
                }
                internal enum Send {
                  internal enum Input {
                    /// Enviar
                    internal static let ime = L10n.tr("Localizable", "mediquo.groups.group.request.join.dialog.send.input.ime")
                  }
                }
              }
            }
          }
        }
        internal enum Information {
          internal enum Dialog {
            /// Recibirás notificaciones del grupo
            internal static let follow = L10n.tr("Localizable", "mediquo.groups.information.dialog.follow")
            internal enum Un {
              /// Ya no recibirás notificaciones
              internal static let follow = L10n.tr("Localizable", "mediquo.groups.information.dialog.un.follow")
            }
          }
        }
        internal enum Join {
          /// CANCELAR SOLICITUD
          internal static let pending = L10n.tr("Localizable", "mediquo.groups.join.pending")
          /// UNIRME AL GRUPO
          internal static let ready = L10n.tr("Localizable", "mediquo.groups.join.ready")
        }
        internal enum List {
          /// Grupos
          internal static let title = L10n.tr("Localizable", "mediquo.groups.list.title")
          internal enum Empty {
            internal enum View {
              /// No estás en ningún grupo aún.
              internal static let description = L10n.tr("Localizable", "mediquo.groups.list.empty.view.description")
            }
          }
          internal enum Search {
            internal enum NoResults {
              /// No se encontró "%@"
              internal static func subtitle(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.groups.list.search.noResults.subtitle", String(describing: p1))
              }
            }
          }
          internal enum Status {
            internal enum Messages {
              /// Leído
              internal static let read = L10n.tr("Localizable", "mediquo.groups.list.status.messages.read")
              /// No Leído
              internal static let unread = L10n.tr("Localizable", "mediquo.groups.list.status.messages.unread")
            }
          }
        }
        internal enum Members {
          internal enum Navigation {
            internal enum Bar {
              /// Solicitudes
              internal static let request = L10n.tr("Localizable", "mediquo.groups.members.navigation.bar.request")
            }
          }
          internal enum Requests {
            /// Motivación
            internal static let motivation = L10n.tr("Localizable", "mediquo.groups.members.requests.motivation")
            /// No hay solicitudes pendientes.
            internal static let title = L10n.tr("Localizable", "mediquo.groups.members.requests.title")
          }
        }
        internal enum Module {
          /// groups
          internal static let name = L10n.tr("Localizable", "mediquo.groups.module.name")
        }
        internal enum Prof {
          internal enum Delete {
            /// Borrar
            internal static let message = L10n.tr("Localizable", "mediquo.groups.prof.delete.message")
          }
          internal enum Kick {
            /// Expulsar Usuario
            internal static let user = L10n.tr("Localizable", "mediquo.groups.prof.kick.user")
          }
        }
      }
      internal enum Groupschat {
        internal enum Buttonaction {
          /// CANCELAR SOLICITUD
          internal static let cancelgroup = L10n.tr("Localizable", "mediquo.groupschat.buttonaction.cancelgroup")
          /// UNIRME AL GRUPO
          internal static let joingroup = L10n.tr("Localizable", "mediquo.groupschat.buttonaction.joingroup")
          /// Salir del grupo
          internal static let leavegroup = L10n.tr("Localizable", "mediquo.groupschat.buttonaction.leavegroup")
        }
        internal enum Cancelgroup {
          /// CANCELAR SOLICITUD
          internal static let buttonaction = L10n.tr("Localizable", "mediquo.groupschat.cancelgroup.buttonaction")
          /// ¿Estás seguro que quieres cancelar la solicitud?
          internal static let textlabel = L10n.tr("Localizable", "mediquo.groupschat.cancelgroup.textlabel")
          /// Cancelar
          internal static let titlebanner = L10n.tr("Localizable", "mediquo.groupschat.cancelgroup.titlebanner")
        }
        internal enum Conditions {
          internal enum Text {
            /// Lo sentimos, no puedes participar en el grupo porque no cumples las condiciones de acceso.
            internal static let subtitle = L10n.tr("Localizable", "mediquo.groupschat.conditions.text.subtitle")
            internal enum Button {
              /// HACERME PREMIUM
              internal static let premium = L10n.tr("Localizable", "mediquo.groupschat.conditions.text.button.premium")
              internal enum Follow {
                /// SEGUIR GRUPO
                internal static let group = L10n.tr("Localizable", "mediquo.groupschat.conditions.text.button.follow.group")
              }
            }
          }
          internal enum Title {
            /// Requisitos
            internal static let alert = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.alert")
            /// Para participar es necesario:
            internal static let conditions = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.conditions")
            internal enum Condition {
              /// Ser 
              internal static let _1 = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.1")
              /// Tener más/menos de 
              internal static let _2 = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.2")
              /// Ser usuario 
              internal static let _3 = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.3")
              internal enum Bold1 {
                /// Mujer
                internal static let bold = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.bold1.bold")
              }
              internal enum Bold2 {
                /// 35 años
                internal static let bold = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.bold2.bold")
              }
              internal enum Bold3 {
                /// Premium
                internal static let bold = L10n.tr("Localizable", "mediquo.groupschat.conditions.title.condition.bold3.bold")
              }
            }
          }
        }
        internal enum Errorgroup {
          /// REINTENTAR
          internal static let buttonaction = L10n.tr("Localizable", "mediquo.groupschat.errorgroup.buttonaction")
          /// Ha habido un error
          internal static let textlabel = L10n.tr("Localizable", "mediquo.groupschat.errorgroup.textlabel")
          /// Error
          internal static let titlebanner = L10n.tr("Localizable", "mediquo.groupschat.errorgroup.titlebanner")
        }
        internal enum Joingroup {
          /// ENVIAR SOLICITUD
          internal static let buttonaction = L10n.tr("Localizable", "mediquo.groupschat.joingroup.buttonaction")
          /// Escribe tus motivos
          internal static let textlabel = L10n.tr("Localizable", "mediquo.groupschat.joingroup.textlabel")
          /// ¿Por qué quieres unirte al grupo?
          internal static let titlebanner = L10n.tr("Localizable", "mediquo.groupschat.joingroup.titlebanner")
        }
        internal enum Leavegroup {
          /// SALIR
          internal static let buttonaction = L10n.tr("Localizable", "mediquo.groupschat.leavegroup.buttonaction")
          /// ¿Estás seguro que quieres salir del grupo?
          internal static let textlabel = L10n.tr("Localizable", "mediquo.groupschat.leavegroup.textlabel")
          /// Salir del grupo
          internal static let titlebanner = L10n.tr("Localizable", "mediquo.groupschat.leavegroup.titlebanner")
        }
        internal enum Prof {
          internal enum Kick {
            internal enum Usser {
              /// Expulsar
              internal static let button = L10n.tr("Localizable", "mediquo.groupschat.prof.kick.usser.button")
              internal enum Subtitle {
                /// ¿Cual es el motivo de expulsión?
                internal static let alert = L10n.tr("Localizable", "mediquo.groupschat.prof.kick.usser.subtitle.alert")
              }
              internal enum Textfield {
                internal enum Reasons {
                  /// Es obligatorio introducir un motivo para poder expulsar a un usuario.
                  internal static let placeholder = L10n.tr("Localizable", "mediquo.groupschat.prof.kick.usser.textfield.reasons.placeholder")
                }
              }
              internal enum Title {
                /// EXPULSAR USUARIO
                internal static let alert = L10n.tr("Localizable", "mediquo.groupschat.prof.kick.usser.title.alert")
              }
            }
          }
        }
        internal enum Rules {
          internal enum Text {
            /// ACEPTO
            internal static let button = L10n.tr("Localizable", "mediquo.groupschat.rules.text.button")
            /// El incumplimiento de estas normas puede ser motivo de expulsión
            internal static let subtitle = L10n.tr("Localizable", "mediquo.groupschat.rules.text.subtitle")
          }
          internal enum Title {
            /// Normas
            internal static let alert = L10n.tr("Localizable", "mediquo.groupschat.rules.title.alert")
            /// NO está Permitido:
            internal static let rules = L10n.tr("Localizable", "mediquo.groupschat.rules.title.rules")
            internal enum Rule {
              /// Enviar o pedir datos personales como el teléfono.
              internal static let _1 = L10n.tr("Localizable", "mediquo.groupschat.rules.title.rule.1")
              /// Faltas de respeto.
              internal static let _2 = L10n.tr("Localizable", "mediquo.groupschat.rules.title.rule.2")
            }
          }
        }
      }
      internal enum How {
        internal enum Much {
          /// Cuánto
          internal static let label = L10n.tr("Localizable", "mediquo.how.much.label")
        }
      }
      internal enum Hx {
        internal enum Allergies {
          internal enum Add {
            /// AÑADIR ALERGIA
            internal static let message = L10n.tr("Localizable", "mediquo.hx.allergies.add.message")
          }
          internal enum Empty {
            /// No has introducido ninguna alergia
            internal static let message = L10n.tr("Localizable", "mediquo.hx.allergies.empty.message")
          }
          internal enum Header {
            /// Esta es la lista de tus alergias, donde tanto tú como tu médico podéis añadir y modificar contenido. Procura mantenerla actualizada para agilizar las consultas con los médicos.
            internal static let title = L10n.tr("Localizable", "mediquo.hx.allergies.header.title")
          }
        }
        internal enum Control {
          internal enum Date {
            internal enum Picker {
              /// Selecciona una fecha
              internal static let message = L10n.tr("Localizable", "mediquo.hx.control.date.picker.message")
            }
          }
          internal enum Description {
            internal enum Description {
              /// Diagnosticado el: %@
              internal static func diagnosed(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.hx.control.description.description.diagnosed", String(describing: p1))
              }
              /// Resuelto el: %@
              internal static func resolved(_ p1: Any) -> String {
                return L10n.tr("Localizable", "mediquo.hx.control.description.description.resolved", String(describing: p1))
              }
            }
            internal enum Title {
              /// Nombre de la alergia
              internal static let allergy = L10n.tr("Localizable", "mediquo.hx.control.description.title.allergy")
              /// Detalles
              internal static let details = L10n.tr("Localizable", "mediquo.hx.control.description.title.details")
              /// Fecha del diagnóstico
              internal static let diagnosed = L10n.tr("Localizable", "mediquo.hx.control.description.title.diagnosed")
              /// Nombre de la enfermedad
              internal static let disease = L10n.tr("Localizable", "mediquo.hx.control.description.title.disease")
              /// Nombre del tratamiento
              internal static let medication = L10n.tr("Localizable", "mediquo.hx.control.description.title.medication")
              /// Posología
              internal static let posology = L10n.tr("Localizable", "mediquo.hx.control.description.title.posology")
              /// Fecha de resolución
              internal static let resolved = L10n.tr("Localizable", "mediquo.hx.control.description.title.resolved")
            }
          }
          internal enum Edit {
            internal enum Description {
              internal enum Suggestion {
                /// ej.: polen
                internal static let allergy = L10n.tr("Localizable", "mediquo.hx.control.edit.description.suggestion.allergy")
                /// ej.: hipertensión
                internal static let disease = L10n.tr("Localizable", "mediquo.hx.control.edit.description.suggestion.disease")
                /// ej.: aspirina
                internal static let medication = L10n.tr("Localizable", "mediquo.hx.control.edit.description.suggestion.medication")
                /// ej.: 200mg cada 8 horas
                internal static let posology = L10n.tr("Localizable", "mediquo.hx.control.edit.description.suggestion.posology")
              }
              internal enum Validation {
                /// Este campo es obligatorio
                internal static let error = L10n.tr("Localizable", "mediquo.hx.control.edit.description.validation.error")
              }
            }
          }
          internal enum Severity {
            /// Gravedad
            internal static let title = L10n.tr("Localizable", "mediquo.hx.control.severity.title")
          }
        }
        internal enum Detail {
          internal enum Allergy {
            /// Alergia
            internal static let title = L10n.tr("Localizable", "mediquo.hx.detail.allergy.title")
          }
          internal enum Disease {
            /// Enfermedad
            internal static let title = L10n.tr("Localizable", "mediquo.hx.detail.disease.title")
          }
          internal enum Medication {
            /// Medicación
            internal static let title = L10n.tr("Localizable", "mediquo.hx.detail.medication.title")
            /// Informes
            internal static let videoCallReports = L10n.tr("Localizable", "mediquo.hx.detail.medication.videoCallReports")
          }
        }
        internal enum Diseases {
          internal enum Add {
            /// AÑADIR ENFERMEDAD
            internal static let message = L10n.tr("Localizable", "mediquo.hx.diseases.add.message")
          }
          internal enum Empty {
            /// No has introducido ninguna enfermedad
            internal static let message = L10n.tr("Localizable", "mediquo.hx.diseases.empty.message")
          }
          internal enum Header {
            /// Esta es la lista de tus enfermedades, donde tanto tú como tu médico podéis añadir y modificar contenido. Procura mantenerla actualizada para agilizar las consultas con los médicos.
            internal static let title = L10n.tr("Localizable", "mediquo.hx.diseases.header.title")
          }
        }
        internal enum Literal {
          /// Eliminar
          internal static let delete = L10n.tr("Localizable", "mediquo.hx.literal.delete")
          /// Editar
          internal static let edit = L10n.tr("Localizable", "mediquo.hx.literal.edit")
          /// Guardar
          internal static let save = L10n.tr("Localizable", "mediquo.hx.literal.save")
        }
        internal enum Main {
          internal enum Header {
            /// Añadiendo información a tu historial médico estás ayudando a que los profesionales te atiendan mejor. Este historial será compartido con ellos.
            internal static let title = L10n.tr("Localizable", "mediquo.hx.main.header.title")
          }
          internal enum Navigation {
            /// Mi historial médico
            internal static let title = L10n.tr("Localizable", "mediquo.hx.main.navigation.title")
          }
          internal enum Option {
            /// Alergias
            internal static let allergies = L10n.tr("Localizable", "mediquo.hx.main.option.allergies")
            /// Enfermedades
            internal static let diseases = L10n.tr("Localizable", "mediquo.hx.main.option.diseases")
            /// Informes médicos
            internal static let medicalReports = L10n.tr("Localizable", "mediquo.hx.main.option.medicalReports")
            /// Medicación
            internal static let medications = L10n.tr("Localizable", "mediquo.hx.main.option.medications")
            /// Recetas
            internal static let recipes = L10n.tr("Localizable", "mediquo.hx.main.option.recipes")
            /// Informes
            internal static let videoCallReports = L10n.tr("Localizable", "mediquo.hx.main.option.videoCallReports")
          }
        }
        internal enum Medications {
          internal enum Add {
            /// AÑADIR MEDICACIÓN
            internal static let message = L10n.tr("Localizable", "mediquo.hx.medications.add.message")
          }
          internal enum Empty {
            /// No has introducido ningún tratamiento
            internal static let message = L10n.tr("Localizable", "mediquo.hx.medications.empty.message")
          }
          internal enum Header {
            /// Esta es la lista de las medicinas que tomas actualmente, donde tanto tú como tu médico podéis añadir y modificar contenido. Procura mantenerla actualizada para agilizar las consultas con los médicos.
            internal static let title = L10n.tr("Localizable", "mediquo.hx.medications.header.title")
          }
        }
        internal enum Recipes {
          internal enum Empty {
            /// Aquí aparecerán las recetas que tu médico cree
            internal static let description = L10n.tr("Localizable", "mediquo.hx.recipes.empty.description")
            /// No hay recetas
            internal static let message = L10n.tr("Localizable", "mediquo.hx.recipes.empty.message")
          }
        }
        internal enum VideoCallReports {
          internal enum Description {
            /// Visita %@ a las %@
            internal static func item(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.hx.videoCallReports.description.item", String(describing: p1), String(describing: p2))
            }
          }
          internal enum Empty {
            /// No hay ningún informe
            internal static let message = L10n.tr("Localizable", "mediquo.hx.videoCallReports.empty.message")
          }
          internal enum Header {
            /// Esta es tu lista de informes que los médicos han escrito.
            internal static let title = L10n.tr("Localizable", "mediquo.hx.videoCallReports.header.title")
          }
        }
      }
      internal enum Inbox {
        internal enum Alert {
          internal enum Banned {
            /// Tu usuario ha sido bloqueado por inclumplimiento de los %@ del servicio. Para cualquier duda o reclamación envía un correo electrónico a %@
            internal static func message(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.inbox.alert.banned.message", String(describing: p1), String(describing: p2))
            }
            /// Usuario Bloqueado
            internal static let title = L10n.tr("Localizable", "mediquo.inbox.alert.banned.title")
            internal enum Message {
              /// info@meetingdoctors.com
              internal static let email = L10n.tr("Localizable", "mediquo.inbox.alert.banned.message.email")
              /// términos y condiciones
              internal static let terms = L10n.tr("Localizable", "mediquo.inbox.alert.banned.message.terms")
            }
          }
        }
        internal enum Contact {
          internal enum Schedule {
            /// Disponible el %@ a las %@
            internal static func forthcoming(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.inbox.contact.schedule.forthcoming", String(describing: p1), String(describing: p2))
            }
            /// 
            internal static let notavailable = L10n.tr("Localizable", "mediquo.inbox.contact.schedule.notavailable")
            /// Disponible hasta las %@
            internal static func now(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.inbox.contact.schedule.now", String(describing: p1))
            }
            /// No conectado
            internal static let offline = L10n.tr("Localizable", "mediquo.inbox.contact.schedule.offline")
            /// Disponible hoy a las %@
            internal static func soon(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.inbox.contact.schedule.soon", String(describing: p1))
            }
            /// Disponible mañana a las %@
            internal static func tomorrow(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.inbox.contact.schedule.tomorrow", String(describing: p1))
            }
            internal enum Cell {
              /// Horario de Conexión:
              internal static let title = L10n.tr("Localizable", "mediquo.inbox.contact.schedule.cell.title")
            }
          }
          internal enum Speciality {
            internal enum And {
              /// %@ (Col. %@)
              internal static func collegiate(_ p1: Any, _ p2: Any) -> String {
                return L10n.tr("Localizable", "mediquo.inbox.contact.speciality.and.collegiate", String(describing: p1), String(describing: p2))
              }
              internal enum Collegiate {
                /// %@ (Col.&nbsp;%@)
                internal static func html(_ p1: Any, _ p2: Any) -> String {
                  return L10n.tr("Localizable", "mediquo.inbox.contact.speciality.and.collegiate.html", String(describing: p1), String(describing: p2))
                }
              }
            }
          }
        }
        internal enum Navigation {
          /// mediQuo
          internal static let title = L10n.tr("Localizable", "mediquo.inbox.navigation.title")
        }
      }
      internal enum Incorrect {
        internal enum Date {
          /// Fecha incorrecta
          internal static let label = L10n.tr("Localizable", "mediquo.incorrect.date.label")
        }
      }
      internal enum Literal {
        /// Aceptar
        internal static let accept = L10n.tr("Localizable", "mediquo.literal.accept")
        /// Cancelar
        internal static let cancel = L10n.tr("Localizable", "mediquo.literal.cancel")
        /// Cerrar
        internal static let close = L10n.tr("Localizable", "mediquo.literal.close")
        /// Privacidad
        internal static let privacy = L10n.tr("Localizable", "mediquo.literal.privacy")
      }
      internal enum Medical {
        internal enum History {
          internal enum List {
            internal enum EmptyList {
              /// Aquí aparecerán las recetas que tu médico cree
              internal static let subtitle = L10n.tr("Localizable", "mediquo.medical.history.list.emptyList.subtitle")
              /// No hay recetas
              internal static let title = L10n.tr("Localizable", "mediquo.medical.history.list.emptyList.title")
            }
          }
        }
      }
      internal enum Messages {
        /// 
        internal static let subtitle = L10n.tr("Localizable", "mediquo.messages.subtitle")
        /// 
        internal static let title = L10n.tr("Localizable", "mediquo.messages.title")
        internal enum Delete {
          /// Borrar conversación
          internal static let conversation = L10n.tr("Localizable", "mediquo.messages.delete.conversation")
          internal enum Alert {
            /// Estás a punto de eliminar todos los mensajes de esta conversación, ¿Deseas continuar?
            internal static let description = L10n.tr("Localizable", "mediquo.messages.delete.alert.description")
            /// Eliminar mensajes
            internal static let title = L10n.tr("Localizable", "mediquo.messages.delete.alert.title")
          }
        }
        internal enum Disconnected {
          /// No estoy disponible, déjame un mensaje y te responderé cuando vuelva a conectarme.
          internal static let message = L10n.tr("Localizable", "mediquo.messages.disconnected.message")
        }
        internal enum Empty {
          /// Estamos trabajando para que pueda disfrutar de todos los médicos que ofrece nuestra comunidad.
          internal static let subtitle = L10n.tr("Localizable", "mediquo.messages.empty.subtitle")
          /// Todavía no hemos encontrado ningún profesional
          internal static let title = L10n.tr("Localizable", "mediquo.messages.empty.title")
        }
        internal enum Error {
          /// Error de conexión
          internal static let message = L10n.tr("Localizable", "mediquo.messages.error.message")
          /// Volver a enviar
          internal static let resend = L10n.tr("Localizable", "mediquo.messages.error.resend")
          internal enum Message {
            /// Tus mensajes se enviarán al recuperar la conexión
            internal static let subtitle = L10n.tr("Localizable", "mediquo.messages.error.message.subtitle")
          }
        }
        internal enum Input {
          ///  Escribe aquí...
          internal static let placeholder = L10n.tr("Localizable", "mediquo.messages.input.placeholder")
          /// Enviar
          internal static let send = L10n.tr("Localizable", "mediquo.messages.input.send")
          internal enum Attachment {
            /// Cámara
            internal static let camera = L10n.tr("Localizable", "mediquo.messages.input.attachment.camera")
            /// Documento
            internal static let document = L10n.tr("Localizable", "mediquo.messages.input.attachment.document")
            /// Foto
            internal static let gallery = L10n.tr("Localizable", "mediquo.messages.input.attachment.gallery")
          }
        }
        internal enum LastMessage {
          internal enum Received {
            /// Documento
            internal static let document = L10n.tr("Localizable", "mediquo.messages.lastMessage.received.document")
            /// Imagen
            internal static let image = L10n.tr("Localizable", "mediquo.messages.lastMessage.received.image")
          }
        }
        internal enum ListChats {
          internal enum Search {
            /// Busca por especialidad, nombre ...
            internal static let title = L10n.tr("Localizable", "mediquo.messages.listChats.search.title")
          }
        }
        internal enum Show {
          /// Ver perfil
          internal static let profile = L10n.tr("Localizable", "mediquo.messages.show.profile")
        }
      }
      internal enum Month {
        internal enum Year {
          /// MM/AA
          internal static let hint = L10n.tr("Localizable", "mediquo.month.year.hint")
        }
      }
      internal enum Payment {
        /// Método de pago
        internal static let method = L10n.tr("Localizable", "mediquo.payment.method")
        internal enum Detail {
          /// Detalles del pago
          internal static let title = L10n.tr("Localizable", "mediquo.payment.detail.title")
        }
        internal enum In {
          internal enum Progress {
            /// Procesando el pago de su tarjeta
            internal static let subtitle = L10n.tr("Localizable", "mediquo.payment.in.progress.subtitle")
            /// Realizando pago
            internal static let title = L10n.tr("Localizable", "mediquo.payment.in.progress.title")
          }
        }
        internal enum Made {
          /// Pago realizado
          internal static let label = L10n.tr("Localizable", "mediquo.payment.made.label")
        }
        internal enum Method {
          /// Tarjeta de crédito o débito
          internal static let hint = L10n.tr("Localizable", "mediquo.payment.method.hint")
        }
      }
      internal enum Privacy {
        internal enum Photo {
          internal enum Usage {
            /// Debes autorizar el acceso a las fotos para enviar imágenes.
            internal static let description = L10n.tr("Localizable", "mediquo.privacy.photo.usage.description")
            /// Configurar
            internal static let settings = L10n.tr("Localizable", "mediquo.privacy.photo.usage.settings")
          }
        }
      }
      internal enum Professionals {
        internal enum Groups {
          internal enum Group {
            internal enum Message {
              internal enum Item {
                /// Mensaje eliminado
                internal static let deleted = L10n.tr("Localizable", "mediquo.professionals.groups.group.message.item.deleted")
              }
            }
          }
        }
      }
      internal enum Profile {
        internal enum Description {
          /// Descripción
          internal static let title = L10n.tr("Localizable", "mediquo.profile.description.title")
        }
        internal enum Schedule {
          /// %@: %@\n
          internal static func disabled(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("Localizable", "mediquo.profile.schedule.disabled", String(describing: p1), String(describing: p2))
          }
          /// %@: de %@ a %@\n
          internal static func intersection(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
            return L10n.tr("Localizable", "mediquo.profile.schedule.intersection", String(describing: p1), String(describing: p2), String(describing: p3))
          }
          /// %@: de %@ a %@ y de %@ a %@\n
          internal static func normal(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any) -> String {
            return L10n.tr("Localizable", "mediquo.profile.schedule.normal", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5))
          }
          /// Horario
          internal static let title = L10n.tr("Localizable", "mediquo.profile.schedule.title")
          internal enum Work {
            internal enum Day {
              /// De Lunes a Viernes de %@ a %@\n
              internal static func intertersection(_ p1: Any, _ p2: Any) -> String {
                return L10n.tr("Localizable", "mediquo.profile.schedule.work.day.intertersection", String(describing: p1), String(describing: p2))
              }
              /// De Lunes a Viernes de %@ a %@ y de %@ a %@\n
              internal static func normal(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
                return L10n.tr("Localizable", "mediquo.profile.schedule.work.day.normal", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
              }
            }
          }
        }
      }
      internal enum Reports {
        /// O. Diagnóstica
        internal static let diagnostic = L10n.tr("Localizable", "mediquo.reports.diagnostic")
        /// INF
        internal static let rep = L10n.tr("Localizable", "mediquo.reports.rep")
        /// Informes médicos
        internal static let title = L10n.tr("Localizable", "mediquo.reports.title")
        internal enum Detail {
          /// Detalles del informe
          internal static let title = L10n.tr("Localizable", "mediquo.reports.detail.title")
          internal enum Creation {
            /// Fecha de creación
            internal static let date = L10n.tr("Localizable", "mediquo.reports.detail.creation.date")
            /// Hora de creación
            internal static let time = L10n.tr("Localizable", "mediquo.reports.detail.creation.time")
          }
          internal enum Patient {
            /// Nombre del paciente
            internal static let name = L10n.tr("Localizable", "mediquo.reports.detail.patient.name")
          }
          internal enum Professional {
            /// Nombre del profesional
            internal static let name = L10n.tr("Localizable", "mediquo.reports.detail.professional.name")
          }
        }
        internal enum EmptyView {
          /// Aquí aparecerán los informes médicos que tu médico cree.
          internal static let subtitle = L10n.tr("Localizable", "mediquo.reports.emptyView.subtitle")
          /// No hay informes
          internal static let title = L10n.tr("Localizable", "mediquo.reports.emptyView.title")
        }
        internal enum Summary {
          /// Orientación diagnóstica
          internal static let diagnosticOrientation = L10n.tr("Localizable", "mediquo.reports.summary.diagnosticOrientation")
          /// Motivo de consulta
          internal static let reasonConsultation = L10n.tr("Localizable", "mediquo.reports.summary.reasonConsultation")
          /// Recomendaciones
          internal static let recommendations = L10n.tr("Localizable", "mediquo.reports.summary.recommendations")
          /// Signos y síntomas manifestados
          internal static let signsAndSymptomsManifested = L10n.tr("Localizable", "mediquo.reports.summary.signsAndSymptomsManifested")
        }
      }
      internal enum Rgpd {
        internal enum Chat {
          /// Para valorar mejor tu caso, el profesional podrá acceder a tu historia clínica.
          internal static let legal = L10n.tr("Localizable", "mediquo.rgpd.chat.legal")
        }
      }
      internal enum Service {
        internal enum Cost {
          /// Coste del servicio
          internal static let label = L10n.tr("Localizable", "mediquo.service.cost.label")
        }
      }
      internal enum Shared {
        /// Confirmar
        internal static let confirm = L10n.tr("Localizable", "mediquo.shared.confirm")
        /// Crear
        internal static let create = L10n.tr("Localizable", "mediquo.shared.create")
        /// Buscar
        internal static let find = L10n.tr("Localizable", "mediquo.shared.find")
        /// Unirse
        internal static let join = L10n.tr("Localizable", "mediquo.shared.join")
        /// No hay resultados
        internal static let searchNoResults = L10n.tr("Localizable", "mediquo.shared.searchNoResults")
        /// No hemos encontrado ningún resultado con la palabra "%@"
        internal static func searchNoResultsSubtitle(_ p1: Any) -> String {
          return L10n.tr("Localizable", "mediquo.shared.searchNoResultsSubtitle", String(describing: p1))
        }
        /// Ayer
        internal static let yesterday = L10n.tr("Localizable", "mediquo.shared.yesterday")
      }
      internal enum Status {
        internal enum Bar {
          internal enum Appointment {
            /// El %@date a las %@hour
            internal static func date(_ p1: Any, _ p2: Any) -> String {
              return L10n.tr("Localizable", "mediquo.status.bar.appointment.date", String(describing: p1), String(describing: p2))
            }
          }
        }
      }
      internal enum Total {
        internal enum Service {
          internal enum Cost {
            /// Coste total
            internal static let label = L10n.tr("Localizable", "mediquo.total.service.cost.label")
          }
        }
      }
      internal enum Tpv {
        /// TPV
        internal static let label = L10n.tr("Localizable", "mediquo.tpv.label")
      }
      internal enum VideoCall {
        internal enum Firstviewwaiting {
          /// Doctor no asignado todavía
          internal static let namedoctor = L10n.tr("Localizable", "mediquo.videoCall.firstviewwaiting.namedoctor")
          /// Próxima Cita
          internal static let nextconsultation = L10n.tr("Localizable", "mediquo.videoCall.firstviewwaiting.nextconsultation")
          /// Tu médico te llamará en \n unos instantes...
          internal static let statuscall = L10n.tr("Localizable", "mediquo.videoCall.firstviewwaiting.statuscall")
          internal enum Buttonaction {
            /// CANCELAR
            internal static let cancelconsultation = L10n.tr("Localizable", "mediquo.videoCall.firstviewwaiting.buttonaction.cancelconsultation")
          }
        }
        internal enum Secondviewwaiting {
          /// Próxima Cita
          internal static let nextconsultation = L10n.tr("Localizable", "mediquo.videoCall.secondviewwaiting.nextconsultation")
          /// Tu médico te llamará en \n unos instantes...
          internal static let statuscall = L10n.tr("Localizable", "mediquo.videoCall.secondviewwaiting.statuscall")
        }
        internal enum Thirdviewwaiting {
          /// Llamada entrante...
          internal static let statuscall = L10n.tr("Localizable", "mediquo.videoCall.thirdviewwaiting.statuscall")
          internal enum Buttonaction {
            /// DESCOLGAR
            internal static let cancelconsultation = L10n.tr("Localizable", "mediquo.videoCall.thirdviewwaiting.buttonaction.cancelconsultation")
          }
        }
      }
      internal enum Videocall {
        internal enum In {
          internal enum Progress {
            /// %@ ha iniciado una videollamada.
            internal static func message(_ p1: Any) -> String {
              return L10n.tr("Localizable", "mediquo.videocall.in.progress.message", String(describing: p1))
            }
          }
        }
      }
      internal enum When {
        /// Cuándo
        internal static let label = L10n.tr("Localizable", "mediquo.when.label")
      }
      internal enum You {
        internal enum Have {
          internal enum An {
            internal enum Appointment {
              /// Tienes cita
              internal static let label = L10n.tr("Localizable", "mediquo.you.have.an.appointment.label")
            }
          }
        }
      }
    }
    internal enum Meetingdoctors {
      internal enum Core {
        internal enum Group {
          /// Ya no perteneces al grupo %@.
          internal static func kicked(_ p1: Any) -> String {
            return L10n.tr("Localizable", "meetingdoctors.core.group.kicked", String(describing: p1))
          }
          internal enum Invitation {
            /// Tu solicitud al grupo %@ ya está aceptada.
            internal static func accepted(_ p1: Any) -> String {
              return L10n.tr("Localizable", "meetingdoctors.core.group.invitation.accepted", String(describing: p1))
            }
            /// Tu solicitud al grupo %@ ha sido rechazada.
            internal static func declined(_ p1: Any) -> String {
              return L10n.tr("Localizable", "meetingdoctors.core.group.invitation.declined", String(describing: p1))
            }
          }
          internal enum New {
            /// Tienes nuevos mensajes en el grupo %@.
            internal static func messages(_ p1: Any) -> String {
              return L10n.tr("Localizable", "meetingdoctors.core.group.new.messages", String(describing: p1))
            }
          }
        }
        internal enum Push {
          internal enum Channel {
            /// Grupos
            internal static let name = L10n.tr("Localizable", "meetingdoctors.core.push.channel.name")
          }
        }
      }
    }
    internal enum Not {
      internal enum Available {
        /// %@ no estará disponible por el momento. Contacta con otro profesional activo para que pueda atenderte.
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "not.available.message", String(describing: p1))
        }
      }
    }
    internal enum Notifications {
      internal enum Not {
        internal enum Activated {
          /// No tienes notificaciones activas
          internal static let message = L10n.tr("Localizable", "notifications.not.activated.message")
          internal enum Sub {
            /// ¡Actívalas!
            internal static let message = L10n.tr("Localizable", "notifications.not.activated.sub.message")
          }
        }
      }
    }
    internal enum Onboarding {
      internal enum Contact {
        internal enum Data {
          /// Informa al profesional de tu correo electrónico para poder recibir informes.
          internal static let description = L10n.tr("Localizable", "onboarding.contact.data.description")
        }
      }
      internal enum Electronic {
        internal enum Prescription {
          /// Facilita tu id para poder recibir recetas electrónicas.
          internal static let description = L10n.tr("Localizable", "onboarding.electronic.prescription.description")
        }
      }
      internal enum Mandatory {
        internal enum Fields {
          /// Los campos son obligatorios
          internal static let legend = L10n.tr("Localizable", "onboarding.mandatory.fields.legend")
        }
      }
      internal enum Medical {
        internal enum Record {
          /// Selecciona tu identidad para añadir a tu ficha médica.
          internal static let description = L10n.tr("Localizable", "onboarding.medical.record.description")
        }
      }
      internal enum Personal {
        internal enum Data {
          /// Completa tus datos para poder dirigirnos a ti de manera personalizada.
          internal static let description = L10n.tr("Localizable", "onboarding.personal.data.description")
        }
      }
      internal enum Second {
        internal enum Medical {
          internal enum Record {
            /// Confirma tu dirección para añadir a tu ficha médica.
            internal static let description = L10n.tr("Localizable", "onboarding.second.medical.record.description")
          }
        }
      }
    }
    internal enum Popup {
      internal enum Permission {
        internal enum Audio {
          /// Para poder usar esta funcionalidad activa los permisos de micro en los ajustes de tu teléfono
          internal static let subtitle = L10n.tr("Localizable", "popup.permission.audio.subtitle")
          /// Llamada rechazada por falta de permisos
          internal static let title = L10n.tr("Localizable", "popup.permission.audio.title")
          internal enum Action {
            /// Ir a mis ajustes
            internal static let button = L10n.tr("Localizable", "popup.permission.audio.action.button")
          }
        }
        internal enum Video {
          /// Para poder usar esta funcionalidad activa los permisos de micro y cámara en los ajustes de tu teléfono
          internal static let subtitle = L10n.tr("Localizable", "popup.permission.video.subtitle")
          /// Videollamada rechazada por falta de permisos
          internal static let title = L10n.tr("Localizable", "popup.permission.video.title")
          internal enum Action {
            /// Ir a mis ajustes
            internal static let button = L10n.tr("Localizable", "popup.permission.video.action.button")
          }
        }
      }
      internal enum Professional {
        internal enum Client {
          internal enum Permission {
            internal enum Audio {
              internal enum Denied {
                /// Puedes recordarle que se los active utilizando el chat
                internal static let subtitle = L10n.tr("Localizable", "popup.professional.client.permission.audio.denied.subtitle")
                /// El paciente no tiene los permisos necesarios y se ha rechazado la llamada
                internal static let title = L10n.tr("Localizable", "popup.professional.client.permission.audio.denied.title")
                internal enum Action {
                  /// De acuerdo
                  internal static let button = L10n.tr("Localizable", "popup.professional.client.permission.audio.denied.action.button")
                }
              }
            }
            internal enum Video {
              internal enum Denied {
                /// Puedes recordarle que se los active utilizando el chat
                internal static let subtitle = L10n.tr("Localizable", "popup.professional.client.permission.video.denied.subtitle")
                /// El paciente no tiene los permisos necesarios y se ha rechazado la videollamada
                internal static let title = L10n.tr("Localizable", "popup.professional.client.permission.video.denied.title")
                internal enum Action {
                  /// De acuerdo
                  internal static let button = L10n.tr("Localizable", "popup.professional.client.permission.video.denied.action.button")
                }
              }
            }
          }
        }
      }
    }
    internal enum Privacy {
      internal enum Photo {
        internal enum Usage {
          /// Debes autorizar el acceso a las fotos para enviar imágenes.
          internal static let description = L10n.tr("Localizable", "privacy.photo.usage.description")
          /// Configurar
          internal static let settings = L10n.tr("Localizable", "privacy.photo.usage.settings")
        }
      }
    }
    internal enum Professionals {
      internal enum ErrorView {
        /// Cierra la app y vuelve a intentarlo en unos minutos
        internal static let subtitle = L10n.tr("Localizable", "professionals.errorView.subtitle")
        /// ¡Ups!
        internal static let title = L10n.tr("Localizable", "professionals.errorView.title")
      }
    }
    internal enum Recipes {
      /// Recetas
      internal static let title = L10n.tr("Localizable", "recipes.title")
      internal enum EmptyView {
        /// Aquí aparecerán las recetas que tu médico cree
        internal static let subtitle = L10n.tr("Localizable", "recipes.emptyView.subtitle")
        /// No hay recetas
        internal static let title = L10n.tr("Localizable", "recipes.emptyView.title")
      }
    }
    internal enum Reports {
      /// O. Diagnóstica
      internal static let diagnostic = L10n.tr("Localizable", "reports.diagnostic")
      /// INF
      internal static let rep = L10n.tr("Localizable", "reports.rep")
      /// Informes médicos
      internal static let title = L10n.tr("Localizable", "reports.title")
      internal enum Detail {
        /// Detalles del informe
        internal static let title = L10n.tr("Localizable", "reports.detail.title")
        internal enum Creation {
          /// Fecha de creación
          internal static let date = L10n.tr("Localizable", "reports.detail.creation.date")
          /// Hora de creación
          internal static let time = L10n.tr("Localizable", "reports.detail.creation.time")
        }
        internal enum Patient {
          /// Nombre del paciente
          internal static let name = L10n.tr("Localizable", "reports.detail.patient.name")
        }
        internal enum Professional {
          /// Nombre del profesional
          internal static let name = L10n.tr("Localizable", "reports.detail.professional.name")
        }
      }
      internal enum EmptyView {
        /// Aquí aparecerán los informes médicos que tu médico cree.
        internal static let subtitle = L10n.tr("Localizable", "reports.emptyView.subtitle")
        /// No hay informes
        internal static let title = L10n.tr("Localizable", "reports.emptyView.title")
      }
      internal enum Summary {
        /// Orientación diagnóstica
        internal static let diagnosticOrientation = L10n.tr("Localizable", "reports.summary.diagnosticOrientation")
        /// Motivo de consulta
        internal static let reasonConsultation = L10n.tr("Localizable", "reports.summary.reasonConsultation")
        /// Recomendaciones
        internal static let recommendations = L10n.tr("Localizable", "reports.summary.recommendations")
        /// Signos y síntomas manifestados
        internal static let signsAndSymptomsManifested = L10n.tr("Localizable", "reports.summary.signsAndSymptomsManifested")
      }
    }
    internal enum Shared {
      /// Aceptar
      internal static let accept = L10n.tr("Localizable", "shared.accept")
      /// Dirección
      internal static let address = L10n.tr("Localizable", "shared.address")
      /// Solicitud cancelada
      internal static let applicationCanceled = L10n.tr("Localizable", "shared.applicationCanceled")
      /// Fecha de nacimiento
      internal static let birthDate = L10n.tr("Localizable", "shared.birthDate")
      /// 
      internal static let blank = L10n.tr("Localizable", "shared.blank")
      /// LLAMANDO
      internal static let calling = L10n.tr("Localizable", "shared.calling")
      /// Cámara y micro desactivados
      internal static let cameraAndMicrophoneDeactivated = L10n.tr("Localizable", "shared.cameraAndMicrophoneDeactivated")
      /// Cámara desactivada
      internal static let cameraDeactivated = L10n.tr("Localizable", "shared.cameraDeactivated")
      /// Cancelar
      internal static let cancel = L10n.tr("Localizable", "shared.cancel")
      /// Consultas
      internal static let consultations = L10n.tr("Localizable", "shared.consultations")
      /// Continuar
      internal static let `continue` = L10n.tr("Localizable", "shared.continue")
      /// Copiar
      internal static let copy = L10n.tr("Localizable", "shared.copy")
      /// País
      internal static let country = L10n.tr("Localizable", "shared.country")
      /// Eliminar
      internal static let delete = L10n.tr("Localizable", "shared.delete")
      /// Hecho
      internal static let done = L10n.tr("Localizable", "shared.done")
      /// Email
      internal static let email = L10n.tr("Localizable", "shared.email")
      /// LLAMADA FINALIZADA
      internal static let endedCall = L10n.tr("Localizable", "shared.endedCall")
      /// Error
      internal static let error = L10n.tr("Localizable", "shared.Error")
      /// Vamos
      internal static let go = L10n.tr("Localizable", "shared.go")
      /// LLAMADA ENTRANTE
      internal static let incomingCall = L10n.tr("Localizable", "shared.incomingCall")
      /// VIDEOLLAMADA ENTRANTE
      internal static let incomingVideoCall = L10n.tr("Localizable", "shared.incomingVideoCall")
      /// % te está solicitando el cambio a videollamada
      internal static let isAskingYouChangeToVideoall = L10n.tr("Localizable", "shared.isAskingYouChangeToVideoall")
      /// Apellidos
      internal static let lastName = L10n.tr("Localizable", "shared.lastName")
      /// Hombre
      internal static let man = L10n.tr("Localizable", "shared.man")
      /// Micro desactivado
      internal static let microphoneDesactivated = L10n.tr("Localizable", "shared.microphoneDesactivated")
      /// Nombre
      internal static let name = L10n.tr("Localizable", "shared.name")
      /// Ok
      internal static let ok = L10n.tr("Localizable", "shared.Ok")
      /// Provincia
      internal static let provinde = L10n.tr("Localizable", "shared.provinde")
      /// Volviendo a conectar
      internal static let reconnecting = L10n.tr("Localizable", "shared.reconnecting")
      /// Rechazar
      internal static let refuse = L10n.tr("Localizable", "shared.refuse")
      /// LLAMADA RECHAZADA
      internal static let rejectedCall = L10n.tr("Localizable", "shared.rejectedCall")
      /// VIDEOLLAMADA RECHAZADA
      internal static let rejectedVideoCall = L10n.tr("Localizable", "shared.rejectedVideoCall")
      /// Solicitando el cambio a videollamada
      internal static let requestingChangeToVideoCall = L10n.tr("Localizable", "shared.requestingChangeToVideoCall")
      /// Saltar
      internal static let skip = L10n.tr("Localizable", "shared.skip")
      /// Altavoz activado
      internal static let speakerActivated = L10n.tr("Localizable", "shared.speakerActivated")
      /// Empezar
      internal static let start = L10n.tr("Localizable", "shared.start")
      /// VIDEOLLAMANDO
      internal static let videoCalling = L10n.tr("Localizable", "shared.videoCalling")
      /// Mujer
      internal static let woman = L10n.tr("Localizable", "shared.woman")
      internal enum Contact {
        /// Datos de contacto
        internal static let information = L10n.tr("Localizable", "shared.contact.information")
      }
      internal enum Docuement {
        /// Documento de identidad
        internal static let id = L10n.tr("Localizable", "shared.docuement.id")
      }
      internal enum Electronic {
        /// Receta electrónica
        internal static let prescription = L10n.tr("Localizable", "shared.electronic.prescription")
      }
      internal enum Medical {
        /// Ficha médica
        internal static let record = L10n.tr("Localizable", "shared.medical.record")
      }
      internal enum Personal {
        /// Datos personales
        internal static let data = L10n.tr("Localizable", "shared.personal.data")
      }
      internal enum Postal {
        /// CP
        internal static let code = L10n.tr("Localizable", "shared.postal.code")
      }
    }
    internal enum Videocall {
      internal enum Banner {
        /// Unirse
        internal static let join = L10n.tr("Localizable", "videocall.banner.join")
      }
      internal enum Call {
        internal enum In {
          internal enum Progress {
            /// %@ ha iniciado una llamada.
            internal static func message(_ p1: Any) -> String {
              return L10n.tr("Localizable", "videocall.call.in.progress.message", String(describing: p1))
            }
          }
        }
      }
      internal enum Popup {
        internal enum Busy {
          /// De acuerdo
          internal static let button = L10n.tr("Localizable", "videocall.popup.busy.button")
          /// Puedes intentar llamar a tu contacto en otro momento
          internal static let subtitle = L10n.tr("Localizable", "videocall.popup.busy.subtitle")
          /// El contacto está comunicando
          internal static let title = L10n.tr("Localizable", "videocall.popup.busy.title")
        }
      }
      internal enum Videocall {
        internal enum In {
          internal enum Progress {
            /// %@ ha iniciado una videollamada.
            internal static func message(_ p1: Any) -> String {
              return L10n.tr("Localizable", "videocall.videocall.in.progress.message", String(describing: p1))
            }
          }
        }
      }
    }
    internal enum Welclome {
      internal enum Generic {
        /// Completa tus datos para explorar la comunidad.
        internal static let subtitle = L10n.tr("Localizable", "welclome.generic.subtitle")
      }
    }
    internal enum Welcome {
      internal enum Generic {
        /// Bienvenido/a a la comunidad de mediQuo
        internal static let title = L10n.tr("Localizable", "welcome.generic.title")
      }
      internal enum Invite {
        /// Bienvenido/a a mi consulta en línea
        internal static let title = L10n.tr("Localizable", "welcome.invite.title")
        internal enum Pro {
          /// Gracias por aceptar la invitación de %@.\n\nRellene sus datos para acceder al chat.
          internal static func subtitle(_ p1: Any) -> String {
            return L10n.tr("Localizable", "welcome.invite.pro.subtitle", String(describing: p1))
          }
          internal enum From {
            internal enum Org {
              /// Gracias por conectar con %@ de %@.\n\nCompleta tus datos para acceder al chat.
              internal static func subtitle(_ p1: Any, _ p2: Any) -> String {
                return L10n.tr("Localizable", "welcome.invite.pro.from.org.subtitle", String(describing: p1), String(describing: p2))
              }
            }
          }
        }
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
