// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let camera = ImageAsset(name: "camera")
  internal static let document = ImageAsset(name: "document")
  internal static let gallery = ImageAsset(name: "gallery")
  internal static let arrow = ImageAsset(name: "Arrow")
  internal static let add = ImageAsset(name: "add")
  internal static let closeGray = ImageAsset(name: "closeGray")
  internal static let downArrow = ImageAsset(name: "downArrow")
  internal static let emptyList = ImageAsset(name: "emptyList")
  internal static let lock = ImageAsset(name: "lock")
  internal static let placeholder = ImageAsset(name: "placeholder")
  internal static let allergy = ImageAsset(name: "allergy")
  internal static let allergyIcon = ImageAsset(name: "allergyIcon")
  internal static let disease = ImageAsset(name: "disease")
  internal static let diseaseIcon = ImageAsset(name: "diseaseIcon")
  internal static let medicine = ImageAsset(name: "medicine")
  internal static let medicineIcon = ImageAsset(name: "medicineIcon")
  internal static let cameraIconLastMessage = ImageAsset(name: "cameraIconLastMessage")
  internal static let documentIconLastMessage = ImageAsset(name: "documentIconLastMessage")
  internal static let closeBlack = ImageAsset(name: "CloseBlack")
  internal static let failed = ImageAsset(name: "Failed")
  internal static let cameraProfessionalIconLastMessage = ImageAsset(name: "cameraProfessionalIconLastMessage")
  internal static let deleted = ImageAsset(name: "deleted")
  internal static let documentProfessionalIconLastMessage = ImageAsset(name: "documentProfessionalIconLastMessage")
  internal static let emptyGroups = ImageAsset(name: "emptyGroups")
  internal static let groupsDownloadImage = ImageAsset(name: "groupsDownloadImage")
  internal static let icCloseWhite = ImageAsset(name: "icCloseWhite")
  internal static let icCopy = ImageAsset(name: "icCopy")
  internal static let icErrorWifi = ImageAsset(name: "icErrorWifi")
  internal static let icErrorWifiWhite = ImageAsset(name: "icErrorWifiWhite")
  internal static let icLock = ImageAsset(name: "icLock")
  internal static let icNotificationsOff = ImageAsset(name: "icNotificationsOff")
  internal static let icOffline = ImageAsset(name: "icOffline")
  internal static let icReSend = ImageAsset(name: "icReSend")
  internal static let icTrash = ImageAsset(name: "icTrash")
  internal static let ic12ArrowDropdown = ImageAsset(name: "ic_12_arrow_dropdown")
  internal static let ic32Canceled = ImageAsset(name: "ic_32_canceled")
  internal static let ic32CitaExpirada = ImageAsset(name: "ic_32_cita_expirada")
  internal static let ic32CitaRealizada = ImageAsset(name: "ic_32_cita_realizada")
  internal static let ic32CitaRechazada = ImageAsset(name: "ic_32_cita_rechazada")
  internal static let ic32CitaRecibida = ImageAsset(name: "ic_32_cita_recibida")
  internal static let ic32CobroExpirado = ImageAsset(name: "ic_32_cobro_Expirado")
  internal static let ic32CobroDeclinado = ImageAsset(name: "ic_32_cobro_declinado")
  internal static let ic32CobroRealizado = ImageAsset(name: "ic_32_cobro_realizado")
  internal static let ic32CobroRecibido = ImageAsset(name: "ic_32_cobro_recibido")
  internal static let ic32ConsultaAceptada = ImageAsset(name: "ic_32_consulta_Aceptada")
  internal static let ic32PagoPendiente = ImageAsset(name: "ic_32_pago_pendiente")
  internal static let icActividadPendiente = ImageAsset(name: "ic_actividad_pendiente")
  internal static let icProfile = ImageAsset(name: "ic_profile")
  internal static let messagePendingNew = ImageAsset(name: "messagePendingNew")
  internal static let messageProfessionalPending = ImageAsset(name: "messageProfessionalPending")
  internal static let messageProfessionalRead = ImageAsset(name: "messageProfessionalRead")
  internal static let messageProfessionalSend = ImageAsset(name: "messageProfessionalSend")
  internal static let messageProfessionalUnread = ImageAsset(name: "messageProfessionalUnread")
  internal static let recipeIcon = ImageAsset(name: "recipeIcon")
  internal static let addReport = ImageAsset(name: "addReport")
  internal static let downloadReport = ImageAsset(name: "downloadReport")
  internal static let downloadReportShape = ImageAsset(name: "downloadReportShape")
  internal static let emptyListReport = ImageAsset(name: "emptyListReport")
  internal static let exitReport = ImageAsset(name: "exitReport")
  internal static let fileReport = ImageAsset(name: "fileReport")
  internal static let reportIcon = ImageAsset(name: "reportIcon")
  internal static let saveReport = ImageAsset(name: "saveReport")
  internal static let paperclip = ImageAsset(name: "Paperclip")
  internal static let messagePending = ImageAsset(name: "messagePending")
  internal static let messageRead = ImageAsset(name: "messageRead")
  internal static let messageReadNew = ImageAsset(name: "messageReadNew")
  internal static let messageSend = ImageAsset(name: "messageSend")
  internal static let messageSentNew = ImageAsset(name: "messageSentNew")
  internal static let messageUnread = ImageAsset(name: "messageUnread")
  internal static let messageUnreadNew = ImageAsset(name: "messageUnreadNew")
  internal static let sendIcon = ImageAsset(name: "sendIcon")
  internal static let call = ImageAsset(name: "call")
  internal static let videocall = ImageAsset(name: "videocall")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
