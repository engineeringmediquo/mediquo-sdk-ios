// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum MedicalHistorySection: StoryboardType {
    internal static let storyboardName = "MedicalHistorySection"

    internal static let initialScene = InitialSceneType<MediQuo_Base.MedicalHistorySectionViewController>(storyboard: MedicalHistorySection.self)

    internal static let medicalHistorySection = SceneType<MediQuo_Base.MedicalHistorySectionViewController>(storyboard: MedicalHistorySection.self, identifier: "MedicalHistorySection")

    internal static let medicalHistorySectionForm = SceneType<MediQuo_Base.MedicalHistorySectionFormViewController>(storyboard: MedicalHistorySection.self, identifier: "MedicalHistorySectionForm")
  }
  internal enum RecipesDetailPDF: StoryboardType {
    internal static let storyboardName = "RecipesDetailPDF"

    internal static let initialScene = InitialSceneType<MediQuo_Base.RecipesDetailPDFViewController>(storyboard: RecipesDetailPDF.self)

    internal static let recipesDetailPDF = SceneType<MediQuo_Base.RecipesDetailPDFViewController>(storyboard: RecipesDetailPDF.self, identifier: "RecipesDetailPDF")
  }
  internal enum RecipesEmptyList: StoryboardType {
    internal static let storyboardName = "RecipesEmptyList"

    internal static let initialScene = InitialSceneType<MediQuo_Base.RecipesEmptyListViewController>(storyboard: RecipesEmptyList.self)

    internal static let recipesEmptyList = SceneType<MediQuo_Base.RecipesEmptyListViewController>(storyboard: RecipesEmptyList.self, identifier: "RecipesEmptyList")
  }
  internal enum RecipesList: StoryboardType {
    internal static let storyboardName = "RecipesList"

    internal static let initialScene = InitialSceneType<MediQuo_Base.RecipesListViewController>(storyboard: RecipesList.self)

    internal static let recipesList = SceneType<MediQuo_Base.RecipesListViewController>(storyboard: RecipesList.self, identifier: "RecipesList")
  }
  internal enum ReportDetail: StoryboardType {
    internal static let storyboardName = "ReportDetail"

    internal static let initialScene = InitialSceneType<MediQuo_Base.ReportDetailViewController>(storyboard: ReportDetail.self)

    internal static let reportDetail = SceneType<MediQuo_Base.ReportDetailViewController>(storyboard: ReportDetail.self, identifier: "ReportDetail")
  }
  internal enum ReportDetailPDF: StoryboardType {
    internal static let storyboardName = "ReportDetailPDF"

    internal static let initialScene = InitialSceneType<MediQuo_Base.ReportDetailPDFViewController>(storyboard: ReportDetailPDF.self)

    internal static let reportDetailPDF = SceneType<MediQuo_Base.ReportDetailPDFViewController>(storyboard: ReportDetailPDF.self, identifier: "ReportDetailPDF")
  }
  internal enum ReportListEmpty: StoryboardType {
    internal static let storyboardName = "ReportListEmpty"

    internal static let initialScene = InitialSceneType<MediQuo_Base.ReportListEmptyViewController>(storyboard: ReportListEmpty.self)

    internal static let reportListEmptyViewController = SceneType<MediQuo_Base.ReportListEmptyViewController>(storyboard: ReportListEmpty.self, identifier: "ReportListEmptyViewController")
  }
  internal enum ReportsList: StoryboardType {
    internal static let storyboardName = "ReportsList"

    internal static let initialScene = InitialSceneType<MediQuo_Base.ReportsListViewController>(storyboard: ReportsList.self)

    internal static let reportsList = SceneType<MediQuo_Base.ReportsListViewController>(storyboard: ReportsList.self, identifier: "ReportsList")
  }
  internal enum Noscene: StoryboardType {
    internal static let storyboardName = "noscene"
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
