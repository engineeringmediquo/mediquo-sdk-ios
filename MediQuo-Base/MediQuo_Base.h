//
//  MediQuo_Base.h
//  MediQuo-Base
//
//  Created by David Martin on 22/07/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MediQuo_Base.
FOUNDATION_EXPORT double MediQuo_BaseVersionNumber;

//! Project version string for MediQuo_Base.
FOUNDATION_EXPORT const unsigned char MediQuo_BaseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MediQuo_Base/PublicHeader.h>


