//
//  InstallationManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import AdSupport
import CoreTelephony
import UIKit

class InstallationManager: InjectableComponent {
    @StorageDatasource
    private var uuidModel: UUIDModel?

    @Inject
    private var useCase: InstallationUseCase

    func install(completion: @escaping RemoteCompletionTypeAlias<InstallationResponse>) {
        self.useCase.install(request: self.buildRequest(), completion: completion)
    }

    private func buildRequest() -> InstallationRequest {
        let installationUUID = self.retrieveUUID()
        let languageCode = Locale.current.languageCode?.lowercased() ?? ""
        let countryCode = self.retrieveCountryCode()
        let platformName = "ios"
        let platformVersion = UIDevice.current.systemVersion
        let sdkVersion = self.retrieveSdkVersion()
        let deviceModel = UIDevice.current.model
        let advertising = ASIdentifierManager.shared()
        let idfa: UUID? = advertising.isAdvertisingTrackingEnabled ? advertising.advertisingIdentifier : nil
        let deviceId = idfa?.uuidString
        return InstallationRequest(installationUUID: installationUUID, languageCode: languageCode, countryCode: countryCode,
                                   platformName: platformName, platformVersion: platformVersion, sdkVersion: sdkVersion, deviceModel: deviceModel, deviceId: deviceId)
    }

    func retrieveUUID() -> String {
        guard let uuid: String = self.uuidModel?.uuid else {
            let uuid: String = UUID().uuidString
            self.uuidModel = UUIDModel(uuid: uuid)
            return uuid
        }
        return uuid
    }

    private func retrieveSdkVersion() -> String {
        return SDK.version.rawValue
    }

    private func retrieveCountryCode() -> String {
        guard let countryCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.isoCountryCode else {
            return NSLocale.current.identifier
        }
        return countryCode.lowercased()
    }
}

struct UUIDModel: Codable {
    let uuid: String
}
