//
//  InstallationRequest.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct InstallationRequest: Codable {
    private enum CodingKeys: String, CodingKey {
        case installationUUID = "installation_uuid"
        case languageCode = "language_code"
        case countryCode = "country_code"
        case platformName = "platform_name"
        case platformVersion = "platform_version"
        case sdkVersion = "sdk_version"
        case deviceModel = "device_model"
        case deviceId = "device_id"
    }

    let installationUUID: String
    let languageCode: String
    let countryCode: String
    let platformName: String
    let platformVersion: String
    let sdkVersion: String
    let deviceModel: String
    let deviceId: String?
}
