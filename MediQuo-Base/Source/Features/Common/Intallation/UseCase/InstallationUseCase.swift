//
//  InstallationUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class InstallationUseCase: InjectableComponent {

    @Inject
    private var repository: InstallationRepository

    func install(request: InstallationRequest, completion: @escaping RemoteCompletionTypeAlias<InstallationResponse>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.install(request: request) { result in
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }
    }
}
