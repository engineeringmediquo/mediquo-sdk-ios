//
//  InstallationRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class InstallationRepository: InjectableComponent {
    @StorageDatasource
    private var response: InstallationResponse?

    private var remote: RemoteManagerProtocol?

    func install(request: InstallationRequest, completion: @escaping RemoteCompletionTypeAlias<InstallationResponse>) {
        let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
        self.remote = RemoteManager(configuration)
        self.remote?.put(RemoteEndpoints.installations, parameters: request, completion: completion)
    }
}
