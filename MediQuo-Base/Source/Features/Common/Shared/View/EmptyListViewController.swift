//
//  EmptyListViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 22/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit

class EmptyListViewController: BaseViewControllerSDK {
    private var didSetupConstraints = false

    private var image: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "emptyGroups")
        return image
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.image)
        updateViewConstraints()
    }

    override func updateViewConstraints() {
        guard !self.didSetupConstraints else {
            super.updateViewConstraints()
            return
        }

        self.image.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalToSuperview()
        }

        self.didSetupConstraints = true

        super.updateViewConstraints()
    }
}
