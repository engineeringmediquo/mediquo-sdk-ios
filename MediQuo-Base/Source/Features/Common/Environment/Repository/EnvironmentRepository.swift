//
//  EnvironmentRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

enum EnvironmentType: String {
    case debug
    case release
    case testing
}

class EnvironmentRepository {
    @StorageDatasource
    private var model: EnvironmentModel?

    func set(environment: EnvironmentType) {
        self.model = EnvironmentModel(environment: environment.rawValue)
    }

    func get() -> EnvironmentType {
        #if TEST
        return .testing
        #else
        return EnvironmentType(rawValue: self.model?.environment ?? "") ?? .release
        #endif
    }
}
