//
//  EnvironmentManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class EnvironmentManager {

    private var repository: EnvironmentRepository = EnvironmentRepository()

    func set(environment: EnvironmentType) {
        self.repository.set(environment: environment)
    }

    func get() -> EnvironmentType {
        return self.repository.get()
    }
}
