//
//  EnvironmentModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public struct EnvironmentModel: Codable, Equatable {
    var environment: String
}
