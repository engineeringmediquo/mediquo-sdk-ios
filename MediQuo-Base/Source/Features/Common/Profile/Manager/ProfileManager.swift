//
//  ProfileManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfileManager: InjectableComponent {
    @StorageDatasource
    private var model: ProfileModel?

    @Inject
    private var useCase: ProfileUseCase

    // MARK: - Authenticate
    func fetch(completion: @escaping (RemoteCompletionTypeAlias<VoidResponse>)) {
        self.useCase.fetch { result in
            if case let .success(response) = result {
                self.model = response.data
                completion(.success(VoidResponse()))
            }
            if case let .failure(error) = result {
                CoreLog.remote.error("%@", error.description)
                completion(.failure(.remoteError(.reason(error.description))))
            }
        }
    }

    func getProfile() -> ProfileModel? {
        return self.model
    }
}
