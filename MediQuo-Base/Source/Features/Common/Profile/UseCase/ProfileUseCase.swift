//
//  ProfileUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfileUseCase: InjectableComponent {
    @Inject
    private var repository: ProfileRepository

    func fetch(completion: @escaping RemoteCompletionTypeAlias<DataResponse<ProfileModel>>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
