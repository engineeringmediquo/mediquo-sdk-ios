//
//  ProfileModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct ProfileModel: Codable {
    enum CodingKeys: String, CodingKey {
        case userHash = "hash"
        case name
    }

    let userHash: String?
    let name: String?
}
