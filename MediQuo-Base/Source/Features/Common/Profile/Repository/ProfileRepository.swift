//
//  ProfileRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfileRepository: InjectableComponent {
    private var remote: RemoteManagerProtocol?

    func fetch(completion: @escaping RemoteCompletionTypeAlias<DataResponse<ProfileModel>>) {
        let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
        self.remote = RemoteManager(configuration)
        self.remote?.get(RemoteEndpoints.profile, headers: nil, parameters: nil, completion: completion)
    }
}
