//
//  AuthRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

class AuthRepository: InjectableComponent {
    @StorageDatasource
    private var apiKeyModel: ApiKeyModel?

    @StorageDatasource
    private var clientCodeModel: ClientCodeModel?

    @StorageDatasource
    private var jwtModel: JwtModel?

    private var remote: RemoteManagerProtocol?

    func set(apiKey: String) {
        let model: ApiKeyModel = ApiKeyModel(apiKey: apiKey)
        self.apiKeyModel = model
    }

    func set(clientCode: String) {
        let model: ClientCodeModel = ClientCodeModel(clientCode: clientCode)
        self.clientCodeModel = model
    }

    func getApiKey() -> String? {
        return self.apiKeyModel?.apiKey
    }

    func getClientCode() -> String? {
        return self.clientCodeModel?.clientCode
    }

    func getJwt() -> String? {
        return self.jwtModel?.jwt
    }

    func getType() -> String? {
        return self.jwtModel?.type
    }

    func authenticate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        guard let clientCode: String = self.getClientCode() else {
            completion(.failure(.repositoryError(.reason("Not client code found."))))
            return
        }

        let request: AuthRequest = AuthRequest(code: clientCode)
        let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
        self.remote = RemoteManager(configuration)
        self.remote?.post(RemoteEndpoints.authenticate, parameters: request) { [weak self] (result: ResultTypeAlias<AuthResponse>) in
            guard let self = self else { return }
            if case let .success(response) = result {
                let jwt = response.accessToken
                let type = response.tokenType
                self.set(jwt: jwt, type: type)

                CoreLog.remote.debug("Auth params (TYPE + JWT): %@", [type, jwt].compactMap { $0 }.joined(separator: " "))

                completion(.success(VoidResponse()))
            }
            if case let .failure(error) = result {
                completion(.failure(.remoteError(.reason(error.description))))
            }
        }
    }

    func invalidate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
        self.remote = RemoteManager(configuration)
        self.remote?.post(RemoteEndpoints.invalidate, parameters: nil) { [weak self] (result: ResultTypeAlias<VoidResponse>) in
            guard let self = self else { return }
            if case .success = result {
                self.clear()
                completion(.success(VoidResponse()))
            }
            if case let .failure(error) = result {
                completion(.failure(.remoteError(.reason(error.description))))
            }
        }
    }
}

extension AuthRepository {
    private func set(jwt: String, type: String) {
        let model: JwtModel = JwtModel(jwt: jwt, type: type)
        self.jwtModel = model
    }

    private func clear() {
        self.apiKeyModel = nil
        self.clientCodeModel = nil
        self.jwtModel = nil
    }
}
