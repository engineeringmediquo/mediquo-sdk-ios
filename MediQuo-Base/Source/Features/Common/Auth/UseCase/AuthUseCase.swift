//
//  AuthUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class AuthUseCase: InjectableComponent {

    @Inject
    private var repository: AuthRepository

    func set(apiKey: String) {
        self.repository.set(apiKey: apiKey)
    }

    func set(clientCode: String) {
        self.repository.set(clientCode: clientCode)
    }

    func getApiKey() -> String? {
        return self.repository.getApiKey()
    }

    func getClientCode() -> String? {
        return self.repository.getClientCode()
    }

    func getJwt() -> String? {
        return self.repository.getJwt()
    }

    func getType() -> String? {
        return self.repository.getType()
    }

    // MARK: - Authenticate
    func authenticate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.authenticate { result in
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }
    }

    // MARK: - Invalidate
    func invalidate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.invalidate { result in
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }
    }
}
