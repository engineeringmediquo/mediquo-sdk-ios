//
//  AuthManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class AuthManager: InjectableComponent {

    @Inject
    private var useCase: AuthUseCase

    func set(apiKey: String) {
        self.useCase.set(apiKey: apiKey)
    }

    func set(clientCode: String) {
        self.useCase.set(clientCode: clientCode)
    }

    func getApiKey() -> String? {
        return self.useCase.getApiKey()
    }

    func getClientCode() -> String? {
        return self.useCase.getClientCode()
    }

    func getJwt() -> String? {
        return self.useCase.getJwt()
    }

    func getType() -> String? {
        return self.useCase.getType()
    }

    // MARK: - Authenticate
    func authenticate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        self.useCase.authenticate(completion: completion)
    }

    // MARK: - Invalidate
    func invalidate(completion: @escaping RemoteCompletionTypeAlias<VoidResponse>) {
        self.useCase.invalidate(completion: completion)
    }
}
