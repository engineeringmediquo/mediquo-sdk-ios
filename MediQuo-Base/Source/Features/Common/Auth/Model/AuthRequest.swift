//
//  LoginRequest.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct AuthRequest: Codable {
    private enum CodingKeys: String, CodingKey {
        case code
    }

    let code: String
}
