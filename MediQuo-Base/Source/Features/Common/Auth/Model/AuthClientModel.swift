//
//  AuthClientModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 2/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct ApiKeyModel: Codable {
    var apiKey: String
}

struct ClientCodeModel: Codable {
    var clientCode: String
}
