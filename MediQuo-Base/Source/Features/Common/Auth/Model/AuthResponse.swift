//
//  AuthResponse.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
    }

    let accessToken: String
    let tokenType: String
}
