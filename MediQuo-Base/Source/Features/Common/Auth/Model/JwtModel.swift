//
//  JwtModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

struct JwtModel: Codable {
    var jwt: String?
    var type: String?
}
