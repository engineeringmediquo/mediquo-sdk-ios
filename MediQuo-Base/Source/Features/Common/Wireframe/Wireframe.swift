//
//  Wireframe.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

class Wireframe {
    enum WireframeType {
        case chat(String,
                  Bool,
                  MediquoChatManager,
                  FetchRoomUseCase,
                  ChatControllerUseCase,
                  ProfessionalSDKModel?,
                  String,
                  String)
        case profile(String)
        case medicalHistory
        case allergies
        case allergiesForm(Int?, String?)
        case diseases
        case diseasesForm(Int?, String?)
        case medications
        case medicationsForm(Int?, String?)
        case reports
        case reportDetail(String)
        case reportPDF(Data?, ReportModel?)
        case recipes
        case recipePDF(Data?, RecipeModel?)
    }

    // swiftlint:disable cyclomatic_complexity
    // swiftlint:disable function_body_length
    static func navigate(to option: WireframeType,
                         from navigationController: UINavigationController?) {
        switch option {
            case .chat(let userHash,
                       let isFromDeeplink,
                       let socket,
                       let fetchRoomUseCase,
                       let chatUseCase,
                       let professionalModel,
                       let currentUserHash,
                       let currentUserName):
                
                let viewController: ChatRoomViewController = ChatRoomViewController()
                viewController.viewModel = DefaultChatRoomViewModel(socket: socket,
                                                                    professionalModel: professionalModel,
                                                                    currentUserHash: currentUserHash,
                                                                    currentUserName:currentUserName,
                                                                    fetchRoomUseCase: fetchRoomUseCase,
                                                                    chatUseCase: chatUseCase,
                                                                    professionalUserHash: userHash)
             
                let animated = !isFromDeeplink
                navigationController?.pushViewController(viewController, animated: animated)
                
            case .profile(let userHash):
                let viewController: ProfessionalProfileViewController = ProfessionalProfileViewController()
                // viewController.userHash = userHash
                navigationController?.pushViewController(viewController, animated: true)
            case .medicalHistory:
                break
            case .allergies:
                let viewController: MedicalHistorySectionViewController = MediQuoSDK.getAllergies()
                navigationController?.pushViewController(viewController, animated: true)
            case let .allergiesForm(id, name):
                let viewController: MedicalHistorySectionFormViewController = MediQuoSDK.getAllergyForm()
                viewController.id = id
                viewController.name = name
                navigationController?.pushViewController(viewController, animated: true)
            case .diseases:
                let viewController: MedicalHistorySectionViewController = MediQuoSDK.getDiseases()
                navigationController?.pushViewController(viewController, animated: true)
            case let .diseasesForm(id, name):
                let viewController: MedicalHistorySectionFormViewController = MediQuoSDK.getDiseaseForm()
                viewController.id = id
                viewController.name = name
                navigationController?.pushViewController(viewController, animated: true)
            case .medications:
                let viewController: MedicalHistorySectionViewController = MediQuoSDK.getMedications()
                navigationController?.pushViewController(viewController, animated: true)
            case let .medicationsForm(id, name):
                let viewController: MedicalHistorySectionFormViewController = MediQuoSDK.getMedicationForm()
                viewController.id = id
                viewController.name = name
                navigationController?.pushViewController(viewController, animated: true)
            case .reports:
                let viewController: ReportsListViewController = MediQuoSDK.getReports()
                navigationController?.pushViewController(viewController, animated: true)
            case let .reportDetail(uuid):
                let viewController: ReportDetailViewController = MediQuoSDK.getReportDetail(uuid)
                navigationController?.pushViewController(viewController, animated: true)
            case let .reportPDF(data, reportModel):
                let viewController: ReportDetailPDFViewController = MediQuoSDK.getReportDetailPDF(data: data, with: reportModel)
                navigationController?.pushViewController(viewController, animated: true)
            case .recipes:
                let viewController: RecipesListViewController = MediQuoSDK.getRecipes()
                navigationController?.pushViewController(viewController, animated: true)
            case let .recipePDF(data, recipeModel):
                let viewController: RecipesDetailPDFViewController = MediQuoSDK.getRecipeDetailPDF(data: data, with: recipeModel)
                navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
