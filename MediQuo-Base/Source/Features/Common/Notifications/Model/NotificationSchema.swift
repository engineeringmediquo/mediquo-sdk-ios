//
//  NotificationSchema.swift
//  MediQuo-Base
//
//  Created by David Martin on 12/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct NotificationSchema: Codable {
    enum CodingKeys: String, CodingKey {
        case roomId = "room_id"
        case avatar = "image"
        case module
        case type
        case message
        case title
        case body
    }

    let roomId: Int?
    let avatar: String?
    let module: String?
    let type: String?
    let message: String?
    let title: String?
    let body: String?

    init(roomId: Int?) {
        self.roomId = roomId
        self.avatar = nil
        self.module = nil
        self.type = nil
        self.message = nil
        self.title = nil
        self.body = nil
    }

    var notificationType: NotificationType {
        return NotificationType(rawValue: self.type ?? "") ?? .unknown
    }
}
