//
//  NotificationType.swift
//  MediQuo-Base
//
//  Created by David Martin on 16/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

enum NotificationType: String {
    case messageCreated = "message_created"
    case callRequested = "call_requested"
    case callRejected = "call_rejected"
    case unknown
}
