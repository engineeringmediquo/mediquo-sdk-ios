//
//  RegisterFirebaseRequest.swift
//  MediQuo-Base
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct RegisterFirebaseRequest: Codable {
    private enum CodingKeys: String, CodingKey {
        case token
        case platform
        case deviceId = "device_id"
    }

    public let token: String
    public let platform: String = "ios"
    public let deviceId: String

    public init(token: String, deviceId: String) {
        self.token = token
        self.deviceId = deviceId
    }
}
