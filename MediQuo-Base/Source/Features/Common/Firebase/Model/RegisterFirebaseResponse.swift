//
//  RegisterFirebaseResponse.swift
//  MediQuo-Base
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct RegisterFirebaseResponse: Codable, Equatable {
    public enum CodingKeys: String, CodingKey {
        case message
    }

    public let message: String?
}
