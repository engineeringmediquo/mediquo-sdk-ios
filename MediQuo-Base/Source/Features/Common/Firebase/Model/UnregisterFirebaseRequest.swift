//
//  UnregisterFirebaseRequest.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/10/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct UnregisterFirebaseRequest {
    public let token: String

    public init(token: String) {
        self.token = token
    }
}
