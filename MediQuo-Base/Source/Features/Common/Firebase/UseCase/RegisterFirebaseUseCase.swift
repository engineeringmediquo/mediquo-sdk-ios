//
//  RegisterFirebaseUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class RegisterFirebaseUseCase: InjectableComponent {

    @Inject
    private var repository: RegisterFirebaseRepository

    func register(request: RegisterFirebaseRequest, completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        self.repository.register(request: request, completion: completion)
    }

    func unregister(completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        self.repository.unregister(completion: completion)
    }
}
