//
//  RegisterFirebaseManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class RegisterFirebaseManager: InjectableComponent {

    @Inject
    private var useCase: RegisterFirebaseUseCase

    func register(request: RegisterFirebaseRequest, completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        self.useCase.register(request: request, completion: completion)
    }

    func unregister(completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        self.useCase.unregister(completion: completion)
    }
}
