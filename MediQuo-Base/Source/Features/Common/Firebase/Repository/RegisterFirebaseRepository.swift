//
//  RegisterFirebaseRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 9/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

protocol RegisterFirebaseRepositoryProtocol: InjectableComponent {
    func register(request: RegisterFirebaseRequest, completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>)
    func unregister(completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>)
}

class RegisterFirebaseRepository: InjectableComponent, RegisterFirebaseRepositoryProtocol {
    private var remote: RemoteManagerProtocol

    @StorageDatasource
    private var model: RegisterFirebaseRequest?

    init(remote: RemoteManagerProtocol) {
        self.remote = remote
    }

    func register(request: RegisterFirebaseRequest, completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        self.model = request
        self.remote.put(RemoteEndpoints.registerFirebase, parameters: request, completion: completion)
    }

    func unregister(completion: @escaping RemoteCompletionTypeAlias<RegisterFirebaseResponse>) {
        guard let token = self.model?.token else {
            completion(.failure(BaseError.repositoryError(.localStorageUpToDate)))
            return
        }
        let endpoint = RemoteEndpoints.unregisterFirebase.replacingOccurrences(of: "{token}", with: token)
        self.remote.put(endpoint, parameters: self.model, completion: completion)
    }
}
