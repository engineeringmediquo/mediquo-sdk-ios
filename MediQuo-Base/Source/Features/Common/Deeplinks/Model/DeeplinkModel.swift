//
//  DeeplinkModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 8/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct DeeplinkModel: Codable {
    var type: DeeplinkType?

    enum DeeplinkType: Codable {
        case chat(Int)

        private enum CodingKeys: String, CodingKey {
            case chat
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            if let value = try? container.decode(Int.self, forKey: .chat) {
                self = .chat(value)
                return
            }
            self = .chat(-1)
        }

        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            switch self {
            case .chat(let roomId):
                try container.encode(roomId, forKey: .chat)
            }
        }
    }
}
