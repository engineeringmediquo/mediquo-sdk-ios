//
//  ProfessionalsListCell.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SDWebImage
import SnapKit

class ProfessionalsListCell: UITableViewCell {
    static let identifier: String = "professionalsList"

    // MARK: Global Stack View
    private let globalHStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 16
        return stackView
    }()

    // MARK: Left Avatar Image View
    private let avatarImageView = UIImageView()
    private let statusView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 2
        return view
    }()

    // MARK: Center Vertical Stack View
    private let centerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorName.ultraDarkGray.color
        label.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        return label
    }()
    private let specialityLabel: UILabel = {
        let label = UILabel()
        label.textColor = MediQuoSDK.instance.style?.secondaryColor
        label.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        return label
    }()
    private let lastMessageLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorName.ultraDarkGray.color
        label.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        return label
    }()

    // MARK: Right Vertical Stack View
    private let rightStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    private let rightView = UIView()
    private let updateDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorName.gray.color
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textAlignment = .right
        return label
    }()
    private let lockImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.lock.image
        imageView.isHidden = true
        return imageView
    }()
    private let pendingMessagesView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = MediQuoSDK.instance.style?.accentColor
        return view
    }()
    private let pendingMessagesLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        label.textAlignment = .center
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.centerStackView.addArrangedSubview(self.nameLabel)
        self.centerStackView.addArrangedSubview(self.specialityLabel)
        self.centerStackView.addArrangedSubview(self.lastMessageLabel)

        self.pendingMessagesView.addSubview(self.pendingMessagesLabel)
        self.rightView.addSubview(self.updateDateLabel)
        self.rightView.addSubview(self.lockImageView)
        self.rightView.addSubview(self.pendingMessagesView)

        self.globalHStackView.addArrangedSubview(self.avatarImageView)
        self.globalHStackView.addArrangedSubview(self.centerStackView)
        self.globalHStackView.addArrangedSubview(self.rightView)

        self.globalHStackView.addSubview(self.statusView)
        self.contentView.addSubview(self.globalHStackView)

        self.setNeedsUpdateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.avatarImageView.rounded()
        self.statusView.rounded()
        self.pendingMessagesView.rounded()
    }

    override func updateConstraints() {
        self.updateGlobalStackViewConstraints()
        self.updateAvatarConstraints()
        self.updateCenterStackViewConstraints()
        self.updateRightStackViewConstraints()

        super.updateConstraints()
    }

    func configure(_ model: ProfessionalModel) {
        self.selectionStyle = .default

        self.contentView.backgroundColor = .white
        self.statusView.backgroundColor = model.status?.color
        self.nameLabel.text = model.name
        self.specialityLabel.text = model.title

        if let avatarUrl = URL(string: model.avatar ?? "") {
            self.avatarImageView.sd_setImage(with: avatarUrl)
        }

        guard let room = model.room else { return }
        self.fill(room.lastMessage)
        self.fill(room.lastMessage?.timestamp)
        self.fill(model.isAvailable ?? false)
        self.fill(room.pendingMessages)
    }
}

// MARK: - Update constraints

extension ProfessionalsListCell {
    private func updateGlobalStackViewConstraints() {
        self.globalHStackView.snp.makeConstraints { make in
            make.center.equalTo(self.contentView)
            make.height.equalTo(self.contentView).offset(-32)
            make.width.equalTo(self.contentView).offset(-32)
        }
    }

    private func updateAvatarConstraints() {
        self.avatarImageView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView.snp.centerY)
            make.leading.equalTo(self.globalHStackView.snp.leading)
            make.size.equalTo(CGSize(width: 48, height: 48))
        }
        self.statusView.snp.makeConstraints { make in
            make.top.equalTo(self.avatarImageView.snp.top)
            make.trailing.equalTo(self.avatarImageView.snp.trailing)
            make.size.equalTo(CGSize(width: 14, height: 14))
        }
    }

    private func updateCenterStackViewConstraints() {
        self.centerStackView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView.snp.centerY)
            make.height.equalTo(self.globalHStackView.snp.height)
        }
        self.nameLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView.snp.centerX)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView.snp.width)
        }
        self.specialityLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView.snp.centerX)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView.snp.width)
        }
        self.lastMessageLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView.snp.centerX)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView.snp.width)
        }
    }

    private func updateRightStackViewConstraints() {
        self.rightView.snp.makeConstraints { make in
            make.trailing.equalTo(self.globalHStackView.snp.trailing)
            make.height.equalTo(self.globalHStackView.snp.height)
            make.centerY.equalTo(self.globalHStackView.snp.centerY)
            make.width.equalTo(self.pendingMessagesView.snp.width)
        }
        self.updateDateLabel.snp.makeConstraints { make in
            make.top.equalTo(self.rightView.snp.top)
            make.trailing.equalTo(self.rightView.snp.trailing)
            make.height.equalTo(20)
        }
        self.lockImageView.snp.makeConstraints { make in
            make.top.equalTo(self.updateDateLabel.snp.bottom)
            make.centerX.equalTo(self.rightView.snp.centerX)
        }
        self.pendingMessagesView.snp.makeConstraints { make in
            make.top.equalTo(self.lockImageView.snp.bottom)
            make.centerX.equalTo(self.rightView.snp.centerX)
            make.size.equalTo(CGSize(width: 20, height: 20))
        }
        self.pendingMessagesLabel.snp.makeConstraints { make in
            make.center.equalTo(self.pendingMessagesView)
        }
    }
}

// MARK: - Fill cell info

extension ProfessionalsListCell {
    private func fill(_ lastMessage: LastMessageModel?) {
        guard let lastMessage = lastMessage else {
            self.lastMessageLabel.attributedText = nil
            return
        }

        let imageStatus = NSTextAttachment()
        imageStatus.image = lastMessage.status.image
        imageStatus.bounds = CGRect(x: 0, y: -2, width: 16, height: 16)

        let attributedString = NSMutableAttributedString()

        switch lastMessage.type {
        case .text:
            attributedString.append(NSAttributedString(attachment: imageStatus))
            attributedString.append(NSAttributedString(string: " \(lastMessage.string)"))
        case .image:
            let imageMeta = NSTextAttachment()
            imageMeta.image = Asset.cameraIconLastMessage.image
            imageMeta.bounds = CGRect(x: 2, y: 0, width: 12, height: 12)

            attributedString.append(NSAttributedString(attachment: imageStatus))
            attributedString.append(NSAttributedString(attachment: imageMeta))
            attributedString.append(NSAttributedString(string: " \(L10n.Localizable.image)"))
        case .file:
            let imageMeta = NSTextAttachment()
            imageMeta.image = Asset.documentIconLastMessage.image
            imageMeta.bounds = CGRect(x: 2, y: 0, width: 12, height: 12)

            attributedString.append(NSAttributedString(attachment: imageStatus))
            attributedString.append(NSAttributedString(attachment: imageMeta))
            attributedString.append(NSAttributedString(string: " \(L10n.Localizable.document)"))
        default: break
        }
        self.lastMessageLabel.attributedText = attributedString
    }

    private func fill(_ updateTime: Date?) {
        guard let date = updateTime else {
            self.updateDateLabel.text = nil
            return
        }
        self.updateDateLabel.text = DateManager.convertToStringForInbox(date)
    }

    private func fill(_ isAvailable: Bool = false) {
        self.lockImageView.isHidden = isAvailable
    }

    private func fill(_ pendingMessages: Int?) {
        guard let pendingMessages = pendingMessages, pendingMessages > 0 else {
            self.pendingMessagesView.isHidden = true
            self.pendingMessagesLabel.text = nil
            self.updateDateLabel.textColor = ColorName.gray.color
            return
        }
        self.pendingMessagesView.isHidden = false
        self.pendingMessagesLabel.text = pendingMessages.description
        self.updateDateLabel.textColor = MediQuoSDK.instance.style?.accentColor
    }
}
