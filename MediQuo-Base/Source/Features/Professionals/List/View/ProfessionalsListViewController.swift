//
//  ProfessionalsListViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit
import UIKit

public class ProfessionalsListViewController: BaseViewControllerSDK {
    
    @Inject
    var viewModel: ProfessionalsListViewModel
    
    private let mediQuoStyle: MediQuoStyleType? = MediQuoSDK.instance.style
    private weak var delegate: ProfessionalsListDelegate? = MediQuoSDK.instance.style?.professionalsListDelegate

    private var didSetupConstraints = false
    private var activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    
    private var currentUnreadMessagesCount: Int?
    private var isFromDeeplink: Bool = false
    private var refreshControl: UIRefreshControl?
    
    private(set) var professionalListSDKView: ProfessionalListSDKView = {
        let professionalListSDKView = ProfessionalListSDKView(frame: .zero)
        return professionalListSDKView
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.initDelegate()
        self.initUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.bindSocketEvents()
        bindViewModelsObservables()
        fetch()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unBindViewModelsObservables()
    }

    override func bindNotifications() {
        super.bindNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetch), name: Notification.Name.Push.messageReceived, object: nil)

        if #available(iOS 13.0, *) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToForeground), name: UIScene.willEnterForegroundNotification, object: nil)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        }
    }

    private func bindViewModelsObservables() {
        self.viewModel.professionals.subscribe { [weak self] professionals in
            guard let self = self,
                  let professionals = professionals else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.updateView(from: professionals)
                self.executePendingDeeplink()
                self.delegate?.onListLoaded()
            }
        }
        
        self.viewModel.professional.subscribe { [weak self] model in
            guard let self = self,
                  let model = model else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if self.executeOnProfessionalClick(model: model) {
                    self.navigateTo(model: model)
                }
            }
        }
        
        self.viewModel.badgeCount.subscribe { [weak self] count in
            guard let self = self, let count = count else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if let currentUnreadMessagesCount = self.currentUnreadMessagesCount {
                    if currentUnreadMessagesCount != count {
                        self.delegate?.onUnreadMessage(countChange: count)
                    }
                } else {
                    self.delegate?.onUnreadMessage(countChange: count)
                }

                self.currentUnreadMessagesCount = count
                UIApplication.shared.applicationIconBadgeNumber = count
            }
        }
    }

    func unBindViewModelsObservables() {
        self.viewModel.professionals.value = nil
        self.viewModel.professional.value = nil
        self.viewModel.badgeCount.value = nil
    }

    public override func updateViewConstraints() {
        guard !self.didSetupConstraints else {
            super.updateViewConstraints()
            return
        }
        self.professionalListSDKView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.didSetupConstraints = true
        super.updateViewConstraints()
    }
}

// MARK: - Private Functions
extension ProfessionalsListViewController {
    
    @objc private func fetch() {
        self.activityIndicatorView.startAnimating()
        self.viewModel.fetch()
    }

    @objc private func refresh() {
        refreshControl?.beginRefreshing()
        self.fetch()
    }

    private func initDelegate() {
        self.delegate?.viewController = self
    }

    private func initUI() {
        self.initTitle()
        self.initRightBarButtons()
        self.initRefreshControl()
        self.setupProfessionalListSDKView()
        self.initActivityIndicatorView()
    }

    private func initTitle() {
        self.title = L10n.Localizable.consultations.capitalized
    }

    private func initRightBarButtons() {
        self.navigationItem.rightBarButtonItem = self.delegate?.getRightBarButton()
    }

    private func initActivityIndicatorView() {
        self.activityIndicatorView.color = self.mediQuoStyle?.accentColor
        self.activityIndicatorView.center = CGPoint(x: view.center.x, y: view.center.y)
        self.activityIndicatorView.startAnimating()
        self.view.addSubview(self.activityIndicatorView)
    }

    private func initRefreshControl() {
        refreshControl = professionalListSDKView.getRefreshControl()
        refreshControl?.tintColor = self.mediQuoStyle?.accentColor
        refreshControl?.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
    }

    private func setupProfessionalListSDKView() {
        professionalListSDKView.navigationDelegate = self
        self.view.addSubview(professionalListSDKView)
        self.view.setNeedsUpdateConstraints()
    }

    private func toogleTableViewBackground() {
        guard let models = self.viewModel.professionals.value,
              !models.isEmpty else {
            professionalListSDKView.setBackgroundViewVisibility(isHidden: false)
            return
        }
        professionalListSDKView.setBackgroundViewVisibility(isHidden: true)
    }

    private func updateView(from professionals: [ProfessionalSDKModel]) {
        self.activityIndicatorView.stopAnimating()
        self.professionalListSDKView.reloadData(from: professionals,
                                                and: [0 : ""],
                                                with: false)
    }

    @objc private func appMovedToForeground() {
        self.fetch()
    }
}

extension ProfessionalsListViewController: ProfessionalListSDKViewNavigationDelegate {
    public func navigateTo(section: Int, position: Int) {
        viewModel.navigateTo(position: position)
    }
    
    private func navigateTo(model: ProfessionalSDKModel) {
        mainCoordinator?.showChatRoomView(professionalUserHash: model.hash,
                                          isFromDeeplink: false,
                                          socket: viewModel.socket,
                                          professionalModel: model,
                                          chatRoomNavigationDelegate: mainCoordinator,
                                          chatRoomAppointmentDelegate: nil)
    }

    private func executeOnProfessionalClick(model: ProfessionalSDKModel) -> Bool {
        guard let hasAccess = model.isAvailable,
              let professionalId = model.id,
              let specialityId = model.speciality?.id,
              let delegate = self.delegate else {
            return false
        }
        return delegate.onProfessionalClick(professionalId: professionalId,
                                            specialityId: specialityId,
                                            hasAccess: hasAccess)
    }
}

// MARK: - Push Notifications Delegate
extension ProfessionalsListViewController: PushNotificationsActionsDelegate {
    func setNotificationPresentation(by roomId: Int, completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([])
    }

    func execute(deeplink: NotificationSchema) {
        self.isFromDeeplink = true
        self.viewModel.executeDeeplink(deeplink: deeplink)
    }

    private func executePendingDeeplink() {
        self.isFromDeeplink = true
        self.viewModel.executePendingDeeplink()
    }
}
