//
//  ProfessionalsListViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation
import mediquo_videocall_lib

class ProfessionalsListViewModel: InjectableComponent & BaseViewModel {
    
    var socket: MediQuoSDKChatManager?
    private let authManager: AuthManager = AuthManager()
    private var profileManager: ProfileManager = ProfileManager()
    private let videoCallManager: VideoCallManagerProtocol? = MediQuoSDK.instance.videoCallManager

    @Inject
    private var useCase: ProfessionalsListUseCase

    // Cache
    private var chatCacheManager: MediquoChatCacheManager? = DefaultMediquoChatCacheManager()
    private var callRejectedCount: Int = 0
    
    var professionals: MediQuoObservable<[ProfessionalSDKModel]?> = MediQuoObservable(nil)
    var professional: MediQuoObservable<ProfessionalSDKModel?> = MediQuoObservable(nil)
    var badgeCount: MediQuoObservable<Int?> = MediQuoObservable(nil)

    init() {
        guard let builder = MediQuoSDKChatManagerBuilder.getSDKBuilder() else { return }
        self.socket = MediQuoSDKChatManager(builder: builder)
    }

    func fetch() {
        self.useCase.fetch { [weak self] result in
            guard let self = self else { return }
            if case let .success(professionals) = result {
                self.professionals.value = self.mapToProfessionalSDKModel(from: professionals.data)
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
        self.useCase.getPendingMessagesCount { [weak self] result in
            guard let self = self else { return }
            if case let .success(count) = result {
                self.badgeCount.value = count
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }

    func navigateTo(position: Int) {
        guard let models = self.professionals.value,
              let model = models[safe: position] else { return }
        self.professional.value = model
    }

    // MARK: - Execute deeplink across deeplink schema (Alived app)
    func executeDeeplink(deeplink: NotificationSchema) {
        if let position = self.professionals.value?.firstIndex(where: { $0.room?.id == deeplink.roomId }) {
            self.navigateTo(position: position)
        }
    }

    // MARK: - Execute deeplink accross deeplink model stored (Dead app)
    func executePendingDeeplink() {
        if let deeplinkModel: DeeplinkModel = MediQuoSDK.instance.deeplinkModel,
         let type: DeeplinkModel.DeeplinkType = deeplinkModel.type {
            if case let .chat(roomId) = type,
                let position = self.professionals.value?.firstIndex(where: { $0.room?.id == roomId }) {
                self.navigateTo(position: position)
                MediQuoSDK.instance.deeplinkModel = nil
            }
        }
    }
}

// MARK: - Mapper
extension ProfessionalsListViewModel {
    private func mapToProfessionalSDKModel(from professionals: [ProfessionalModel]) -> [ProfessionalSDKModel] {
        return professionals.map { professional in
            return ProfessionalSDKModel(id: professional.id,
                                        hash: professional.hash,
                                        avatar: professional.avatar,
                                        avatarURL: URL(string: professional.avatar ?? ""),
                                        name: professional.name,
                                        title: professional.title,
                                        overview: professional.overview,
                                        collegiateNumber: professional.collegiateNumber,
                                        isAvailable: professional.isAvailable,
                                        specialityId: professional.specialityId,
                                        lastMessageTimestamp: getLastMessageTimestampFromCache(from: professional) ?? professional.room?.lastMessage?.timestamp.timeIntervalSince1970,
                                        pending: professional.room?.pendingMessages ?? 0,
                                        typeLastMessage: getTypeLastMessageFromCache(from: professional) ?? professional.room?.lastMessage?.type.rawValue,
                                        fromUserHash: professional.hash,
                                        stringLastMessage: getLastMessageFromCache(from: professional) ?? professional.room?.lastMessage?.string,
                                        currentAppointmentType: nil,
                                        currentAppointmentStatus: nil,
                                        statusLastMessage: getLastMessageStatusFromCache(from: professional) ?? professional.room?.lastMessage?.status.rawValue,
                                        isOwnMessage: ProfileManager().getProfile()?.userHash == professional.room?.lastMessage?.fromUserHash ?? "",
                                        status: SDKStatus(rawValue: professional.status?.rawValue ?? ""),
                                        room: getProfessionalRoom(from: professional),
                                        speciality: SDKSpecialityModel(id: professional.speciality?.id ?? 0,
                                                                       name: professional.speciality?.name,
                                                                       code: professional.speciality?.code),
                                        license: SDKLicenseModel(organizationName: professional.license?.organization?.name,
                                                                 organizationColor: professional.license?.organization?.color,
                                                                 organizationBackgroundColor: professional.license?.organization?.backgroundColor),
                                        channel: nil)
        }
    }
    
    private func getProfessionalRoom(from professional: ProfessionalModel) -> SDKRoomModel? {
        if let room = professional.room {
            return SDKRoomModel(id: room.id,
                                roomId: room.id,
                                lastMessage: room.lastMessage,
                                pendingMessages: room.pendingMessages,
                                status: SDKRoomModel.RoomSDKStatus(rawValue: room.status?.rawValue ?? ""),
                                title: professional.room?.title,
                                avatar: professional.room?.avatar,
                                meta: professional.room?.meta,
                                currentCall: professional.room?.currentCall)
        } else {
            return nil
        }
    }
    
    private func getLastMessageFromCache(from model: ProfessionalModel) -> String? {
        let messagesPendingToSentFromCache = chatCacheManager?.getLastMessagesPendingToSentFromCache(toUserHash: model.hash,
                                                                                                     myUserHash: self.profileManager.getProfile()?.userHash ?? "") ?? []
        
        if messagesPendingToSentFromCache.isEmpty {
            // No hay mensajes en pendientes en caché
            return nil
        } else {
            // Hay mensajes en pendientes en caché
            guard let metadata = messagesPendingToSentFromCache.last?.metadata else {return ""}
            switch metadata {
                case .text(content: let content):
                    return content
                case .image(url: let url, thumb:_, width:_, height:_, size:_):
                    return url?.absoluteString
                case .file(name: let name, url:_, size:_):
                    return name
                case .none:
                    return ""
            }
        }
    }
    
    private func getTypeLastMessageFromCache(from model: ProfessionalModel) -> String? {
        let messagesPendingToSentFromCache = chatCacheManager?.getLastMessagesPendingToSentFromCache(toUserHash: model.hash,
                                                                                                     myUserHash: self.profileManager.getProfile()?.userHash ?? "") ?? []
        
        if messagesPendingToSentFromCache.isEmpty {
            // No hay mensajes en pendientes en caché
            return nil
        } else {
            // Hay mensajes en pendientes en caché
            guard let metadata = messagesPendingToSentFromCache.last?.metadata else {return ""}
            switch metadata {
                case .text(content:_):
                    return "string"
                case .image(url: _, thumb:_, width:_, height:_, size:_):
                    return "image"
                case .file(name:_, url:_, size:_):
                    return "file"
                case .none:
                    return ""
            }
        }
    }
    
    private func getLastMessageStatusFromCache(from model: ProfessionalModel) -> Int? {
        let messagesPendingToSentFromCache = chatCacheManager?.getLastMessagesPendingToSentFromCache(toUserHash: model.hash,
                                                                                                     myUserHash: self.profileManager.getProfile()?.userHash ?? "") ?? []
        return messagesPendingToSentFromCache.isEmpty ? nil : messagesPendingToSentFromCache.last?.status.rawValue
    }
    
    private func getLastMessageTimestampFromCache(from model: ProfessionalModel) -> TimeInterval? {
        let messagesPendingToSentFromCache = chatCacheManager?.getLastMessagesPendingToSentFromCache(toUserHash: model.hash,
                                                                                                     myUserHash: self.profileManager.getProfile()?.userHash ?? "") ?? []
        return messagesPendingToSentFromCache.isEmpty ? nil : messagesPendingToSentFromCache.last?.timestamp
    }
}

// MARK: - Socket management

extension ProfessionalsListViewModel {
    func bindSocketEvents() {
        self.unBindSocket()
        self.socket?.onVideoCallEvents()
        
        self.socket?.videoCallRequested.subscribe { model in
            DispatchQueue.main.async { [weak self] in
                guard let self = self,
                      let schema: VideoCallNotificationSchema = model,
                       !self.isVideoCallMainSceneAsCurrentScene() else { return }

                MediQuoSDK.instance.isVideoCallSocketInitialized = true
                guard let viewController = self.videoCallManager?.showIncomingCall(by: .patient, by: schema) else { return }
                viewController.modalPresentationStyle = .fullScreen
                UIApplication.topViewController()?.present(viewController, animated: true)
            }
        }

        self.socket?.videoCallRejected.subscribe { model in
            guard let schema: VideoCallNotificationSchema = model else { return }
            MediQuoSDK.instance.isVideoCallSocketInitialized = false
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.videoCallManager?.rejectIncomingCall(by: .patient, by: schema) { _ in
                    weak var pvc = UIApplication.topViewController()?.presentingViewController
                    self.callRejectedCount += 1
                    if self.callRejectedCount == 1 {
                        pvc?.dismiss(animated: true)
                    }
                }
            }
        }

        self.socket?.videoCallPickedUp.subscribe { model in
            guard let schema: VideoCallNotificationSchema = model,
                      schema.type == .pickedUp,
                     !MediQuoSDK.instance.isVideoCallSocketInitialized else { return }
            DispatchQueue.main.async { 
                weak var pvc = UIApplication.topViewController()?.presentingViewController
                pvc?.dismiss(animated: true, completion: nil)
            }
        }
    }

    func unBindSocket() {
        self.socket?.videoCallRequested.value = nil
        self.socket?.videoCallRejected.value = nil
        self.socket?.videoCallPickedUp.value = nil
    }

    private func isVideoCallMainSceneAsCurrentScene() -> Bool {
        return UIApplication.topViewController() is mediquo_videocall_lib.MainViewController
    }
}
