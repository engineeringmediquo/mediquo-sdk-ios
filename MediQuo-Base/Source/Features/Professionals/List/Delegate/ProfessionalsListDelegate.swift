//
//  ProfessionalsListDelegate.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

public protocol ProfessionalsListDelegate: AnyObject {
    // Self instance of view controller that contains a professionals list
    var viewController: BaseViewControllerSDK? { get set }

    // Implementation of right bar button in the professionals list view
    func getRightBarButton() -> UIBarButtonItem

    // This callback is fired when the professionals list is loaded
    func onListLoaded()

    // This callback is fired when press in any professional, for navigate to room
    func onProfessionalClick(professionalId: Int, specialityId: Int, hasAccess: Bool) -> Bool

    // This callback is fired when the number of unread messages has changed
    func onUnreadMessage(countChange: Int)
}
