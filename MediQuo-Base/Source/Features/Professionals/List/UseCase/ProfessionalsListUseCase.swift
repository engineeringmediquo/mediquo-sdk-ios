//
//  ProfessionalsListUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalsListUseCase: InjectableComponent {
    @Inject
    private var repository: ProfessionalsListRepository

    func fetch(completion: @escaping RemoteCompletionTypeAlias<DataResponse<[ProfessionalModel]>>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func getPendingMessagesCount(completion: @escaping RemoteCompletionTypeAlias<Int>) {
        DispatchQueue.global(qos: .background).async {
            self.repository.getPendingMessagesCount { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
