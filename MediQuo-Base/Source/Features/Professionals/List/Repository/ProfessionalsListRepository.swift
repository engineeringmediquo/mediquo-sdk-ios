//
//  ProfessionalsListRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalsListRepository: InjectableComponent {
    private var remote: RemoteManagerProtocol

    init(remote: RemoteManagerProtocol) {
        self.remote = remote
    }

    func fetch(completion: @escaping RemoteCompletionTypeAlias<DataResponse<[ProfessionalModel]>>) {
        self.remote.get(RemoteEndpoints.professionals, headers: nil, parameters: nil, completion: completion)
    }

    func getPendingMessagesCount(completion: @escaping RemoteCompletionTypeAlias<Int>) {
        self.remote.get(RemoteEndpoints.unreadMessages, headers: nil, parameters: nil) { (result: ResultTypeAlias<MetaResponse<UnreadMessagesModel>>) in
            if case let .success(response) = result {
                completion(.success(response.meta.total))
            }
            if case let .failure(error) = result {
                completion(.failure(.remoteError(.reason(error.description))))
            }
        }
    }
}
