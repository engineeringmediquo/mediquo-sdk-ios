//
//  ProfessionalsListManager.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalsListManager: InjectableComponent {

    @Inject
    private var useCase: ProfessionalsListUseCase

    func getPendingMessagesCount(completion: @escaping RemoteCompletionTypeAlias<Int>) {
        self.useCase.getPendingMessagesCount(completion: completion)
    }
}
