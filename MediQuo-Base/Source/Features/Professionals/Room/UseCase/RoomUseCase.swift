//
//  RoomUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public protocol FetchRoomUseCase {
    func fetchRoom<T: Any>(by roomId: Int, completion: @escaping (Result<T?, BaseError>) -> Void)
    func fetchRoomForSDK(by roomId: Int, completion: @escaping (Result<RoomModel?, BaseError>) -> Void)
}

public protocol FetchRoomMessagesUseCase {
    func fetchMessages(by roomId: Int,
                       pivotId: String?,
                       mode: MessageModel.PivotMode?,
                       limit: Int,
                       jwt: String,
                       completion: @escaping (Result<[MessageApiModel], BaseError>) -> Void)
}

public protocol ReadRoomUseCase {
    func read(by roomId: String)
}

public protocol ChatControllerUseCase {
    func receive(message: String, professionalHash: String)
    func read(room id: Int)
    func delete(message id: String)
}

public class RoomUseCase: FetchRoomUseCase, ReadRoomUseCase, ChatControllerUseCase, FetchRoomMessagesUseCase {
    
    // TODO: Cane - Arreglar esta inyección de dependencia de alguna manera
    private var repository: RoomRepository?

    public init() {
        self.repository = RoomRepository()
    }
    
    public func fetchRoom<T>(by roomId: Int, completion: @escaping (Result<T?, BaseError>) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.repository?.fetch(by: String(roomId)) { response in
                DispatchQueue.main.async {
                    switch response {
                        case .success(let model):
                            guard let roomModel = model as? T else { return }
                            completion(.success(roomModel))
                        case .failure(let error):
                            completion(.failure(error))
                    }
                }
            }
        }
    }
    
    public func fetchRoomForSDK(by roomId: Int, completion: @escaping (Result<RoomModel?, BaseError>) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.repository?.fetch(by: String(roomId)) { response in
                DispatchQueue.main.async {
                    switch response {
                        case .success(let response):
                            completion(.success(response.data))
                        case .failure(let error):
                            completion(.failure(error))
                    }
                }
            }
        }
    }
    
    public func read(by roomId: String) {
        self.repository?.read(by: roomId)
    }
    
    public func fetchMessages(by roomId: Int,
                              pivotId: String?,
                              mode: MessageModel.PivotMode?,
                              limit: Int,
                              jwt: String,
                              completion: @escaping (Result<[MessageApiModel], BaseError>) -> Void) {
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            self.repository?.fetchMessages(by: String(roomId),
                                           params: FetchMessagesRepositoryParams(direction: mode?.rawValue ?? 1,
                                                                                 limit: limit,
                                                                                 pivot_message_id: pivotId),
                                           jwt: jwt,
                                           completion: { response in
                switch response {
                    case .success(let model):
                       completion(.success(model.data))
                    case .failure(let error):
                        completion(.failure(error))
                }
            })
        }
    }
    
    
    // TODO: Cane - Solucionar estos métodos vacíos, desacoplando las dependencias
    public func read(room id: Int) {}
    
    public func delete(message id: String) {}
    
    public func receive(message: String, professionalHash: String) {}
}


