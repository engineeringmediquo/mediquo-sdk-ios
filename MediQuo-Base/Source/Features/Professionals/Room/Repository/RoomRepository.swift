//
//  RoomRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation
import Alamofire

struct FetchMessagesRepositoryParams: Codable {
    public enum CodingKeys: String, CodingKey {
        case direction
        case limit
        case pivot_message_id
    }
    
    let direction: Int
    let limit: Int
    let pivot_message_id: String?
}

class RoomRepository {
    private var remote: RemoteManagerProtocol?
    private var remoteChatApi: RemoteManagerProtocol?

    init(remote: RemoteManagerProtocol = RemoteManager(),
         remoteChatApi: RemoteManagerProtocol? = RemoteManager(RemoteConfiguration(baseUrl: RemoteBasesUrl.chatApi))) {
        self.remote = remote
        self.remoteChatApi = remoteChatApi
    }

    func fetch(by roomId: String, completion: @escaping RemoteCompletionTypeAlias<DataResponse<RoomModel>>) {
        let endpoint: String = RemoteEndpoints.room.replacingOccurrences(of: "{roomId}", with: roomId)
        self.remote?.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }
    
    func fetchMessages(by roomId: String,
                       params: FetchMessagesRepositoryParams,
                       jwt: String,
                       completion: @escaping RemoteCompletionTypeAlias<DataResponse<[MessageApiModel]>>) {

        let endpoint: String = RemoteEndpoints.messages.replacingOccurrences(of: "{roomId}", with: roomId)
        var headers: HTTPHeaders = HTTPHeaders()
        headers[Header.contentType] = Header.applicationJson
        headers[Header.authorization] = "Bearer \(jwt)"

        self.remoteChatApi?.get(endpoint,
                                headers: headers,
                                parameters: params,
                                completion: completion)
    }

    func read(by roomId: String) {
        let endpoint: String = RemoteEndpoints.read.replacingOccurrences(of: "{roomId}", with: roomId)
        self.remote?.put(endpoint, parameters: nil) { (result: ResultTypeAlias<VoidResponse>) in
            if case .success = result {
                CoreLog.remote.debug("Room %@ marked as read", roomId)
            }
            if case let .failure(error) = result {
                CoreLog.remote.error("%@", error.description)
            }
        }
    }
}
