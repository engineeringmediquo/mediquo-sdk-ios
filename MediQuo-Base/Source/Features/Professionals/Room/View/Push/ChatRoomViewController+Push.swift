//
//  ChatRoomViewController+Push.swift
//  MediQuo-Base
//
//  Created by David Martin on 8/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UserNotifications

// MARK: - Push Notifications Delegate

extension ChatRoomViewController: PushNotificationsActionsDelegate {
    func setNotificationPresentation(by roomId: Int, completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if roomId == self.viewModel?.roomId {
            completionHandler([])
        } else {
            completionHandler([.alert, .badge, .sound])
        }
    }

    func execute(deeplink: NotificationSchema) {
        if deeplink.roomId != self.viewModel?.roomId {
            if let viewController = self.navigationController?.back(to: ProfessionalsListViewController.self) as? ProfessionalsListViewController {
                viewController.execute(deeplink: deeplink)
            }
        }
    }
}


