// Copyright © 2022 Medipremium S.L. All rights reserved.


import Lottie
import SnapKit
// import SwiftDate

// swiftlint:disable file_length
public protocol AppointmentDelegate: AnyObject {
    func didTapAction(by appointmentId: String, status: AppointmentSDKModel.StatusType)
}

extension AppointmentBanner {
    private enum Localizables {
        // Pending
        static let chargePendingStatusTitle = Localizable.string("mediquo.charge.pending.title").uppercased()
        static let chargePendingStatusSubTitle = Localizable.string("mediquo.charge.pending.subtitle")
        // AppointmentPay
        static let appointmentPay = Localizable.string("mediquo.appointment.pay").uppercased()
        // Declined
        static let chargeDeclinedTitle = Localizable.string("mediquo.charge.declined.title").uppercased()
        static let chargeDeclinedSubTitle = Localizable.string("mediquo.charge.declined.subtitle")
        // Canceled
        static let chargeCanceledTitle = Localizable.string("mediquo.charge.cancelled.title").uppercased()
        static let chargeCanceledSubTitle = Localizable.string("mediquo.charge.cancelled.subtitle")
        // Expired
        static let chargeExpiredTitle = Localizable.string("mediquo.charge.expired.title").uppercased()
        static let chargeExpiredSubTitle = Localizable.string("mediquo.charge.expired.subtitle")
        // Accepted
        static let chargeAcceptedTitle = Localizable.string("mediquo.charge.accepted.title").uppercased()
        static let chargeAcceptedSubTitle = Localizable.string("mediquo.charge.accepted.subtitle")
        // AppointmentInProgress
        static let appointmentInProgressTitle = Localizable.string("mediquo.appointment.in.progress.title").uppercased()
        static let appointmentInProgressSubTitle = Localizable.string("mediquo.appointment.in.progress.subtitle")
        // AppointmentFinished
        static let appointmentFinishedTitle = Localizable.string("mediquo.appointment.finished.title").uppercased()
        static let appointmentFinishedSubTitle = Localizable.string("mediquo.appointment.how.was.label")
        // AppointmentScheduled
        static let appointmentScheduledTitle = Localizable.string("mediquo.appointment.scheduled").uppercased()
        // Confirm
        static let confirmTitle = Localizable.string("mediquo.shared.confirm").uppercased()
        // YouHaveAnAppointment
        static let youHaveAnAppointmentTitle = Localizable.string("mediquo.you.have.an.appointment.label").uppercased()
        // MediquoSee
        static let mediquoSeeTitle = Localizable.string("mediquo.see").uppercased()
        // MediquoAppointmentToday
        static let mediquoAppointmentToday = Localizable.string("mediquo.appointment.today")
        // MediquoStatusBarAppointmentDate
        static let mediquoStatusBarAppointmentDate = Localizable.string("mediquo.status.bar.appointment.date")
        // MediquoAppoinmentReceivedProposalReview
        static let mediquoAppoinmentReceivedProposalReview = Localizable.string("mediquo.appoinment.received.proposal.review")
        // MediquoDeclinedConsultationTitle
        static let mediquoDeclinedConsultationTitle = Localizable.string("mediquo.declined.consultation.title").uppercased()
        // MediquoDeclinedConsultationSubtitle
        static let mediquoDeclinedConsultationSubtitle = Localizable.string("mediquo.declined.consultation.subtitle")
        // MediquoCancelAppointmentTitle
        static let mediquoCancelAppointmentTitle = Localizable.string("mediquo.cancel.appointment.title").uppercased()
        // MediquoCancelAppointmentSubtitle
        static let mediquoCancelAppointmentSubtitle = Localizable.string("mediquo.cancel.appointment.subtitle")
        // MediquoAppointmentExpiredTitle
        static let mediquoAppointmentExpiredTitle = Localizable.string("mediquo.appointment.expired.title").uppercased()
        // MediquoAppointmentExpiredSubtitle
        static let mediquoAppointmentExpiredSubtitle = Localizable.string("mediquo.appointment.expired.subtitle")
        // MediquoAppointmentPendingTitle
        static let mediquoAppointmentPendingTitle = Localizable.string("mediquo.appointment.pending.title").uppercased()
        // MediquoAppointmentUnpaidSubtitle
        static let mediquoAppointmentUnpaidSubtitle = Localizable.string("mediquo.appointment.unpaid.subtitle")
    }
}

class AppointmentBanner: UIView {
        
    private let globalHStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        return stackView
    }()

    private let leftView: UIView = {
        let view = UIView()
        view.backgroundColor = .primary()
        return view
    }()

    private let centerView: UIView = {
        let view = UIView()
        view.backgroundColor = .primary()
        return view
    }()

    private let rightView: UIView = {
        let view = UIView()
        view.backgroundColor = .primary()
        return view
    }()

    private let animationView: AnimationView = {
        let view = AnimationView()
        view.contentMode = .scaleAspectFit
        view.loopMode = .loop
        return view
    }()

    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = .primary()
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        return label
    }()

    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(hex: "#E7E3F1FF") ?? .gray3()
        button.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        button.setTitleColor(.primary(), for: .normal)
        button.layer.cornerRadius = 8
        button.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        return button
    }()

    // swiftlint:disable:this weak_delegate
    weak var delegate: AppointmentDelegate?

    private var appointment: AppointmentSDKModel? {
        didSet {
            self.clearBanner()
            self.fillByType()
        }
    }

    var width: CGFloat = 390 {
        didSet {
            self.setNeedsUpdateConstraints()
        }
    }

    // MARK: View lifeCycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.actionButton.addTarget(self, action: #selector(self.didTapActionButton), for: .touchUpInside)
        self.animationView.addSubview(self.iconImage)
        self.leftView.addSubview(self.animationView)

        self.centerView.addSubview(self.titleLabel)
        self.centerView.addSubview(self.subtitleLabel)

        self.rightView.addSubview(self.actionButton)

        self.globalHStackView.addArrangedSubview(self.leftView)
        self.globalHStackView.addArrangedSubview(self.centerView)
        self.globalHStackView.addArrangedSubview(self.rightView)

        self.addSubview(self.globalHStackView)

        self.setNeedsUpdateConstraints()
    }

    func configure(_ appointment: AppointmentSDKModel?) {
        self.appointment = appointment
    }

    private func fillByType() {
        guard let type = self.appointment?.type else { return }
        switch type {
        case .past:
            self.configureChargeType()
        case .future:
            self.configurePaymentType()
        }
    }

    @objc private func didTapActionButton(sender _: UIButton!) {
        guard let delegate = self.delegate,
             let appointmentId = self.appointment?.id,
             let status = self.appointment?.status else { return }
        
        delegate.didTapAction(by: appointmentId, status: status)

        if status == .pending {
            let dataDict: [String: String] = [Notification.Key.Appointments.id: appointmentId]
            NotificationCenter.default.post(name: Notification.Appointments.SeeButtonBannerClick, object: nil, userInfo: dataDict)
        }
    }
}

// MARK: - Charge status

extension AppointmentBanner {
    private func configureChargeType() {
        guard let status = self.appointment?.status else { return }
        switch status {
            case .pending:
                self.configureChargePendingStatus()
            case .declined:
                self.configureChargeDeclinedStatus()
            case .cancelled:
                self.configureChargeCancelledStatus()
            case .expired:
                self.configureChargeExpiredStatus()
            case .finished:
                self.configureChargeFinishedStatus()
            default:
                print("No such appointment status")
        }
    }

    private func clearBanner() {
        self.stopAnimation()

        self.leftView.backgroundColor = .clear
        self.centerView.backgroundColor = .clear
        self.rightView.backgroundColor = .clear

        self.iconImage.image = nil

        self.titleLabel.textColor = .clear
        self.titleLabel.text?.removeAll()

        self.subtitleLabel.textColor = .clear
        self.subtitleLabel.text?.removeAll()

        self.actionButton.backgroundColor = .clear
        self.actionButton.contentHorizontalAlignment = .center
        self.actionButton.setAttributedTitle(nil, for: .normal)
        self.actionButton.setImage(nil, for: .normal)
    }

    private func configureChargePendingStatus() {
        self.leftView.backgroundColor = .lightBlue()
        self.centerView.backgroundColor = .lightBlue()
        self.rightView.backgroundColor = .lightBlue()
        self.iconImage.image = Asset.ic32CobroRecibido.image
        self.titleLabel.textColor = .darkBlue()
        self.titleLabel.text = Localizables.chargePendingStatusTitle

        self.subtitleLabel.textColor = .darkBlue()
        self.subtitleLabel.text = Localizables.chargePendingStatusSubTitle

        self.actionButton.backgroundColor = .darkBlue()
        self.actionButton.contentHorizontalAlignment = .center
        self.actionButton.setTitleColor(.white, for: .normal)
        self.actionButton.setAttributedTitle(NSAttributedString(string: Localizables.appointmentPay,
                                                                attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)

        self.actionButton.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(self.rightView).offset(-16)
        }
    }

    private func configureChargeDeclinedStatus() {
        self.leftView.backgroundColor = .darkRed()
        self.centerView.backgroundColor = .darkRed()
        self.rightView.backgroundColor = .darkRed()

        self.iconImage.image = Asset.ic32CobroDeclinado.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.chargeDeclinedTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.chargeDeclinedSubTitle

        self.actionButton.backgroundColor = .darkRed()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configureChargeCancelledStatus() {
        self.leftView.backgroundColor = .darkRed()
        self.centerView.backgroundColor = .darkRed()
        self.rightView.backgroundColor = .darkRed()

        self.iconImage.image = Asset.ic32Canceled.image
        self.iconImage.tintColor = .darkRed()

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.chargeCanceledTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.chargeCanceledSubTitle

        self.actionButton.backgroundColor = .darkRed()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configureChargeExpiredStatus() {
        self.leftView.backgroundColor = .darkOrange()
        self.centerView.backgroundColor = .darkOrange()
        self.rightView.backgroundColor = .darkOrange()

        self.iconImage.image = Asset.ic32CobroExpirado.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.chargeExpiredTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.chargeExpiredSubTitle
        self.actionButton.backgroundColor = .darkOrange()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configureChargeFinishedStatus() {
        self.leftView.backgroundColor = .darkGreen()
        self.centerView.backgroundColor = .darkGreen()
        self.rightView.backgroundColor = .darkGreen()

        self.iconImage.image = Asset.ic32CobroRealizado.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.chargeAcceptedTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.chargeAcceptedSubTitle
        self.actionButton.backgroundColor = .darkGreen()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }
}

// MARK: - Payment status

extension AppointmentBanner {
    private func configurePaymentType() {
        guard let status = self.appointment?.status else { return }
        switch status {
            case .pending:
                self.configurePaymentPendingStatus()
            case .accepted:
                self.configurePaymentAcceptedStatus()
            case .unpaid:
                self.configurePaymentUnpaidStatus()
            case .declined:
                self.configurePaymentDeclinedStatus()
            case .cancelled:
                self.configurePaymentCancelledStatus()
            case .expired:
                self.configurePaymentExpiredStatus()
            case .finished, .freeOfCharge:
                self.configurePaymentFinishedStatus()
            default:
                break
        }
    }

    private func configurePaymentPendingStatus() {
        guard let startDate = self.appointment?.startDate,
              let startDateDate = DateManager.convertToDateFromRemoteISO(startDate) else { return }
        self.leftView.backgroundColor = .lightBlue()
        self.centerView.backgroundColor = .lightBlue()
        self.rightView.backgroundColor = .lightBlue()

        self.iconImage.image = Asset.ic32CitaRecibida.image

        let isToTwoHoursRange = self.isToTwoHoursRange(from: startDateDate)
        let isBetweenTwoHoursToNextDay = self.isBetweenTwoHoursToNextDay(from: startDateDate)

        if isToTwoHoursRange {
            self.initAndStartAnimation()
            self.titleLabel.text = Localizables.appointmentInProgressTitle
            self.subtitleLabel.text = Localizables.appointmentInProgressSubTitle
        } else if isBetweenTwoHoursToNextDay {
            self.initAndStartAnimation()
            self.titleLabel.text = Localizables.appointmentFinishedTitle
            self.subtitleLabel.text = Localizables.appointmentFinishedSubTitle
        } else {
            self.titleLabel.text = Localizables.appointmentScheduledTitle
            self.subtitleLabel.text = self.formatSubtitle(startDate: startDate)
        }

        self.titleLabel.textColor = .primary()
        self.subtitleLabel.textColor = .primary()

        self.actionButton.backgroundColor = .primary()
        self.actionButton.contentHorizontalAlignment = .center
        self.actionButton.setTitleColor(.white, for: .normal)
        self.actionButton.setAttributedTitle(NSAttributedString(string:Localizables.confirmTitle,
                                                                attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.snp.makeConstraints { make in
            make.width.equalTo(100)
        }
    }

    private func configurePaymentAcceptedStatus() {
        guard let startDate = self.appointment?.startDate,
              let startDateDate = DateManager.convertToDateFromRemoteISO(startDate) else { return }
        
        self.leftView.backgroundColor = .primary()
        self.centerView.backgroundColor = .primary()
        self.rightView.backgroundColor = .primary()

        self.iconImage.image = Asset.ic32ConsultaAceptada.image

        let isToTwoHoursRange = self.isToTwoHoursRange(from: startDateDate)
        let isBetweenTwoHoursToNextDay = self.isBetweenTwoHoursToNextDay(from: startDateDate)
        
        // TODO: Cane - Revisar si esta va fino ;)
        let isToday = Calendar.current.isDateInToday(startDateDate) // startDateDate.isToday

        if isToTwoHoursRange {
            self.initAndStartAnimation()
            self.titleLabel.text = Localizables.appointmentInProgressTitle
            self.subtitleLabel.text = Localizables.appointmentInProgressSubTitle
        } else if isBetweenTwoHoursToNextDay {
            self.initAndStartAnimation()
            self.titleLabel.text = Localizables.appointmentFinishedTitle
            self.subtitleLabel.text = Localizables.appointmentFinishedSubTitle
        } else if isToday {
            self.initAndStartAnimation()
            self.titleLabel.text = Localizables.youHaveAnAppointmentTitle
            self.subtitleLabel.text = self.formatSubtitle(startDate: startDate)
        } else {
            self.titleLabel.text = Localizables.appointmentScheduledTitle
            self.subtitleLabel.text = self.formatSubtitle(startDate: startDate)
        }

        self.titleLabel.textColor = .white
        self.subtitleLabel.textColor = .white

        self.actionButton.backgroundColor = UIColor(hex: "#E7E3F1FF") ?? .gray3()
        self.actionButton.contentHorizontalAlignment = .center
        self.actionButton.setTitleColor(.primary(), for: .normal)
        self.actionButton.setAttributedTitle(NSAttributedString(string:Localizables.mediquoSeeTitle,
                                                                attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(self.rightView).offset(-36)
        }
    }

    private func formatSubtitle(startDate: String?) -> String {
        if let startDate = startDate,
            let date = DateManager.convertToDateFromRemoteISO(startDate) {
            let formater = DateFormatter()
            formater.setLocalizedDateFormatFromTemplate("d MMMM yyyy")
            let formatedDate = formater.string(from: date)

            formater.setLocalizedDateFormatFromTemplate("HH:mm")
            let formatedHour = formater.string(from: date)
            return Calendar.current.isDateInToday(date) ? mediquoAppointmentToday(formatedHour: formatedHour) : mediquoStatusBarAppointmentDate(formatedDate: formatedDate, formatedHour: formatedHour)
        } else {
            return Localizables.mediquoAppoinmentReceivedProposalReview
        }
    }
    
    private func mediquoAppointmentToday(formatedHour: String) -> String {
        return Localizables.mediquoAppointmentToday.replacingOccurrences(of: "%@", with: formatedHour)
    }
    
    private func mediquoStatusBarAppointmentDate(formatedDate: String, formatedHour: String) -> String {
        var result = Localizables.mediquoStatusBarAppointmentDate.replacingOccurrences(of: "%@date", with: formatedDate)
        result = result.replacingOccurrences(of: "%@hour", with: formatedHour)
        return result
    }

    private func configurePaymentDeclinedStatus() {
        self.leftView.backgroundColor = .darkRed()
        self.centerView.backgroundColor = .darkRed()
        self.rightView.backgroundColor = .darkRed()

        self.iconImage.image = Asset.ic32CitaRechazada.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.mediquoDeclinedConsultationTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.mediquoDeclinedConsultationSubtitle

        self.actionButton.backgroundColor = .darkRed()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configurePaymentCancelledStatus() {
        self.leftView.backgroundColor = .darkRed()
        self.centerView.backgroundColor = .darkRed()
        self.rightView.backgroundColor = .darkRed()

        self.iconImage.image = Asset.ic32Canceled.image
        self.iconImage.tintColor = .darkRed()

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.mediquoCancelAppointmentTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.mediquoCancelAppointmentSubtitle

        self.actionButton.backgroundColor = .darkRed()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configurePaymentExpiredStatus() {
        self.leftView.backgroundColor = .darkOrange()
        self.centerView.backgroundColor = .darkOrange()
        self.rightView.backgroundColor = .darkOrange()

        self.iconImage.image = Asset.ic32CitaExpirada.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.mediquoAppointmentExpiredTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.mediquoAppointmentExpiredSubtitle
        self.actionButton.backgroundColor = .darkOrange()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configurePaymentFinishedStatus() {
        self.leftView.backgroundColor = .darkGreen()
        self.centerView.backgroundColor = .darkGreen()
        self.rightView.backgroundColor = .darkGreen()

        self.iconImage.image = Asset.ic32CitaRealizada.image

        self.titleLabel.textColor = .white
        self.titleLabel.text = Localizables.appointmentFinishedTitle
        self.subtitleLabel.textColor = .white
        self.subtitleLabel.text = Localizables.appointmentFinishedSubTitle
        self.actionButton.backgroundColor = .darkGreen()
        self.actionButton.contentHorizontalAlignment = .right
        self.actionButton.setAttributedTitle(NSAttributedString(string: "", attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
        self.actionButton.setImage(Asset.closeBlack.image, for: .normal)
        self.actionButton.tintColor = .white
    }

    private func configurePaymentUnpaidStatus() {
        self.leftView.backgroundColor = .lightRed()
        self.centerView.backgroundColor = .lightRed()
        self.rightView.backgroundColor = .lightRed()

        self.iconImage.image = Asset.ic32PagoPendiente.image

        self.titleLabel.textColor = .darkRed()
        self.titleLabel.text = Localizables.mediquoAppointmentPendingTitle
        self.subtitleLabel.textColor = .darkRed()
        self.subtitleLabel.text = Localizables.mediquoAppointmentUnpaidSubtitle
        self.actionButton.backgroundColor = .white
        self.actionButton.contentHorizontalAlignment = .center
        self.actionButton.setTitleColor(.primary(), for: .normal)
        self.actionButton.setAttributedTitle(NSAttributedString(string: Localizables.appointmentPay,
                                                                attributes: [NSAttributedString.Key.kern: 1.11]), for: .normal)
    }
}

// MARK: - Animation
extension AppointmentBanner {
    private func initAndStartAnimation() {
        guard let path = self.url?.path else { return }
        self.animationView.animation = Animation.filepath(path)
        self.animationView.play()
    }

    private func stopAnimation() {
        self.animationView.stop()
    }

    private var url: URL? {
        let bundle = Bundle(for: PackageClass.self)
        return bundle.urls(forResourcesWithExtension: "json", subdirectory: nil)?.first(where: { $0.absoluteString.contains("todayPayment") })
    }
}

// MARK: - Time management
extension AppointmentBanner {
    private func isToTwoHoursRange(from date: Date) -> Bool {
        return DateManager.calculateDateRangeUpToHours(date: date, upTo: 2)
    }

    private func isBetweenTwoHoursToNextDay(from date: Date) -> Bool {
        return DateManager.calculateDateRangeBetweenHours(date: date, fromUpTo: 2, toUpTo: 24)
    }
}

private class PackageClass {}

// MARK: - Constraints
extension AppointmentBanner {
    override func updateConstraints() {
        self.globalHStackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: self.width, height: 68))
        }
        
        self.leftView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView)
            make.size.equalTo(CGSize(width: 64, height: 68))
        }
        
        self.centerView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView)
            make.size.equalTo(CGSize(width: self.width - 64 - 80, height: 68))
        }
        
        self.rightView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView)
            make.size.equalTo(CGSize(width: 80, height: 68))
        }

        self.animationView.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 64, height: 68))
            make.center.equalTo(self.leftView)
        }
        
        self.iconImage.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 32, height: 32))
            make.center.equalTo(self.animationView)
        }

        self.titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.centerView).offset(-5)
            make.trailing.equalTo(self.centerView)
            make.height.equalTo(16)
            make.centerY.equalTo(self.centerView).offset(-10)
        }
        
        self.subtitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(self.centerView).offset(-5)
            make.trailing.equalTo(self.centerView)
            make.height.equalTo(16)
            make.centerY.equalTo(self.centerView).offset(10)
        }

        self.actionButton.snp.makeConstraints { make in
            make.centerY.equalTo(self.rightView)
            make.height.equalTo(32)
            make.width.greaterThanOrEqualTo(self.rightView)
            make.trailing.equalTo(self.rightView).offset(-8)
        }

        super.updateConstraints()
    }
}


