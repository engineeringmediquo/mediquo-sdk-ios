//
//  ChatRoomConstants.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

enum ScrollDirectionType {
    case down
    case up
    case unknown
}

enum ChatConstants {
    static let firstPosition = 0
    static var threshold: Int {
        return Int((Double(numberOfMessagesForFirstFetch) * 0.25).rounded(.down))
    }
    static let scrollButtonThreshold = 10
    static let numberOfMessagesForFirstFetch = 25
    static let numberOfMessagesForPagination = 50
}

enum MessagesFetchType {
    case initial
    case onScroll
}
