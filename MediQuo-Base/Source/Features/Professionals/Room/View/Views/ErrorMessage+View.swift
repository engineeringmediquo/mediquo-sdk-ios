//
//  ErrorMessage+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 13/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import SnapKit

public protocol ErrorMessageViewDelegate {
    func closeIconTapped()
}

public class ErrorMessageView: UIView {

    public var delegate: ErrorMessageViewDelegate?
    
    private let messageView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.125, green: 0.082, blue: 0.322, alpha: 1)
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()

    private var containerHorizontalStackView: UIStackView = {
        let containerHorizontalStackView = UIStackView(frame: .zero)
        containerHorizontalStackView.axis = .horizontal
        containerHorizontalStackView.distribution = .fill
        containerHorizontalStackView.spacing = 8
        return containerHorizontalStackView
    }()
    
    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icErrorWifiWhite", in: nil, with: nil) //Asset.icErrorWifiWhite.image
        return imageView
    }()
    
    private var containerVerticalStackView: UIStackView = {
        let containerVerticalStackView = UIStackView(frame: .zero)
        containerVerticalStackView.axis = .vertical
        containerVerticalStackView.distribution = .fill
        containerVerticalStackView.spacing = 0
        return containerVerticalStackView
    }()
    
    private let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.text = Localizable.string("mediquo.messages.error.message")
        return titleLabel
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = Localizable.string("mediquo.messages.error.message.subtitle")
        return label
    }()

    private var closeIconContainerView: UIView = {
        let closeIconContainerView = UIView(frame: .zero)
        return closeIconContainerView
    }()
    
    private let closeIconImage: UIImageView = {
        let closeIconImage = UIImageView()
        closeIconImage.image = UIImage(named: "icCloseWhite", in: nil, with: nil) // Asset.icCloseWhite.image
        return closeIconImage
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.messageView)
        setupSubViews()
    }
    
    private func setupSubViews() {
        self.messageView.addSubview(iconImage)
        containerVerticalStackView.addArrangedSubview(titleLabel)
        containerVerticalStackView.addArrangedSubview(messageLabel)
        containerHorizontalStackView.addArrangedSubview(containerVerticalStackView)
        closeIconContainerView.addSubview(closeIconImage)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeContainerTapped))
        closeIconContainerView.addGestureRecognizer(tapGesture)
        
        containerHorizontalStackView.addArrangedSubview(closeIconContainerView)
        messageView.addSubview(containerHorizontalStackView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func updateConstraints() {
        self.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(80)
        }
        
        self.messageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        self.iconImage.snp.makeConstraints { make in
            make.top.equalTo(self.messageView.snp.top).offset(16)
            make.leading.equalTo(self.messageView.snp.leading).offset(16)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        self.containerHorizontalStackView.snp.makeConstraints { make in
            make.top.equalTo(self.messageView.snp.top).offset(16)
            make.leading.equalTo(self.iconImage.snp.trailing).offset(16)
            make.trailing.equalTo(self.messageView.snp.trailing).offset(-16)
            make.bottom.equalTo(self.messageView.snp.bottom).offset(-10)
            make.height.equalTo(46)
        }
        
        self.closeIconContainerView.snp.makeConstraints { make in
            make.top.equalTo(self.containerHorizontalStackView.snp.top)
            make.trailing.equalTo(self.containerHorizontalStackView.snp.trailing)
            make.width.equalTo(24)
            make.height.equalToSuperview()
        }
        
        self.closeIconImage.snp.makeConstraints { make in
            make.top.equalTo(self.closeIconContainerView.snp.top)
            make.height.equalTo(16)
            make.width.equalTo(16)
        }
        super.updateConstraints()
    }
    
    @objc private func closeContainerTapped() {
        delegate?.closeIconTapped()
    }
}
