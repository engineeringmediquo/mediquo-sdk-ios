//
//  OfflineMessageView.swift
//  MediQuo-Base
//
//  Created by David Martin on 5/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit

class OfflineMessageView: UIView {

    private let messageView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.disconnectedBackground() 
        view.layer.masksToBounds = true
        return view
    }()

    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.icOffline.image
        return imageView
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = Localizable.string("mediquo.messages.disconnected.message")
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.messageView)
        self.messageView.addSubview(self.iconImage)
        self.messageView.addSubview(self.messageLabel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        self.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(64)
        }
        self.messageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        self.iconImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(22)
            make.leading.equalToSuperview().offset(16)
        }
        self.messageLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(self.iconImage.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
        super.updateConstraints()
    }
}
