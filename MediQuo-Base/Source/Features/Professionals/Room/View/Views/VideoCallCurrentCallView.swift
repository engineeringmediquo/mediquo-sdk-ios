//
//  VideoCallCurrentCallView.swift
//  MediQuo-Base
//
//  Created by David Martin on 11/1/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import SnapKit
import UIKit
import mediquo_videocall_lib

extension VideoCallCurrentCallView {
    private enum Localizables {
        static let mediquoCallInProgressMessage = Localizable.string("mediquo.call.in.progress.message")
        static let mediquoVideocallInProgressMessage = Localizable.string("mediquo.videocall.in.progress.message")
        static let mediquoVideocallSharedJoinMessage = Localizable.string("mediquo.shared.join")
    }
}

protocol VideoCallCurrentCallViewDelegate {
    func navigateToCall(with model: VideoCallCurrentCallModel)
}

class VideoCallCurrentCallView: UIView {
    
    var delegate: VideoCallCurrentCallViewDelegate?
    
    private let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = ColorName.primary.color
        view.layer.masksToBounds = true
        return view
    }()

    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizables.mediquoVideocallSharedJoinMessage, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        button.addTarget(self, action: #selector(self.didTapActionButton), for: .touchUpInside)
        return button
    }()

    private let actionImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.arrow.image
        imageView.tintColor = .white
        return imageView
    }()

    private var model: VideoCallCurrentCallModel? {
        didSet {
            guard let model = self.model else { return }

            switch model.type {
                case .call:
                    self.iconImage.image = Asset.call.image
                    self.messageLabel.text = Localizables.mediquoCallInProgressMessage.replacingOccurrences(of: "%@", with: model.name ?? "")
                case .video:
                    self.iconImage.image = Asset.videocall.image
                    self.messageLabel.text = Localizables.mediquoVideocallInProgressMessage.replacingOccurrences(of: "%@", with: model.name ?? "")
                default:
                    break
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.iconImage)
        self.contentView.addSubview(self.messageLabel)
        self.contentView.addSubview(self.actionButton)
        self.contentView.addSubview(self.actionImage)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        self.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(64)
        }
        self.contentView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        self.iconImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.height.equalTo(32)
            make.width.equalTo(32)
            make.leading.equalToSuperview().offset(16)
        }
        self.messageLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(self.iconImage.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-96)
        }
        self.actionButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(self.actionImage.snp.trailing).offset(-8)
        }
        self.actionImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-16)
        }
        super.updateConstraints()
    }

    func configure(_ model: VideoCallCurrentCallModel) {
        self.model = model
    }

    @objc private func didTapActionButton(sender _: UIButton!) {
        guard let model = self.model else { return }
        delegate?.navigateToCall(with: model)
    }
}
