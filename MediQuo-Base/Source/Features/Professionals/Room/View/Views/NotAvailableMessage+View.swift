//
//  NotAvailableMessage+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 10/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import SnapKit

extension NotAvailableMessageView {
    private enum Localizables {
        static let notAvailableMessage = Localizable.string("not.available.message")
    }
}

class NotAvailableMessageView: UIView {
    private let messageView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightRed()
        view.layer.masksToBounds = true
        return view
    }()

    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Asset.icProfile.image
        return imageView
    }()

    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault 
        label.textColor = .darkRed()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.messageView)
        self.messageView.addSubview(self.iconImage)
        self.messageView.addSubview(self.messageLabel)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        self.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(80)
        }
        self.messageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        self.iconImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.height.equalTo(24)
            make.width.equalTo(22)
            make.leading.equalToSuperview().offset(16)
        }
        self.messageLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(self.iconImage.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        super.updateConstraints()
    }

    func configure(_ name: String) {
        self.messageLabel.text = Localizables.notAvailableMessage.replacingOccurrences(of: "%@", with: name)
    }
}
