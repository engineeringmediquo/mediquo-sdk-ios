//
//  CustomMessagesFlowLayout.swift
//  MediQuo-Base
//
//  Created by David Martin on 4/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation
import UIKit
import MessageKit

class CustomMessagesFlowLayout: MessagesCollectionViewFlowLayout {
    lazy var customMessageSizeCalculator = CustomMessageSizeCalculator(layout: self)
    lazy var neutralMessageSizeCalculator = NeutralMessageSizeCalculator(layout: self)
    lazy var unreadMessagesSizeCalculator = UnreadMessagesSizeCalculator(layout: self)
        
    override func cellSizeCalculatorForItem(at indexPath: IndexPath) -> CellSizeCalculator {
        if isSectionReservedForTypingIndicator(indexPath.section) {
            return typingIndicatorSizeCalculator
        }
        
        guard let message: MessageCellType = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView) as? MessageCellType else {
            return super.cellSizeCalculatorForItem(at: indexPath)
        }
        
        if case .custom = message.kind {
            if let senderType = message.model?.senderType,
               senderType == .system {
                return neutralMessageSizeCalculator
            } else {
                return customMessageSizeCalculator
            }
        }
        return super.cellSizeCalculatorForItem(at: indexPath)
    }

    override func messageSizeCalculators() -> [MessageSizeCalculator] {
        var superCalculators = super.messageSizeCalculators()
        superCalculators.append(self.unreadMessagesSizeCalculator)
        superCalculators.append(self.customMessageSizeCalculator)
        superCalculators.append(self.neutralMessageSizeCalculator)
        return superCalculators
    }
}

class CustomMessageSizeCalculator: MessageSizeCalculator {
    override init(layout: MessagesCollectionViewFlowLayout? = nil) {
        super.init()
        self.layout = layout
    }

    override func sizeForItem(at indexPath: IndexPath) -> CGSize {
        guard let layout = layout else { return .zero }
        let collectionViewWidth = layout.collectionView?.bounds.width ?? 0
        let contentInset = layout.collectionView?.contentInset ?? .zero
        let inset = layout.sectionInset.left + layout.sectionInset.right + contentInset.left + contentInset.right
        return CGSize(width: collectionViewWidth - inset, height: 44)
    }
}

class NeutralMessageSizeCalculator: MessageSizeCalculator {
    override init(layout: MessagesCollectionViewFlowLayout? = nil) {
        super.init()
        self.layout = layout
    }

    override func messageContainerSize(for message: MessageType) -> CGSize {
        let maxWidth = messageContainerMaxWidth(for: message)

        var messageContainerSize: CGSize
        let attributedText: NSMutableAttributedString

        switch message.kind {
            case let .custom(text):
                guard let text = text as? String else { return CGSize(width: 0, height: 0) }
                attributedText = NSMutableAttributedString(string: text)
            default:
                attributedText = NSMutableAttributedString(string: "")
                //fatalError("messageContainerSize received unhandled MessageDataType: \(message.kind)")
        }

        messageContainerSize = self.labelSize(for: attributedText, considering: maxWidth)

        let messageInsets = self.messageLabelInsets(for: message)
        messageContainerSize.width += messageInsets.horizontal
        messageContainerSize.height += messageInsets.vertical

        return messageContainerSize
    }

    internal func labelSize(for attributedText: NSAttributedString, considering maxWidth: CGFloat) -> CGSize {
        let constraintBox = CGSize(width: maxWidth, height: .greatestFiniteMagnitude)
        let rect = attributedText.boundingRect(with: constraintBox, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).integral

        return rect.size
    }

    internal func messageLabelInsets(for _: MessageType) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
    }

    override func sizeForItem(at indexPath: IndexPath) -> CGSize {
        let dataSource = messagesLayout.messagesDataSource
        let message = dataSource.messageForItem(at: indexPath, in: messagesLayout.messagesCollectionView)
        let itemHeight = cellContentHeight(for: message, at: indexPath) + 12
        return CGSize(width: 275, height: itemHeight)
    }
}

class UnreadMessagesSizeCalculator: MessageSizeCalculator {
    override init(layout: MessagesCollectionViewFlowLayout? = nil) {
        super.init()
        self.layout = layout
    }

    override func sizeForItem(at _: IndexPath) -> CGSize {
        guard let layout = layout else { return .zero }
        let collectionViewWidth = layout.collectionView?.bounds.width ?? 0
        let contentInset = layout.collectionView?.contentInset ?? .zero
        let inset = layout.sectionInset.left + layout.sectionInset.right + contentInset.left + contentInset.right
        return CGSize(width: collectionViewWidth - inset, height: 44)
    }
}

extension UIEdgeInsets {
    internal var vertical: CGFloat {
        return top + bottom
    }

    internal var horizontal: CGFloat {
        return left + right
    }
}
