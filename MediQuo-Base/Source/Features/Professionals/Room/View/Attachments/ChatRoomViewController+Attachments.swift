//
//  ChatRoomViewController+Attachments.swift
//  MediQuo-Base
//
//  Created by David Martin on 2/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import InputBarAccessoryView
import Photos
import UIKit

extension ChatRoomViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func documentPicker(item: InputBarButtonItem) {
        let defaultStatus: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        PHPhotoLibrary.requestAuthorization { (status: PHAuthorizationStatus) in
            switch status {
                case .denied:
                    if defaultStatus != .notDetermined {
                        self.showEnableDeniedPhotoPermissions()
                    }
                case .authorized:
                    self.dismissKeyboard()
                    self.showMediaOptions(at: item)
                @unknown default:
                    CoreLog.ui.error("Fatal error")
            }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let mediaTypeString = info[.mediaType] as? String else { return }
        if mediaTypeString == "public.movie" {
            if let videoURL = info[.mediaURL] as? URL {
                picker.dismiss(animated: true)
                self.viewModel?.send(file: videoURL)
            } else if let asset = info[.phAsset] as? PHAsset {
                let options = PHVideoRequestOptions()
                options.version = .original
                PHImageManager.default().requestAVAsset(forVideo: asset, options: options, resultHandler: {(asset, _, _) -> Void in
                    Task {
                        if let urlAsset = asset as? AVURLAsset {
                            picker.dismiss(animated: true)
                            self.viewModel?.send(file: urlAsset.url)
                        } else {
                            picker.dismiss(animated: true)
                            return
                        }
                    }
                })
                
            }
        } else {
            guard let image = info[.originalImage] as? UIImage else { return }

            var privateUrl: NSURL?
            let messageId = UUID().uuidString

            if picker.sourceType == .photoLibrary {
                privateUrl = info[.imageURL] as? NSURL
            }
            if picker.sourceType == .camera {
                self.canFetch = false
                let fileManager = FileManager.default
                let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
                let imagePath = documentsPath?.appendingPathComponent("\(messageId).jpg")

                if let pickedImage = info[.originalImage] as? UIImage,
                   let imagePath = imagePath {
                   let imageData = pickedImage.compressWith(quality: .lowest)
                    (try? imageData?.write(to: imagePath)) as ()??
                    privateUrl = imagePath as NSURL
                    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
                }
            }
            picker.dismiss(animated: true)
            self.viewModel?.send(image: privateUrl, image: image, uuid: messageId)
        }
    }
    

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }

    private func showEnableDeniedPhotoPermissions() {
        DispatchQueue.main.sync { [weak self] in
            guard let self = self else { return }
            let alert: UIAlertController = UIAlertController(title: Localizables.privacy,
                                                             message: Localizables.privacyUsageDescription,
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: Localizables.privacyPhoto, style: .default) { _ in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            
            alert.addAction(UIAlertAction(title: Localizables.cancel, style: .cancel))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true)
        }
    }

    private func showMediaOptions(at source: InputBarButtonItem) {
        DispatchQueue.main.sync { [weak self] in
            guard let self = self else { return }
            let alert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            if let camera = self.imageAction(title: Localizables.attachmentCamera,
                                             image: Asset.camera.image,
                                             type: .camera) {
                alert.addAction(camera)
            }
            
            if let gallery = self.imageAction(title: Localizables.attachmentGallery,
                                              image: Asset.gallery.image,
                                              type: .photoLibrary) {
                alert.addAction(gallery)
            }
            
            let action = UIAlertAction(title: Localizables.attachmentDocument,
                                       style: .default,
                                       image: Asset.document.image,
                                       handler: { _ in
                self.documentPicker = DocumentExplorer().add { [weak self] _, urls in
                    guard let self = self else { return }
                    for url in urls {
                        self.viewModel?.send(file: url)
                    }
                }.add { [weak self] _ in
                    guard let self = self else { return }
                    self.messageInputBar.inputTextView.resignFirstResponder()
                    self.messagesCollectionView.scrollToBottom(animated: true)
                }
                self.documentPicker?.present(on: self)
            })
            
            alert.addAction(action)

            alert.addAction(UIAlertAction(title: Localizables.cancel, style: .cancel) { [weak self] _ in
                guard let self = self else { return }
                self.messageInputBar.inputTextView.resignFirstResponder()
                self.messagesCollectionView.scrollToBottom(animated: true)
            })

            alert.popoverPresentationController?.sourceView = source.inputAccessoryView
            alert.popoverPresentationController?.sourceRect = source.frame
            self.present(alert, animated: true)
        }
    }

    private func imageAction(title: String?,
                             image: UIImage?,
                             type: UIImagePickerController.SourceType) -> UIAlertAction? {
        
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            NSLog("[MessagesViewController] Invalid image picker source type '\(type)' for device")
            return nil
        }

        return UIAlertAction(title: title,
                             style: .default,
                             image: image,
                             handler: { (_: UIAlertAction) -> Void in
            self.imagePicker.sourceType = type
            self.present(self.imagePicker, animated: true)
        })
    }
}
