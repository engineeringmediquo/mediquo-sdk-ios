//
//  ImageDetailViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 3/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Photos
import SDWebImage
import SnapKit
import UIKit
 
extension ImageDetailViewController {
    private enum Localizables {
        static let alertTitle = Localizable.string("mediquo.literal.privacy")
        static let alertMessage = Localizable.string("mediquo.privacy.photo.usage.description")
        static let alertUsageSettings = Localizable.string("mediquo.privacy.photo.usage.settings")
        static let alertCancel = Localizable.string("mediquo.literal.cancel")
        static let sharedDone = Localizable.string("shared.done")
    }
}

class ImageDetailViewController: BaseViewControllerSDK {
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .black
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    private var canDownload: Bool = false

    var url: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.imageView)
        guard let url = self.url else { return }
        self.imageView.sd_setImage(with: url, placeholderImage: Asset.placeholder.image)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .black
        self.configureNavigationController()
        self.setCloseButtonNavigationController(target: self, selector: "didTapClose:")
        self.addSaveButton()
        self.checkIfDownloadImage()
    }

    override func updateViewConstraints() {
        self.imageView.snp.makeConstraints { make in
            make.size.equalToSuperview()
            make.center.equalToSuperview()
        }
        super.updateViewConstraints()
    }
}

extension ImageDetailViewController {
    private func configureNavigationController() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = .black
            self.navigationController?.navigationBar.standardAppearance = appearance
            self.navigationController?.navigationBar.compactAppearance = appearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
        } else {
            self.navigationController?.navigationBar.backgroundColor = .black
            self.navigationController?.navigationBar.isOpaque = true
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.backgroundColor = .black
            self.navigationController?.navigationBar.barTintColor = .black
        }
    }

    @objc private func didTapClose(_: Any) {
        self.dismiss(animated: true)
    }

    private func checkIfDownloadImage() {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(self.url!.lastPathComponent) {
            if FileManager.default.fileExists(atPath: pathComponent.path) {
                self.canDownload = false
            } else {
                self.canDownload = true
            }
        }
    }

    private func addSaveButton() {
        let button = UIBarButtonItem(image: Asset.groupsDownloadImage.image.withRenderingMode(.alwaysOriginal),
                                     style: .plain, target: self, action: #selector(self.didTapDownloadImage))
        self.navigationItem.rightBarButtonItem = button
    }

    @objc private func didTapDownloadImage() {
        guard let image = self.imageView.image else { return }

        PHPhotoLibrary.requestAuthorization { (status: PHAuthorizationStatus) in
            switch status {
            case .denied:
                guard PHPhotoLibrary.authorizationStatus() != .notDetermined else { return }
                self.showEnableDeniedPhotoPermissions()
            case .authorized:
                guard let path: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first,
                      let lastPathComponent = self.url?.lastPathComponent,
                      let pathComponent = NSURL(fileURLWithPath: path).appendingPathComponent(lastPathComponent) else { return }

                DispatchQueue.main.async {
                    do {
                        let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                        let fileURL = documentDirectory.appendingPathComponent(lastPathComponent)
                        let imageData = self.imageView.image!.compressWith(quality: .highest)
                        try imageData?.write(to: fileURL, options: .withoutOverwriting)
                    } catch {
                        CoreLog.ui.error("%@", error.localizedDescription)
                    }

                    Toast.show(message: Localizables.sharedDone)

                    self.checkIfDownloadImage()

                    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
                }
            default:
                break
            }
        }
    }

    private func showEnableDeniedPhotoPermissions() {
        DispatchQueue.main.sync {
            let alert: UIAlertController = .init(title: Localizables.alertTitle,
                                                 message: Localizables.alertMessage,
                                                 preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Localizables.alertUsageSettings, style: .default) { _ in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            
            alert.addAction(UIAlertAction(title: Localizables.alertCancel, style: .cancel))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true)
        }
    }
}
