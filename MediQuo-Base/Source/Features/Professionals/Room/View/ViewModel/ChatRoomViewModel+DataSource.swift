//
//  ChatRoomViewModel+DataSource.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 7/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation
import MessageKit

// MARK: - MessagesDataSource
extension DefaultChatRoomViewModel {
    func currentSender() -> SenderType {
        guard let userHash = self.currentUserHash,
              let userName = self.currentUserName else {
            return SenderModel(senderId: "", displayName: "")
        }
        return SenderModel(senderId: userHash, displayName: userName)
    }

    func messageForItem(at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> MessageType {
        guard let message = _messages[safe: indexPath.section],
              let cell = self.convertToCellType(from: message) else {
            return MessageCellType(model: nil, sender: SenderModel(senderId: "", displayName: ""))
        }
        return cell
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        self._messages.count ?? 0
    }

    func customCell(for message: MessageType, at indexPath: IndexPath,
                    in messagesCollectionView: MessagesCollectionView) -> UICollectionViewCell {
        guard let message: MessageCellType = self.messageForItem(at: indexPath, in: messagesCollectionView) as? MessageCellType else {
            return UICollectionViewCell()
        }
        
        if case .custom = message.kind {
            if let senderType = message.model?.senderType,
               senderType == .system {
                let cell: NeutralCell = messagesCollectionView.dequeueReusableCell(NeutralCell.self, for: indexPath)
                cell.configure(with: message)
                return cell
            } else {
                let cell: CustomCell = messagesCollectionView.dequeueReusableCell(CustomCell.self, for: indexPath)
                cell.configure(with: message)
                return cell
            }
        }
        return messagesCollectionView.cellForItem(at: indexPath) ?? UICollectionViewCell()
    }
}
