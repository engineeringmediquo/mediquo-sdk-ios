//
//  ChatRoomViewModel+Cache.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 7/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

// MARK: - Messages Cache
extension DefaultChatRoomViewModel {
    func setMessagesInCache() {
        guard let professionalUserHash = self.professionalUserHash,
              let socket = self.socket else { return }
        socket.setMessagesInCache(toUserHash: professionalUserHash,
                                  myUserHash: currentUserHash ?? "",
                                  messages: self._messages)
    }

    func setMessageInCache(message: String) {
        guard let professionalUserHash = self.professionalUserHash,
              let socket = self.socket else { return }
        socket.setMessageInCache(toUserHash: professionalUserHash,
                                 myUserHash: currentUserHash ?? "",
                                 message: message)
    }
    
    func getMessageFromCache() -> String {
        guard let professionalUserHash = self.professionalUserHash,
              let socket = self.socket else { return "" }
        return socket.getMessageFromCache(toUserHash: professionalUserHash,
                                          myUserHash: currentUserHash ?? "")
    }
    
    func getMessagesFromCache() -> [MessageModel] {
        guard let professionalUserHash = self.professionalUserHash,
              let socket = self.socket else { return [] }
        return socket.getMessagesFromCache(toUserHash: professionalUserHash,
                                           myUserHash: currentUserHash ?? "")
    }
}
