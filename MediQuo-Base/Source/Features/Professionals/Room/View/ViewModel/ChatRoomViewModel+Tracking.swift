//
//  ChatRoomViewModel+Tracking.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 7/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

// MARK: - Tracking
extension DefaultChatRoomViewModel {
    func sendChatViewTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.Chat.view, object: nil, userInfo: getUserInfo())
    }

    func sendMessageSendTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.Chat.messageSend, object: nil, userInfo: getUserInfo())
    }

    func sendMessageReceivedTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.Chat.messageReceived, object: nil, userInfo: getUserInfo())
    }
    
    private func getUserInfo() -> [AnyHashable : Any]? {
        guard let userHash = self.professionalModel?.hash,
              let specialityId = self.professionalModel?.specialityId else { return nil }
        return [Notification.Name.Key.professionalHash: userHash, Notification.Name.Key.specialityId: specialityId]
    }
}
