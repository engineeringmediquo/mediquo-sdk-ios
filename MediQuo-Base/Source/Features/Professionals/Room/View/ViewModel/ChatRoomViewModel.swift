//
//  ChatRoomViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import MessageKit

enum ChatRoomState {
    case defaultState, errorState
}

protocol ChatRoomViewModelChatCache {
    func setMessagesInCache()
    func setMessageInCache(message: String)
    func getMessageFromCache() -> String
}

protocol ChatRoomViewModelInput {
    // Exposed Properties
    var currentUserHash: String? {get set }
    var currentUserName: String? {get set }
    var roomId: Int? { get set }
    var professionalModel: ProfessionalSDKModel? { get set}
    
    // Exposed Methods
    func getSocketStatus()
    func joinRoom()
    func fetchRoom()
    func fetchMessages()
    func fetchMoreMessages()
    func isMessageFromCurrentSender(message: MessageType) -> Bool
    func formattedHeaderDate(for message: MessageType) -> String
    func typing(isTyping: Bool)
    func send(message text: String)
    func send(image privateUrl: NSURL?, image: UIImage, uuid: String)
    func send(file url: URL)
    func sendMessageToSocket(message: MessageModel)
    func receive(message: String)
    func delete(message id: String)
    func isDeleted(message: MessageModel) -> Bool
    func getMessagesList() -> [MessageModel]?
    
    
    // User Actions
    func didTapMessage(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView)
    func didTapImage(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView, completion: @escaping (URL?) -> Void)
}

protocol ChatRoomViewModelOutput {
    var chatRoomState: MediQuoObservable<ChatRoomState?> {get set }
    var reloadData: MediQuoObservable<Any?> {get set }
    var isSocketConnected: MediQuoObservable<Bool?> {get set }
    var room: MediQuoObservable<RoomModel?> {get set}
    var isTyping: MediQuoObservable<Bool?> {get set }
    var appointment: MediQuoObservable<AppointmentSDKModel?> { get set }
    var currentCall: MediQuoObservable<VideoCallCurrentCallModel?> { get set }
    var canDeleteMessage: MediQuoObservable<Bool?> { get set }
}

protocol ChatRoomViewModel: ChatRoomViewModelOutput,
                            ChatRoomViewModelInput,
                            BaseViewModel,
                            MessageKit.MessagesDataSource,
                            ChatRoomViewModelChatCache {}

class DefaultChatRoomViewModel: ChatRoomViewModel {
    
    // UseCases
    private var fetchRoomUseCase: FetchRoomUseCase?
    private var readRoomUseCase: ReadRoomUseCase?
    private var chatUseCase: ChatControllerUseCase?
    
    var professionalUserHash: String?

    // Current messages in memory
    private(set) var _messages: [MessageModel] = [] {
        didSet {
            self.reloadData.value = _messages
        }
    }

    // Unread messages
    private var hasUnreadMessageCell: Bool = false
    private var isFetchingMessages: Bool = false
    
    // Exposed Properties
    let socket: MediquoChatManager?
    var roomId: Int?
    var currentUserHash: String?
    var currentUserName: String?
    var professionalModel: ProfessionalSDKModel?

    // Bindings
    var chatRoomState: MediQuoObservable<ChatRoomState?> = MediQuoObservable(nil)
    var reloadData: MediQuoObservable<Any?> = MediQuoObservable(nil)
    var isSocketConnected: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    var room: MediQuoObservable<RoomModel?> = MediQuoObservable(nil)
    var isTyping: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    var appointment: MediQuoObservable<AppointmentSDKModel?> = MediQuoObservable(nil)
    var currentCall: MediQuoObservable<VideoCallCurrentCallModel?> = MediQuoObservable(nil)
    var canDeleteMessage: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    
    init(socket: MediquoChatManager?,
         professionalModel: ProfessionalSDKModel? = nil,
         currentUserHash: String?,
         currentUserName: String?,
         fetchRoomUseCase: FetchRoomUseCase? = RoomUseCase(),
         readRoomUseCase: ReadRoomUseCase? = RoomUseCase(),
         chatUseCase: ChatControllerUseCase?,
         professionalUserHash: String?) {
        self.fetchRoomUseCase = fetchRoomUseCase
        self.readRoomUseCase = readRoomUseCase
        self.professionalModel = professionalModel
        self.currentUserHash = currentUserHash
        self.currentUserName = currentUserName
        self.chatUseCase = chatUseCase
        self.professionalUserHash = professionalUserHash
        self.socket = socket
        self.bindSocketEvents()
        self.joinRoom()
    }
}

// MARK: - Bindings Sockets
extension DefaultChatRoomViewModel {
    // swiftlint:disable function_body_length
    private func bindSocketEvents() {
        self.socket?.onEnterRoom()
        self.socket?.onReceive()
        self.socket?.onUpdate()
        self.socket?.onTyping()
        self.socket?.onStopTyping()
        self.socket?.onAppointment()
        self.socket?.onRoomAccepted()
        self.socket?.onRoomRejected()

        self.socket?.error.subscribe { [weak self] error in
            guard let self = self,
                  let error = error else { return }
            
            if !self._messages.isEmpty {
                self._messages.insert(contentsOf: self.filterMessagesForCurrentRoomId(self.getMessagesFromCache()),
                                      at: ChatConstants.firstPosition)
            } else {
                self._messages = self.getMessagesFromCache()
            }
            self.manageChatMessages(with: self._messages)
            self.chatRoomState.value = .errorState
        }
        
        self.socket?.roomId.subscribe { [weak self] roomId in
            guard let self = self,
                  let roomId = roomId else { return }
            self.roomId = roomId
            self.initChatRoom()
        }
        
        self.socket?.messages.subscribe { [weak self] messages in
            guard let self = self,
                    let messages = messages else { return }

            // Insert or set new messages
            if !self._messages.isEmpty {
                self._messages.insert(contentsOf: self.filterMessagesForCurrentRoomId(messages),
                                      at: ChatConstants.firstPosition)
            } else {
                self._messages = self.filterMessagesForCurrentRoomId(messages)
            }
            self.manageChatMessages(with: self._messages)
            self.chatRoomState.value = .defaultState
            self.sendChatViewTracking()
        }
        
        self.socket?.message.subscribe { [weak self] message in
            guard let self = self, let message = message,
                  let cell = self.convertToCellType(from: message) else { return }
        
            if self.isMessageForCurrentRoomId(message) {
                self.updateMessageAsRead(cell)
                self._messages.append(message)
                self.sendMessageReceivedTracking()
            }
        }
        
        self.socket?.messageStatus.subscribe { [weak self] messageStatus in
            guard let self = self,
                  let messageStatus = messageStatus,
                  let row = self._messages.firstIndex(where: {
                
                let isTheSameMessageId = $0.messageId == messageStatus.messageId
                let isFromCurrentSender = $0.fromUserHash == self.currentSender().senderId
                let isNotRead = $0.status != .read
                return isTheSameMessageId && isFromCurrentSender && isNotRead
            
            }) else { return }
            
            guard self._messages[safe: row] != nil else { return }
            self._messages[row].status.update(status: messageStatus.status)
        }
        
        self.socket?.isTyping.subscribe { [weak self] isTyping in
            guard let self = self else { return }
            self.isTyping.value = isTyping
        }
        
        self.socket?.appointment.subscribe { [weak self] appointment in
            guard let self = self,
                  let appointment = appointment else { return }
            self.appointment.value = appointment
        }
        
        self.socket?.isRoomAccepted.subscribe { [weak self] isRoomAccepted in
            guard let self = self,
                  let isRoomAccepted = isRoomAccepted,
                  isRoomAccepted else { return }
            self.fetchRoom()
        }
        
        self.socket?.isRoomRejected.subscribe { [weak self] isRoomRejected in
            guard let self = self,
                  let isRoomRejected = isRoomRejected,
                  isRoomRejected else { return }
            self.fetchRoom()
        }
    }

    private func initChatRoom() {
        self.fetchRoom()
        self.fetchMessages()
        self.markRoomAsRead()
    }
    
    private func unBindSocket() {
        self.socket?.error.unsubscribe()
        self.socket?.roomId.unsubscribe()
        self.socket?.messages.unsubscribe()
        self.socket?.message.unsubscribe()
        self.socket?.messageStatus.unsubscribe()
        self.socket?.isRoomAccepted.unsubscribe()
        self.socket?.isRoomRejected.unsubscribe()
        self.socket?.appointment.unsubscribe()
    }
}

// MARK: - Exposed Methods
extension DefaultChatRoomViewModel {
    func getSocketStatus() {
        self.isSocketConnected.value = self.socket?.isConnected
    }
    
    func joinRoom() {
        guard let professionalUserHash = self.professionalUserHash else { return }
        self.socket?.join(by: professionalUserHash,
                          and: professionalUserHash)
    }
    
    func fetchRoom() {
        guard let roomId = self.roomId else { return }
        self.appointment.value = nil
        self.currentCall.value = nil
        fetchRoomForSdk(roomId: roomId)
    }
    
    func fetchRoomForSdk(roomId: Int) {
        let completion: (Result<RoomModel?, BaseError>) -> Void = { [weak self] result in
            guard let self = self else { return }
            if case let .success(roomModel) = result {
                self.room.value = roomModel
                self.appointment.value = roomModel?.currentAppointment
                self.currentCall.value = roomModel?.currentCall
                self.canDeleteMessage.value = roomModel?.meta?.permissions?.chat?.canDeleteMessages
            }
            if case let .failure(error) = result {
                self.chatRoomState.value = .errorState
                CoreLog.business.error("%@", error.description)
            }
        }
        fetchRoomUseCase?.fetchRoomForSDK(by: roomId,
                                          completion: completion)
    }
            
    func fetchMessages() {
        guard let roomId = self.roomId else { return }
        if !isFetchingMessages {
            self.isFetchingMessages = true
            self.socket?.fetch(by: roomId)
        }
    }
    
    func fetchMoreMessages() {
        guard let roomId = self.roomId,
              let message = getLastMessage() else { return }
        if !isFetchingMessages {
            self.isFetchingMessages = true
            self.socket?.fetch(by: roomId,
                               pivotId: message.messageId,
                               mode: .older)
        }
    }
    
    func isMessageFromCurrentSender(message: MessageType) -> Bool {
        return isFromCurrentSender(message: message)
    }
    
    func formattedHeaderDate(for message: MessageType) -> String {
        return DateManager.convertToString(message.sentDate, dateStyle: .medium, timeStyle: DateFormatter.Style.none) ?? ""
    }
    
    func typing(isTyping: Bool) {
        guard let roomId: Int = self.roomId else { return }
        if isTyping {
            self.socket?.typing(by: roomId)
        } else {
            self.socket?.stopTyping(by: roomId)
        }
    }
        
    func send(message text: String) {
        guard let fromUserHash = self.currentUserHash else { return }

        let model = MessageModel(text: text,
                                 roomId: roomId ?? 0,
                                 messageId: UUID().uuidString,
                                 fromUserHash: fromUserHash,
                                 senderType: .ordinary)
        
        self._messages.append(model)
        self.sendMessageToSocket(message: model)
        self.sendMessageSendTracking()
    }
    
    func send(image privateUrl: NSURL?,
              image: UIImage,
              uuid: String) {
        
        guard let fromUserHash = self.currentUserHash,
              let privateUrlString = privateUrl?.absoluteString,
              let url = URL(string: privateUrlString),
              let data = image.compressWith(quality: .lowest) else { return }

        let size = data.count
        let isBiggerSizeThanAllowed = self.checkIsBiggerThanMaximumAllowed(by: size)

        guard !isBiggerSizeThanAllowed else {
            // TODO: Cane - Revisar que hacer a nivel de UI con este error
            CoreLog.ui.error("Image is greather than allowed")
            return
        }
        let width = Int(image.size.width)
        let height = Int(image.size.height)

        let metaData: MessageModel.Metadata = .image(url: url,
                                                     thumb: url,
                                                     width: width,
                                                     height: height,
                                                     size: size)
        
        let model = MessageModel(image: image,
                                 roomId: roomId ?? 0,
                                 messageId: uuid,
                                 fromUserHash: fromUserHash,
                                 fileName: "\(url.lastPathComponent)",
                                 metadata: metaData,
                                 senderType: .ordinary)
        self._messages.append(model)
        self.sendMessageToSocket(message: model)
        self.sendMessageSendTracking()
    }

    func send(file url: URL) {
        guard let fromUserHash = self.currentUserHash,
              let data: Data = try? Data(contentsOf: url) else { return }

        let size = data.count
        let isBiggerSizeThanAllowed = self.checkIsBiggerThanMaximumAllowed(by: size)

        guard !isBiggerSizeThanAllowed else {
            CoreLog.ui.error("File is greather than allowed")
            return
        }
    
        let metaData: MessageModel.Metadata = .file(name: "\(url.lastPathComponent)",
                                                    url: url,
                                                    size: size)
        
        let model = MessageModel(file: data,
                                 roomId: roomId ?? 0,
                                 messageId: UUID().uuidString,
                                 fromUserHash: fromUserHash,
                                 fileName: "\(url.lastPathComponent)",
                                 metadata: metaData,
                                 senderType: .ordinary)
        
        self._messages.append(model)
        self.sendMessageToSocket(message: model)
        self.sendMessageSendTracking()
    }
    
    func sendMessageToSocket(message: MessageModel) {
        var messageToSend: MessageModel = message
        if messageToSend.roomId == 0 {
            if let roomId = self.roomId {
                messageToSend.roomId = roomId
            }
        }
        self.socket?.send(message: messageToSend,
                          messagesForCache: _messages)
    }
    
    func receive(message: String) {
        guard let professionalHash = self.professionalUserHash else { return }
        self.chatUseCase?.receive(message: message,
                                  professionalHash: professionalHash)
    }
    
    func delete(message id: String) {
        self.chatUseCase?.delete(message: id)
    }

    func isDeleted(message: MessageModel) -> Bool {
        message.isDeleted
    }
    
    func convertToCellType(from model: MessageModel) -> MessageCellType? {
        return MessageCellType(model: model,
                               sender: SenderModel(senderId: model.fromUserHash, displayName: ""))
    }
    
    func getMessagesList() -> [MessageModel]? {
        return _messages
    }
    
    func getLastMessage() -> MessageModel? {
        return _messages.first
    }
}

// MARK: - User Actions
extension DefaultChatRoomViewModel {
    func didTapMessage(at indexPath: IndexPath,
                       in messagesCollectionView: MessagesCollectionView) {
        let message = self.messageForItem(at: indexPath, in: messagesCollectionView)
        switch message.kind {
            case .attributedText(let file):
                self.openDocument(from: file)
            default:
                break
        }
    }
    
    func didTapImage(at indexPath: IndexPath,
                     in messagesCollectionView: MessagesCollectionView,
                     completion: @escaping (URL?) -> Void) {
        let message = self.messageForItem(at: indexPath, in: messagesCollectionView)
        if case let .photo(item) = message.kind {
            completion(item.url)
        }
    }
    
    private func openDocument(from file: NSAttributedString) {
        if let url: URL = file.attribute(NSAttributedString.Key.link, at: 0, effectiveRange: nil) as? URL {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:]) { _ in }
            }
        }

        if let url: URL = URL(string: file.string) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:]) { _ in }
            }
        }
    }
}

// MARK: - Private Methods
extension DefaultChatRoomViewModel {
    private func manageChatMessages(with messages: [MessageModel]) {
        // Sort messages by time
        self._messages.sort { $0.timestamp ?? Date().timeIntervalSince1970 < $1.timestamp ?? Date().timeIntervalSince1970 }

        // Insert or remove unread message cell if needed
        self.insertOrRemoveUnreadMessagesCell(messages)

        // Update messages list and removing updates if needed
        self._messages = self._messages.removeDuplicates()
        self.isFetchingMessages = false
    }
        
    private func updateMessageAsRead(_ message: MessageCellType) {
        guard self.canReadMessage(message),
              let model = message.model else { return }
        self.socket?.update(message: model, status: .read)
    }

    private func canReadMessage(_ message: MessageCellType) -> Bool {
        return !self.isMessageFromCurrentSender(message: message) && message.status != .read
    }

    private func checkIsBiggerThanMaximumAllowed(by dataSize: Int) -> Bool {
        let oneMegabyte = 1_048_566
        let maxMegabytes = 50
        let maxMegabytesGranted = Int64(oneMegabyte * maxMegabytes)
        return Int64(dataSize) > maxMegabytesGranted
    }

    private func isMessageForCurrentRoomId(_ message: MessageModel) -> Bool {
        guard let roomId = self.roomId else { return false }
        return message.roomId == roomId
    }

    private func filterMessagesForCurrentRoomId(_ messages: [MessageModel]) -> [MessageModel] {
        guard let roomId = self.roomId else { return [] }
        return messages.filter { $0.roomId == roomId }
    }

    private func insertOrRemoveUnreadMessagesCell(_ messages: [MessageModel]) {
        if let index = messages.firstIndex(where: { $0.status != .read && $0.fromUserHash != self.currentUserHash }) {
            let messageIndex = messages.index(after: index)
            let position = messageIndex > 0 ? messageIndex - 1 : messageIndex
            let count = messages.count - position

            if let roomId = self.roomId,
               messages.count >= position,
                !self.hasUnreadMessageCell {
                self.hasUnreadMessageCell = true
                self._messages.insert(MessageModel(unreadCount: count, roomId: roomId), at: position)
            }
        } else {
            self._messages.removeAll(where: { $0.isUnreadMessage })
        }
    }

    private func markRoomAsRead() {
        guard let roomId = self.roomId else { return }
        self.readRoomUseCase?.read(by: String(roomId))
    }
}






