//
//  CustomCell.swift
//  MediQuo-Base
//
//  Created by David Martin on 4/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit
import MessageKit

extension CustomCell {
    private enum Localizables {
        static let messageNotRead = Localizable.string("mediquo.groups.chat.message.not.read")
        static let messagesNotRead = Localizable.string("mediquo.groups.chat.messages.not.read")
    }
}

class CustomCell: UICollectionViewCell {
    
    let label = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }

    func setupSubviews() {
        self.contentView.addSubview(self.label)
        self.label.textAlignment = .center
        self.label.textColor = ColorName.ultraDarkGray.color
        self.label.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        self.label.numberOfLines = 1
        self.label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.label.clipsToBounds = true
        self.label.backgroundColor = ColorName.lightGray.color
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.frame = self.contentView.bounds
    }

    func configure(with message: MessageType) {
        switch message.kind {
            case .custom(let data):
                guard let value = data as? String,
                      let count: Int = Int(value) else { return }
            
                if count == 1 {
                    self.label.text = Localizables.messageNotRead.replacingOccurrences(of: "%@",
                                                                                       with: "\(count)")
                } else {
                    self.label.text = Localizables.messagesNotRead.replacingOccurrences(of: "%@",
                                                                                       with: "\(count)") 
                }
            default:
                break
        }
    }
}
