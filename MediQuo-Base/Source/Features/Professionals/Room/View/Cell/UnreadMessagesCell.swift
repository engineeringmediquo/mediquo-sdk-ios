// Copyright © 2022 Medipremium S.L. All rights reserved.

import MessageKit
import UIKit

extension UnreadMessagesCell {
    private enum Localizables {
        static let messageNotRead = Localizable.string("mediquo.groups.chat.message.not.read")
        static let messagesNotRead = Localizable.string("mediquo.groups.chat.messages.not.read")
    }
}

// TODO: Cane - Revisar si esta celda no se está usando actualmente tras la migración
class UnreadMessagesCell: UICollectionViewCell {
    let label = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }

    func setupSubviews() {
        self.contentView.addSubview(self.label)
        self.label.textAlignment = .center
        self.label.textColor =  UIColor.darkText
        self.label.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
        self.label.numberOfLines = 1
        self.label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.label.clipsToBounds = true
        self.label.backgroundColor = UIColor.lightGray
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.frame = self.contentView.bounds
    }

    func configure(with message: MessageType) {
        switch message.kind {
        case let .custom(data):
            guard let value = data as? String, let count: Int = Int(value) else { return }
            if count == 1 {
                self.label.text = Localizables.messageNotRead.replacingOccurrences(of: "%@",
                                                                                   with: "\(count)")
            } else {
                self.label.text = Localizables.messagesNotRead.replacingOccurrences(of: "%@",
                                                                                   with: "\(count)")
            }
        default:
            break
        }
    }
}
