//
//  MessageView.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import MessageKit

class MessageView: MessageReusableView {
    class func reuseIdentifier() -> String { return "MessageView" }

    // MARK: - Properties

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints(_ view: UIView) {
        guard let superview = view.superview else {
            return
        }
        view.translatesAutoresizingMaskIntoConstraints = false

        let constraints: [NSLayoutConstraint] = [
            superview.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 20),
            view.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 20),
            view.topAnchor.constraint(equalTo: superview.topAnchor),
            view.bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}

class MessageDateHeaderView: MessageView {
    override class func reuseIdentifier() -> String { return "MessageDateHeaderView" }

    // MARK: - Properties
    let dateLabel = UILabel()

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.dateLabel)
        self.setupConstraints(self.dateLabel)
        
        self.dateLabel.textAlignment = .center
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MessageDateFooterView: MessageView {
    override class func reuseIdentifier() -> String { return "MessageDateFooterView" }

    // MARK: - Properties
    let dateLabel = UILabel()

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.dateLabel)
        self.setupConstraints(self.dateLabel)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func labelPosition(isFromCurrentSender: Bool) {
        self.dateLabel.textAlignment = isFromCurrentSender ? .right : .left
    }
}

/// Mensaje inicial del Chat hecho por el sistema.
class MessageCenterHeaderView: MessageView {
    override class func reuseIdentifier() -> String { return "MessageCenterHeaderView" }

    // MARK: - Properties
    let textLabel = UILabel()

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(red: 0.925, green: 0.937, blue: 0.945, alpha: 1) // .chatOutgoingTextColor()
        self.layer.cornerRadius = 10
        self.addSubview(self.textLabel)
        self.setupConstraints(self.textLabel)
        self.textLabel.textAlignment = .center
        self.textLabel.numberOfLines = 0
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
