//
//  NeutralCell.swift
//  MediQuo-Base
//
//  Created by David Martin on 30/11/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import MessageKit
import UIKit

/// Esta se usa para los mensajes de llamada finalizada/ realizada por ejemplo
class NeutralCell: UICollectionViewCell {
    var label = PaddingLabel(top: 8, bottom: 8, left: 16, right: 16, frame: CGRect.zero)

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }

    func setupSubviews() {
        self.label = PaddingLabel(top: 8, bottom: 8, left: 16, right: 16, frame: super.frame)
        self.contentView.addSubview(self.label)
        
        self.label.textAlignment = .center
        self.label.textColor = UIColor(red: 0.565, green: 0.643, blue: 0.682, alpha: 1)
        self.label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        self.label.numberOfLines = 0
        self.label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.label.clipsToBounds = true
        self.label.backgroundColor = UIColor(red: 0.925, green: 0.937, blue: 0.945, alpha: 1) 
        self.label.layer.cornerRadius = 8
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.frame = self.contentView.bounds
    }

    func configure(with message: MessageType) {
        switch message.kind {
            case let .custom(value):
                guard let text = value as? String else { return }
                self.label.text = text
            default:
                break
        }
    }
}

class PaddingLabel: UILabel {
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat

    required init(top: CGFloat, bottom: CGFloat, left: CGFloat, right: CGFloat, frame _: CGRect) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        var contentSize = super.intrinsicContentSize
        contentSize.height += self.topInset + self.bottomInset
        contentSize.width += self.leftInset + self.rightInset
        return contentSize
    }
}
