//
//  MessageCellType.swiftess
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import MessageKit

extension MessageCellType {
    private enum Localizables {
        static let chatMessageDeletedOwn = Localizable.string("chat.message.deleted.own")
        static let chatMessageDeletedAlien = Localizable.string("chat.message.deleted.alien")
    }
}

class MessageCellType: MessageType {
    enum Status: String {
        case error
        case delivered
        case received
        case read
    }

    var model: MessageModel?
    var sender: SenderType

    var messageId: String {
        return self.model?.messageId ?? ""
    }

    var sentDate: Date {
        guard let time: TimeInterval = self.model?.timestamp else { return Date() }
        return Date(timeIntervalSince1970: time / 1000)
    }

    var kind: MessageKind {
        switch self.model?.type {
            case .text:
                guard case let .text(value) = self.model?.metadata else { return .text("") }
                
                if let senderType = self.model?.senderType,
                   senderType == .system {
                    // Neutral system message
                     return .custom(value)
                } else {
                    if let isDeleted = self.model?.isDeleted,
                           isDeleted {
                        // Deleted message
                        return .attributedText(getDeletedMessageTextWithStyle())
                    } else {
                        // Common text message
                        return .attributedText(getCommonMessageTextWithStyle(from: value))
                    }
                }
            case .image:
                guard case let .image(url, _, _, _, _) = self.model?.metadata else {
                    return .photo(MessagePhotoModel(url: nil))
                }
                return .photo(MessagePhotoModel(url: url))
            case .file:
                guard case let .file(name, url, size) = self.model?.metadata else { return .text("") }
                
                return .attributedText(getFileMessageTextWithStyle(from: name, url: url, size: size))
            case .custom:
                guard case let .text(value) = self.model?.metadata else { return .text("") }
                return .custom(value)
            default:
                return .text("")
        }
    }

    var status: Status {
        switch self.model?.status {
            case .error:
                return .error
            case .delivered:
                return .delivered
            case .received:
                return .received
            case .read:
                return .read
            case .none:
                return .error
        }
    }

    var icon: UIImage? {
        switch self.status {
            case .error:
                return Asset.messagePendingNew.image
            case .delivered:
                return Asset.messageUnreadNew.image // Asset.messageSentNew.image
            case .received:
                return Asset.messageUnreadNew.image
            case .read:
                return Asset.messageReadNew.image
        }
    }

    init(model: MessageModel?, sender: SenderModel) {
        self.sender = sender
        self.model = model
    }
    
    private func getDeletedMessageTextWithStyle() -> NSMutableAttributedString {
        let text: String = self.model?.isCurrentSender ?? false ? Localizables.chatMessageDeletedOwn : Localizables.chatMessageDeletedAlien
        let fontColor: UIColor = UIColor(hex: "#546E7AFF") ?? .black
        let attributes: [NSAttributedString.Key: Any] = [.font: FontFamily.GothamRounded.book.font(size: 15).customOrDefault ?? UIFont.systemFont(ofSize: 15.0), .foregroundColor: fontColor]
        
        let imageAttachment = NSTextAttachment()
        let iconSize = CGRect(x: 0, y: 0, width: 10, height: 10)
        imageAttachment.bounds = iconSize
        imageAttachment.image = Asset.deleted.image
        
        let fullString = NSMutableAttributedString(string: text,
                                                   attributes: attributes)
        fullString.append(NSAttributedString(string: " "))
        fullString.append(NSAttributedString(attachment: imageAttachment))
        return fullString
    }
    
    private func getCommonMessageTextWithStyle(from value: String) -> NSMutableAttributedString {
        let isCurrentSender = self.sender.senderId == self.model?.fromUserHash
        let textColor: UIColor? = isCurrentSender ? MediQuoSDK.instance.style?.messageTextOutgoingColor : MediQuoSDK.instance.style?.messageTextIncomingColor
        
        let attributedText = NSMutableAttributedString()
        attributedText.append(NSAttributedString(string: value,
                                                 attributes: [
                                                    NSAttributedString.Key.foregroundColor: isUrlMessage(from: value) ? UIColor.link : textColor,
                                                    NSAttributedString.Key.font: FontFamily.GothamRounded.book.font(size: 15).customOrDefault
                                                 ]))
        return attributedText
    }
    
    private func isUrlMessage(from value: String) -> Bool {
        if let url: URL = URL(string: value) {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
    }
    
    private func getFileMessageTextWithStyle(from name: String,
                                             url: URL?,
                                             size: Int?) -> NSMutableAttributedString {
        if let url = url,
           let size = size {
            let formattedSize = ByteCountFormatter.string(fromByteCount: Int64(size), countStyle: ByteCountFormatter.CountStyle.file)
            let attributedText: NSMutableAttributedString = NSMutableAttributedString(string: "\(name) (\(formattedSize))")
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.dark(), range: NSRange(location: 0, length: name.count))
            attributedText.addAttribute(NSAttributedString.Key.link, value: url, range: NSRange(location: 0, length: name.count))
            attributedText.addAttribute(NSAttributedString.Key.font, value: FontFamily.GothamRounded.book.font(size: 15).customOrDefault, range: NSRange(location: 0, length: name.count))
            return attributedText
        } else {
            return NSMutableAttributedString(string: "\(name)")
        }
    }
}

struct MessagePhotoModel: MediaItem {
    public var url: URL?
    public var image: UIImage?
    public var placeholderImage: UIImage = Asset.placeholder.image
    public var size: CGSize = CGSize(width: 240, height: 240)

    init(url: URL? = nil) {
        self.url = url
    }
}
