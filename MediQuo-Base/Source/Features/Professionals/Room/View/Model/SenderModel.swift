//
//  SenderModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 28/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import MessageKit

struct SenderModel: SenderType, Equatable {
    var senderId: String
    var displayName: String
}
