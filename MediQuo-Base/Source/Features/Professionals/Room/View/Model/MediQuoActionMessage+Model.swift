//
//  MediQuoActionMessage+Model.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 10/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

enum MediQuoActionMessage {
    case incoming(String)
    case outgoing(String)
    case none
}
