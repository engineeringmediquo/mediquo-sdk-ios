//
//  VideoCallCurrentCallModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 11/1/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

public struct VideoCallCurrentCallModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case uuid
        case type
        case session
        case avatar
        case name
    }

    public let uuid: String
    public let type: VideoCallType?
    public let session: VideoCallSession
    public let avatar: String?
    public let name: String?

    public init(uuid: String, type: VideoCallType?, session: VideoCallSession, avatar: String?, name: String?) {
        self.uuid = uuid
        self.type = type
        self.session = session
        self.avatar = avatar
        self.name = name
    }
}

public enum VideoCallType: String, Codable {
    case call
    case video
}

public struct VideoCallSession: Codable {
    public enum CodingKeys: String, CodingKey {
        case sessionId = "session_id"
        case customerToken = "customer_token"
    }

    public let sessionId: String
    public let customerToken: String
    
    public init(sessionId: String,
                customerToken: String) {
        self.sessionId = sessionId
        self.customerToken = customerToken
    }
}
