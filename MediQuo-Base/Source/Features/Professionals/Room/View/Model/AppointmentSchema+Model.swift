//
//  AppointmentSchema+Model.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 10/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

public struct AppointmentSDKModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case id
        case type
        case status
        case startDate = "start_date"
        case price
        case fee
        case tpvFee = "tpv_fee"
        case currency
        case from
        case createdAt = "created_at"
        case paymentProvider = "payment_provider"
    }

    public var id: String
    public var type: AppointmentType?
    public var status: StatusType?
    public var startDate: String?
    public var price: Int?
    public var fee: Int?
    public var tpvFee: Int?
    public var currency: String?
    public var from: From?
    public var createdAt: String?
    public var paymentProvider: PaymentProvider?

    public init(id: String,
                type: AppointmentType?,
                status: StatusType?,
                startDate: String?,
                price: Int?,
                fee: Int?,
                tpvFee: Int?,
                currency: String?,
                from: From?,
                createdAt: String?,
                paymentProvider: PaymentProvider?) {
        self.id = id
        self.type = type
        self.status = status
        self.startDate = startDate
        self.price = price
        self.fee = fee
        self.tpvFee = tpvFee
        self.currency = currency
        self.from = from
        self.createdAt = createdAt
        self.paymentProvider = paymentProvider
    }
    
    public static func getFakeAppointmentSDKModel() -> AppointmentSDKModel {
        return AppointmentSDKModel(id: "fake",
                                   type: .future,
                                   status: .declined,
                                   startDate: "2023-03-23",
                                   price: 20,
                                   fee: 10,
                                   tpvFee: 9,
                                   currency: "euro",
                                   from: From(hash: "fake", name: "Cane", avatar: "", speciality: ""),
                                   createdAt: "2023-03-23",
                                   paymentProvider: .mercadopago)
    }
    
    public struct From: Codable, AutoEquatable {
        public enum CodingKeys: String, CodingKey {
            case hash
            case name
            case avatar
            case speciality
        }

        public var hash: String
        public var name: String?
        public var avatar: String?
        public var speciality: String?
        
        public init(hash: String,
             name: String?,
             avatar: String?,
             speciality: String?) {
            self.hash = hash
            self.name = name
            self.avatar = avatar
            self.speciality = speciality
        }
    }

    public enum AppointmentType: String, Codable {
        case past
        case future
    }

    public enum StatusType: String, Codable {
        // Action status
        case pending
        case accepted
        case paymentPending = "payment_pending"

        // Final status
        case unpaid
        case freeOfCharge = "free_of_charge"
        case cancelled
        case declined
        case expired
        case finished
        case owed
    }

    public enum PaymentProvider: String, Codable {
        case mangopay
        case mercadopago
    }

    public var isMangoPay: Bool {
        return self.paymentProvider == .mangopay
    }

    public var isMercadoPago: Bool {
        return self.paymentProvider == .mercadopago
    }
}

extension AppointmentSDKModel: AutoEquatable {}
protocol AutoEquatable {}
