//
//  ChatRoomViewController+Delegates.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 10/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView

// MARK: - MessagesDisplayDelegate
extension ChatRoomViewController: MessageKit.MessagesDisplayDelegate {
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        guard let style = MediQuoSDK.instance.style,
              let isFromCurrentSender: Bool = self.viewModel?.isMessageFromCurrentSender(message: message) else { return ColorName.contrast.color }
        return isFromCurrentSender ? style.bubbleBackgroundOutgoingColor : style.bubbleBackgroundIncomingColor
    }

    // TODO: Cane - Por aquí no está entrando 🥴
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        guard let style = MediQuoSDK.instance.style,
              let isFromCurrentSender: Bool = self.viewModel?.isMessageFromCurrentSender(message: message) else { return ColorName.secondary.color }
        return isFromCurrentSender ? style.messageTextOutgoingColor : style.messageTextIncomingColor
    }
    
    func messageStyle(for message: MessageType,
                      at indexPath: IndexPath,
                      in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let isFromCurrentSender: Bool = self.viewModel?.isMessageFromCurrentSender(message: message) ?? false
      
        let closureIncoming = { (view: MessageContainerView) in
            view.layer.backgroundColor = UIColor.white.cgColor
            let path = UIBezierPath(roundedRect: view.bounds,
                                    byRoundingCorners: [.topRight, .bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 8, height: 8))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
            path.fill()
        }
        
        let closureOut = { (view: MessageContainerView) in
            view.layer.backgroundColor = UIColor(red: 0.906, green: 0.89, blue: 0.945, alpha: 1).cgColor
            let path = UIBezierPath(roundedRect: view.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 8, height: 8))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
            path.fill()
        }
        
        return isFromCurrentSender ? .custom(closureOut) : .custom(closureIncoming)
    }
    
    func messageHeaderView(for indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageReusableView {
        if indexPath.section == 0 {
            let header: MessageCenterHeaderView = messagesCollectionView.dequeueReusableHeaderView(MessageCenterHeaderView.self, for: indexPath) 
            let text = Localizables.headerLegal

            let attributedText = NSMutableAttributedString()
            attributedText.append(NSAttributedString(string: text,
                                                     attributes: [
                                                        NSAttributedString.Key.foregroundColor: UIColor(red: 0.565, green: 0.643, blue: 0.682, alpha: 1),
                                                        NSAttributedString.Key.font: FontFamily.GothamRounded.book.font(size: 12).customOrDefault
                                                     ]))
            header.textLabel.attributedText = attributedText

            guard let superview = header.superview else { return header }
            header.translatesAutoresizingMaskIntoConstraints = false

            header.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(16)
                make.width.equalTo(superview.bounds.size.width - 80)
                make.height.equalTo(64)
            }
            self.updateViewConstraints()

            return header
        } else {
            let header: MessageDateHeaderView = messagesCollectionView.dequeueReusableHeaderView(MessageDateHeaderView.self, for: indexPath)
            guard let dataSource = self.messagesCollectionView.messagesDataSource else { return header }
            let message = dataSource.messageForItem(at: indexPath, in: messagesCollectionView)

            let attributedText = NSMutableAttributedString()
            attributedText.append(NSAttributedString(string: self.viewModel?.formattedHeaderDate(for: message) ?? "",
                                                     attributes: [
                                                        NSAttributedString.Key.foregroundColor: MediQuoSDK.instance.style?.messageTextDateColor ?? UIColor.black,
                                                        NSAttributedString.Key.font: FontFamily.GothamRounded.medium.font(size: 12).customOrDefault
                                                     ]))
            header.dateLabel.attributedText = attributedText
            return header
        }
    }

    func messageFooterView(for indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageReusableView {
        let footer: MessageDateFooterView = messagesCollectionView.dequeueReusableFooterView(MessageDateFooterView.self, for: indexPath)
        guard let dataSource = self.messagesCollectionView.messagesDataSource else { return footer }
        
        let message = dataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        footer.labelPosition(isFromCurrentSender: self.viewModel?.isMessageFromCurrentSender(message: message) ?? false)
        
        if case .custom(_) = message.kind {
            // Remove date if it's a unread message cell
            footer.dateLabel.attributedText = nil
        } else {
            footer.dateLabel.attributedText = self.formattedFooterDate(for: message)
        }
        return footer
    }
    
    func formattedFooterDate(for message: MessageType) -> NSAttributedString? {
        let date: String = DateManager.convertToString(message.sentDate, dateStyle: DateFormatter.Style.none, timeStyle: .short) ?? ""
        let attributedText = NSMutableAttributedString()
        
        attributedText.append(NSAttributedString(string: date,
                                                 attributes: [
                                                    NSAttributedString.Key.foregroundColor: ColorName.dateTextColor.color,
                                                    NSAttributedString.Key.font: FontFamily.GothamRounded.book.font(size: 12).customOrDefault
                                                 ]))

        if let model = message as? MessageCellType,
           let isMessageFromCurrentSender = self.viewModel?.isMessageFromCurrentSender(message: message),
           let icon = model.icon,
           isMessageFromCurrentSender {
            
            let attachment = NSTextAttachment()
            attachment.image = model.icon
            
            switch icon {
                case Asset.messageSentNew.image:
                    // Estado enviado
                    attachment.bounds = CGRect(x: 0, y: -1.5, width: 15, height: 10)
                case Asset.messagePendingNew.image:
                    // Estado Pending
                    attachment.bounds = CGRect(x: 0, y: -2, width: 16, height: 16)
                default:
                    // Estado recibido
                    attachment.bounds = CGRect(x: 0, y: -1.5, width: 20, height: 10)
            }
            attributedText.append(NSAttributedString(attachment: attachment))
        }
        return attributedText
    }

    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at _: IndexPath, in _: MessagesCollectionView) {
        switch message.kind {
            case let .photo(value):
                if let url = value.url {
                    imageView.sd_setImage(with: url, placeholderImage: value.placeholderImage)
                }
            default: break
        }
    }
}

// MARK: - MessagesLayoutDelegate
extension ChatRoomViewController: MessageKit.MessagesLayoutDelegate {
    func headerViewSize(for section: Int, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        if section == 0 {
            return CGSize(width: messagesCollectionView.bounds.width, height: 96)
        } else {
            let sizeHidden = CGSize(width: messagesCollectionView.bounds.width, height: 0)
            let size = CGSize(width: messagesCollectionView.bounds.width, height: 20)
            let indexPath = IndexPath(item: 0, section: section)
            return self.shouldDisplayHeader(at: indexPath, in: messagesCollectionView) ? size : sizeHidden
        }
    }

    private func shouldDisplayHeader(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
        guard let dataSource = self.messagesCollectionView.messagesDataSource else { return false }
        if indexPath.section == 0 { return dataSource.numberOfItems(inSection: 0, in: messagesCollectionView) > 0 }
        let message = dataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        let previousSection = indexPath.section - 1
        let previousIndexPath = IndexPath(item: 0, section: previousSection)
        let previousMessage = dataSource.messageForItem(at: previousIndexPath, in: messagesCollectionView)
        return !Calendar.current.isDate(message.sentDate, inSameDayAs: previousMessage.sentDate)
    }

    func footerViewSize(for section: Int, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: messagesCollectionView.bounds.width, height: 20)
    }
}

// MARK: - MessageCellDelegate
extension ChatRoomViewController: MessageKit.MessageCellDelegate {
    func didTapMessage(in cell: MessageCollectionViewCell) {
        guard let indexPath: IndexPath = self.messagesCollectionView.indexPath(for: cell) else { return }
        self.viewModel?.didTapMessage(at: indexPath, in: self.messagesCollectionView)
    }

    func didTapImage(in cell: MessageCollectionViewCell) {
        guard let indexPath: IndexPath = self.messagesCollectionView.indexPath(for: cell) else { return }
        self.viewModel?.didTapImage(at: indexPath, in: self.messagesCollectionView) { [weak self] url in
            guard let self = self else { return }
            let viewController = ImageDetailViewController()
            viewController.url = url
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true)
        }
    }
}

// MARK: - MessageInputBarDelegate
extension ChatRoomViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, textViewTextDidChangeTo text: String) {
        let isTyping = !text.isEmpty
        self.viewModel?.typing(isTyping: isTyping)
    }

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        self.viewModel?.send(message: text)
        inputBar.inputTextView.text = String()
        inputBar.invalidatePlugins()
    }

    func inputBar(_ inputBar: InputBarAccessoryView, didChangeIntrinsicContentTo size: CGSize) {
        guard let text = inputBar.inputTextView.text, !text.isEmpty else { return }
        self.reloadMessages()
    }
}

// MARK: - UITextViewDelegate
extension ChatRoomViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.messageInputBar.inputTextView {
            self.viewDidLayoutSubviews()
        }
    }
}
// swiftlint:disable:this file_length
// MARK: - Contextual Menu
extension ChatRoomViewController {
    func collectionView(_: UICollectionView,
                        contextMenuConfigurationForItemAt indexPath: IndexPath,
                        point _: CGPoint) -> UIContextMenuConfiguration? {
        
        guard var message = self.viewModel?.getMessagesList()?[indexPath.section],
              let senderType = message.senderType,
              senderType == .ordinary else {
            return nil
        }
       
        return UIContextMenuConfiguration(identifier: message.messageId as NSCopying, previewProvider: nil) { _ in
            let copyAction = UIAction(title: Localizables.sharedCopy,
                                      image: Asset.icCopy.image) { _ in
                if case .text(let content) = message.metadata {
                    UIPasteboard.general.string = content
                }
            }
            
            let reSendAction = UIAction(title: Localizables.reSendAction,
                                        image: Asset.icReSend.image) { _ in
                self.viewModel?.sendMessageToSocket(message: message)
            }
            
            let deleteAction = UIAction(title: Localizables.sharedDelete,
                                        image: Asset.icTrash.image,
                                        attributes: .destructive) { _ in
                self.viewModel?.delete(message: message.messageId)
            }

            var actions: [UIAction] = []
            if message.type == .text {
                actions.append(copyAction)
            }
            
            if message.status == .error {
                actions.append(reSendAction)
            }
            
            let isFromCurrentSender = message.fromUserHash == self.viewModel?.currentSender().senderId
            
            if self.canDeleteMessage,
               isFromCurrentSender,
               !message.isDeleted {
                actions.append(deleteAction)
            }
            return UIMenu(title: "", children: actions)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        previewForHighlightingContextMenuWithConfiguration configuration: UIContextMenuConfiguration) -> UITargetedPreview? {
        guard let identifier = configuration.identifier as? String,
              let section = self.viewModel?.getMessagesList()?.firstIndex(where: { $0.messageId == identifier }),
              let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: section)) as? MessageContentCell
            else {
            return nil
        }
        let parameters = UIPreviewParameters()
        parameters.backgroundColor = cell.messageContainerView.backgroundColor
        return UITargetedPreview(view: cell.messageContainerView, parameters: parameters)
    }
}

// MARK: - AppointmentDelegate
extension ChatRoomViewController: AppointmentDelegate {
    func executeAppointmentDeeplink() {
        guard let appointmentId = self.appointmentId,
              let appointmentStatus = self.appointmentStatus else { return }
        self.didTapAction(by: appointmentId,
                          status: appointmentStatus)
    }

    func clearAppointmentDeeplinkData() {
        self.appointmentId = nil
        self.appointmentStatus = nil
    }
    
    func didTapAction(by appointmentId: String,
                      status: AppointmentSDKModel.StatusType) {
        switch status {
            case .accepted:
                self.chatRoomAppointmentDelegate?.navigateToAppointment(by: appointmentId, status: status)
            case .pending, .unpaid:
                self.chatRoomAppointmentDelegate?.navigateToAppointment(by: appointmentId, status: status)
            default:
                self.dismissAppointmentBanner()
                self.chatRoomAppointmentDelegate?.navigateToAppointment(by: appointmentId, status: status)
        }
    }
}

// MARK: - VideoCallCurrentCallViewDelegate
extension ChatRoomViewController: VideoCallCurrentCallViewDelegate {
    func navigateToCall(with model: VideoCallCurrentCallModel) {
        chatRoomNavigationDelegate?.navigateOnRoomCallRequested(with: model)
    }
}

// MARK: - ErrorMessageViewDelegate
extension ChatRoomViewController: ErrorMessageViewDelegate {
    func closeIconTapped() {
        hideErrorMessage()
    }
}
