//
//  ChatRoomViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//


import MessageKit
import SnapKit

extension ChatRoomViewController {
    enum Localizables {
        static let headerLegal = Localizable.string("mediquo.rgpd.chat.legal")
        static let attachmentGallery = Localizable.string("mediquo.messages.input.attachment.gallery")
        static let attachmentCamera = Localizable.string("mediquo.messages.input.attachment.camera")
        static let attachmentDocument = Localizable.string("mediquo.messages.input.attachment.document")
        static let inputPlaceHolder = Localizable.string("mediquo.messages.input.placeholder")
        
        static let cancel = Localizable.string("mediquo.literal.cancel")
        static let privacy = Localizable.string("mediquo.literal.privacy")
        static let privacyUsageDescription = Localizable.string("mediquo.privacy.photo.usage.description")
        static let privacyPhoto = Localizable.string("mediquo.privacy.photo.usage.settings")
        
        static let sharedCopy = Localizable.string("shared.copy")
        static let sharedDelete = Localizable.string("shared.delete")
        static let reSendAction = Localizable.string("mediquo.messages.error.resend")
        static let writing = Localizable.string("mediquo.chat.writing")
        static let connecting = Localizable.string("connecting")
    }
    
    enum NavigationTitleState {
        case defaultState, typing, connecting
    }
}

public protocol ChatRoomNavigationDelegate {
    func navigateToProfile(with professionalModel: ProfessionalSDKModel?)
    func navigateOnRoomCallRequested(with model: VideoCallCurrentCallModel)
}

public protocol ChatRoomAppointmentDelegate {
    func navigateToAppointment(by appointmentId: String,
                               status: AppointmentSDKModel.StatusType)
    func dismissAppointmentBanner(appointmentId: String)
}

class ChatRoomViewController: MessagesViewController {

    // Dependencies
    var viewModel: ChatRoomViewModel?
    
    // Input Bar
    var buttonsOnPlaceholder: UIStackView = UIStackView()

    // Attachments
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var documentPicker: DocumentExplorer?

    // Fetch control
    var canFetch: Bool = true
    
    // Feature flags
    var canDeleteMessage: Bool = false
    
    // Exposed Delegates
    var chatRoomNavigationDelegate: ChatRoomNavigationDelegate?
    var chatRoomAppointmentDelegate: ChatRoomAppointmentDelegate?

    var appointmentId: String?
    var appointmentStatus: AppointmentSDKModel.StatusType?
    var actionMessage: MediQuoActionMessage = .none
    
    // Header
    var headerTitle: String?
    var headerSubtitle: String?
    
    // Room
    private var room: RoomModel?
    
    // Scroll management
    private var isFirstFetch: Bool = true
    private var isScrolling: Bool = false
    private var isFromBackground: Bool = false
    private var scrollDirectionType: ScrollDirectionType = .unknown
    private var lastContentOffset: CGFloat = 0
    
    // LoadingHelper
    private let loadingHelper: LoadingHelper? = LoadingHelper()
    
    // Banner payments
    private(set) var appointmentBanner: AppointmentBanner = {
        let appointmentBanner = AppointmentBanner(frame: .zero)
        appointmentBanner.isHidden = true
        return appointmentBanner
    }()
    
    // Offline Message View
    private(set) var offlineView: OfflineMessageView = {
        let offlineView = OfflineMessageView(frame: .zero)
        offlineView.isHidden = true
        return offlineView
    }()
    
    // Scroll button
    private(set) var scrollButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderWidth = 1
        button.layer.borderColor = ColorName.lightGray.color.cgColor
        button.setImage(Asset.downArrow.image, for: .normal)
        button.isHidden = true
        button.addTarget(self, action: #selector(scrollToBottom), for: .touchUpInside)
        return button
    }()
    
    // Avatar View
    private(set) var avatarStatusView: AvatarStatusView = {
        let avatarStatusView = AvatarStatusView(frame: .zero)
        avatarStatusView.translatesAutoresizingMaskIntoConstraints = false
        return avatarStatusView
    }()
    
    // Error Message View
    private(set) var errorMessageView: ErrorMessageView = {
        let errorMessageView = ErrorMessageView(frame: .zero)
        errorMessageView.isHidden = true
        return errorMessageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollButton()
        disableLargeTitle()
        enableScrollsToBottomOnKeyboardBeginsEditing()
        viewSafeAreaInsetsDidChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Bindings
        bindNotifications()
        bindViewModels()
        // Inicio del Flujo de Carga
        getSocketStatus()
        // UI
        hideMessageInput()
        setBackButtonNavigationController()
        setupAppointmentBannerView()
        setupOfflineView()
        setupErrorMessageView()
        showLoader()
        self.hideTabbar()
        // Inits
        initMessageKit()
        initInputBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Caché
        setCache()
        // UnBind
        unBindNotifications()
        unBindViewModels()
        // UI
        showTabbar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.scrollButton.isHidden = true        
    }

    deinit {
        CoreLog.business.debug("<<< Deinit called successfully >>>")
    }
        
    func setupAppointmentBannerView() {
        view.addSubview(appointmentBanner)
        view.setNeedsUpdateConstraints()
    }
    
    func setupOfflineView() {
        view.addSubview(offlineView)
        view.setNeedsUpdateConstraints()
    }
    
    func setupErrorMessageView() {
        errorMessageView.delegate = self
        view.addSubview(errorMessageView)
        view.setNeedsUpdateConstraints()
    }
    
    func bindNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        if #available(iOS 13.0, *) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedFromForeground), name: UIScene.willEnterForegroundNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToForeground), name: UIScene.willDeactivateNotification, object: nil)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedFromForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchRoomWhenCallIsEnded),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.Call.Ended, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchRoomWhenCallIsEnded),
                                               name: Notification.Name.MediQuoVideoCall.Sdk.VideoCall.Ended, object: nil)
    }

    func unBindNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        if #available(iOS 13.0, *) {
            NotificationCenter.default.removeObserver(self, name: UIScene.willEnterForegroundNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIScene.willDeactivateNotification, object: nil)
        } else {
            NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        }
    }
    
    private func bindViewModels() {
        self.viewModel?.chatRoomState.subscribe { [weak self] viewState in
            DispatchQueue.main.async { [weak self] in
                guard let viewState = viewState,
                      let self = self else { return }
                self.updateUI(from: viewState)
            }
        }
        
        self.viewModel?.reloadData.subscribe { [weak self] reloadData in
            DispatchQueue.main.async { [weak self] in
                guard let reloadData = reloadData,
                      let self = self else { return }
                if self.isFirstFetch {
                    self.messagesCollectionView.reloadData()
                } else {
                    self.messagesCollectionView.reloadDataAndKeepOffset()
                }
            }
        }
        
        // TODO: Cane - Mejorar este flujo de reconexión con número de intentos
        // TODO: Cane - Añadir gestión de errores tanto de back como de front
        self.viewModel?.isSocketConnected.subscribe { [weak self] isConnected in
            guard let self = self,
                  let isConnected = isConnected else {
                // Reconnecting socket if is disconnected
                self?.initSocket()
                return
            }
        }
        
        self.viewModel?.room.subscribe { room in
            DispatchQueue.main.async { [weak self] in
                guard let self = self,
                      let room = room else { return }
                 self.room = room
            }
        }
        
        self.viewModel?.isTyping.subscribe { [weak self] isTyping in
            DispatchQueue.main.async { [weak self] in
                guard let self = self,
                      let isTyping = isTyping else { return }
                if isTyping {
                    self.setNavigationTitleView(from: .typing)
                } else {
                    self.setNavigationTitleView(from: .defaultState)
                }
            }
        }
        
        self.viewModel?.appointment.subscribe { [weak self] appointment in
            DispatchQueue.main.async {
                guard let self = self,
                      let model = appointment else { return }
                self.initAppointmentBanner(from: model)
            }
        }
                
        self.viewModel?.currentCall.subscribe { [weak self] currentCall in
            DispatchQueue.main.async {
                guard let self = self,
                      let currentCall = currentCall else {
                    self?.hideCurrentCallView()
                    return
                }
                self.showCurrentCallView(currentCall)
            }
        }
        
        self.viewModel?.canDeleteMessage.subscribe { [weak self] canDeleteMessage in
            guard let self = self,
                  let canDeleteMessage = canDeleteMessage else { return }
            self.canDeleteMessage = canDeleteMessage ?? false
        }
    }

    private func unBindViewModels() {
        viewModel?.chatRoomState.unsubscribe()
        viewModel?.isSocketConnected.unsubscribe()
        viewModel?.room.unsubscribe()
        viewModel?.isTyping.unsubscribe()
        viewModel?.appointment.unsubscribe()
        viewModel?.currentCall.unsubscribe()
        viewModel?.canDeleteMessage.unsubscribe()
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 willDisplay cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath) {
        self.showHideScrollButton(by: indexPath.section)
        let isScrolling = self.isScrolling
        let isScrollingUp = self.scrollDirectionType == .up
        let isFirstFetch = self.isFirstFetch

        guard !isScrolling else { return }
        guard isScrollingUp else { return }
        guard !isFirstFetch else { return }

        if indexPath.section == ChatConstants.firstPosition
        || indexPath.section == ChatConstants.threshold {
            self.isScrolling = true
            self.fetchMoreMessages()
        }
    }
}

// MARK: - Update UI
extension ChatRoomViewController {    
    private func updateUI(from state: ChatRoomState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            switch state {
                case .defaultState:
                    self.updateDefaultUI()
                case .errorState:
                    self.updateUIWithError()
            }
        }
    }
    
    private func updateDefaultUI() {
        if self.isFirstFetch {
            self.isFirstFetch = false
            self.reloadFirstMessages()
        } else if self.isScrolling || self.isFromBackground {
            self.isScrolling = false
            self.isFromBackground = false
        } else {
            self.reloadMessages()
        }

        initHeader(profile: self.viewModel?.professionalModel,
                   navigationTitleState: .defaultState)
        hideErrorMessage()
        performActionMessage()
        executeAppointmentDeeplink()
        hideLoader()
        self.view.layoutIfNeeded()
    }
    
    private func updateUIWithError() {
        if self.isFirstFetch {
            self.isFirstFetch = false
            self.reloadFirstMessages()
        } else if self.isScrolling || self.isFromBackground {
            self.isScrolling = false
            self.isFromBackground = false
        } else {
            self.reloadMessages()
        }
        initHeader(profile: self.viewModel?.professionalModel,
                   navigationTitleState: .connecting)
        showErrorMessage()
        performActionMessage()
        executeAppointmentDeeplink()
        hideLoader()
        self.view.layoutIfNeeded()
    }

    private func reloadFirstMessages() {
        UIView.animate(withDuration: 0, animations: { [weak self] in
            guard let self = self else { return }
            self.messagesCollectionView.scrollToLastItem(at: .bottom,
                                                         animated: false)
        }, completion: nil)
    }

    func reloadMessages() {
        UIView.animate(withDuration: 0, animations: { [weak self] in
            guard let self = self else { return }
            self.messagesCollectionView.collectionViewLayout.invalidateLayout()
            self.messagesCollectionView.reloadData()
        }, completion: { [weak self] _ in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.messagesCollectionView.scrollToLastItem(at: .bottom, animated: false)
            }
        })
    }
}

// MARK: - BaseUIState
extension ChatRoomViewController {
    func showLoader() {
        loadingHelper?.showLoading(onView: view)
    }

    func hideLoader() {
        loadingHelper?.hideLoading()
    }
}

// MARK: - Cache
extension ChatRoomViewController {
    private func setCache() {
        self.viewModel?.setMessagesInCache()
        self.viewModel?.setMessageInCache(message: self.messageInputBar.inputTextView.text)
    }
}

// MARK: - Socket Methods
extension ChatRoomViewController {
    @objc private func didBecomeActive() {
        self.getSocketStatus()
    }

    private func getSocketStatus() {
        self.viewModel?.getSocketStatus()
    }

    @objc private func initSocket() {
        self.setNavigationTitleView(from: .connecting)
        self.viewModel?.joinRoom()
    }
}

// MARK: - Fetch Methods
extension ChatRoomViewController {
    @objc private func appMovedToForeground() {
        setCache()
    }
    
    @objc private func appMovedFromForeground() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.isFromBackground = true
            self.getSocketStatus()
        }
    }
    
    private func fetchMoreMessages() {
        self.showLoader()
        self.viewModel?.fetchMoreMessages()
    }
}

// MARK: - User Actions - Navigation
extension ChatRoomViewController {
    @objc func navigateToProfile(_: Any) {
        guard let profile = self.viewModel?.professionalModel else { return }
        chatRoomNavigationDelegate?.navigateToProfile(with: profile)
    }
}

// MARK: - Header View
extension ChatRoomViewController {
    func initHeader(profile: ProfessionalSDKModel?,
                    navigationTitleState: NavigationTitleState) {
        DispatchQueue.main.async { [weak self] in
            
            guard let self = self,
                  let profile = profile,
                  let avatar = profile.avatar,
                  let url = URL(string: avatar) else { return }
 
            self.headerTitle = profile.name
            self.headerSubtitle = profile.title?.shorted(to: 30)
            
            self.setNavigationTitleView(from: navigationTitleState)
            
            self.avatarStatusView.configureView(from: profile.status,
                                                avatarURL: url)
            self.addRightViewOnNavigationController(view: self.avatarStatusView,
                                                    target: self)
        }
    }
}

// MARK: - Appointment Banner
extension ChatRoomViewController {
    private func initAppointmentBanner(from model: AppointmentSDKModel) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.showAppointmentBanner(from: model)
        }
    }
    
    private func showAppointmentBanner(from model: AppointmentSDKModel) {
        self.appointmentBanner.width = self.view.frame.size.width
        self.appointmentBanner.delegate = self
        self.appointmentBanner.configure(model)
        self.appointmentBanner.isHidden = false
    }
}

// MARK: - Offline Message
extension ChatRoomViewController {
    private func showHideOfflineView() {
        guard let profile = self.viewModel?.professionalModel else { return }
        switch profile.status {
            case .online:
                self.hideOfflineView()
            case .offline:
                self.showOfflineView()
            default:
                break
        }
    }
    
    private func showOfflineView() {
        DispatchQueue.main.async { [weak self] in
            self?.offlineView.isHidden = false
            self?.view.layoutIfNeeded()
        }
    }

    private func hideOfflineView() {
        DispatchQueue.main.async { [weak self] in
            self?.offlineView.isHidden = true
            self?.view.layoutIfNeeded()
        }
    }
}

// MARK: - Error Message
extension ChatRoomViewController {
    private func showErrorMessage() {
        hideOfflineView()
        errorMessageView.isHidden = false
    }
    
    func hideErrorMessage() {
        errorMessageView.isHidden = true
        showHideOfflineView()
    }
}

// MARK: - Appointment banner
extension ChatRoomViewController {
    func dismissAppointmentBanner() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.appointmentBanner.isHidden = true
            self.view.layoutIfNeeded()
            self.view.setNeedsUpdateConstraints()
        }
    }
}

// MARK: - Current Call View
extension ChatRoomViewController {
    func showCurrentCallView(_ model: VideoCallCurrentCallModel) {
        guard let avatar = self.viewModel?.professionalModel?.avatar,
              let name = self.viewModel?.professionalModel?.name else { return }
        
        let subView = VideoCallCurrentCallView()
        subView.delegate = self
        let schema = VideoCallCurrentCallModel(uuid: model.uuid,
                                               type: model.type,
                                               session: model.session,
                                               avatar: avatar,
                                               name: name)
        
        subView.configure(schema)
        self.messageInputBar.topStackView.addArrangedSubview(subView)
        UIView.animate(withDuration: 0.1, animations: { subView.alpha = 1.0 },
                       completion: { (_: Bool) in
                           self.viewDidLayoutSubviews()
        })
        self.messageInputBar.topStackView.isHidden = false
        self.messageInputBar.separatorLine.backgroundColor = UIColor.clear
    }

    @objc func hideCurrentCallView() {
        guard let subView: VideoCallCurrentCallView = self.messageInputBar.topStackView.subviews.last as? VideoCallCurrentCallView else { return }
        self.messageInputBar.topStackView.removeArrangedSubview(subView)
        self.messageInputBar.topStackView.isHidden = true
        messageInputBar.topStackView.layoutIfNeeded()
    }
    
    @objc func fetchRoomWhenCallIsEnded() {
        self.viewModel?.fetchRoom()
    }
}

// MARK: - Blocked Room Management
extension ChatRoomViewController {
    func showHideMessageInput() {
        guard let model = self.room else { return }
        switch model.status {
            case .blocked:
                self.hideMessageInput()
            default:
                self.showMessageInput()
        }
    }

    func showMessageInput() {
        self.messageInputBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }

    func hideMessageInput() {
        self.messageInputBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
}

// MARK: - Action Message
extension ChatRoomViewController {
    private func performActionMessage() {
        switch self.actionMessage {
            case let .outgoing(message):
                self.actionMessage = .none
                self.viewModel?.send(message: message)
            case let .incoming(message):
                self.viewModel?.receive(message: message)
                self.actionMessage = .none
            default:
                break
        }
    }
}

// MARK: - Scroll Management
extension ChatRoomViewController {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate, !self.isScrolling { self.scrollDirectionType = .unknown }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset + 50 < scrollView.contentOffset.y {
            self.scrollDirectionType = .down
        } else if self.lastContentOffset - 50 > scrollView.contentOffset.y {
            self.scrollDirectionType = .up
        }
    }

    func scrollViewDidEndDecelerating(_: UIScrollView) {
        if !self.isScrolling { self.scrollDirectionType = .unknown }
    }

    func scrollViewShouldScrollToTop(_: UIScrollView) -> Bool {
        return false
    }
}

// MARK: - Scroll Button - Constraints
extension ChatRoomViewController {
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollButton.circularShape()
    }

    override func updateViewConstraints() {
        self.updateConstraints()
        super.updateViewConstraints()
    }

    private func initScrollButton() {
        self.scrollButton.isHidden = true
        self.messagesCollectionView.insertSubview(self.scrollButton, at: 1)
        self.view.setNeedsUpdateConstraints()
    }

    private func updateConstraints() {
        self.appointmentBanner.snp.makeConstraints { make in
            make.centerX.equalTo(self.view)
            make.top.equalTo(self.view.layoutMarginsGuide).offset(0)
            make.size.equalTo(CGSize(width: self.view.frame.size.width, height: 68))
        }
        
        self.offlineView.snp.makeConstraints { make in
            make.centerX.equalTo(self.view)
            make.bottom.equalToSuperview().offset(-80)
            make.size.equalTo(CGSize(width: self.view.frame.size.width, height: 64))
        }
        
        self.errorMessageView.snp.makeConstraints { make in
            make.centerX.equalTo(self.view)
            make.bottom.equalToSuperview().offset(-90)
            make.size.equalTo(CGSize(width: self.view.frame.size.width, height: 64))
        }

        self.scrollButton.snp.makeConstraints { make in
            make.trailing.equalTo(self.view.snp.trailing).offset(-16)
            let messageInputBarHeight: Int = .init(self.messageInputBar.layer.bounds.size.height)
            make.bottom.equalTo(self.view.snp.bottom).offset(-(messageInputBarHeight + 16))
            make.size.equalTo(CGSize(width: 48, height: 48))
        }
    }

    private func showHideScrollButton(by currentPosition: Int) {
        guard let messages = self.viewModel?.getMessagesList() else { return }
        switch self.scrollDirectionType {
            case .down:
                if currentPosition == (messages.count - ChatConstants.scrollButtonThreshold) {
                    self.scrollButton.isHidden = true
                }
            case .up:
                self.scrollButton.isHidden = false
            case .unknown:
                break
        }
    }

    @objc private func scrollToBottom() {
        self.scrollButton.isHidden = true
        self.messagesCollectionView.scrollToLastItem(at: .bottom, animated: false)
    }
    
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}




