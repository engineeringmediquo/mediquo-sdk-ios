//
//  ChatRoomInputTextStyle.swift
//  MediQuo-Base
//
//  Created by David Martin on 13/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import InputBarAccessoryView
import MessageKit
import UIKit

extension ChatRoomViewController {
    func initMessageKit() {
        self.messagesCollectionView.messagesDataSource = self.viewModel
        self.messagesCollectionView.messagesLayoutDelegate = self
        self.messagesCollectionView.messagesDisplayDelegate = self
        self.messagesCollectionView.messageCellDelegate = self
        
        self.messagesCollectionView.register(MessageDateHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        self.messagesCollectionView.register(MessageCenterHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        self.messagesCollectionView.register(MessageDateFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter)
        
        self.messagesCollectionView.register(CustomCell.self)
        self.messagesCollectionView.register(NeutralCell.self)
        self.messagesCollectionView.register(UnreadMessagesCell.self)
        
        self.messagesCollectionView.collectionViewLayout = CustomMessagesFlowLayout()
        self.messagesCollectionView.backgroundColor = MediQuoSDK.instance.style?.chatBackgroundColor
        self.messagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            for messageSizeCalculator in layout.messageSizeCalculators() {
                messageSizeCalculator.outgoingAvatarSize = .zero
                messageSizeCalculator.incomingAvatarSize = .zero
            }
        }
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        self.imagePicker.mediaTypes = ["public.image", "public.movie"]
        self.imagePicker.videoQuality = .typeHigh
        self.additionalBottomInset = 16
    }

    func initInputBar() {
        showMessageInput()
        self.hidesBottomBarWhenPushed = true
        self.scrollsToBottomOnKeyboardBeginsEditing = true

        self.messageInputBar.delegate = self
        self.messageInputBar.backgroundView.backgroundColor = .white
        self.messageInputBar.separatorLine.isHidden = true

        self.messageInputBar.inputTextView.delegate = self
        self.messageInputBar.inputTextView.resignFirstResponder()
        self.messageInputBar.inputTextView.layer.cornerRadius = 18
        self.messageInputBar.inputTextView.layer.borderWidth = 1
        self.messageInputBar.inputTextView.layer.borderColor = ColorName.gray.color.cgColor
        self.messageInputBar.inputTextView.placeholderLabel.text = Localizables.inputPlaceHolder
        self.messageInputBar.inputTextView.font = FontFamily.GothamRounded.book.font(size: 16).customOrDefault
        self.messageInputBar.inputTextView.textColor = ColorName.ultraDarkGray.color
        self.messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
        self.messageInputBar.inputTextView.backgroundColor = .white
        self.messageInputBar.inputTextView.text = viewModel?.getMessageFromCache()
        self.messageInputBar.sendButton.image = Asset.sendIcon.image 
        self.messageInputBar.sendButton.title = nil
        self.messageInputBar.clipsToBounds = true
        self.messageInputBar.invalidatePlugins()
        self.messageInputBar.invalidateIntrinsicContentSize()
        
        self.buttonsOnPlaceholder.axis = .horizontal
        self.buttonsOnPlaceholder.distribution = .fillEqually
        self.buttonsOnPlaceholder.alignment = .fill
        
        if self.buttonsOnPlaceholder.subviews.isEmpty {
            let clipButton = self.createButtonOnPlaceHolder()
            clipButton.addTarget(self, action: #selector(self.attachMessage), for: .touchUpInside)
            self.buttonsOnPlaceholder.addArrangedSubview(clipButton)
            self.messageInputBar.inputTextView.addSubview(self.buttonsOnPlaceholder)
            self.constraintsStackView()
        }
        self.listenActionInputBar()
    }
        
    private func createButtonOnPlaceHolder() -> UIButton {
        let image = Asset.paperclip.image
        let button = UIButton()
        button.setImage(image, for: .normal)

        var buttonFrame = button.frame
        buttonFrame.size = image.size
        button.frame = buttonFrame

        self.buttonsOnPlaceholder.frame = buttonFrame
        button.tintColor = ColorName.ultraDarkGray.color
        
        return button
    }

    @objc private func attachMessage() {
        let item = InputBarButtonItem(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.documentPicker(item: item)
    }

    func constraintsStackView() {
        self.buttonsOnPlaceholder.snp.makeConstraints { make in
            make.right.equalTo(self.messageInputBar.rightStackView.snp.left).offset(-self.messageInputBar.middleContentViewPadding.right - 8)
            make.centerY.equalTo(self.messageInputBar.inputTextView.snp.centerY)
        }
        self.updateViewConstraints()
    }

    private func listenActionInputBar() {
        self.messageInputBar.sendButton
            .onDisabled { [weak self] _ in
                self?.buttonsOnPlaceholder.isHidden = false
            }.onTextViewDidChange { [weak self] _, textView in
                self?.buttonsOnPlaceholder.isHidden = !textView.text.isEmpty
            }
    }
    
    func enableScrollsToBottomOnKeyboardBeginsEditing() {
        self.scrollsToBottomOnKeyboardBeginsEditing = true
    }
    
    func setNavigationTitleView(from state: NavigationTitleState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            switch state {
                case .defaultState:
                    self.navigationItem.titleView = self.getTitleView(title: self.headerTitle,
                                                                      subtitle: self.headerSubtitle)
                case .typing:
                    self.navigationItem.titleView = self.getTitleView(title: self.headerTitle,
                                                                      subtitle: Localizables.writing)
                case .connecting:
                    let titleView = ConnectingTitleView() 
                    titleView.configureView(with: self.headerTitle,
                                            and: Localizables.connecting)
                    self.navigationItem.titleView = titleView
            }
        }
    }
    
    func getTitleView(title: String?, subtitle: String? = nil) -> UIView {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -4, width: 0, height: 0))
        titleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        titleLabel.textColor = MediQuoSDK.instance.style?.primaryContrastColor
        titleLabel.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        titleLabel.text = title
        titleLabel.sizeToFit()

        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: 0, height: 0))
        subtitleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        subtitleLabel.textColor = MediQuoSDK.instance.style?.chatSubTitleViewColor 
        subtitleLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()

        let titleView = UIView(frame: CGRect(x: 0,
                                             y: 0,
                                             width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width),
                                             height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)

        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(self.navigateToProfile))
        
        self.navigationItem.titleView?.isUserInteractionEnabled = true
        titleView.addGestureRecognizer(recognizer)
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width

        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }

        return titleView
    }
}
