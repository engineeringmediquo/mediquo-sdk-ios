//
//  LastMessageModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

public struct LastMessageModel {
    let type: MessageModel.Kind
    let timestamp: Date
    let metadata: MessageModel.Metadata
    let string: String
    let status: MessageStatus
    let fromUserHash: String
}

extension LastMessageModel: Codable {
    enum CodingKeys: String, CodingKey {
        case createdAt
        case type
        case string
        case status
        case fromUserHash
    }

    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decode(MessageModel.Kind.self, forKey: CodingKeys.type)
        if let timestamp: String = try? container.decode(String.self, forKey: CodingKeys.createdAt) {
            self.timestamp = DateManager.convertToDateFromRemoteISO(timestamp) ?? Date()
        } else {
            self.timestamp = Date()
        }
        self.string = try container.decode(String.self, forKey: CodingKeys.string)
        self.status = try container.decode(MessageStatus.self, forKey: CodingKeys.status)
        self.metadata = try MessageModel.Metadata(from: decoder)
        if let fromUserHash: String = try? container.decode(String.self, forKey: CodingKeys.fromUserHash) {
            self.fromUserHash = fromUserHash
        } else {
            self.fromUserHash = ""
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.type, forKey: CodingKeys.type)
        try container.encode(self.timestamp, forKey: CodingKeys.createdAt)
        try container.encode(self.string, forKey: CodingKeys.string)
        try container.encode(self.status, forKey: CodingKeys.status)
        try self.metadata.encode(to: encoder)
        try container.encode(self.fromUserHash, forKey: CodingKeys.fromUserHash)
    }
}

public enum MessageStatus: Int, Codable {
    case unread = 1
    case send = 2
    case read = 3

    var image: UIImage {
        switch self {
        case .read:
            return Asset.messageRead.image
        default:
            return Asset.messageUnread.image
        }
    }
}
