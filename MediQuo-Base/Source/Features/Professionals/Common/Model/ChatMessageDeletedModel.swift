//
//  ChatMessageDeletedModel.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 31/1/23.
//

import Foundation

public struct ChatMessageDeletedModel: Hashable, Codable {
    public let messageId: String
    public let roomId: Int
    public let userHash: String
    
    enum CodingKeys: String, CodingKey {
        case messageId = "message_id"
        case roomId = "room_id"
        case userHash = "user_hash"
    }
    
    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.messageId = try container.decode(String.self, forKey: CodingKeys.messageId)
        self.roomId = try container.decode(Int.self, forKey: CodingKeys.roomId)
        self.userHash = try container.decode(String.self, forKey: CodingKeys.userHash)
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.messageId, forKey: CodingKeys.messageId)
        try container.encode(self.userHash, forKey: CodingKeys.userHash)
        try container.encode(self.roomId, forKey: CodingKeys.roomId)
    }
}
