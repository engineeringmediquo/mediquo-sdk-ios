//
//  SpecialityModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct SpecialityModel: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
    }

    let id: Int
    let name: String?
    let code: String?
}
