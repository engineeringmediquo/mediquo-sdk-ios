//
//  ProfessionalSDK+Model.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 2/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

public struct ProfessionalSDKModel {
    let id: Int?
    let hash: String
    let avatar: String?
    let avatarURL: URL?
    public let name: String?
    let title: String?
    let overview: String?
    let collegiateNumber: String?
    let isAvailable: Bool?
    let specialityId: Int?
    let lastMessageTimestamp: TimeInterval?
    let pending: Int?
    let typeLastMessage: String?
    public let fromUserHash: String?
    let stringLastMessage: String?
    let currentAppointmentType: String?
    let currentAppointmentStatus: String?
    let statusLastMessage: Int?
    let isOwnMessage: Bool?
    let status: SDKStatus?
    let room: SDKRoomModel?
    let speciality: SDKSpecialityModel?
    let license: SDKLicenseModel?
    public let channel: String?
    var avatarUIImage: UIImage?
    
    public init(id: Int?,
                hash: String,
                avatar: String?,
                avatarURL: URL?,
                name: String?,
                title: String?,
                overview: String?,
                collegiateNumber: String?,
                isAvailable: Bool?,
                specialityId: Int?,
                lastMessageTimestamp: TimeInterval?,
                pending: Int,
                typeLastMessage: String?,
                fromUserHash: String?,
                stringLastMessage: String?,
                currentAppointmentType: String?,
                currentAppointmentStatus: String?,
                statusLastMessage: Int?,
                isOwnMessage: Bool?,
                status: SDKStatus?,
                room: SDKRoomModel?,
                speciality: SDKSpecialityModel?,
                license: SDKLicenseModel?,
                channel: String?) {
        self.id = id
        self.hash = hash
        self.fromUserHash = fromUserHash
        self.avatar = avatar
        self.avatarURL = avatarURL
        self.name = name
        self.title = title
        self.overview = overview
        self.collegiateNumber = collegiateNumber
        self.isAvailable = isAvailable
        self.specialityId = specialityId
        self.lastMessageTimestamp = lastMessageTimestamp
        self.pending = pending
        self.typeLastMessage = typeLastMessage
        self.stringLastMessage = stringLastMessage
        self.currentAppointmentType = currentAppointmentType
        self.currentAppointmentStatus = currentAppointmentStatus
        self.statusLastMessage = statusLastMessage
        self.isOwnMessage = isOwnMessage
        self.status = status
        self.room = room
        self.speciality = speciality
        self.license = license
        self.channel = channel
        
        if let imageUrl = self.avatarURL,
           let data = try? Data(contentsOf: imageUrl),
           let image = UIImage(data: data) {
            self.avatarUIImage = image
        }
    }
    
    public init(from professional: ProfessionalModel) {
        self.id = professional.id
        self.hash = professional.hash
        self.fromUserHash = professional.hash
        self.avatar = professional.avatar
        self.avatarURL = URL(string: professional.avatar ?? "")
        self.name = professional.name
        self.title = professional.title
        self.overview = professional.overview
        self.collegiateNumber = professional.collegiateNumber
        self.isAvailable = professional.isAvailable
        self.specialityId = professional.specialityId
        self.lastMessageTimestamp =  professional.room?.lastMessage?.timestamp.timeIntervalSince1970
        self.pending = professional.room?.pendingMessages
        self.typeLastMessage = professional.room?.lastMessage?.type.rawValue
        self.stringLastMessage = professional.room?.lastMessage?.string
        self.status = SDKStatus(rawValue: professional.status?.rawValue ?? "")
        self.statusLastMessage = professional.room?.lastMessage?.status.rawValue
        self.speciality = SDKSpecialityModel(id: professional.speciality?.id ?? 0,
                                             name: professional.speciality?.name,
                                             code: professional.speciality?.code)
        self.license = SDKLicenseModel(organizationName: professional.license?.organization?.name,
                                       organizationColor: professional.license?.organization?.color,
                                       organizationBackgroundColor: professional.license?.organization?.backgroundColor)
        
        if let room = professional.room {
            self.room = SDKRoomModel(id: room.id,
                                     roomId: room.roomId,
                                     lastMessage: room.lastMessage,
                                     pendingMessages: room.pendingMessages,
                                     status: SDKRoomModel.RoomSDKStatus(rawValue: room.status?.rawValue ?? ""),
                                     title: professional.room?.title,
                                     avatar: professional.room?.avatar,
                                     meta: professional.room?.meta,
                                     currentCall: professional.room?.currentCall)
        } else {
            self.room = nil
        }
        
        self.isOwnMessage = ProfileManager().getProfile()?.userHash == professional.room?.lastMessage?.fromUserHash ?? "" 
        if let imageUrl = self.avatarURL,
           let data = try? Data(contentsOf: imageUrl),
           let image = UIImage(data: data) {
            self.avatarUIImage = image
        }
        
        // TODO: Cane - Revisar estas properties
        self.currentAppointmentType = nil
        self.currentAppointmentStatus = nil
        self.channel = nil
    }
    
    func isOwnUserMessage(messageHash: String) -> Bool {
        return ProfileManager().getProfile()?.userHash == messageHash
    }
}
 
public extension ProfessionalSDKModel {
    var lastMessageDate: Date? {
        guard let timestamp = self.lastMessageTimestamp, timestamp != 0 else {
            return nil
        }
        return Date(timeIntervalSince1970: timestamp)
    }

    var appointmentType: AppointmentType? {
        guard let type = self.currentAppointmentType else { return nil }
        return AppointmentType(rawValue: type)
    }

    var appointmentStatus: StatusType? {
        guard let status = self.currentAppointmentStatus else { return nil }
        return StatusType(rawValue: status)
    }
}

public enum AppointmentType: String, Codable {
    case past
    case future
}

public enum StatusType: String, Codable {
    // Action status
    case pending
    case accepted
    case paymentPending = "payment_pending"

    // Final status
    case unpaid
    case freeOfCharge = "free_of_charge"
    case cancelled
    case declined
    case expired
    case finished
    case owed
}

public enum SDKStatus: String, Codable {
    case offline
    case online
    case busy
    case unknown

    var color: UIColor? {
        switch self {
        case .online:
            return UIColor.green //ColorName.green.color
        case .offline:
            return UIColor.gray // ColorName.gray.color
        default:
            return UIColor.gray // ColorName.gray.color
        }
    }
    
    public init?(rawValue: String) {
        switch rawValue {
            case "offline":
                self = .offline
            case "online":
                self = .online
            case "busy":
                self = .busy
            case "unknown":
                self = .unknown
            default:
                self = .unknown
        }
    }
}

public protocol ContactStatusViewModelType {
    var color: UIColor? { get }
}


public enum ContactStatusViewModel: String, ContactStatusViewModelType{
    case unknown
    case offline
    case online
}

public extension ContactStatusViewModel {
    public var color: UIColor? {
        switch self {
            case .online:
                return UIColor.listProfressionalOnline()
            case .offline:
                return UIColor.gray2()
            case .unknown:
                return UIColor.gray2()
        }
    }
}

public extension ContactStatusViewModel {
    init(from model: SDKStatus) {
        switch model {
            case .unknown:
                self = .unknown
            case .offline:
                self = .offline
            case .online:
                self = .online
            default:
                self = .unknown
        }
    }
}

public enum ContactStatusModelSDK: String {
    case unknown
    case offline
    case online
}

public extension ContactStatusModelSDK {
    static let count: Int = 3
    static let allCases: [ContactStatusModelSDK] = [
        .unknown,
        .offline,
        .online
    ]
}

public struct SDKRoomModel: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case id
        case roomId = "room_id"
        case lastMessage = "last_message"
        case pendingMessages = "pending_messages"
        case status
        case title
        case avatar
        case meta
        case currentCall = "current_call"
    }

    let id: Int?
    let roomId: Int?
    let lastMessage: LastMessageModel?
    let pendingMessages: Int?
    let status: RoomSDKStatus?
    let title: String?
    let avatar: String?
    let meta: MetaRoomModel?
    let currentCall: VideoCallCurrentCallModel?

    public enum RoomSDKStatus: String, Codable {
        case accepted
        case blocked
        case pending
    }

    public static func == (lhs: SDKRoomModel, rhs: SDKRoomModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    public init(id: Int?,
                roomId: Int?,
                lastMessage: LastMessageModel?,
                pendingMessages: Int?,
                status: RoomSDKStatus?,
                title: String?,
                avatar: String?,
                meta: MetaRoomModel?,
                currentCall: VideoCallCurrentCallModel?) {
        self.id = id
        self.roomId = roomId
        self.lastMessage = lastMessage
        self.pendingMessages = pendingMessages
        self.status = status
        self.title = title
        self.avatar = avatar
        self.meta = meta
        self.currentCall = currentCall
    }
    
    public init(roomId: Int?) {
        self.id = nil
        self.roomId = roomId
        self.lastMessage = nil
        self.pendingMessages = nil
        self.status = nil
        self.title = nil
        self.avatar = nil
        self.meta = nil
        self.currentCall = nil
    }
}



public struct SDKSpecialityModel: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
    }

    let id: Int
    let name: String?
    let code: String?
}

public struct SDKLicenseModel {
     let organizationName: String?
     let organizationColor: String?
     let organizationBackgroundColor: String?
    
    public init(organizationName: String?,
                organizationColor: String?,
                organizationBackgroundColor: String?) {
        self.organizationName = organizationName
        self.organizationColor = organizationColor
        self.organizationBackgroundColor = organizationBackgroundColor
    }
}

