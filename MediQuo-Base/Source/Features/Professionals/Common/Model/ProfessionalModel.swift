//
//  ProfessionalModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

public struct ProfessionalModel: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case hash
        case status
        case avatar
        case name
        case title
        case overview
        case room
        case speciality
        case collegiateNumber = "collegiate_number"
        case isAvailable = "is_available"
        case license
        case specialityId = "speciality_id"
    }

    let id: Int?
    let hash: String
    let status: Status?
    let avatar: String?
    let name: String?
    let title: String?
    let overview: String?
    let room: RoomModel?
    let speciality: SpecialityModel?
    let collegiateNumber: String?
    let isAvailable: Bool?
    let license: LicenseModel?
    let specialityId: Int?
}

enum Status: String, Codable {
    case offline
    case online
    case busy
    case unknown

    var color: UIColor? {
        switch self {
        case .online:
            return ColorName.green.color
        case .offline:
            return ColorName.gray.color
        default:
            return ColorName.gray.color
        }
    }
}
