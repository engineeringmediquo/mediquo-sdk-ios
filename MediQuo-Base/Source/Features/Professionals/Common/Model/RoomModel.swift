//
//  RoomModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public struct RoomModel: Codable, Equatable { 
    enum CodingKeys: String, CodingKey {
        case id
        case roomId = "room_id"
        case lastMessage = "last_message"
        case pendingMessages = "pending_messages"
        case status
        case title
        case avatar
        case meta
        case currentCall = "current_call"
        case currentAppointment = "current_appointment"
    }

    var id: Int?
    var roomId: Int?
    var lastMessage: LastMessageModel?
    var pendingMessages: Int?
    var status: RoomStatus?
    var title: String?
    var avatar: String?
    var meta: MetaRoomModel?
    var currentCall: VideoCallCurrentCallModel?
    var currentAppointment: AppointmentSDKModel?

    public enum RoomStatus: String, Codable {
        case accepted
        case blocked
        case pending
    }

    public static func == (lhs: RoomModel, rhs: RoomModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    public init(roomId: Int?,
                status: RoomStatus?,
                currentAppointment: AppointmentSDKModel?,
                currentCall: VideoCallCurrentCallModel?,
                meta: MetaRoomModel?) {
        self.roomId = roomId
        self.status = status
        self.currentAppointment = currentAppointment
        self.currentCall = currentCall
        self.meta = meta
    }
}

public struct MetaRoomModel: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case userHash = "hash"
        case isPremium = "is_premium"
        case bannedAt = "banned_at"
        case specialityColor = "speciality_color"
        case specialityAlias = "speciality_alias"
        case countryCode = "country_code"
        case gender
        case birthDate = "birth_date"
        case permissions
    }

    var userHash: String?
    var isPremium: Bool?
    var bannedAt: String?
    var specialityColor: String?
    var specialityAlias: String?
    var countryCode: String?
    var gender: String?
    var birthDate: String?
    var permissions: Permissions?
    
    public init(permissions: Permissions?) {
        self.permissions = permissions
    }
}

public struct Permissions: Codable, Equatable {
    public enum CodingKeys: String, CodingKey {
        case chat
    }
    public var chat: Chat?
    
    public init(chat: Chat?) {
        self.chat = chat
    }
}

public struct Chat: Codable, Equatable {
    public enum CodingKeys: String, CodingKey {
        case canDeleteMessages = "can_delete_messages"
    }
    public var canDeleteMessages: Bool?
    
    public init(canDeleteMessages: Bool?) {
        self.canDeleteMessages = canDeleteMessages
    }
}
