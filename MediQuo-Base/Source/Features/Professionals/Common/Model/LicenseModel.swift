//
//  LicenseModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 18/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct LicenseModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case name
        case organization
    }

    public let name: String?
    public let organization: OrganizationModel?
}
