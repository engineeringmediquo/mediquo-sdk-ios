//
//  UnreadMessagesModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct UnreadMessagesModel: Codable {
    enum CodingKeys: String, CodingKey {
        case total
    }

    public let total: Int
}
