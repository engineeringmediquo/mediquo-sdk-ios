//
//  MessageModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

public struct MessageModel: Hashable {
    public enum Kind: String, Codable {
        case text = "string"
        case image
        case file
        case custom
    }

    public enum MessageStatusSDK: Int, Codable {
        case error = 0
        case delivered = 1
        case received = 2
        case read = 3

        public mutating func update(status: MessageStatusSDK) {
            self = status
        }
    }

    public enum PivotMode: Int {
        case older = 1
        case newer = 2
    }

    public enum Metadata: Hashable {
        case text(content: String)
        case image(url: URL?, thumb: URL?, width: Int?, height: Int?, size: Int?)
        case file(name: String, url: URL?, size: Int?)
        case none
    }

    public let messageId: String
    public let fromUserHash: String
    public let type: Kind
    public var roomId: Int
    public var status: MessageStatusSDK
    public var timestamp: TimeInterval? = nil
    public var deletedAt: TimeInterval? = nil
    public let metadata: Metadata
    public var fileName: String?
    public var data: Data?
    public let senderType: SenderType?
    public var isCurrentSender: Bool = false

    public enum SenderType: String, Codable {
        case ordinary
        case system
        case unread
    }
    
    public static func == (lhs: MessageModel, rhs: MessageModel) -> Bool {
        return lhs.messageId == rhs.messageId
    }

    private let unreadMessagesId = "UNREAD_MESSAGES"

    public var isUnreadMessage: Bool {
        return self.messageId == self.unreadMessagesId
    }

    public var isDeleted: Bool = false
}

extension MessageModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case messageId
        case fromUserHash
        case roomId
        case createdAt
        case deletedAt
        case timestamp = "time"
        case status
        case type
        case senderType
        case fileName
        case data
    }
    
    public init(messageApiModel: MessageApiModel) {
        self.messageId = messageApiModel.messageId
        self.fromUserHash = messageApiModel.fromUserHash
        self.type = messageApiModel.type
        self.roomId = messageApiModel.roomId
        self.status = messageApiModel.status
        if let date: Date = DateManager.convertToDateFromRemoteISO(messageApiModel.createdAt) {
            self.timestamp = date.timeIntervalSince1970 * 1000
        }
        
        if let deletedAt = messageApiModel.deletedAt {
            self.deletedAt = TimeInterval(deletedAt)
        }
        switch messageApiModel.type {
        case .text:
            self.metadata = .text(content: messageApiModel.string)
        case .image:
            self.metadata = .image(url: URL(string: messageApiModel.attachment?.url ?? ""),
                                   thumb: URL(string: messageApiModel.attachment?.url ?? ""),
                                   width: messageApiModel.attachment?.meta?.width,
                                   height: messageApiModel.attachment?.meta?.height,
                                   size: messageApiModel.attachment?.fileSize)
        case .file:
            self.metadata = .file(name: messageApiModel.attachment?.url ?? "",
                                  url: URL(string: messageApiModel.attachment?.url ?? ""),
                                  size: messageApiModel.attachment?.fileSize)
        case .custom:
            self.metadata = .none
        }
        self.fileName = nil
        self.data = nil
        self.senderType = messageApiModel.senderType
    }
    
    public init(text: String, roomId: Int, messageId: String?, fromUserHash: String, senderType: SenderType?) {
        self.roomId = roomId
        self.messageId = messageId ?? ""
        self.type = .text
        self.fromUserHash = fromUserHash
        self.status = .error
        self.timestamp = Date().timeIntervalSince1970 * 1000
        self.deletedAt = nil
        self.metadata = .text(content: text)
        self.fileName = nil
        self.data = nil
        self.senderType = senderType
    }

    public init(image: UIImage,
                roomId: Int,
                messageId: String?,
                fromUserHash: String,
                fileName: String? = nil,
                metadata: Metadata,
                senderType: SenderType?) {
        self.roomId = roomId
        self.messageId = messageId ?? ""
        self.type = .image
        self.fromUserHash = fromUserHash
        self.status = .error
        self.timestamp = Date().timeIntervalSince1970 * 1000
        self.deletedAt = nil
        self.metadata = metadata
        self.fileName = fileName
        self.data = image.resizedData().0
        self.senderType = senderType
    }

    public init(file: Data, roomId: Int, messageId: String?, fromUserHash: String, fileName: String? = nil, metadata: Metadata, senderType: SenderType?) {
        self.roomId = roomId
        self.messageId = messageId ?? ""
        self.type = .file
        self.fromUserHash = fromUserHash
        self.status = .error
        self.timestamp = Date().timeIntervalSince1970 * 1000
        self.deletedAt = nil
        self.metadata = metadata
        self.fileName = fileName
        self.data = file
        self.senderType = senderType
    }

    public init(unreadCount: Int, roomId: Int) {
        self.roomId = roomId
        self.messageId = self.unreadMessagesId
        self.type = .custom
        self.fromUserHash = ""
        self.status = .read
        self.timestamp = Date().timeIntervalSince1970 * 1000
        self.deletedAt = nil
        self.metadata = .text(content: "\(unreadCount)")
        self.fileName = nil
        self.data = nil
        self.senderType = .unread
    }

    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        
        self.messageId = try container.decode(String.self, forKey: CodingKeys.messageId)
        self.roomId = try container.decode(Int.self, forKey: CodingKeys.roomId)
        self.fromUserHash = try container.decode(String.self, forKey: CodingKeys.fromUserHash)
        self.type = try container.decode(Kind.self, forKey: CodingKeys.type)
        self.status = try container.decode(MessageStatusSDK.self, forKey: CodingKeys.status)
        if container.contains(CodingKeys.timestamp) {
            self.timestamp = try container.decode(TimeInterval.self, forKey: CodingKeys.timestamp)
        } else {
            self.timestamp = try container.decode(TimeInterval.self, forKey: CodingKeys.createdAt)
        }
        self.deletedAt = container.contains(CodingKeys.deletedAt) ? try? container.decode(TimeInterval.self, forKey: CodingKeys.deletedAt) : nil
        self.metadata = try Metadata(from: decoder)
        self.senderType = try? container.decode(SenderType.self, forKey: CodingKeys.senderType)
        self.fileName = container.contains(CodingKeys.fileName) ? try? container.decode(String.self, forKey: CodingKeys.fileName) : nil
        self.data = container.contains(CodingKeys.data) ? try? container.decode(Data.self, forKey: CodingKeys.data) : nil
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.messageId, forKey: CodingKeys.messageId)
        try? container.encode(self.fromUserHash, forKey: CodingKeys.fromUserHash)
        try? container.encode(self.type, forKey: CodingKeys.type)
        try? container.encode(self.roomId, forKey: CodingKeys.roomId)
        try? container.encode(self.status, forKey: CodingKeys.status)
        try? container.encode(self.timestamp, forKey: CodingKeys.timestamp)
        try? container.encode(self.deletedAt, forKey: CodingKeys.deletedAt)
        try? self.metadata.encode(to: encoder)
        try? container.encode(self.senderType, forKey: CodingKeys.senderType)
        try? container.encode(self.fileName, forKey: CodingKeys.fileName)
        try? container.encode(self.data, forKey: CodingKeys.data)
    }

    public var description: String {
        switch self.metadata {
        case let .text(value):
            return value
        case .image:
            guard let data = data else { return "" }
            return data.base64EncodedString(options: .endLineWithLineFeed)
        case .file:
            guard let data = data else { return "" }
            return data.base64EncodedString(options: .endLineWithLineFeed)
        case .none:
            return ""
        }
    }

    mutating public func update(status: MessageStatusSDK) {
        self.status = status
    }
}

extension MessageModel.Metadata: Codable {
    public enum TextCodingKeys: String, CodingKey {
        case content = "string"
    }

    public enum ImageCodingKeys: String, CodingKey {
        case url = "imageUrl"
        case thumb = "thumbUrl"
        case width = "imageWidth"
        case height = "imageHeight"
        case size = "fileSize"
    }

    public enum FileCodingKeys: String, CodingKey {
        case url = "fileUrl"
        case name = "fileName"
        case size = "fileSize"
    }

    public init(from decoder: Decoder) throws {
        let image: KeyedDecodingContainer<ImageCodingKeys> = try decoder.container(keyedBy: ImageCodingKeys.self)
        let file: KeyedDecodingContainer<FileCodingKeys> = try decoder.container(keyedBy: FileCodingKeys.self)
        let text: KeyedDecodingContainer<TextCodingKeys> = try decoder.container(keyedBy: TextCodingKeys.self)

        if image.contains(ImageCodingKeys.url) {
            let url: URL? = try? image.decode(URL.self, forKey: ImageCodingKeys.url)
            let thumb: URL? = try? image.decode(URL.self, forKey: ImageCodingKeys.thumb)
            let width: Int? = try? image.decode(Int.self, forKey: ImageCodingKeys.width)
            let height: Int? = try? image.decode(Int.self, forKey: ImageCodingKeys.height)
            let size: Int? = try? image.decode(Int.self, forKey: ImageCodingKeys.size)
            self = .image(url: url, thumb: thumb, width: width, height: height, size: size)
        } else if file.contains(FileCodingKeys.url) {
            let url: URL? = try? file.decode(URL.self, forKey: FileCodingKeys.url)
            let name: String? = try? file.decode(String.self, forKey: FileCodingKeys.name)
            let size: Int? = try? file.decode(Int.self, forKey: FileCodingKeys.size)
            self = .file(name: name ?? "", url: url, size: size)
        } else if text.contains(TextCodingKeys.content) {
            let value: String = try text.decode(String.self, forKey: TextCodingKeys.content)
            self = .text(content: value)
        } else {
            self = .none
        }
    }

    public func encode(to encoder: Encoder) throws {
        switch self {
        case let .image(url, thumb, width, height, size):
            var image: KeyedEncodingContainer<ImageCodingKeys> = encoder.container(keyedBy: ImageCodingKeys.self)
            try image.encode(url, forKey: ImageCodingKeys.url)
            try image.encode(thumb, forKey: ImageCodingKeys.thumb)
            try image.encode(width, forKey: ImageCodingKeys.width)
            try image.encode(height, forKey: ImageCodingKeys.height)
            try image.encode(size, forKey: ImageCodingKeys.size)
        case let .file(name, url, size):
            var file: KeyedEncodingContainer<FileCodingKeys> = encoder.container(keyedBy: FileCodingKeys.self)
            try file.encode(name, forKey: MessageModel.Metadata.FileCodingKeys.name)
            try file.encode(url, forKey: MessageModel.Metadata.FileCodingKeys.url)
            try file.encode(size, forKey: MessageModel.Metadata.FileCodingKeys.size)
        case let .text(value):
            var text: KeyedEncodingContainer<TextCodingKeys> = encoder.container(keyedBy: TextCodingKeys.self)
            try text.encode(value, forKey: TextCodingKeys.content)
        case .none:
            NSLog("[MessageSchema.Metadata] Encoding '\(self)' writes no attachment fields")
            return
        }
    }

    public var name: String? {
        switch self {
            case let .image(_, url, _, _, _):
                return url?.lastPathComponent
            case let .file(name, _, _):
                return name
            default:
                return nil
        }
    }

    public var isImage: Bool {
        switch self {
        case .image:
            return true
        default:
            return false
        }
    }

    public var isFile: Bool {
        switch self {
        case .file:
            return true
        default:
            return false
        }
    }
}
