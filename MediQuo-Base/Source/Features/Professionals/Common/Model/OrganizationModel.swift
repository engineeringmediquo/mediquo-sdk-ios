//
//  OrganizationModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 18/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public struct OrganizationModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case name
        case color
        case backgroundColor = "background_color"
    }

    public let name: String?
    public let color: String?
    public let backgroundColor: String?
}
