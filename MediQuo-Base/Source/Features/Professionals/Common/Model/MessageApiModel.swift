//
//  MessageApiModel.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 15/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

public struct MessagesApiModel: Hashable, Codable {
    public enum CodingKeys: String, CodingKey {
        case data
    }
    
    public let data: [MessageApiModel]
    
    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decode([MessageApiModel].self, forKey: CodingKeys.data)
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.data, forKey: CodingKeys.data)
    }
}

public struct MessageApiModel: Hashable, Codable {
    
    public enum CodingKeys: String, CodingKey {
        case roomId
        case messageId
        case string
        case status
        case type
        case attachment
        case senderType
        case fromUserHash
        case auto
        case createdAt
        case deletedAt
    }
    
    public let roomId: Int
    public let messageId: String
    public let string: String
    public var status: MessageModel.MessageStatusSDK
    public let type: MessageModel.Kind
    public let attachment: AttachmentApiModel?
    public let senderType: MessageModel.SenderType?
    public let fromUserHash: String
    public let auto: Int
    public let createdAt: String
    public let deletedAt: String?
        
    public static func == (lhs: MessageApiModel, rhs: MessageApiModel) -> Bool {
        return lhs.messageId == rhs.messageId
    }
    
    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.roomId = try container.decode(Int.self, forKey: CodingKeys.roomId)
        self.messageId = try container.decode(String.self, forKey: CodingKeys.messageId)
        self.string = try container.decode(String.self, forKey: CodingKeys.string)
        self.status = try container.decode(MessageModel.MessageStatusSDK.self, forKey: CodingKeys.status)
        self.type = try container.decode(MessageModel.Kind.self, forKey: CodingKeys.type)
        self.attachment = container.contains(CodingKeys.attachment) ? try? container.decode(AttachmentApiModel.self, forKey: CodingKeys.attachment) : nil
        self.senderType = try? container.decode(MessageModel.SenderType.self, forKey: CodingKeys.senderType)
        self.fromUserHash = try container.decode(String.self, forKey: CodingKeys.fromUserHash)
        self.auto = try container.decode(Int.self, forKey: CodingKeys.auto)
        self.createdAt = try container.decode(String.self, forKey: CodingKeys.createdAt)
        self.deletedAt = container.contains(CodingKeys.deletedAt) ? try? container.decode(String.self, forKey: CodingKeys.deletedAt) : nil
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.roomId, forKey: CodingKeys.roomId)
        try? container.encode(self.messageId, forKey: CodingKeys.messageId)
        try? container.encode(self.string, forKey: CodingKeys.string)
        try? container.encode(self.status, forKey: CodingKeys.status)
        try? container.encode(self.type, forKey: CodingKeys.type)
        try? container.encode(self.attachment, forKey: CodingKeys.attachment)
        try? container.encode(self.senderType, forKey: CodingKeys.senderType)
        try? container.encode(self.fromUserHash, forKey: CodingKeys.fromUserHash)
        try? container.encode(self.auto, forKey: CodingKeys.auto)
        try? container.encode(self.createdAt, forKey: CodingKeys.createdAt)
        try? container.encode(self.deletedAt, forKey: CodingKeys.deletedAt)
    }
}

public struct AttachmentApiModel: Hashable, Codable {
    public enum CodingKeys: String, CodingKey {
        case fileName = "file_name"
        case fileSize = "file_size"
        case url = "url"
        case meta = "meta"
    }
    
    public let fileName: String
    public let fileSize: Int
    public let url: String
    public let meta: AttachmentMetaApiModel?
    

    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.fileName = try container.decode(String.self, forKey: CodingKeys.fileName)
        self.fileSize = try container.decode(Int.self, forKey: CodingKeys.fileSize)
        self.url = try container.decode(String.self, forKey: CodingKeys.url)
        self.meta = container.contains(CodingKeys.meta) ? try? container.decode(AttachmentMetaApiModel.self, forKey: CodingKeys.meta) : nil
     }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.fileName, forKey: CodingKeys.fileName)
        try? container.encode(self.fileSize, forKey: CodingKeys.fileSize)
        try? container.encode(self.url, forKey: CodingKeys.url)
        try? container.encode(self.meta, forKey: CodingKeys.meta)
    }
}

public struct AttachmentMetaApiModel: Hashable, Codable {
    public enum CodingKeys: String, CodingKey {
        case width = "image_width"
        case height = "image_height"
        case thumb = "thumb"
    }
    
    public let width: Int
    public let height: Int
    public let thumb: AttachmentMetaThumbApiModel
    
    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.width = try container.decode(Int.self, forKey: CodingKeys.width)
        self.height = try container.decode(Int.self, forKey: CodingKeys.height)
        self.thumb = try container.decode(AttachmentMetaThumbApiModel.self, forKey: CodingKeys.thumb)
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.width, forKey: CodingKeys.width)
        try? container.encode(self.height, forKey: CodingKeys.height)
        try? container.encode(self.thumb, forKey: CodingKeys.thumb)
    }
}

public struct AttachmentMetaThumbApiModel: Hashable, Codable {
    public enum CodingKeys: String, CodingKey {
        case url = "url"
    }
    
    public let url: String

    public init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decode(String.self, forKey: CodingKeys.url)
    }

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.url, forKey: CodingKeys.url)
    }
}
