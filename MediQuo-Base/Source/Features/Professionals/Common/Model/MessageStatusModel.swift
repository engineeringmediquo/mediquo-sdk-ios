//
//  MessageStatusModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public struct MessageStatusModel: Codable {
    public enum CodingKeys: String, CodingKey {
        case messageId
        case roomId
        case imageUrl
        case thumbUrl
        case time
        case status
    }

    public let messageId: String
    public let roomId: Int
    public let status: MessageModel.MessageStatusSDK
    public let time: TimeInterval?
    public let imageUrl: URL?
    public let thumbUrl: URL?
}
