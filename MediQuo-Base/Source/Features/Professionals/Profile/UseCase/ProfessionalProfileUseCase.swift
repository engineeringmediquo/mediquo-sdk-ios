//
//  ProfessionalProfileUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalProfileUseCase {

    private var repository: ProfessionalProfileRepository?
    
    init(repository: ProfessionalProfileRepository = ProfessionalProfileRepository()) {
        self.repository = repository
    }

    func fetch(by userHash: String,
               completion: @escaping RemoteCompletionTypeAlias<DataResponse<ProfessionalModel?>>) {
        DispatchQueue.global(qos: .background).async {
            self.repository?.fetch(by: userHash) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
