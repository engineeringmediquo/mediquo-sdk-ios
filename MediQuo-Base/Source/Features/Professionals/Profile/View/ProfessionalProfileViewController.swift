//
//  ProfessionalProfileViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

class ProfessionalProfileViewController: BaseViewControllerSDK {
    
    var professionalModel: ProfessionalSDKModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func bindViewModels() {
        super.bindViewModels()
    }

    override func unBindViewModels() {
        super.unBindViewModels()
    }

    private func setupView() {
        guard let professionalModel = self.professionalModel else { return }        
        self.addTitle(with: professionalModel.name)
        self.addView(with: professionalModel)
    }
    
    private func addTitle(with name: String?) {
        self.navigationItem.titleView = self.setTitle(title: name)
    }
    
    private func addView(with model: ProfessionalSDKModel) {
        let view = ProfessionalProfileView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        view.configure(by: model)
        self.view.addSubview(view)
    }
}

extension ProfessionalProfileViewController: PushNotificationsActionsDelegate {
    func setNotificationPresentation(by roomId: Int, completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }

    func execute(deeplink: NotificationSchema) {
        if let viewController = self.navigationController?.back(to: ProfessionalsListViewController.self) as? ProfessionalsListViewController {
            viewController.execute(deeplink: deeplink)
        }
    }
}
