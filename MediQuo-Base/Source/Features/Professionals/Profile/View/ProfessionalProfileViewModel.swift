//
//  ProfessionalProfileViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalProfileViewModel: InjectableComponent & BaseViewModel {
    @Inject
    private var useCase: ProfessionalProfileUseCase

    var professional: Observable<ProfessionalModel> = Observable<ProfessionalModel>()

    func fetch(by userHash: String) {
        self.useCase.fetch(by: userHash) { [weak self] result in
            guard let self = self else { return }
            if case let .success(professional) = result {
                self.professional.value = professional.data

                self.sendProfessionalProfileTracking()
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }
}

// MARK: - Tracking

extension ProfessionalProfileViewModel {
    private func sendProfessionalProfileTracking() {
        guard let userHash = self.professional.value?.hash else { return }
        let userInfo: [AnyHashable : Any]? = [Notification.Name.Key.professionalHash: userHash]
        NotificationCenter.default.post(name: Notification.Name.Event.ProfessionalProfile.view, object: nil, userInfo: userInfo)
    }
}
