//
//  ProfessionalProfileView.swift
//  MediQuo-Base
//
//  Created by David Martin on 11/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit

extension ProfessionalProfileView {
    private enum Localizables {
        static let description = Localizable.string("description")
    }
}

// TODO: Cane - Añadir estilos desde config del SDK
class ProfessionalProfileView: UIView {
    private let avatarView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = ColorName.lightGray.color
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let firstStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.backgroundColor =  MediQuoSDK.instance.style?.primaryColor.withAlphaComponent(0.2)
        return stackView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.bold.font(size: 20).customOrDefault
        label.textColor = ColorName.ultraDarkGray.color
        label.numberOfLines = 2
        return label
    }()
    
    private let specialityLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        label.textColor =  MediQuoSDK.instance.style?.primaryColor
        label.numberOfLines = 2
        return label
    }()

    private let secondView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = MediQuoSDK.instance.style?.secondaryColor.withAlphaComponent(0.2)
        return view
    }()
    
    private let titleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.backgroundColor = MediQuoSDK.instance.style?.secondaryColor 
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = FontFamily.GothamRounded.bold.font(size: 20).customOrDefault
        label.textColor = .white
        label.text = Localizables.description
        label.numberOfLines = 1
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textColor = ColorName.ultraDarkGray.color
        label.numberOfLines = 0
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubviews()
    }

    required init?(coder unarchiver: NSCoder) {
        super.init(coder: unarchiver)
        self.addSubviews()
    }
    
    func configure(by model: ProfessionalSDKModel) {
        if let avatar = model.avatar, let url = URL(string: avatar) {
            self.avatarView.sd_setImage(with: url)
        }
        self.nameLabel.text = model.name
        self.specialityLabel.text = model.title
        self.descriptionLabel.text = model.overview
    }
    
    private func addSubviews() {
        self.backgroundColor = .white
        self.addSubview(self.avatarView)

        self.firstStackView.addArrangedSubview(self.nameLabel)
        self.firstStackView.addArrangedSubview(self.specialityLabel)
        self.addSubview(self.firstStackView)

        self.titleView.addSubview(self.titleLabel)
        self.secondView.addSubview(self.titleView)
        self.secondView.addSubview(self.descriptionLabel)
        self.addSubview(self.secondView)

        self.setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        self.avatarView.snp.makeConstraints { make in
            let navigationHeight = Int(UIApplication.shared.statusBarFrame.size.height + UINavigationController().navigationBar.frame.height)
            make.top.equalToSuperview().offset(navigationHeight)
            make.centerX.equalToSuperview()
            make.height.greaterThanOrEqualTo(self.avatarView.snp.height)
            make.width.equalTo(self.frame.width)
            make.width.equalTo(self.avatarView.snp.height).multipliedBy(2)
        }

        self.firstStackView.snp.makeConstraints { make in
            make.top.equalTo(self.avatarView.snp.bottom)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(85)
        }
        self.nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
        }
        self.specialityLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
        }

        self.secondView.snp.makeConstraints { make in
            make.top.equalTo(self.firstStackView.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().offset(-32)
            make.height.greaterThanOrEqualTo(self.descriptionLabel).offset(50)
        }
        self.titleView.snp.makeConstraints { make in
            make.top.equalTo(self.secondView)
            make.centerX.equalTo(self.secondView)
            make.width.equalTo(self.secondView)
            make.height.greaterThanOrEqualTo(36)
        }
        self.titleLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.secondView)
            make.width.equalTo(self.titleView)
            make.height.equalTo(self.titleView)
        }
        self.descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(self.titleView.snp.bottom).offset(8)
            make.centerX.equalTo(self.secondView)
            make.width.equalTo(self.secondView).offset(-24)
            make.height.greaterThanOrEqualTo(50).offset(-16)
        }
        super.updateConstraints()
    }
}
