//
//  ProfessionalProfileRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 10/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ProfessionalProfileRepository {
    private var remote: RemoteManagerProtocol?

    init(remote: RemoteManagerProtocol = RemoteManager()) {
        self.remote = remote
    }

    func fetch(by userHash: String,
               completion: @escaping RemoteCompletionTypeAlias<DataResponse<ProfessionalModel?>>) {
        let endpoint: String = RemoteEndpoints.professionalProfile.replacingOccurrences(of: "{hash}", with: userHash)
        self.remote?.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }
}
