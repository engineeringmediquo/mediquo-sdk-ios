// Copyright © 2021 Medipremium S.L. All rights reserved.

import Foundation

struct ReportModel: Codable, Equatable {
    struct Customer: Codable, Equatable {
        public let name: String?

        private enum CodingKeys: String, CodingKey {
            case name
        }
    }

    struct Professional: Codable, Equatable {
        public let name: String?

        private enum CodingKeys: String, CodingKey {
            case name
        }
    }

    let uuid: String?
    let customer: Customer?
    let professional: Professional?
    let objectiveData: String?
    let subjectiveData: String?
    let analysis: String?
    let plan: String?
    let isPublic: Bool?
    let downloadAvailable: Bool?
    let createdAt: String?

    private enum CodingKeys: String, CodingKey {
        case uuid
        case customer
        case professional
        case objectiveData = "objective_data"
        case subjectiveData = "subjective_data"
        case analysis
        case plan
        case isPublic = "is_public"
        case downloadAvailable = "download_available"
        case createdAt = "created_at"
    }
}
