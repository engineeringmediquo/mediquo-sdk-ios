// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class ReportListEmptyViewController: BaseViewControllerSDK {
    @IBOutlet private var skeletonImageView: UIImageView! {
        didSet {
            self.skeletonImageView.image = Asset.emptyListReport.image
        }
    }

    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = L10n.Localizable.Reports.EmptyView.title
            self.titleLabel.font = FontFamily.GothamRounded.medium.font(size: 18).customOrDefault
            self.titleLabel.textColor = MediQuoSDK.instance.style?.primaryColor
        }
    }

    @IBOutlet private var infoLabel: UILabel! {
        didSet {
            self.infoLabel.text = L10n.Localizable.Reports.EmptyView.subtitle
            self.infoLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
            self.infoLabel.textColor = ColorName.ultraDarkGray.color
            self.infoLabel.textAlignment = .center
        }
    }
}
