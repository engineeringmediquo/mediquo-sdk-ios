// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

protocol ReportListCellDelegate: AnyObject {
    func didTapDownloadReport(reportModel: ReportModel?)
}

class ReportListCell: UITableViewCell {
    static let reusableIdentifier: String = "ReportListCell"

    // swiftlint:disable:this weak_delegate
    weak var delegate: ReportListCellDelegate?

    private var report: ReportModel? {
        didSet {
            self.fillReportTitle()
            self.fillReportSubTitle()
            self.fillReportDescription()
            self.fillDownloadButton()
        }
    }

    @IBOutlet private var documentImage: UIImageView! {
        didSet {
            self.documentImage.tintColor = MediQuoSDK.instance.style?.primaryColor
        }
    }

    @IBOutlet private var documentView: UIView! {
        didSet {
            self.documentView.rounded()
        }
    }

    @IBOutlet private var reportTitle: UILabel! {
        didSet {
            self.reportTitle.isHidden = true
            self.reportTitle.textColor = ColorName.ultraDarkGray.color
            self.reportTitle.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var reportSubTitle: UILabel! {
        didSet {
            self.reportSubTitle.isHidden = true
            self.reportSubTitle.textColor = .lightGray
            self.reportSubTitle.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var reportDescription: UILabel! {
        didSet {
            self.reportDescription.isHidden = true
            self.reportDescription.textColor = .lightGray
            self.reportDescription.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var downloadView: UIView! {
        didSet {
            self.downloadView.isHidden = true
            self.downloadView.rounded()
            self.downloadView.tintColor = MediQuoSDK.instance.style?.primaryColor

            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapDownloadReport))
            self.downloadView.addGestureRecognizer(tap)
        }
    }

    func configure(report: ReportModel) {
        self.report = report
    }
}

extension ReportListCell {
    @objc func didTapDownloadReport() {
        self.delegate?.didTapDownloadReport(reportModel: self.report)
    }

    private func fillReportTitle() {
        guard let createdAt = self.report?.createdAt,
            let date = DateManager.convertToDateFromRemoteISO(createdAt),
            let formattedDate = DateManager.convertToString(date, dateStyle: .short, timeStyle: DateFormatter.Style.none) else {
            return
        }
        self.reportTitle.isHidden = false
        self.reportTitle.text = [L10n.Localizable.Reports.rep, formattedDate].compactMap { $0 }.joined(separator: " · ")
    }

    private func fillReportSubTitle() {
        guard let name = self.report?.professional?.name else { return }
        self.reportSubTitle.isHidden = false
        self.reportSubTitle.text = name
    }

    private func fillReportDescription() {
        guard let data = self.report?.analysis else { return }
        self.reportDescription.isHidden = false
        self.reportDescription.text = [L10n.Localizable.Reports.diagnostic, data].compactMap { $0 }.joined(separator: " ")
    }

    private func fillDownloadButton() {
        guard let isDownloadAvailable = self.report?.downloadAvailable else { return }
        self.downloadView.isHidden = false
        self.downloadView.isHidden = !isDownloadAvailable
    }
}
