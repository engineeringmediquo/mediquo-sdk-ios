// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

class ReportDetailViewController: BaseViewControllerSDK {
    @IBOutlet private var reportDataTitle: UILabel! {
        didSet {
            self.reportDataTitle.text = L10n.Localizable.Reports.Detail.title
            self.reportDataTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.reportDataTitle.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        }
    }

    @IBOutlet private var patientNameTitle: UILabel! {
        didSet {
            self.patientNameTitle.text = L10n.Localizable.Reports.Detail.Patient.name
            self.patientNameTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.patientNameTitle.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        }
    }

    @IBOutlet private var patientNameLabel: UILabel! {
        didSet {
            self.patientNameLabel.textColor = ColorName.ultraDarkGray.color
            self.patientNameLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var professionalNameTitle: UILabel! {
        didSet {
            self.professionalNameTitle.text = L10n.Localizable.Reports.Detail.Professional.name
            self.professionalNameTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.professionalNameTitle.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        }
    }

    @IBOutlet private var professionalNameLabel: UILabel! {
        didSet {
            self.professionalNameLabel.textColor = ColorName.ultraDarkGray.color
            self.professionalNameLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var creationDateTitle: UILabel! {
        didSet {
            self.creationDateTitle.text = L10n.Localizable.Reports.Detail.Creation.date
            self.creationDateTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.creationDateTitle.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        }
    }

    @IBOutlet private var creationDateLabel: UILabel! {
        didSet {
            self.creationDateLabel.textColor = ColorName.ultraDarkGray.color
            self.creationDateLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var creationHourTitle: UILabel! {
        didSet {
            self.creationHourTitle.text = L10n.Localizable.Reports.Detail.Creation.time
            self.creationHourTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.creationHourTitle.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        }
    }

    @IBOutlet private var creationHourLabel: UILabel! {
        didSet {
            self.creationHourLabel.textColor = ColorName.ultraDarkGray.color
            self.creationHourLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var reasonTitle: UILabel! {
        didSet {
            self.reasonTitle.text = L10n.Localizable.Reports.Summary.reasonConsultation
            self.reasonTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.reasonTitle.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        }
    }

    @IBOutlet private var reasonLabel: UILabel! {
        didSet {
            self.reasonLabel.textColor = ColorName.ultraDarkGray.color
            self.reasonLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var reasonHeight: NSLayoutConstraint!

    @IBOutlet private var symptomsTitle: UILabel! {
        didSet {
            self.symptomsTitle.text = L10n.Localizable.Reports.Summary.signsAndSymptomsManifested
            self.symptomsTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.symptomsTitle.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        }
    }

    @IBOutlet private var symptomsLabel: UILabel! {
        didSet {
            self.symptomsLabel.textColor = ColorName.ultraDarkGray.color
            self.symptomsLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var symptomsHeight: NSLayoutConstraint!

    @IBOutlet private var diagnosticTitle: UILabel! {
        didSet {
            self.diagnosticTitle.text = L10n.Localizable.Reports.Summary.diagnosticOrientation
            self.diagnosticTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.diagnosticTitle.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        }
    }

    @IBOutlet private var diagnosticLabel: UILabel! {
        didSet {
            self.diagnosticLabel.textColor = ColorName.ultraDarkGray.color
            self.diagnosticLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var diagnosticHeight: NSLayoutConstraint!

    @IBOutlet private var recommendationsTitle: UILabel! {
        didSet {
            self.recommendationsTitle.text = L10n.Localizable.Reports.Summary.recommendations
            self.recommendationsTitle.textColor = MediQuoSDK.instance.style?.primaryColor
            self.recommendationsTitle.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        }
    }

    @IBOutlet private var recommendationsLabel: UILabel! {
        didSet {
            self.recommendationsLabel.textColor = ColorName.ultraDarkGray.color
            self.recommendationsLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var recommendationsHeight: NSLayoutConstraint!

    @Inject
    var viewModel: ReportsViewModel

    var reportUUID: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        self.setBackButtonNavigationController()
        self.loadReport()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fillTitle()
        self.addDownloadButtonIfNecessary()
    }

    override func bindViewModels() {
        super.bindViewModels()
        self.viewModel.model.subscribe { [weak self] model in
            guard let self = self, model != nil else { return }
            self.fillReport()
        }
        self.viewModel.data.subscribe { [weak self] data in
            guard let self = self, let data = data, let reportModel = self.viewModel.pdfModel else { return }
            Wireframe.navigate(to: .reportPDF(data, reportModel), from: self.navigationController)
        }
    }

    override func unBindViewModels() {
        super.unBindViewModels()
        self.viewModel.model.unsubscribe()
    }
}

extension ReportDetailViewController {
    private func loadReport() {
        guard let uuid = self.reportUUID else { return }
        self.viewModel.fetch(by: uuid)
    }

    private func fillReport() {
        self.fillTitle()
        self.fillPatientName()
        self.fillProfessionalName()
        self.fillCreationDate()
        self.fillCreationTime()
        self.fillReason()
        self.fillSymptoms()
        self.fillDiagnostic()
        self.fillRecomendations()
    }

    private func fillTitle() {
        guard let createdAt = self.viewModel.model.value?.createdAt,
            let date = DateManager.convertToDateFromRemoteISO(createdAt),
            let formattedDate = DateManager.convertToString(date, dateStyle: .short, timeStyle: DateFormatter.Style.none) else {
            return
        }
        self.navigationController?.navigationBar.topItem?.title =
            [L10n.Localizable.Reports.rep, formattedDate].compactMap { $0 }.joined(separator: " · ")
    }

    private func fillPatientName() {
        self.patientNameLabel.text = self.viewModel.model.value?.customer?.name
    }

    private func fillProfessionalName() {
        self.professionalNameLabel.text = self.viewModel.model.value?.professional?.name
    }

    private func fillCreationDate() {
        guard let createdAt = self.viewModel.model.value?.createdAt,
            let date = DateManager.convertToDateFromRemoteISO(createdAt),
            let formattedDate = DateManager.convertToString(date, dateStyle: .short, timeStyle: DateFormatter.Style.none) else {
            return
        }
        self.creationDateLabel.text = formattedDate
    }

    private func fillCreationTime() {
        guard let createdAt = self.viewModel.model.value?.createdAt,
            let date = DateManager.convertToDateFromRemoteISO(createdAt),
            let formattedDate = DateManager.convertToString(date, with: DateManager.DateFormat.onlyHours) else { return }
        self.creationHourLabel.text = [formattedDate, "h"].compactMap { $0 }.joined(separator: "")
    }

    private func fillReason() {
        guard let text = self.viewModel.model.value?.subjectiveData else { return }
        self.reasonLabel.text = text
        self.reasonHeight.constant = self.getHeightLabelForView(text: text, font: self.reasonLabel.font, width: self.reasonLabel.bounds.size.width)
    }

    private func fillSymptoms() {
        guard let text = self.viewModel.model.value?.objectiveData else { return }
        self.symptomsLabel.text = text
        self.symptomsHeight.constant = self.getHeightLabelForView(text: text, font: self.symptomsLabel.font, width: self.symptomsLabel.bounds.size.width)
    }

    private func fillDiagnostic() {
        guard let text = self.viewModel.model.value?.analysis else { return }
        self.diagnosticLabel.text = text
        self.diagnosticHeight.constant = self.getHeightLabelForView(text: text, font: self.diagnosticLabel.font, width: self.diagnosticLabel.bounds.size.width)
    }

    private func fillRecomendations() {
        guard let text = self.viewModel.model.value?.plan else { return }
        self.recommendationsLabel.text = text
        self.recommendationsHeight.constant = self.getHeightLabelForView(text: text, font: self.recommendationsLabel.font, width: self.recommendationsLabel.bounds.size.width)
    }

    private func getHeightLabelForView(text: String, font: UIFont, width: CGFloat) -> CGFloat {
        let extraPadding: CGFloat = 64
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height + extraPadding
    }
}

extension ReportDetailViewController {
    private func addDownloadButtonIfNecessary() {
        guard let isDownloadAvailable = self.viewModel.model.value?.downloadAvailable, isDownloadAvailable else { return }
        let options = UIBarButtonItem(image: Asset.downloadReportShape.image, style: .plain, target: self, action: #selector(self.didTapDownload))
        options.tintColor = MediQuoSDK.instance.style?.primaryContrastColor
        self.navigationItem.rightBarButtonItem = options
    }

    @objc private func didTapDownload() {
        guard let reportModel = self.viewModel.model.value, let uuid = reportModel.uuid else { return }
        self.viewModel.pdfModel = reportModel
        self.viewModel.download(by: uuid)
    }
}
