//
//  ReportsViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ReportsViewModel: InjectableComponent & BaseViewModel {
    @Inject
    private var useCase: ReportsUseCase

    var models: Observable<[ReportModel]> = Observable<[ReportModel]>()
    var model: Observable<ReportModel> = Observable<ReportModel>()
    var data: Observable<Data> = Observable<Data>()

    var pdfModel: ReportModel?

    func fetch() {
        self.useCase.fetch { (result: Result<DataResponse<[ReportModel]>, BaseError>) in
            if case let .success(response) = result {
                self.models.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
            self.sendViewTracking()
        }
    }

    func fetch(by uuid: String) {
        self.useCase.fetch(by: uuid) { (result: Result<DataResponse<ReportModel>, BaseError>) in
            if case let .success(response) = result {
                self.model.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }

    func download(by uuid: String) {
        self.useCase.download(by: uuid) { (result: Result<Data?, BaseError>) in
            if case let .success(response) = result {
                self.data.value = response
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }

    func getReport(position: Int) -> ReportModel? {
        return self.models.value?[safe: position]
    }
}

// MARK: - Tracking

extension ReportsViewModel {
    private func sendViewTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.Reports.view, object: nil)
    }
}
