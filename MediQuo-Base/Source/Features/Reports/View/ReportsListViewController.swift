// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class ReportsListViewController: BaseViewControllerSDK {
    @IBOutlet private var tableView: UITableView! {
        didSet {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.backgroundColor = .white
            self.tableView.tableFooterView = UIView()
            self.tableView.refreshControl = UIRefreshControl()
            self.tableView.refreshControl?.tintColor = MediQuoSDK.instance.style?.accentColor
            self.tableView.refreshControl?.addTarget(self, action: #selector(self.refreshViewFromRefreshControl), for: .valueChanged)
        }
    }

    @Inject
    var viewModel: ReportsViewModel

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        self.setupNavigationBar()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetch()
    }

    override func bindViewModels() {
        super.bindViewModels()
        self.viewModel.models.subscribe { [weak self] models in
            guard let self = self, let models = self.viewModel.models.value else { return }
            if models.isEmpty {
                self.loadEmptyView()
            } else {
                self.removeChild()
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
        self.viewModel.model.subscribe { [weak self] model in
            guard let self = self, let uuid = model?.uuid else { return }
            Wireframe.navigate(to: .reportDetail(uuid), from: self.navigationController)
        }
        self.viewModel.data.subscribe { [weak self] data in
            guard let self = self, let data = data, let reportModel = self.viewModel.pdfModel else { return }
            Wireframe.navigate(to: .reportPDF(data, reportModel), from: self.navigationController)
        }
    }

    override func unBindViewModels() {
        super.unBindViewModels()
        self.viewModel.models.unsubscribe()
        self.viewModel.model.unsubscribe()
        self.viewModel.data.unsubscribe()
    }
}

extension ReportsListViewController: UITableViewDataSource {
    public func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.viewModel.models.value?.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReportListCell.reusableIdentifier, for: indexPath) as? ReportListCell,
              let report = self.viewModel.getReport(position: indexPath.row) else {
            return UITableViewCell()
        }
        cell.configure(report: report)
        cell.delegate = self
        return cell
    }
}

extension ReportsListViewController: UITableViewDelegate {
    public func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let report = self.viewModel.getReport(position: indexPath.row), let uuid = report.uuid else { return }
        self.viewModel.fetch(by: uuid)
    }

    public func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 128
    }

    public func tableView(_: UITableView, heightForFooterInSection _: Int) -> CGFloat {
        return 80
    }

    public func tableView(_: UITableView, viewForFooterInSection _: Int) -> UIView? {
        return self.buildFooterView()
    }

    private func buildFooterView() -> UIView {
        let footerView = UIView()
        let colorTop = UIColor.white.withAlphaComponent(0.25).cgColor
        let colorBottom = UIColor.white.withAlphaComponent(1).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 80))
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        footerView.layer.addSublayer(gradientLayer)

        return footerView
    }
}

extension ReportsListViewController: ReportListCellDelegate {
    func didTapDownloadReport(reportModel: ReportModel?) {
        guard let uuid = reportModel?.uuid else { return }
        self.viewModel.pdfModel = reportModel
        self.viewModel.download(by: uuid)
    }
}

extension ReportsListViewController {
    private func setupNavigationBar() {
        self.setupNavigationBarTitle()
    }

    private func setupNavigationBarTitle() {
        self.title = Localizable.string("reports.title") 
    }

    private func fetch() {
        self.viewModel.fetch()
    }

    private func loadEmptyView() {
        let viewController = MediQuoSDK.getReportsListEmpty()
        self.add(asChildViewController: viewController)
    }

    @objc func refreshViewFromRefreshControl() {
        self.tableView.refreshControl?.beginRefreshing()
        self.fetch()
    }
}
