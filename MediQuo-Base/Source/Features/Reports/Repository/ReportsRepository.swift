//
//  ReportsRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ReportsRepository: InjectableComponent {
    private var remote: RemoteManagerProtocol

    init(remote: RemoteManagerProtocol) {
        self.remote = remote
    }

    func fetch(completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[ReportModel]>>)) {
        let endpoint = RemoteEndpoints.reports
        self.remote.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }

    func fetch(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<ReportModel>>)) {
        let endpoint = RemoteEndpoints.report.replacingOccurrences(of: "{uuid}", with: uuid)
        self.remote.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }

    func download(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<Data?>)) {
        let endpoint = RemoteEndpoints.reportPDF.replacingOccurrences(of: "{uuid}", with: uuid)
        self.remote.download(endpoint, fileName: uuid, completion: completion)
    }
}
