//
//  ReportsUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class ReportsUseCase: InjectableComponent {
    @Inject
    private var repository: ReportsRepository

    func fetch(completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[ReportModel]>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func fetch(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<ReportModel>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch(by: uuid) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func download(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<Data?>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.download(by: uuid) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
