//
//  MedicalHistoryUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class MedicalHistoryUseCase: InjectableComponent {
    @Inject
    private var repository: MedicalHistoryRepository

    func fetch<T>(_ type: MedicalHistoryType, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[T]>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch(type) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func create<T>(_ type: MedicalHistoryType, with name: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<T>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.create(type, with: name) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func update<T>(_ type: MedicalHistoryType, by id: Int, with name: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<T>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.update(type, by: id, with: name) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func delete(_ type: MedicalHistoryType, with id: Int, completion: @escaping (RemoteCompletionTypeAlias<VoidResponse>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.delete(type, with: id) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
