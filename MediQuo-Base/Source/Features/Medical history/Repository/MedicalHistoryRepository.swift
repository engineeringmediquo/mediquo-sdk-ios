// Copyright © 2021 Medipremium S.L. All rights reserved.

import Foundation

class MedicalHistoryRepository: InjectableComponent {
    private var remote: RemoteManagerProtocol

    init(remote: RemoteManagerProtocol) {
        self.remote = remote
    }

    func fetch<T: Codable>(_ type: MedicalHistoryType, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[T]>>)) {
        let endpoint = RemoteEndpoints.medicalHistory + type.rawValue
        self.remote.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }

    func create<T: Codable>(_ type: MedicalHistoryType, with name: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<T>>)) {
        let endpoint = RemoteEndpoints.medicalHistory + type.rawValue
        let parameters: Codable = MedicalHistoryRequest(name: name)
        self.remote.post(endpoint, parameters: parameters, completion: completion)
    }

    func update<T: Codable>(_ type: MedicalHistoryType, by id: Int, with name: String, completion: @escaping (RemoteCompletionTypeAlias<DataResponse<T>>)) {
        let endpoint = RemoteEndpoints.medicalHistory + type.rawValue + "/\(id)"
        let parameters: Codable = MedicalHistoryRequest(name: name)
        self.remote.put(endpoint, parameters: parameters, completion: completion)
    }

    func delete(_ type: MedicalHistoryType, with id: Int, completion: @escaping (RemoteCompletionTypeAlias<VoidResponse>)) {
        let endpoint = RemoteEndpoints.medicalHistory + type.rawValue + "/\(id)"
        self.remote.delete(endpoint, parameters: nil, completion: completion)
    }
}
