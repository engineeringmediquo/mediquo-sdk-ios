// Copyright © 2021 Medipremium S.L. All rights reserved.

import Foundation

public enum MedicalHistoryType: String {
    case unknown
    case allergies
    case diseases
    case medications
    case reports
    case recipes
}
