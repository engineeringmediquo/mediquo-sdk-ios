// Copyright © 2021 Medipremium S.L. All rights reserved.

public struct Disease: Codable, Equatable {
    let id: Int?
    let name: String?
    let description: String?
    let customerHash: String?
    let diagnosisDate: String?
    let resolutionDate: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case customerHash = "customer_hash"
        case diagnosisDate = "diagnosis_date"
        case resolutionDate = "resolution_date"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
}

extension Disease: MedicalHistoryModel {}
