// Copyright © 2021 Medipremium S.L. All rights reserved.

public struct Allergy: Codable, Equatable {
    let id: Int?
    let severity: Int?
    let name: String?
    let description: String?
    let customerHash: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case severity
        case name
        case description
        case customerHash = "customer_hash"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
}

extension Allergy: MedicalHistoryModel {}
