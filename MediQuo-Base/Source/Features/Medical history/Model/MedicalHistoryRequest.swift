//
//  MedicalHistoryRequest.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

public struct MedicalHistoryRequest: Codable, Equatable {
    let name: String
    let severity: Int = 1
    let posology: String = ""

    private enum CodingKeys: String, CodingKey {
        case name
        case severity
        case posology
    }
}
