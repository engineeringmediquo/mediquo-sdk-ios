//
//  MedicalHistorySectionFormViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 26/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

protocol MedicalHistorySectionFormProtocol: AnyObject {
    var sectionImage: UIImage { get }
    var sectionText: String { get }
    var positiveButtonText: String { get }
    var navigationBarAddTitle: String { get }
    var navigationBarEditTitle: String { get }
}

class MedicalHistorySectionFormViewController: BaseViewControllerSDK {
    private enum Constants {
        static let defaultAnimationLength: TimeInterval = 0.3
        static let defaultMinCharsToFirstExpand: Int = 30
        static let defaultMinCharsToSecondExpand: Int = 100
    }

    @IBOutlet private var sectionImage: UIImageView! {
        didSet {
            self.sectionImage.image = self.delegate?.sectionImage
        }
    }

    @IBOutlet private var sectionView: UIView!
    @IBOutlet private var sectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var sectionText: UITextView! {
        didSet {
            self.sectionText.delegate = self
            self.sectionText.text = self.delegate?.sectionText
            self.sectionText.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
            self.sectionText.textColor = ColorName.lightGray.color
        }
    }

    @IBOutlet private var separatorTextView: UIView! {
        didSet {
            self.separatorTextView.backgroundColor = .lightGray
        }
    }

    @Inject
    var viewModel: MedicalHistoryViewModel

    var type: MedicalHistoryType = .unknown
    var id: Int?
    var name: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindEvents()
        self.fill()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBar()
    }

    override func bindViewModels() {
        super.bindViewModels()
        self.viewModel.updated.subscribe { [weak self] updated in
            guard let self = self, updated != nil else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func unBindViewModels() {
        super.unBindViewModels()
        self.viewModel.updated.unsubscribe()
    }

    private var delegate: MedicalHistorySectionFormProtocol? {
        switch self.type {
        case .allergies:
            return AllergiesSectionController()
        case .diseases:
            return DiseasesSectionController()
        case .medications:
            return MedicationsSectionController()
        default:
            return nil
        }
    }
}

extension MedicalHistorySectionFormViewController {
    private func setupNavigationBar() {
        self.setupNavigationBarTitle()
        self.setBackButtonNavigationController()
        self.setRightButtonNavigationController(title: Localizable.string("save"), isEnabled: false, target: self, selector: "didTapSave:")
    }

    private func setupNavigationBarTitle() {
        if self.id != nil {
            self.navigationItem.title = self.delegate?.navigationBarEditTitle
        } else {
            self.navigationItem.title = self.delegate?.navigationBarAddTitle
        }
    }

    private func bindEvents() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapView))
        self.view.addGestureRecognizer(tap)
    }

    @objc private func tapView() {
        self.separatorTextView.backgroundColor = .lightGray
        self.dismissKeyboard()
    }

    private func fill() {
        guard let name = self.name, !name.isEmpty else { return }
        self.sectionText.textColor = MediQuoSDK.instance.style?.primaryColor
        self.sectionText.text = self.name
        self.separatorTextView.backgroundColor = MediQuoSDK.instance.style?.accentColor
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }

    @objc private func didTapSave(_: Any) {
        if let text = self.sectionText.text {
            switch self.type {
            case .allergies:
                if let id = self.id {
                    self.viewModel.updateAllergy(id: id, name: text)
                } else {
                    self.viewModel.createAllergy(name: text)
                }
            case .diseases:
                if let id = self.id {
                    self.viewModel.updateDisease(id: id, name: text)
                } else {
                    self.viewModel.createDisease(name: text)
                }
            case .medications:
                if let id = self.id {
                    self.viewModel.updateMedication(id: id, name: text)
                } else {
                    self.viewModel.createMedication(name: text)
                }
            default: break
            }
        }
    }
}

extension MedicalHistorySectionFormViewController: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == ColorName.lightGray.color {
            textView.text = nil
            textView.textColor = ColorName.ultraDarkGray.color
        }
    }

    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.delegate?.sectionText
            textView.textColor = MediQuoSDK.instance.style?.primaryColor
        }
    }

    public func textViewDidChange(_ textView: UITextView) {
        self.resize(textView)
        if textView.text.isEmpty {
            self.separatorTextView.backgroundColor = .lightGray
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        } else {
            self.separatorTextView.backgroundColor = MediQuoSDK.instance.style?.accentColor
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }

    private func resize(_ textView: UITextView) {
        UIView.animate(withDuration: Constants.defaultAnimationLength) {
            if textView.text.isEmpty || textView.text.count < Constants.defaultMinCharsToFirstExpand {
                // Do nothing on the firsts chats introduced
            } else if textView.text.count >= Constants.defaultMinCharsToFirstExpand, textView.text.count < Constants.defaultMinCharsToSecondExpand {
                self.adaptTextViewHeight(newHeight: 72)
            } else {
                self.adaptTextViewHeight(newHeight: 108)
                self.sectionText.flashScrollIndicators()
            }
        }
    }

    private func adaptTextViewHeight(newHeight: CGFloat) {
        self.sectionText.isScrollEnabled = newHeight > 32
        self.sectionText.translatesAutoresizingMaskIntoConstraints = false
        self.sectionHeightConstraint.constant = newHeight
        self.sectionText.setNeedsLayout()
    }
}
