//
//  MedicalHistoryViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class MedicalHistoryViewModel: InjectableComponent & BaseViewModel {
    @Inject
    private var useCase: MedicalHistoryUseCase

    var models: Observable<[MedicalHistoryModel]> = Observable<[MedicalHistoryModel]>()
    var updated: Observable<Bool> = Observable<Bool>()
}

// MARK: - Allergies

extension MedicalHistoryViewModel {
    @objc func fetchAllergies() {
        self.useCase.fetch(.allergies) { (result: Result<DataResponse<[Allergy]>, BaseError>) in
            if case let .success(response) = result {
                self.models.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
            self.sendViewTracking(.allergies)
        }
    }

    @objc func createAllergy(name: String) {
        self.useCase.create(.allergies, with: name) { (result: Result<DataResponse<Allergy>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func updateAllergy(id: Int, name: String?) {
        guard let name = name else { return }
        self.useCase.update(.allergies, by: id, with: name) { (result: Result<DataResponse<Allergy>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func deleteAllergy(id: Int) {
        self.useCase.delete(.allergies, with: id) { (result: Result<VoidResponse, BaseError>) in
            if case .success = result {
                self.models.value?.removeAll(where: { (model: MedicalHistoryModel) -> Bool in
                    guard let model: Allergy = model as? Allergy else { return false }
                    return model.id == id
                })
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }
}

// MARK: - Diseases

extension MedicalHistoryViewModel {
    @objc func fetchDiseases() {
        self.useCase.fetch(.diseases) { (result: Result<DataResponse<[Disease]>, BaseError>) in
            if case let .success(response) = result {
                self.models.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
            self.sendViewTracking(.diseases)
        }
    }

    @objc func createDisease(name: String) {
        self.useCase.create(.diseases, with: name) { (result: Result<DataResponse<Disease>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func updateDisease(id: Int, name: String?) {
        guard let name = name else { return }
        self.useCase.update(.diseases, by: id, with: name) { (result: Result<DataResponse<Disease>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func deleteDisease(id: Int) {
        self.useCase.delete(.diseases, with: id) { (result: Result<VoidResponse, BaseError>) in
            if case .success = result {
                self.models.value?.removeAll(where: { (model: MedicalHistoryModel) -> Bool in
                    guard let model: Disease = model as? Disease else { return false }
                    return model.id == id
                })
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }
}

// MARK: - Medications

extension MedicalHistoryViewModel {
    @objc func fetchMedications() {
        self.useCase.fetch(.medications) { (result: Result<DataResponse<[Medication]>, BaseError>) in
            if case let .success(response) = result {
                self.models.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
            self.sendViewTracking(.medications)
        }
    }

    @objc func createMedication(name: String) {
        self.useCase.create(.medications, with: name) { (result: Result<DataResponse<Medication>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func updateMedication(id: Int, name: String?) {
        guard let name = name else { return }
        self.useCase.update(.medications, by: id, with: name) { (result: Result<DataResponse<Medication>, BaseError>) in
            if case .success = result {
                self.updated.value = true
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
                self.updated.value = false
            }
        }
    }

    @objc func deleteMedication(id: Int) {
        self.useCase.delete(.medications, with: id) { (result: Result<VoidResponse, BaseError>) in
            if case .success = result {
                self.models.value?.removeAll(where: { (model: MedicalHistoryModel) -> Bool in
                    guard let model: Medication = model as? Medication else { return false }
                    return model.id == id
                })
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }
}

// MARK: - Tracking

extension MedicalHistoryViewModel {
    private func sendViewTracking(_ type: MedicalHistoryType) {
        switch type {
        case .allergies:
            NotificationCenter.default.post(name: Notification.Name.Event.Allergies.view, object: nil, userInfo: nil)
        case .diseases:
            NotificationCenter.default.post(name: Notification.Name.Event.Illnesses.view, object: nil, userInfo: nil)
        case .medications:
            NotificationCenter.default.post(name: Notification.Name.Event.Medications.view, object: nil, userInfo: nil)
        default: break
        }
    }
}
