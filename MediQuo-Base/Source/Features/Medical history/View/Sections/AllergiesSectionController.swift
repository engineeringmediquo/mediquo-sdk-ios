// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class AllergiesSectionController: MedicalHistorySectionProtocol {
    public var sectionImage: UIImage {
        return Asset.allergy.image
    }

    public var sectionDescription: String {
        return Localizable.string("medicalHistory.allergy.empty")  //L10n.Localizable.MedicalHistory.Allergy.empty
    }

    public var positiveButtonText: String {
        return Localizable.string("add") // L10n.Localizable.add
    }

    public var navigationBarTitle: String {
        return Localizable.string("medicalHistory.allergy.title") // L10n.Localizable.MedicalHistory.Allergy.title
    }

    public func getCellLabelTitle(from model: MedicalHistoryModel) -> String {
        guard let cell = model as? Allergy, let name = cell.name else { return "" }
        return name
    }

    public func getModelId(from model: MedicalHistoryModel) -> Int {
        guard let cell = model as? Allergy, let id = cell.id else { return 0 }
        return id
    }
}

extension AllergiesSectionController: MedicalHistorySectionFormProtocol {
    public var sectionText: String {
        return Localizable.string("medicalHistory.allergy.add.placeholder") // L10n.Localizable.MedicalHistory.Allergy.Add.placeholder
    }

    public var navigationBarAddTitle: String {
        return  Localizable.string("medicalHistory.allergy.add.title") // L10n.Localizable.MedicalHistory.Allergy.Add.title
    }

    public var navigationBarEditTitle: String {
        return Localizable.string("medicalHistory.allergy.edit.title") // L10n.Localizable.MedicalHistory.Allergy.Edit.title
    }
}
