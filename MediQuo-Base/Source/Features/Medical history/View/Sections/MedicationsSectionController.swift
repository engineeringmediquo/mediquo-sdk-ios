// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class MedicationsSectionController: MedicalHistorySectionProtocol {
    public var sectionImage: UIImage {
        return Asset.medicine.image
    }

    public var sectionDescription: String {
        return Localizable.string("medicalHistory.medication.empty")
    }

    public var positiveButtonText: String {
        return Localizable.string("add")
    }

    public var navigationBarTitle: String {
        return Localizable.string("medicalHistory.medication.title")
    }

    public func getCellLabelTitle(from model: MedicalHistoryModel) -> String {
        guard let cell = model as? Medication, let name = cell.name else { return "" }
        return name
    }

    public func getModelId(from model: MedicalHistoryModel) -> Int {
        guard let cell = model as? Medication, let id = cell.id else { return 0 }
        return id
    }
}

extension MedicationsSectionController: MedicalHistorySectionFormProtocol {
    public var sectionText: String {
        return Localizable.string("medicalHistory.medication.add.placeholder")
    }

    public var navigationBarAddTitle: String {
        return Localizable.string("medicalHistory.medication.add.title") 
    }

    public var navigationBarEditTitle: String {
        return Localizable.string("medicalHistory.medication.edit.title")
    }
}
