// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class DiseasesSectionController: MedicalHistorySectionProtocol {
    public var sectionImage: UIImage {
        return Asset.disease.image
    }

    public var sectionDescription: String {
        return Localizable.string("medicalHistory.disease.empty") 
    }

    public var positiveButtonText: String {
        return Localizable.string("add")
    }

    public var navigationBarTitle: String {
        return Localizable.string("medicalHistory.disease.title")
    }

    public func getCellLabelTitle(from model: MedicalHistoryModel) -> String {
        guard let cell = model as? Disease, let name = cell.name else { return "" }
        return name
    }

    public func getModelId(from model: MedicalHistoryModel) -> Int {
        guard let cell = model as? Disease, let id = cell.id else { return 0 }
        return id
    }
}

extension DiseasesSectionController: MedicalHistorySectionFormProtocol {
    public var sectionText: String {
        return Localizable.string("medicalHistory.disease.add.placeholder")
    }

    public var navigationBarAddTitle: String {
        return Localizable.string("medicalHistory.disease.add.title")
    }

    public var navigationBarEditTitle: String {
        return Localizable.string("medicalHistory.disease.edit.title")
    }
}
