//
//  MedicalHistoryListCell.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit

class MedicalHistoryListCell: UITableViewCell {
    static let identifier: String = "medicalHistoryList"

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = MediQuoSDK.instance.style?.primaryColor
        return imageView
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorName.ultraDarkGray.color
        label.font = FontFamily.GothamRounded.medium.font(size: 16).customOrDefault
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(self.iconImageView)
        self.contentView.addSubview(self.descriptionLabel)

        self.setNeedsUpdateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        self.iconImageView.snp.makeConstraints { make in
            make.centerY.equalTo(self.contentView)
            make.leading.equalTo(self.contentView.snp.leading).offset(16)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        self.descriptionLabel.snp.makeConstraints { make in
            make.centerY.equalTo(self.contentView)
            make.leading.equalTo(self.iconImageView.snp.trailing).offset(16)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(16)
        }

        super.updateConstraints()
    }

    func configure(_ type: MedicalHistoryType) {
        self.selectionStyle = .default

        self.contentView.backgroundColor = .white

        switch type {
        case .allergies:
            self.iconImageView.image = Asset.allergyIcon.image
            self.descriptionLabel.text = Localizable.string("medicalHistory.allergy.title") 
        case .diseases:
            self.iconImageView.image = Asset.diseaseIcon.image
            self.descriptionLabel.text = Localizable.string("medicalHistory.disease.title")
        case .medications:
            self.iconImageView.image = Asset.medicineIcon.image
            self.descriptionLabel.text = Localizable.string("medicalHistory.medication.title")
        case .reports:
            self.iconImageView.image = Asset.reportIcon.image
            self.descriptionLabel.text = Localizable.string("reports.title")
        case .recipes:
            self.iconImageView.image = Asset.recipeIcon.image
            self.descriptionLabel.text = Localizable.string("recipes.title")
        default: break
        }
    }
}
