//
//  MedicalHistorySectionViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 25/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

protocol MedicalHistorySectionProtocol: AnyObject {
    var sectionImage: UIImage { get }
    var sectionDescription: String { get }
    var positiveButtonText: String { get }
    var navigationBarTitle: String { get }
    func getCellLabelTitle(from model: MedicalHistoryModel) -> String
    func getModelId(from model: MedicalHistoryModel) -> Int
}

public class MedicalHistorySectionViewController: BaseViewControllerSDK {
    @IBOutlet private var tableView: UITableView!

    @IBOutlet private var sectionImage: UIImageView! {
        didSet {
            self.sectionImage.isHidden = true
            self.sectionImage.image = self.delegate?.sectionImage
        }
    }

    @IBOutlet private var sectionDescription: UILabel! {
        didSet {
            self.sectionDescription.isHidden = true
            self.sectionDescription.text = self.delegate?.sectionDescription
            self.sectionDescription.font = FontFamily.GothamRounded.medium.font(size: 18).customOrDefault
            self.sectionDescription.textColor = ColorName.ultraDarkGray.color
        }
    }

    @IBOutlet private var positiveButton: UIButton! {
        didSet {
            self.positiveButton.isHidden = true
            if let text = self.delegate?.positiveButtonText {
                self.positiveButton.titleLabel?.font = FontFamily.GothamRounded.medium.font(size: 18).customOrDefault
                self.positiveButton.backgroundColor = MediQuoSDK.instance.style?.primaryColor
                self.positiveButton.setTitle(text, for: .normal)
                self.positiveButton.setTitleColor(.white, for: .normal)
                self.positiveButton.layer.cornerRadius = 10
                self.positiveButton.layer.masksToBounds = true
            }
        }
    }

    @IBAction private func didTapPositiveButton() {
        switch self.type {
            case .allergies:
                Wireframe.navigate(to: .allergiesForm(nil, nil), from: self.navigationController)
            case .diseases:
                Wireframe.navigate(to: .diseasesForm(nil, nil), from: self.navigationController)
            case .medications:
                Wireframe.navigate(to: .medicationsForm(nil, nil), from: self.navigationController)
        default: break
        }
    }

    @Inject
    var viewModel: MedicalHistoryViewModel

    var type: MedicalHistoryType = .unknown

    private var delegate: MedicalHistorySectionProtocol? {
        switch self.type {
        case .allergies:
            return AllergiesSectionController()
        case .diseases:
            return DiseasesSectionController()
        case .medications:
            return MedicationsSectionController()
        default:
            return nil
        }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        self.prepareTableView()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBar()
        self.fetch()
    }

    override func bindViewModels() {
        super.bindViewModels()
        self.viewModel.models.subscribe { [weak self] models in
            guard let self = self, models != nil else { return }
            self.tableView.reloadData()
        }
    }

    override func unBindNotifications() {
        super.unBindNotifications()
        self.viewModel.models.unsubscribe()
    }
}

extension MedicalHistorySectionViewController {
    private func setupNavigationBar() {
        self.setupNavigationBarTitle()
        self.setBackButtonNavigationController()
        self.setupPositiveNavigationBarActionButton()
    }

    private func setupNavigationBarTitle() {
        self.navigationItem.title = self.delegate?.navigationBarTitle
    }

    private func setupPositiveNavigationBarActionButton() {
        let button = UIButton(type: .custom)
        button.setImage(Asset.add.image, for: .normal)
        button.addTarget(self, action: #selector(self.didTapPositiveButton), for: .touchUpInside)
        button.tintColor = MediQuoSDK.instance.style?.primaryContrastColor
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
}

extension MedicalHistorySectionViewController {
    private func prepareTableView() {
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
}

extension MedicalHistorySectionViewController: UITableViewDataSource {
    public func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        self.toggleTableViewBackground()
        return self.viewModel.models.value?.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.viewModel.models.value?[safe: indexPath.row] else {
            return UITableViewCell()
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicalHistoryCell", for: indexPath)
        cell.textLabel?.font = FontFamily.GothamRounded.book.font(size: 16).customOrDefault
        cell.textLabel?.textColor = ColorName.ultraDarkGray.color
        cell.textLabel?.text = self.delegate?.getCellLabelTitle(from: model)
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }

    private func toggleTableViewBackground() {
        guard let isEmpty = self.viewModel.models.value?.isEmpty else {
            self.tableView.isHidden = true
            return
        }
        self.tableView.isHidden = isEmpty
        self.sectionImage.isHidden = !isEmpty
        self.sectionDescription.isHidden = !isEmpty
        self.positiveButton.isHidden = !isEmpty
    }
}

extension MedicalHistorySectionViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard let model = self.viewModel.models.value?[safe: indexPath.row], let id = self.delegate?.getModelId(from: model) else { return }

        switch self.type {
        case .allergies:
            guard let name = (model as? Allergy)?.name else { return }
            Wireframe.navigate(to: .allergiesForm(id, name), from: self.navigationController)
        case .diseases:
            guard let name = (model as? Disease)?.name else { return }
            Wireframe.navigate(to: .diseasesForm(id, name), from: self.navigationController)
        case .medications:
            guard let name = (model as? Medication)?.name else { return }
            Wireframe.navigate(to: .medicationsForm(id, name), from: self.navigationController)
        default: break
        }
    }

    // swiftlint:disable cyclomatic_complexity
    public func tableView(_: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let model = self.viewModel.models.value?[safe: indexPath.row], let id = self.delegate?.getModelId(from: model) else { return [] }

        let delete = UITableViewRowAction(style: .destructive, title: L10n.Localizable.delete) { [weak self] _, _ in
            guard let self = self else { return }
            switch self.type {
            case .allergies:
                self.viewModel.deleteAllergy(id: id)
            case .diseases:
                self.viewModel.deleteDisease(id: id)
            case .medications:
                self.viewModel.deleteMedication(id: id)
            default: break
            }
        }
        let update = UITableViewRowAction(style: .normal, title: L10n.Localizable.edit) { [weak self] _, _ in
            guard let self = self else { return }
            switch self.type {
            case .allergies:
                guard let name = (model as? Allergy)?.name else { return }
                Wireframe.navigate(to: .allergiesForm(id, name), from: self.navigationController)
            case .diseases:
                guard let name = (model as? Disease)?.name else { return }
                Wireframe.navigate(to: .diseasesForm(id, name), from: self.navigationController)
            case .medications:
                guard let name = (model as? Medication)?.name else { return }
                Wireframe.navigate(to: .medicationsForm(id, name), from: self.navigationController)
            default: break
            }
        }
        return [delete, update]
    }
}

extension MedicalHistorySectionViewController {
    private func fetch() {
        switch self.type {
        case .allergies:
            self.viewModel.fetchAllergies()
        case .diseases:
            self.viewModel.fetchDiseases()
        case .medications:
            self.viewModel.fetchMedications()
        default: break
        }
    }
}
