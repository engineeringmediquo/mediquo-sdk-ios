//
//  MedicalHistoryListViewController.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SnapKit

public class MedicalHistoryListViewController: BaseViewControllerSDK {
    private var didSetupConstraints = false
    private var tableView: UITableView = UITableView()

    private let elements: [MedicalHistoryType] = [.allergies, .diseases, .medications, .reports, .recipes]

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        self.initTableView()
        self.sendViewTracking()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle()
    }

    public override func updateViewConstraints() {
        guard !self.didSetupConstraints else {
            super.updateViewConstraints()
            return
        }

        self.tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        self.didSetupConstraints = true

        super.updateViewConstraints()
    }

    private func initTableView() {
        self.tableView.tableFooterView = UIView()
        self.tableView.register(MedicalHistoryListCell.self, forCellReuseIdentifier: MedicalHistoryListCell.identifier)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = .white
        self.view.addSubview(self.tableView)
        self.view.setNeedsUpdateConstraints()
    }

    private func setupNavigationBarTitle() {
        self.navigationItem.title = Localizable.string("medicalHistory.title")  
    }
}

// MARK: - TableView DataSource

extension MedicalHistoryListViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: MedicalHistoryListCell = tableView.dequeueReusableCell(
                withIdentifier: MedicalHistoryListCell.identifier, for: indexPath) as? MedicalHistoryListCell else { return UITableViewCell() }
        let type: MedicalHistoryType = self.elements[indexPath.row]
        cell.configure(type)
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

// MARK: - TableView Delegate

extension MedicalHistoryListViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let type: MedicalHistoryType = self.elements[indexPath.row]
        self.navigateTo(type)
    }

    private func navigateTo(_ type: MedicalHistoryType) {
        switch type {
        case .allergies:
            Wireframe.navigate(to: .allergies, from: self.navigationController)
        case .diseases:
            Wireframe.navigate(to: .diseases, from: self.navigationController)
        case .medications:
            Wireframe.navigate(to: .medications, from: self.navigationController)
        case .reports:
            Wireframe.navigate(to: .reports, from: self.navigationController)
        case .recipes:
            Wireframe.navigate(to: .recipes, from: self.navigationController)
        default: break
        }
    }
}

// MARK: - Tracking

extension MedicalHistoryListViewController {
    private func sendViewTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.MedicalHistory.view, object: nil, userInfo: nil)
    }
}
