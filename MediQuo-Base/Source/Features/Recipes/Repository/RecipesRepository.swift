//
//  RecipesRepository.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class RecipesRepository: InjectableComponent {
    private var remote: RemoteManagerProtocol

    init(remote: RemoteManagerProtocol) {
        self.remote = remote
    }

    func fetch(completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[RecipeModel]>>)) {
        let endpoint = RemoteEndpoints.recipes
        self.remote.get(endpoint, headers: nil, parameters: nil, completion: completion)
    }

    func download(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<Data?>)) {
        let endpoint = RemoteEndpoints.recipePDF.replacingOccurrences(of: "{uuid}", with: uuid)
        self.remote.download(endpoint, fileName: uuid, completion: completion)
    }
}
