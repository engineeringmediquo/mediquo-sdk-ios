//
//  RecipesUseCase.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class RecipesUseCase: InjectableComponent {
    @Inject
    private var repository: RecipesRepository

    func fetch(completion: @escaping (RemoteCompletionTypeAlias<DataResponse<[RecipeModel]>>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.fetch { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }

    func download(by uuid: String, completion: @escaping (RemoteCompletionTypeAlias<Data?>)) {
        DispatchQueue.global(qos: .background).async {
            self.repository.download(by: uuid) { response in
                DispatchQueue.main.async {
                    completion(response)
                }
            }
        }
    }
}
