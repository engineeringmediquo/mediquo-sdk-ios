// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit
import PDFKit

class RecipesDetailPDFViewController: BaseViewControllerSDK {
    var data: Data?
    var recipeModel: RecipeModel?

    private var path: URL? {
        didSet {
            if let path = path, let data = self.data {
                self.saveFile(data, in: path)
                self.showPDF(path)
            } else {
                self.dismiss(animated: true)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackButtonNavigationController()
        self.loadPath()
    }

    private func loadPath() {
        guard let professionalName = self.recipeModel?.professional?.name, let prescribedAt = self.recipeModel?.prescribedAt, let date = DateManager.convertToDateFromRemoteISO(prescribedAt),
            let formattedDate = DateManager.convertToString(date, with: "dd_MM_yyyy") else {
            return
        }
        let fileName = "REC_\(professionalName)_\(formattedDate).pdf"
        self.path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(fileName)
    }

    private func saveFile(_ data: Data, in path: URL) {
        self.write(data, in: path)
        self.showActivityViewController(path)
    }

    private func write(_ data: Data, in path: URL) {
        do {
            try data.write(to: path, options: .atomic)
        } catch {
            NSLog("[RecipesDetailPDFViewController] Write pdf file Error: : %@", error.localizedDescription)
        }
    }

    private func showActivityViewController(_ url: URL) {
        let activityController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.present(activityController, animated: true)
    }

    private func showPDF(_ path: URL) {
        let pdfView = PDFView()
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.autoScales = true
        self.view.addSubview(pdfView)

        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true

        if let document = PDFDocument(url: path) {
            pdfView.document = document
            DispatchQueue.main.async {
                guard let firstPage = pdfView.document?.page(at: 0) else { return }
                pdfView.go(to: CGRect(x: 0, y: Int.max, width: 0, height: 0), on: firstPage)
            }
        }
    }
}
