//
//  RecipesViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

class RecipesViewModel: InjectableComponent & BaseViewModel {
    @Inject
    private var useCase: RecipesUseCase

    var models: Observable<[RecipeModel]> = Observable<[RecipeModel]>()
    var data: Observable<Data> = Observable<Data>()

    var pdfModel: RecipeModel?

    func fetch() {
        self.useCase.fetch { (result: Result<DataResponse<[RecipeModel]>, BaseError>) in
            if case let .success(response) = result {
                self.models.value = response.data
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
            self.sendViewTracking()
        }
    }

    func download(by uuid: String) {
        self.useCase.download(by: uuid) { (result: Result<Data?, BaseError>) in
            if case let .success(response) = result {
                self.data.value = response
            }
            if case let .failure(error) = result {
                CoreLog.business.error("%@", error.description)
            }
        }
    }

    func getRecipe(position: Int) -> RecipeModel? {
        return self.models.value?[safe: position]
    }
}

// MARK: - Tracking

extension RecipesViewModel {
    private func sendViewTracking() {
        NotificationCenter.default.post(name: Notification.Name.Event.Recipes.view, object: nil)
    }
}
