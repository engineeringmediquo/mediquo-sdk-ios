// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

protocol RecipesListCellDelegate: AnyObject {
    func didTapDownloadRecipe(model: RecipeModel?)
}

class RecipesListCell: UITableViewCell {
    static let reusableIdentifier: String = "RecipesListCell"

    // swiftlint:disable:this weak_delegate
    weak var delegate: RecipesListCellDelegate?

    private var recipe: RecipeModel? {
        didSet {
            self.fillRecipeTitle()
            self.fillRecipeSubTitle()
            self.fillRecipeDescription()
        }
    }

    @IBOutlet private var documentImage: UIImageView! {
        didSet {
            self.documentImage.tintColor = MediQuoSDK.instance.style?.primaryColor
        }
    }

    @IBOutlet private var documentView: UIView! {
        didSet {
            self.documentView.rounded()
        }
    }

    @IBOutlet private var recipeTitle: UILabel! {
        didSet {
            self.recipeTitle.isHidden = true
            self.recipeTitle.textColor = ColorName.ultraDarkGray.color
            self.recipeTitle.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var recipeSubTitle: UILabel! {
        didSet {
            self.recipeSubTitle.isHidden = true
            self.recipeSubTitle.textColor = .lightGray
            self.recipeSubTitle.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var recipeDescription: UILabel! {
        didSet {
            self.recipeDescription.isHidden = true
            self.recipeDescription.textColor = .lightGray
            self.recipeDescription.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        }
    }

    @IBOutlet private var downloadView: UIView! {
        didSet {
            self.downloadView.isHidden = false
            self.downloadView.rounded()
            self.downloadView.tintColor = MediQuoSDK.instance.style?.primaryColor

            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapDownloadRecipe))
            self.downloadView.addGestureRecognizer(tap)
        }
    }

    func configure(recipe: RecipeModel) {
        self.recipe = recipe
    }
}

extension RecipesListCell {
    @objc func didTapDownloadRecipe() {
        self.delegate?.didTapDownloadRecipe(model: self.recipe)
    }

    private func fillRecipeTitle() {
        guard let activeSubstances = self.recipe?.activeSubstances, !activeSubstances.isEmpty else { return }
        let activeSubstancesString = activeSubstances.map { $0 }.joined(separator: ", ")
        self.recipeTitle.isHidden = false
        self.recipeTitle.text = activeSubstancesString
    }

    private func fillRecipeSubTitle() {
        guard let name = self.recipe?.professional?.name else { return }
        self.recipeSubTitle.isHidden = false
        self.recipeSubTitle.text = name
    }

    private func fillRecipeDescription() {
        guard let prescribedAt = self.recipe?.prescribedAt,
            let date = DateManager.convertToDateFromRemoteISO(prescribedAt),
            let formattedDate = DateManager.convertToString(date, with: DateManager.DateFormat.onlyDate) else {
            return
        }
        self.recipeDescription.isHidden = false
        self.recipeDescription.text = formattedDate
    }
}
