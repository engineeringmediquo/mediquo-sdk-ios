// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class RecipesListViewController: BaseViewControllerSDK {
    @IBOutlet private var tableView: UITableView! {
        didSet {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.backgroundColor = .white
            self.tableView.tableFooterView = UIView()
            self.tableView.refreshControl = UIRefreshControl()
            self.tableView.refreshControl?.tintColor = MediQuoSDK.instance.style?.accentColor
            self.tableView.refreshControl?.addTarget(self, action: #selector(self.refreshViewFromRefreshControl), for: .valueChanged)
        }
    }

    @Inject
    var viewModel: RecipesViewModel

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.disableLargeTitle()
        self.setupNavigationBar()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetch()
    }

    override func bindViewModels() {
        super.bindViewModels()
        self.viewModel.models.subscribe { [weak self] models in
            guard let self = self, let models = self.viewModel.models.value else { return }
            if models.isEmpty {
                self.loadEmptyView()
            } else {
                self.removeChild()
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
        self.viewModel.data.subscribe { [weak self] data in
            guard let self = self, let data = data, let recipeModel = self.viewModel.pdfModel else { return }
            Wireframe.navigate(to: .recipePDF(data, recipeModel), from: self.navigationController)
        }
    }

    override func unBindViewModels() {
        super.unBindViewModels()
        self.viewModel.models.unsubscribe()
        self.viewModel.data.unsubscribe()
    }
}

extension RecipesListViewController: UITableViewDataSource {
    public func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.viewModel.models.value?.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RecipesListCell.reusableIdentifier, for: indexPath) as? RecipesListCell,
              let recipe = self.viewModel.getRecipe(position: indexPath.row) else {
            return UITableViewCell()
        }
        cell.configure(recipe: recipe)
        cell.delegate = self
        return cell
    }
}

extension RecipesListViewController: UITableViewDelegate {
    public func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    public func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 128
    }

    public func tableView(_: UITableView, heightForFooterInSection _: Int) -> CGFloat {
        return 80
    }

    public func tableView(_: UITableView, viewForFooterInSection _: Int) -> UIView? {
        return self.buildFooterView()
    }

    private func buildFooterView() -> UIView {
        let footerView = UIView()
        let colorTop = UIColor.white.withAlphaComponent(0.25).cgColor
        let colorBottom = UIColor.white.withAlphaComponent(1).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 80))
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        footerView.layer.addSublayer(gradientLayer)

        return footerView
    }
}

extension RecipesListViewController: RecipesListCellDelegate {
    func didTapDownloadRecipe(model: RecipeModel?) {
        guard let uuid = model?.uuid else { return }
        self.viewModel.pdfModel = model
        self.viewModel.download(by: uuid)
    }
}

extension RecipesListViewController {
    private func setupNavigationBar() {
        self.setupNavigationBarTitle()
    }

    private func setupNavigationBarTitle() {
        self.title = Localizable.string("recipes.title")
    }

    private func fetch() {
        self.viewModel.fetch()
    }

    private func loadEmptyView() {
        let viewController = MediQuoSDK.getRecipesListEmpty()
        self.add(asChildViewController: viewController)
    }

    @objc func refreshViewFromRefreshControl() {
        self.tableView.refreshControl?.beginRefreshing()
        self.fetch()
    }
}
