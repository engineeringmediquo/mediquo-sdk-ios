// Copyright © 2021 Medipremium S.L. All rights reserved.

import UIKit

public class RecipesEmptyListViewController: BaseViewControllerSDK {
    @IBOutlet private var skeletonImageView: UIImageView! {
        didSet {
            self.skeletonImageView.image = Asset.emptyListReport.image
        }
    }

    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = L10n.Localizable.Recipes.EmptyView.title
            self.titleLabel.font = FontFamily.GothamRounded.medium.font(size: 18).customOrDefault
            self.titleLabel.textColor = MediQuoSDK.instance.style?.primaryColor
        }
    }

    @IBOutlet private var subtitleLabel: UILabel! {
        didSet {
            self.subtitleLabel.text = L10n.Localizable.Recipes.EmptyView.subtitle
            self.subtitleLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
            self.subtitleLabel.textColor = ColorName.ultraDarkGray.color
        }
    }
}
