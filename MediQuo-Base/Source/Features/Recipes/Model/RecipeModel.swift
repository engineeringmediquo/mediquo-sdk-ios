//
//  RecipeModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 1/3/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct RecipeModel: Codable, Equatable {
    public let id: Int?
    public let professional: Professional?
    public let activeSubstances: [String]?
    public let prescribedAt: String?
    public let uuid: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case professional
        case activeSubstances = "active_substances"
        case prescribedAt = "prescribed_at"
        case uuid
    }

    public struct Professional: Codable, Equatable {
        public let name: String?
        public let hash: String?
        public let avatar: String?

        private enum CodingKeys: String, CodingKey {
            case name
            case hash
            case avatar
        }
    }
}
