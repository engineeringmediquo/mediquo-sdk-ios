//
//  Localizable+Helper.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 2/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

class Localizable {
    fileprivate static let TargetLocalizable: String = "TargetLocalizable"

    class func string(_ key: String) -> String {
        guard let path = Bundle(for:MediQuoSDK.self).path(forResource: "MediQuo_Base", ofType: "bundle") else { return ""}
        let bundle = Bundle(path: path) ?? Bundle.main
        return NSLocalizedString(key, bundle: bundle, comment: "my_comment")
    }
}
