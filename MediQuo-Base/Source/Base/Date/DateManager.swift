// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

public enum DateManager {
    public enum DateFormat {
        static let remoteDateFormat = "yyyy-MM-dd HH:mm:ss"
        static let remoteDateFormatISO = "yyyy-MM-dd'T'HH:mm:ssZZ"
        public static let remoteDateFormatOnlyDate = "yyyy-MM-dd"
        static let onlyHours = "HH:mm"
        public static let onlyDate = "dd/MM/yy"
        static let onlyDayAndMonth = "dd MMMM"
    }

    static func convertToDateFromRemote(_ string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.remoteDateFormat
        return formatter.date(from: string)
    }

    static func convertToDateFromRemoteISO(_ string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.remoteDateFormatISO
        return formatter.date(from: string)
    }

    static func convertToString(_ date: Date, dateStyle: DateFormatter.Style?, timeStyle: DateFormatter.Style?) -> String? {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle ?? .none
        formatter.timeStyle = timeStyle ?? .none
        return formatter.string(from: date)
    }

    public static func convertToString(_ date: Date, with format: String?) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format ?? DateFormat.remoteDateFormat
        return formatter.string(from: date)
    }

    public static func isValidDate(date: Date) -> Bool {
        let currentDate = Date()
        let eighteenYearsAgo = Calendar.current.date(byAdding: .year, value: -18, to: currentDate) ?? Date()
        return date <= currentDate && date <= eighteenYearsAgo
    }
    
    static func convertToStringForInbox(_ date: Date) -> String {
        switch true {
        case Calendar.current.isDateInToday(date):
            return DateManager.convertToString(date, with: DateManager.DateFormat.onlyHours) ?? ""
        case Calendar.current.isDateInYesterday(date):
            return L10n.Localizable.yesterday
        default:
            return DateManager.convertToString(date, with: DateManager.DateFormat.onlyDate) ?? ""
        }
    }
    
    public static func formatColloquial(_ date: Date) -> String {
        if Calendar.current.isDateInToday(date) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let date: String? = dateFormatter.string(from: date)
            return date ?? ""
        } else if Calendar.current.isDateInYesterday(date) {
            return Localizable.string("mediquo.shared.yesterday")
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yy"
            let date: String? = dateFormatter.string(from: date)
            return date ?? ""
        }
    }

    public static func formatColloquialInRoom(_ date: Date) -> String {
        if Calendar.current.isDateInToday(date) {
            return Localizable.string("mediquo.groups.group.message.separator.today")
        } else if Calendar.current.isDateInYesterday(date) {
            return Localizable.string("mediquo.shared.yesterday")
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yy"
            let date: String? = dateFormatter.string(from: date)
            return date ?? ""
        }
    }
    
    static func calculateDateRangeUpToHours(date: Date, upTo hours: Int) -> Bool {
        guard let upToDate = Calendar.current.date(byAdding: .hour, value: hours, to: date) else { return false }
        let range = date ... upToDate
        return range.contains(Date())
    }

    static func calculateDateRangeBetweenHours(date: Date, fromUpTo fromHours: Int, toUpTo toHours: Int) -> Bool {
        guard let fromUpToDate = Calendar.current.date(byAdding: .hour, value: fromHours, to: date),
              let toUpToDate = Calendar.current.date(byAdding: .hour, value: toHours, to: date) else { return false }
        let range = fromUpToDate ... toUpToDate
        return range.contains(Date())
    }
}
