//
//  RemoteBasesUrl.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

public struct RemoteBasesUrl {
    private static var environmentManager: EnvironmentManager = EnvironmentManager()

    private enum Sdk: String {
        case debug = "https://sdk.dev.mediquo.com"
        case release = "https://sdk.mediquo.com"
    }

    private enum Socket: String {
        case debug = "https://chat-server-dev.mediquo.com"
        case release = "https://chat-server.mediquo.com"
    }
    
    private enum Api: String {
        case debug = "https://chat-dev.mediquo.com/api"
        case release = "https://chat.api.mediquo.com/api"
    }
}

extension RemoteBasesUrl {
    static public var sdkApi: String {
        let environment: EnvironmentType = RemoteBasesUrl.environmentManager.get()
        switch environment {
            case .debug, .testing:
                return RemoteBasesUrl.Sdk.debug.rawValue
            case .release:
                return RemoteBasesUrl.Sdk.release.rawValue
        }
    }

    static public var socketApi: String {
        let environment: EnvironmentType = RemoteBasesUrl.environmentManager.get()
        switch environment {
            case .debug, .testing:
                return RemoteBasesUrl.Socket.debug.rawValue
            case .release:
                return RemoteBasesUrl.Socket.release.rawValue
        }
    }
        
    static public var chatApi: String {
        let environment: EnvironmentType = RemoteBasesUrl.environmentManager.get()
        switch environment {
            case .debug, .testing:
                return RemoteBasesUrl.Api.debug.rawValue
            case .release:
                return RemoteBasesUrl.Api.release.rawValue
        }
    }
}
