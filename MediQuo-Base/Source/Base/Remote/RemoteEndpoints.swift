//
//  RemoteEndpoints.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

enum RemoteEndpoints {
    // MARK: - Installations
    static let installations = "/v1/installations"

    // MARK: - Auth
    static let authenticate = "/v1/authenticate"
    static let invalidate = "/v1/session/invalidate"

    // MARK: - Register Firebase
    static let registerFirebase = "/v1/push-tokens"
    static let unregisterFirebase = "/v1/push-tokens/{token}/unregister"

    // MARK: - Profile
    static let profile = "/v1/profile"

    // MARK: - Professionals
    static let professionals = "/v1/professionals"
    static let unreadMessages = "/v1/unread-messages"
    static let professionalProfile = "/v1/professionals/{hash}"

    // MARK: - Room
    static let room = "/v1/rooms/{roomId}"
    static let read = "/v1/rooms/{roomId}/messages/read"
    static let messages = "/v1/rooms/{roomId}/messages"

    // MARK: - Medical History
    static let medicalHistory = "/v1/customers/medical-history/"

    // MARK: - Reports
    static let reports = "/v1/reports"
    static let report = "/v1/reports/{uuid}"
    static let reportPDF = "/v1/reports/{uuid}/download"

    // MARK: - Recipes
    static let recipes = "/v1/prescriptions"
    static let recipePDF = "/v1/prescriptions/{uuid}/download"
}
