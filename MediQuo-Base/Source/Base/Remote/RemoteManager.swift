// Copyright © 2020 Medipremium S.L. All rights reserved.

import Alamofire
import UIKit

public protocol RemoteManagerProtocol: AnyObject {
    func post<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func get<Response: Codable>(_ endpoint: String, headers: HTTPHeaders?, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func put<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func delete<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>)
    func download<Response: Codable>(_ endpoint: String, fileName: String, completion: @escaping RemoteCompletionTypeAlias<Response>)
}

enum Header {
    static let contentType = "Content-Type"
    static let accept = "Accept"
    static let acceptLanguage = "Accept-Language"
    static let apiKeyName = "X-API-Key"
    static let applicationJson = "application/json"
    static let authorization = "Authorization"
}

public class RemoteManager: RemoteManagerProtocol {
    private var configuration: RemoteConfiguration
    private let baseUrl: String

    public init(_ configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)) {
        self.configuration = configuration
        self.baseUrl = configuration.baseUrl
    }

    deinit {
        self.configuration.session.session.invalidateAndCancel()
    }

    public func post<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .post, headers: nil, parameters: parameters, encoding: JSONEncoding.default) { response in
            completion(response)
        }
    }

    public func get<Response: Codable>(_ endpoint: String, headers: HTTPHeaders?, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .get, headers: headers, parameters: parameters, encoding: URLEncoding.default) { response in
            completion(response)
        }
    }

    public func put<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .put, headers: nil, parameters: parameters, encoding: JSONEncoding.default) { response in
            completion(response)
        }
    }

    public func delete<Response: Codable>(_ endpoint: String, parameters: Codable?, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        self.request(endpoint, method: .delete, headers: nil, parameters: parameters, encoding: JSONEncoding.default) { response in
            completion(response)
        }
    }

    public func download<Response: Codable>(_ endpoint: String, fileName: String, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        guard let url = URL(string: self.baseUrl + endpoint) else { return }
        let isNeedsAuth: Bool = self.isNeedsAuth(by: endpoint)
        let newHeaders = self.fillHeaders(needsAuth: isNeedsAuth)
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(fileName)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        self.configuration.session
            .download(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: newHeaders, to: destination)
            .responseData { response in
                guard let value: Response = response.value as? Response else {
                    completion(.failure(.remoteError(.reason(response.error?.localizedDescription ?? "Wrong response received"))))
                    return
                }
                completion(.success(value))
            }
    }

    // swiftlint:disable cyclomatic_complexity function_body_length
    internal func request<Response: Codable>(_ endpoint: String,
                                             method: HTTPMethod,
                                             headers: HTTPHeaders?,
                                             parameters: Codable?,
                                             encoding: ParameterEncoding = JSONEncoding.default,
                                             completion: @escaping RemoteCompletionTypeAlias<Response>) {
        
        guard let manager = NetworkReachabilityManager(), manager.isReachable else {
            completion(.failure(.remoteError(.reason("Networking error connection"))))
            return
        }
        
        guard let url = URL(string: self.baseUrl + endpoint) else { return }
       // debugPrint("hey hey Request:",url.description)
        CoreLog.remote.debug("Request: %@", url.description)

        let isNeedsAuth: Bool = self.isNeedsAuth(by: endpoint)
        self.configuration.session
            .request(url,
                     method: method,
                     parameters: parameters?.dictionary,
                     encoding: encoding,
                     headers: headers ?? self.fillHeaders(needsAuth: isNeedsAuth))
            .validate()
            .responseData { [weak self] response in
                guard let self = self else { return }

                guard let stringResponse: String = self.parse(response, completion: completion) else { return }
                CoreLog.remote.debug("Response: %@", stringResponse)

                if stringResponse.isEmpty, let voidResponse = VoidResponse() as? Response {
                    completion(.success(voidResponse))
                } else {
                    self.process(response, completion: completion)
                }
            }
    }
}

extension RemoteManager {
    private func fillHeaders(needsAuth: Bool = true) -> HTTPHeaders {
        var headers: HTTPHeaders = HTTPHeaders()
        headers[Header.accept] = Header.applicationJson
        headers[Header.acceptLanguage] = Locale.current.identifier
        headers[Header.contentType] = Header.applicationJson

        let authManager: AuthManager = AuthManager()

        guard let apiKey: String = authManager.getApiKey() else { return headers }
        headers[Header.apiKeyName] = apiKey

        guard needsAuth, let jwt: String = authManager.getJwt(), let type: String = authManager.getType() else { return headers }
        headers[Header.authorization] = [type, jwt].compactMap { $0 }.joined(separator: " ")

        return headers
    }

    private func isNeedsAuth(by url: String) -> Bool {
        return !url.contains(RemoteEndpoints.authenticate)
    }

    private func parse<Response: Codable>(_ response: Alamofire.DataResponse<Data>, completion: @escaping RemoteCompletionTypeAlias<Response>) -> String? {
        guard let data = response.data, let stringResponse: String = String(data: data, encoding: .utf8) else {
            completion(.failure(.remoteError(.reason(response.error?.localizedDescription ?? "Networking error connection"))))
            return nil
        }
        return stringResponse
    }

    private func process<Response: Codable>(_ response: Alamofire.DataResponse<Data>, completion: @escaping RemoteCompletionTypeAlias<Response>) {
        switch response.result {
        case let .success(data):
            guard let response: Response = try? JSONDecoder().decode(Response.self, from: data) else {
                completion(.failure(.remoteError(.reason("Unable to decode response object"))))
                return
            }
            completion(.success(response))
        case let .failure(error):
            completion(.failure(.remoteError(.reason(error.localizedDescription))))
        }
    }
}
