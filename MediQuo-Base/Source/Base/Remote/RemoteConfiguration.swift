// Copyright © 2020 Medipremium S.L. All rights reserved.

import Alamofire
import UIKit

public struct RemoteConfiguration {
    let baseUrl: String
    let session: SessionManager

    public init(baseUrl: String) {
        self.baseUrl = baseUrl

        let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(publicKeys: ServerTrustPolicy.publicKeys(in: Bundle.main), validateCertificateChain: true, validateHost: true)
        let manager: ServerTrustPolicyManager = ServerTrustPolicyManager(policies: [self.baseUrl: serverTrustPolicy])
        self.session = SessionManager(configuration: self.configuration, serverTrustPolicyManager: manager)
    }

    private var configuration: URLSessionConfiguration = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        return configuration
    }()
}
