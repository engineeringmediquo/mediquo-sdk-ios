// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

public enum BaseError: Error {
    case repositoryError(RepositoryError)
    case remoteError(RemoteError)
    case socketError(SocketError)

    public enum RepositoryError {
        case noResponseData
        case localStorageUpToDate
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            default:
                return String(describing: self)
            }
        }
    }

    public enum RemoteError {
        case httpUrlResponse(HTTPURLResponse)
        case code(Int)
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            default:
                return String(describing: self).capitalizingFirstLetter()
            }
        }
    }

    public enum SocketError: Equatable {
        case socketIsDisconnect
        case wrongDataResponse(String)
        case reason(String)

        var description: String {
            switch self {
            case let .reason(reason):
                return "Reason: \(reason)"
            case let .wrongDataResponse(details):
                return "WrongDataResponse: \(details)"
            default:
                return String(describing: self).capitalizingFirstLetter()
            }
        }
    }

    public var description: String {
        switch self {
        case let .repositoryError(error):
            return "RepositoryError - " + error.description
        case let .remoteError(error):
            return "RemoteError - " + error.description
        case let .socketError(error):
            return "SocketError - " + error.description
        }
    }
}

extension BaseError: Equatable {
    public static func == (lhs: BaseError, rhs: BaseError) -> Bool {
        return lhs.description == rhs.description
    }
}

extension String {
    public func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
}
