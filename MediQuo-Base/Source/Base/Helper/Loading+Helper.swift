//
//  Loading+Helper.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 22/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

class LoadingHelper {
    private var activityIndicatorView: UIActivityIndicatorView = .init(style: .large)

    public func showLoading(onView: UIView?,
                            style _: UIActivityIndicatorView.Style = .large) {
        guard let onView = onView else { return }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.activityIndicatorView.color = UIColor.primary()
            self.activityIndicatorView.center = CGPoint(x: onView.center.x, y: onView.center.y)
            self.activityIndicatorView.startAnimating()
            onView.addSubview(self.activityIndicatorView)
            onView.layoutIfNeeded()
        }
    }

    static func centerSpinnerInView(spinnerView: UIView,
                                    activityIndicator: UIActivityIndicatorView) {
        let xCenterConstraint = NSLayoutConstraint(item: spinnerView,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerX,
                                                   multiplier: 1,
                                                   constant: 0)
        spinnerView.addConstraint(xCenterConstraint)

        let yCenterConstraint = NSLayoutConstraint(item: spinnerView,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerY,
                                                   multiplier: 1,
                                                   constant: 0)
        spinnerView.addConstraint(yCenterConstraint)
    }

    public func hideLoading() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
        }
    }
}
