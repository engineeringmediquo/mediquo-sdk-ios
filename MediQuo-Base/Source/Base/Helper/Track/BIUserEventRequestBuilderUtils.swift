// Copyright © 2022 Medipremium S.L. All rights reserved.

import CoreTelephony
import Foundation
import SystemConfiguration

var networkCountryCode: String? {
    return CTTelephonyNetworkInfo().subscriberCellularProvider?.isoCountryCode
}

var currentCountryCode: String? {
    guard let countryCode = networkCountryCode else {
        return Locale.current.languageCode?.lowercased()
    }
    return countryCode.lowercased()
}

enum BIUserEventRequestKey: String {
    case OpenApp = "open app"
    case LoginView = "login view"
    case OpenChat = "chat open"
    case ChatsViewSearch = "chats view search"
    case ChatsViewSearchDismiss = "chats view search dismiss"
    case ChatsViewSearchSelect = "chats view search select"
    case LoginButtonClick = "login button click"
    case Login = "login"
    case Purchase = "purchase"
    case TryPurchase = "try purchase"
    case PurchaseError = "puchase error"
    case UnSubscribe = "unsubscribe"
    case Invite = "invite share"
    case MessageSent = "chat message sent"
    case MessageReceived = "chat message received"
    case ChatsView = "chats view"
    case MyAccountView = "my account view"
    case MedicalHistoryView = "medical history view"
    case RatingNotification = "rating request notification"
    case WelcomeMessageReceived = "welcome message received"
    case WelcomeMessageDelayed = "welcome message delayed"
    case HelpView = "help view"
    case TermsView = "terms view"
    case PrivacyView = "privacy view"
    case AddStripeCardView = "add stripe card view"
    case StripeCardAdded = "stripe card added"
    case AddGoogleCardView = "add google card view"
    case GoogleCardAdded = "google card added"
    case TrialDialogView = "trial dialog view"
    case VerificationCodeView = "verification code view"
    case PaymentMethodView = "payment method view"
    case BannerMGMClick = "banner mgm click"
    // Saturation events
    case SaturationDialogView = "saturation dialog view"
    case SaturationDialogCancel = "saturation dialog cancel"
    case SaturationDialogButton = "saturation dialog wait button"
    // Groups events
    case GroupsPrelaunchView = "groups prelaunch view"
    case GroupsListView = "groups view"
    case GroupsChatView = "open group chat"
    case GroupsDetailView = "open group info"
    case GroupsFollowAction = "follow group"
    case GroupsUnfollowAction = "unfollow group"
    case GroupsAutoCreateRequestAction = "autojoin group"
    case GroupsCreateRequestAction = "created request to group"
    case GroupsCancelRequestAction = "cancelled request to group"
    case GroupsSendMessageAction = "message sent to group"
    case GroupsLeaveAction = "leave group"
    case GroupsBlockedView = "blocked group view"
    case GroupsBlockedButtonAction = "blocked group premium button"

    // Groups Alert Rules events
    case GroupsRulesView = "group rules view"
    case GroupsRulesAccepted = "group rules accepted"
    case GroupsRulesExit = "group rules exit"

    // Groups Alert Conditions events
    case GroupsConditionsView = "join conditions view"
    case GroupsConditionsExit = "join conditions exit"
    case GroupsConditionsEnablePremium = "join conditions enable premium"
    case GroupsConditionsFollowGroup = "join conditions follow group"

    case ChatConnectionWatchdog = "chat connection watchdog"

    // Blog
    case BlogView = "blog view"
    case OpenPost = "blog post view"
    case SharePost = "blog post share"

    // IAP Purchase
    case EnablePremiumClick = "enable premium click"
    case InfoPricingView = "info pricing view"
    case InfoPricingButtonClick = "info pricing button click"
    case InfoPricingCancelIAP = "info pricing cancel"
    case InfoPricingRestoreIAP = "info pricing restore iap"

    // Search Groups
    case SearchRequest = "groups search"
    case SearchCancel = "groups search cancel"

    // Invitation Pro Accepted
    case InvitationProAccepted = "invitation pro accepted"

    // Reports
    case ReportsListView = "reports list view"
    case ReportsListSelect = "reports list select"
    case ReportsListDownload = "reports list download"

    case ReportView = "reports view"
    case ReportViewDownload = "reports view download"

    // VideoCall
    case VideoCallView = "call view"
    case PickUpCall = "call view pick up"
    case HangUpCall = "call view hang up"
    case RejectCall = "call view reject"
    case ToggleAudio = "call view toggle audio"
    case ToggleVideo = "call view toggle video"
    case SwapCamera = "call view switch camera"
    case PermissionAudioRequest = "call view permission audio request"
    case PermissionAudioAccept = "call view permission audio accept"
    case PermissionVideoRequest = "call view permission video request"
    case PermissionVideoAccept = "call view permission video accept"
    case RatingView = "call rating view"
    case RatingClose = "close"
    case RatingGood = "good button"
    case RatingImprovable = "improvable button"

    // Recipes
    case PrescriptionListView = "prescription list view"
    case PrescriptionListDownload = "prescription list download"

    // Appointments
    case AppointmentSeeButtonClick = "appointment see button click"
    case AppointmentAddCreditCardButtonClick = "appointment add credit card button click"
    case AppointmentChangeCreditCardButtonClick = "appointment change credit card button click"

    // Tracking permission
    case TrackingPermissionAllow = "tracking ask app not to track button click"
    case TrackingPermissionNotAllow = "tracking allow button click"

    // Push permission
    case PushNotificationsPopupAllowButtonClick = "push notifications popup allow button click"
    case PushNotificationsPopupNotAllowButtonClick = "push notifications popup not allow button click"

    // Page view
    case PageView = "pageview"

    // Onboarding
    case OnboardingWelcomeView = "onboarding welcome view"
    case OnboardingWelcomeB2bView = "onboarding welcome b2b view"
    case OnboardingWelcomeB2cView = "onboarding welcome b2c view"
    case OnboardingWelcomeStartButtonClick = "onboarding welcome start button click"
    case OnboardingPersonalDataContinueButtonClick = "onboarding personal data continue button click"
    case OnboardingContactDataSkipButtonClick = "onboarding contact data skip button click"
    case OnboardingContactDataContinueButtonClick = "onboarding contact data continue button click"
    case OnboardingElectronicPrescriptionSkipButtonClick = "onboarding electronic prescription skip button click"
    case OnboardingElectronicPrescriptionContinueButtonClick = "onboarding electronic prescription continue button click"
    case OnboardingGenderMedicalRecordSkipButtonClick = "onboarding gender medical record skip button click"
    case OnboardingGenderMedicalRecordContinueButtonClick = "onboarding gender medical record continue button click"
    case OnboardingAddressMedicalRecordSkipButtonClick = "onboarding address medical record skip button click"
    case OnboardingAddressMedicalRecordContinueButtonClick = "onboarding address medical record continue button click"

    // Push received
    case RemoteMessageReceived = "remote message received"
}

extension BIUserEventRequest {
    // TODO: Cane - Revisar como montar el Builer ya no para PAT sino para SDK
//    static func create(key: BIUserEventRequestKey, actionType: String? = "event") -> BIUserEventRequest? {
//        return BIUserEventRequest(builder: BIUserEventRequestBuilder {
//            $0.installation_id = NetManager.shared.getInstallationGuid()
//            $0.event_type = key.rawValue
//            $0.action_type = actionType
//
//            let device = UIDevice.current
//
//            $0.device_id = device.identifierForVendor?.uuidString
//            $0.device_os = "ios"
//            $0.device_os_version = device.systemVersion
//            $0.device_model = device.model
//            $0.device_connection_type = BIUserEventRequest.connectionType().rawValue
//            $0.device_carrier = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName
//            $0.app_version = Bundle.main.shortVersionNumber
//            $0.app_referrer = MediQuo.referrer?.data[LoginSocialRequest.Keys.InvitationCode]
//            $0.app_notifications_enabled = userNotificationsEnabled
//
//            let user = UserData.load()
//
//            $0.user_token = user?.customerToken
//            $0.user_hash = user?.customerToken
//            $0.user_birthdate = user?.birthDate
//            $0.user_gender = user?.gender.hashValue == 1 // See android
//            $0.user_country = currentCountryCode
//            $0.user_language = currentLanguageCode
//            $0.user_plan = StripeManager.shared.purchaseStatus.currentPlan?.type.rawValue
//            $0.user_free_days_trial = user?.freePlanFreeDays
//            $0.user_free_days_premium = user?.premiumFreeDays
//
//            let emmaUser = EmmaUserData.load()
//
//            $0.emma_referrer_id = emmaUser?.emma_referrer_id
//            $0.emma_eat_sub1 = emmaUser?.eat_sub1
//            $0.emma_eat_sub2 = emmaUser?.eat_sub2
//            $0.emma_eat_sub3 = emmaUser?.eat_sub3
//            $0.emma_eat_sub4 = emmaUser?.eat_sub4
//            $0.emma_eat_sub5 = emmaUser?.eat_sub5
//            $0.emma_eat_sub6 = emmaUser?.eat_sub6
//            $0.emma_eat_sub7 = emmaUser?.eat_sub7
//            $0.emma_eat_sub8 = emmaUser?.eat_sub8
//            $0.emma_eat_sub9 = emmaUser?.eat_sub9
//            $0.emma_eat_sub10 = emmaUser?.eat_sub10
//
//            if let name = UserData.load()?.subscription?.name {
//                // 🙈 TODO: Algún dia se tendrá que quitar Vithas hardcoded
//                $0.organization = name == PlanProvider.vithas.rawValue ? PlanProvider.vithas.rawValue : ""
//            }
//        })
//    }

    fileprivate static func connectionType() -> ConnectionType {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, "8.8.8.8") else {
            return .unknown
        }
        var flags = SCNetworkReachabilityFlags()
        let success = SCNetworkReachabilityGetFlags(reachability, &flags)
        if !success {
            return .unknown
        }
        let isReachable: Bool = (flags.rawValue & SCNetworkReachabilityFlags.reachable.rawValue) != 0
        let needsConnection: Bool = (flags.rawValue & SCNetworkReachabilityFlags.connectionRequired.rawValue) != 0
        let isNetworkReachable: Bool = isReachable && !needsConnection

        if !isNetworkReachable {
            return .none
        } else if (flags.rawValue & SCNetworkReachabilityFlags.isWWAN.rawValue) != 0 {
            return .network
        } else {
            return .wifi
        }
    }
}

enum IAPActionOrigin: String {
    case divider
    case profile
    case services
    case chatListPopup = "chat_list_popup"
    case blockedGroup = "blocked_group"
}

private enum ConnectionType: String {
    case unknown
    case none
    case network
    case wifi
}
