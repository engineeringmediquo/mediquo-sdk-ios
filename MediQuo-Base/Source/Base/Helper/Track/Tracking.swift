// Copyright © 2022 Medipremium S.L. All rights reserved.

import Alamofire

class Tracking {
    public class func track(_ option: Option) {
        if let biEvent = option.biEvent {
            trackBIEvent(parameters: biEvent)
        }
    }

    // MARK: Private methods
    private class func trackBIEvent(parameters: BIUserEventRequest) {
        var baseUrl: String = "https://c7dqhozv39.execute-api.eu-central-1.amazonaws.com/default/"
        var endpoint: String = "biapiUserActivity"
        var remote: RemoteManagerProtocol? = RemoteManager(RemoteConfiguration(baseUrl: baseUrl))
        
        var headers: HTTPHeaders = HTTPHeaders()
        headers[Header.contentType] = Header.applicationJson
        headers[Header.acceptLanguage] = Locale.current.identifier
        remote?.post(endpoint,
                     parameters: parameters) { (result: ResultTypeAlias<VoidResponse>) in
            switch result {
                case .success(_):
                    // Nothing to do on success
                    print("Event send success")
                case .failure(_):
                    // Do with error
                    print("Event send failure")
            }
        }
    }
    
    fileprivate struct Event {
        let key: String
        let attributes: [AttributeKey.RawValue: Any]

        init(key: String, attributes: [AttributeKey.RawValue: Any] = [:]) {
            self.key = key
            self.attributes = attributes
        }
    }

    fileprivate enum EmmaEventKey: String {
        case LAUNCH_LOGIN = "LaunchLogin"
        case LAUNCH_VERIFICATION_CODE = "LaunchVerificationCode"
        case LAUNCH_QUESTION_1 = "LaunchQuestion1"
        case LAUNCH_QUESTION_2 = "LaunchQuestion2"
        case LAUNCH_QUESTION_3 = "LaunchQuestion3"
        case LAUNCH_QUESTION_4 = "LaunchQuestion4"
        case LAUNCH_TUTORIAL_PAGE1 = "LaunchTutorialPage1"
        case LAUNCH_TUTORIAL_PAGE2 = "LaunchTutorialPage2"
        case LAUNCH_TUTORIAL_PAGE3 = "LaunchTutorialPage3"
        case SHOW_TRIAL_DIALOG = "ShowTrialDialog"
        case LOGIN_WITH_APPLE = "LoginWithApple"
        case LOGIN_WITH_APPLE_BUTTON = "LoginWithAppleButton"
        case LOGIN_WITH_SMS = "LoginWithSms"
        case LOGIN_WITH_SMS_BUTTON = "LoginWithSmsButton"
        case LOGIN_WITH_FACEBOOK = "LoginWithFacebook"
        case LOGIN_WITH_FACEBOOK_BUTTON = "LoginWithFacebookButton"
        case LOGIN_WITH_GOOGLE = "LoginWithGoogle"
        case LOGIN_WITH_GOOGLE_BUTTON = "LoginWithGoogleButton"
        case LAUNCH_CHATS = "LaunchChats"
        case LAUNCH_MY_ACCOUNT = "LaunchMyAccount"
        case LAUNCH_PREMIUM = "LaunchPremium"
        case LAUNCH_MEDICAL_HISTORY = "LaunchMedicalHistory"
        case LAUNCH_BLOG = "LaunchBlog"
        case OPEN_BLOG_POST = "OpenBlogPost"
        case SHARE_BLOG_POST = "ShareBlogPost"
        case OPEN_CHAT = "OpenChat"
        case MESSAGE_SENT = "MessageSent"
        case MESSAGE_RECEIVED = "messageReceived"
        case UNSUBSCRIBE = "Unsubscribe"
        case LAUNCH_PAYMENT_METHOD = "LaunchPaymentMethod"
        case LAUNCH_ADD_CARD_WITH_STRIPE = "LaunchAddCardWithStripe"
        case CARD_ADDED_WITH_STRIPE = "CardAddedWithStripe"
        // TODO:
        case LAUNCH_ADD_CARD_WITH_GOOGLE = "LaunchAddCardWithGoogle"
        // TODO:
        case CARD_ADDED_WITH_GOOGLE = "CardAddedWithGoogle"
        case BANNER_MGM = "BannerMGM"
        // Saturation
        case SaturationDialogView
        case SaturationDialogCancel
        case SaturationDialogButton = "SaturationDialogWaitButton"
        // Groups
        case GroupsPrelaunchView = "LaunchGroupsPrelaunch"
        case GroupsListView = "LaunchGroups"
        case GroupsChatView = "OpenGroupChat"
        case GroupsDetailView = "OpenGroupInfo"
        case GroupsFollowAction = "FollowGroup"
        case GroupsUnfollowAction = "UnfollowGroup"
        case GroupsAutoCreateRequestAction = "AutojoinToGroup"
        case GroupsCreateRequestAction = "CreatedRequestToGroup"
        case GroupsCancelRequestAction = "CancelledRequestToGroup"
        case GroupsSendMessageAction = "MessageSentToGroup"
        case GroupsLeaveAction = "LeaveGroup"

        // Groups Alert Rules events
        case GroupsRulesView = "group rules view"
        case GroupsRulesAccepted = "group rules accepted"
        case GroupsRulesExit = "group rules exit"

        // Groups Alert Conditions events
        case GroupsConditionsView = "join conditions view"
        case GroupsConditionsExit = "join conditions exit"
        case GroupsConditionsEnablePremium = "join conditions enable premium"
        case GroupsConditionsFollowGroup = "join conditions follow group"

        // FreePlanActivated
        case FreePlanActivated

        // IAP Purchase
        case InfoPricingView = "info pricing view"
        case Purchase = "purchase"

        // Invitation Pro Accepted
        case InvitationProAccepted
    }

    fileprivate enum AttributeKey: String {
        case fromInvitation = "from invitation"
        case country
        case hasAccess = "has access"
        case speciality
        case method
        case groupId
        case blogPostId
        case blogPostName
        case price
        case currencyCode
        case plan
        case planId
        case professionalHash
    }

    private init() {}

    // TODO: Cane - Crear las Options para el tracking del SDK y eliminar las de PAT que no se vayan a usar
    struct Option {
        fileprivate let biEvent: BIUserEventRequest?
        fileprivate let emmaEvent: Event?
        fileprivate let appsFlyerEvent: Event?
        fileprivate let fabricEvent: Event?

        private init(biEvent: BIUserEventRequest? = nil, emmaEvent: Event? = nil, appsFlyerEvent: Event? = nil, fabricEvent: Event? = nil) {
            self.biEvent = biEvent
            self.emmaEvent = emmaEvent
            self.appsFlyerEvent = appsFlyerEvent
            self.fabricEvent = fabricEvent
        }

        private static var countryAttributes: [AttributeKey.RawValue: Any] {
            var withAttributes: [AttributeKey.RawValue: Any] = [:]
            withAttributes[AttributeKey.country.rawValue] = currentCountryCode
            return withAttributes
        }
    }
}
