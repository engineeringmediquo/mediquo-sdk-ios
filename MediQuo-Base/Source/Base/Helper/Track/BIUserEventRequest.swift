// Copyright © 2022 Medipremium S.L. All rights reserved.

public class BIUserEventRequestBuilder {
    public var installation_id: String?
    public var event_type: String?
    public var action_type: String?

    public var device_id: String?
    public var device_os: String?
    public var device_os_version: String?
    public var device_model: String?
    public var device_connection_type: String?
    public var device_carrier: String?
    public var app_version: String?
    public var app_referrer: String?
    public var app_notifications_enabled: Bool?

    public var user_token: String?
    public var user_hash: String?
    public var user_birthdate: String?
    public var user_gender: Bool?
    public var user_longitude: Double?
    public var user_latitude: Double?
    public var user_city: String?
    public var user_country: String?
    public var user_language: String?
    public var user_banned: Bool?
    public var user_uninstalled: Bool?
    public var user_plan: String?
    public var user_free_days_trial: Int?
    public var user_free_days_premium: Int?

    public var event_message_type: String?
    public var event_message_id: String?
    public var event_message_room_id: Int?
    public var event_message_speciality: String?
    public var event_message_professional_hash: String?
    public var event_message_content: String?
    public var event_rating_type: String?
    public var event_rating_stars: Int?
    public var event_open_chat_has_access: Bool?
    public var event_open_chat_speciality: String?
    public var event_purchase_item: String?
    public var event_purchase_price: Double?
    public var event_purchase_currency: String?
    public var event_origin: String?
    public var event_login_method: String?
    public var event_query: String?
    public var event_remote_message_id: String?

    public var emma_referrer_id: String?
    public var emma_eat_sub1: String?
    public var emma_eat_sub2: String?
    public var emma_eat_sub3: String?
    public var emma_eat_sub4: String?
    public var emma_eat_sub5: String?
    public var emma_eat_sub6: String?
    public var emma_eat_sub7: String?
    public var emma_eat_sub8: String?
    public var emma_eat_sub9: String?
    public var emma_eat_sub10: String?
    public var organization: String?

    public var event_blog_post_id: Int?

    public var event_appointment_id: String?

    public var event_purchase_error_id: String?
    public var event_purchase_error_message: String?
    public var event_puchase_item: String?
    public var event_puchase_price: String?
    public var event_puchase_currency: String?

    public var event_call_type: String?
    public var event_call_id: String?

    public typealias BuilderClosure = (BIUserEventRequestBuilder) -> Void

    public required init(builder: BuilderClosure) {
        builder(self)
    }
}

public struct BIUserEventRequest: Codable { 
    public let installation_id: String
    public let event_type: String
    public var action_type: String = "event"

    public var device_id: String? = nil
    public var device_os: String? = nil
    public var device_os_version: String? = nil
    public var device_model: String? = nil
    public var device_connection_type: String? = nil
    public var device_carrier: String? = nil
    public var app_version: String? = nil
    public var app_referrer: String? = nil
    public var app_notifications_enabled: Bool? = nil

    public var user_token: String? = nil
    public var user_hash: String? = nil
    public var user_birthdate: String? = nil
    public var user_gender: Bool? = nil
    public var user_longitude: Double? = nil
    public var user_latitude: Double? = nil
    public var user_city: String? = nil
    public var user_country: String? = nil
    public var user_language: String? = nil
    public var user_banned: Bool? = nil
    public var user_uninstalled: Bool? = nil
    public var user_plan: String? = nil
    public var user_free_days_trial: Int? = nil
    public var user_free_days_premium: Int? = nil

    public var event_message_type: String? = nil
    public var event_message_id: String? = nil
    public var event_message_room_id: Int? = nil
    public var event_message_speciality: String? = nil
    public var event_message_professional_hash: String? = nil
    public var event_message_content: String? = nil
    public var event_rating_type: String? = nil
    public var event_rating_stars: Int? = nil
    public var event_open_chat_has_access: Bool? = nil
    public var event_open_chat_speciality: String? = nil
    public var event_purchase_item: String? = nil
    public var event_purchase_price: Double? = nil
    public var event_purchase_currency: String? = nil
    public var event_origin: String? = nil
    public var event_login_method: String? = nil
    public var event_group_id: String? = nil
    public var event_query: String? = nil
    public var event_position: Int? = nil
    public var event_remote_message_id: String? = nil

    public var emma_referrer_id: String? = nil
    public var emma_eat_sub1: String? = nil
    public var emma_eat_sub2: String? = nil
    public var emma_eat_sub3: String? = nil
    public var emma_eat_sub4: String? = nil
    public var emma_eat_sub5: String? = nil
    public var emma_eat_sub6: String? = nil
    public var emma_eat_sub7: String? = nil
    public var emma_eat_sub8: String? = nil
    public var emma_eat_sub9: String? = nil
    public var emma_eat_sub10: String? = nil
    public var organization: String? = nil

    public var event_blog_post_id: Int?
    public var event_alert_group_id: Int?

    public var event_appointment_id: String? = nil

    public var event_purchase_error_id: String? = nil
    public var event_purchase_error_message: String? = nil
    public var event_puchase_item: String? = nil
    public var event_puchase_price: String? = nil
    public var event_puchase_currency: String? = nil

    public var event_call_type: String? = nil
    public var event_call_id: String? = nil

    init(installation_id: String, event_type: String) {
        self.installation_id = installation_id
        self.event_type = event_type
    }

    public init?(builder: BIUserEventRequestBuilder) {
        guard let installation_id = builder.installation_id,
            let event_type = builder.event_type else {
            return nil
        }
        self.installation_id = installation_id
        self.event_type = event_type
        action_type = builder.action_type ?? "event"

        device_id = builder.device_id
        device_os = builder.device_os
        device_os_version = builder.device_os_version
        device_model = builder.device_model
        device_connection_type = builder.device_connection_type
        device_carrier = builder.device_carrier
        app_version = builder.app_version
        app_referrer = builder.app_referrer
        app_notifications_enabled = builder.app_notifications_enabled

        user_token = builder.user_token
        user_hash = builder.user_hash
        user_birthdate = builder.user_birthdate
        user_gender = builder.user_gender
        user_longitude = builder.user_longitude
        user_latitude = builder.user_latitude
        user_city = builder.user_city
        user_country = builder.user_country
        user_language = builder.user_language
        user_banned = builder.user_banned
        user_uninstalled = builder.user_uninstalled
        user_plan = builder.user_plan
        user_free_days_trial = builder.user_free_days_trial
        user_free_days_premium = builder.user_free_days_premium

        event_message_type = builder.event_message_type
        event_message_id = builder.event_message_id
        event_message_room_id = builder.event_message_room_id
        event_message_speciality = builder.event_message_speciality
        event_message_professional_hash = builder.event_message_professional_hash
        event_message_content = builder.event_message_content
        event_rating_type = builder.event_rating_type
        event_rating_stars = builder.event_rating_stars
        event_open_chat_has_access = builder.event_open_chat_has_access
        event_open_chat_speciality = builder.event_open_chat_speciality
        event_purchase_item = builder.event_purchase_item
        event_purchase_price = builder.event_purchase_price
        event_purchase_currency = builder.event_purchase_currency
        event_origin = builder.event_origin
        event_login_method = builder.event_login_method
        event_query = builder.event_query
        event_remote_message_id = builder.event_remote_message_id

        emma_referrer_id = builder.emma_referrer_id
        emma_eat_sub1 = builder.emma_eat_sub1
        emma_eat_sub2 = builder.emma_eat_sub2
        emma_eat_sub3 = builder.emma_eat_sub3
        emma_eat_sub4 = builder.emma_eat_sub4
        emma_eat_sub5 = builder.emma_eat_sub5
        emma_eat_sub6 = builder.emma_eat_sub6
        emma_eat_sub7 = builder.emma_eat_sub7
        emma_eat_sub8 = builder.emma_eat_sub8
        emma_eat_sub9 = builder.emma_eat_sub9
        emma_eat_sub10 = builder.emma_eat_sub10
        organization = builder.organization
        event_blog_post_id = builder.event_blog_post_id

        event_appointment_id = builder.event_appointment_id

        event_purchase_error_id = builder.event_purchase_error_id
        event_purchase_error_message = builder.event_purchase_error_message
        event_puchase_item = builder.event_puchase_item
        event_purchase_price = builder.event_purchase_price
        event_puchase_currency = builder.event_puchase_currency

        event_call_type = builder.event_call_type
        event_call_id = builder.event_call_id
    }
}
