//
//  UIFont+Helper.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 15/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

public class UIFontHelper {
    
    public enum UIFontType {
        case bold, book, light, medium
    }
    
    static public func getStandarUIFont(from type: UIFontType,
                                        and size: CGFloat) -> UIFont {
        switch type {
            case .bold:
                return FontFamily.GothamRounded.bold.font(size: size)
            case .book:
                return FontFamily.GothamRounded.book.font(size: size)
            case .light:
                return FontFamily.GothamRounded.light.font(size: size)
            case .medium:
                return FontFamily.GothamRounded.medium.font(size: size)
        }
    }
}
