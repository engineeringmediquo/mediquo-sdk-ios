// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UIApplication {
    public class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return self.topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return self.topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return self.topViewController(presented)
        }
        if let viewController = base?.children.first {
            return self.topViewController(viewController)
        }
        return base
    }
}
