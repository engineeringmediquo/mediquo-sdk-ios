// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

public protocol Nib: AnyObject {
    static var nibName: String { get }
    static func loadFromNib<T: Nib>() -> T?
}

extension Nib {
    public static func loadFromNib<T: Nib>() -> T? {
        return Bundle(for: T.self).loadNibNamed(T.nibName, owner: nil, options: [:])?.first as? T
    }
}
