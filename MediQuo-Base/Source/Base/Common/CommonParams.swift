// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

enum SDK: String {
    case version = "1.0.0"
}

enum RepositoryOrigin {
    case local
    case remote
}
