//
//  LocalizablesVideoCall.swift
//  MediQuo-Base
//
//  Created by David Martin on 16/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import mediquo_videocall_lib

var localizableVC: LocalizableVC {
    return LocalizableVC(incomingCall: L10n.Localizable.Shared.incomingCall,
                         incomingVideoCall: L10n.Localizable.Shared.incomingVideoCall,
                         calling: L10n.Localizable.Shared.calling,
                         videoCalling: L10n.Localizable.Shared.videoCalling,
                         rejectedCall: L10n.Localizable.Shared.rejectedCall,
                         rejectedVideoCall: L10n.Localizable.Shared.rejectedVideoCall,
                         endedVideoCall: L10n.Localizable.Shared.endedCall,
                         cameraAndMicrophoneDeactivated: L10n.Localizable.Shared.cameraAndMicrophoneDeactivated,
                         cameraDeactivated: L10n.Localizable.Shared.cameraDeactivated,
                         microphoneDesactivated: L10n.Localizable.Shared.microphoneDesactivated,
                         speakerActivated: L10n.Localizable.Shared.speakerActivated,
                         reconnecting: L10n.Localizable.Shared.reconnecting,
                         requestingChangeToVideoCall: L10n.Localizable.Shared.requestingChangeToVideoCall,
                         cancel: L10n.Localizable.Shared.cancel,
                         refuse: L10n.Localizable.Shared.refuse,
                         accept: L10n.Localizable.Shared.accept,
                         applicationCanceled: L10n.Localizable.Shared.applicationCanceled,
                         isAskingYouChangeToVideoall: L10n.Localizable.Shared.isAskingYouChangeToVideoall,
                         ok: L10n.Localizable.Shared.ok,
                         error: L10n.Localizable.Shared.error,
                         microProLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Professional.Micro.label,
                         camaraProLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Professional.Camara.label,
                         microAndCamaraProLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Professional.MicroAndCamara.label,
                         microCustomerLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Customer.Micro.label,
                         camaraCustomerLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Customer.Camara.label,
                         microAndCamaraCustomerLabel: L10n.Localizable.Mainviewcontoller.CentralToast.Customer.MicroAndCamara.label,
                         permissionAudioTitle: L10n.Localizable.Popup.Permission.Audio.title,
                         permissionAudioSubTitle: L10n.Localizable.Popup.Permission.Audio.subtitle,
                         permissionAudioButton: L10n.Localizable.Popup.Permission.Audio.Action.button,
                         permissionVideoTitle: L10n.Localizable.Popup.Permission.Video.title,
                         permissionVideoSubTitle: L10n.Localizable.Popup.Permission.Video.subtitle,
                         permissionVideoButton: L10n.Localizable.Popup.Permission.Video.Action.button,
                         permissionAudioDeniedTitle: L10n.Localizable.Popup.Professional.Client.Permission.Audio.Denied.title,
                         permissionAudioDeniedSubTitle: L10n.Localizable.Popup.Professional.Client.Permission.Audio.Denied.subtitle,
                         permissionAudioDeniedButton: L10n.Localizable.Popup.Professional.Client.Permission.Audio.Denied.Action.button,
                         permissionVideoDeniedTitle: L10n.Localizable.Popup.Professional.Client.Permission.Video.Denied.title,
                         permissionVideoDeniedSubTitle: L10n.Localizable.Popup.Professional.Client.Permission.Video.Denied.subtitle,
                         permissionVideoDeniedButton: L10n.Localizable.Popup.Professional.Client.Permission.Video.Denied.Action.button,
                         createReportMessage: L10n.Localizable.Createreportcontroller.Message.label,
                         createReportExitButton: L10n.Localizable.Createreportcontroller.Exit.button,
                         createReportCreateButton: L10n.Localizable.Createreportcontroller.Create.button,
                         popupBusyTitle: L10n.Localizable.Videocall.Popup.Busy.title,
                         popupBusySubtitle: L10n.Localizable.Videocall.Popup.Busy.subtitle,
                         popupBusyButton: L10n.Localizable.Videocall.Popup.Busy.button,
                         ratingTitle: "",
                         ratingMessage: "",
                         ratingGood: "",
                         ratingImprovable: "")
}
