// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

public typealias ResultTypeAlias<Response> = Result<Response, BaseError>

public typealias RemoteCompletionTypeAlias<Response: Codable> = (ResultTypeAlias<Response>) -> Void

public typealias EmptyCompletionTypeAlias = (Result<Void, Error>) -> Void
