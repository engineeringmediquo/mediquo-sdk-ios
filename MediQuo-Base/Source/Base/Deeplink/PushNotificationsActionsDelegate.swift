// Copyright © 2020 Medipremium S.L. All rights reserved.

import UserNotifications

protocol PushNotificationsActionsDelegate: AnyObject {
    @available(iOS 10.0, *)
    func setNotificationPresentation(by roomId: Int, completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    func execute(deeplink: NotificationSchema)
}
