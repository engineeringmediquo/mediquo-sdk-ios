//
//  MetaResponse.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct MetaResponse<Meta: Codable>: Codable {
    enum CodingKeys: String, CodingKey {
        case meta
    }

    let meta: Meta
}
