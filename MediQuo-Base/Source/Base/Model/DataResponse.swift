//
//  DataResponse.swift
//  MediQuo-Base
//
//  Created by David Martin on 18/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct DataResponse<Data: Codable>: Codable {
    enum CodingKeys: String, CodingKey {
        case data
    }

    let data: Data
}
