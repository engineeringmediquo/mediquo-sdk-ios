// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

extension CoreLog {
    public static let business = CoreLog(identifier: "com.mediquo.base.lib", category: "business")
    public static let ui = CoreLog(identifier: "com.mediquo.base.lib", category: "ui")
    public static let firebase = CoreLog(identifier: "com.mediquo.base.lib", category: "firebase")
    public static let remote = CoreLog(identifier: "com.mediquo.base.lib", category: "remote")
    public static let socketLog = CoreLog(identifier: "com.mediquo.base.lib", category: "socket")
    public static let tracking = CoreLog(identifier: "com.mediquo.base.lib", category: "tracking")
}
