// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UIViewController {
    func disableLargeTitle() {
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    func setTitle(title: String?, subtitle: String? = nil) -> UIView {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -4, width: 0, height: 0))
 
        titleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        titleLabel.textColor = MediQuoSDK.instance.style?.primaryContrastColor
        titleLabel.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        titleLabel.text = title
        titleLabel.sizeToFit()

        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: 0, height: 0))
        subtitleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        subtitleLabel.textColor = MediQuoSDK.instance.style?.primaryContrastColor
        subtitleLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()

        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)

        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width

        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }

        return titleView
    }

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    func setBackButtonNavigationController(with title: String = "", color: UIColor = .black) {
        let backButton = UIBarButtonItem()
        backButton.title = title
        backButton.tintColor = color
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

    func setCloseButtonNavigationController(with title: String = "", color: UIColor = ColorName.gray.color, target: AnyObject, selector: String) {
        let closeButton = UIBarButtonItem()
        closeButton.image = Asset.closeGray.image
        closeButton.title = title
        closeButton.tintColor = color
        closeButton.target = target
        closeButton.action = Selector(selector)
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = closeButton
    }

    func addRightImageButtonOnNavigationController(image: UIImage?, target: AnyObject, selector: String? = nil) {
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.image = image
        imageview.contentMode = .scaleAspectFill
        imageview.layer.cornerRadius = 20
        imageview.layer.masksToBounds = true

        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        containView.addSubview(imageview)

        let rightBarButton = UIBarButtonItem(customView: containView)
        rightBarButton.target = target
        rightBarButton.action = Selector(selector ?? "")
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func addRightViewOnNavigationController(view: UIView, target: AnyObject, selector: String? = nil) {
        let rightBarButton = UIBarButtonItem(customView: view)
        rightBarButton.target = target
        rightBarButton.action = Selector(selector ?? "")
        self.navigationItem.rightBarButtonItem = rightBarButton
    }

    func setRightButtonNavigationController(title: String = "", isEnabled: Bool = true, target: AnyObject, selector: String) {
        let rightButton = UIBarButtonItem()
        rightButton.title = title
        rightButton.isEnabled = isEnabled

        rightButton.setTitleTextAttributes([
            .foregroundColor: MediQuoSDK.instance.style?.primaryContrastColor ?? UIColor.white,
            .font: FontFamily.GothamRounded.medium.font(size: 14).customOrDefault as Any
        ], for: .normal)
        rightButton.setTitleTextAttributes([
            .foregroundColor: UIColor.lightGray,
            .font: FontFamily.GothamRounded.medium.font(size: 14).customOrDefault as Any
        ], for: .selected)
        rightButton.setTitleTextAttributes([
            .foregroundColor: UIColor.lightGray,
            .font: FontFamily.GothamRounded.medium.font(size: 14).customOrDefault as Any
        ], for: .disabled)
        rightButton.tintColor = MediQuoSDK.instance.style?.primaryContrastColor ?? .white
        rightButton.target = target
        rightButton.action = Selector(selector)
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    var topbarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0)
                + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        } else {
            return self.navigationController?.navigationBar.frame.height ?? 0.0
        }
    }
    
    func showTabbar() {
        self.tabBarController?.tabBar.layer.zPosition = 0
    }

    func hideTabbar() {
        self.tabBarController?.tabBar.layer.zPosition = -1
    }
}

extension UIViewController {
    func add(asChildViewController viewController: UIViewController, with insets: CGRect? = nil) {
        self.addChild(viewController)
        self.view.addSubview(viewController.view)
        viewController.view.frame = insets ?? view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }

    func removeChild() {
        self.children.forEach {
            $0.willMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
}
