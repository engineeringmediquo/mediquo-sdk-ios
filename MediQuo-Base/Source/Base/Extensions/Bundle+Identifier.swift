// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation
import UIKit

extension Bundle {
    private static var identifier: String {
        return Bundle(for: MediQuoSDK.self).bundleIdentifier ?? "com.mediquo.mediquo-sdk-ios"
    }

    static func localStorageKey(with key: String) -> String {
        return self.identifier + "." + key
    }
}


extension Bundle {
    static var packageBundle: Bundle {
        return Bundle(for: PackageBundleClass.self)
    }
}

private class PackageBundleClass: NSObject {}
