// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UIView {
    func rounded() {
        self.contentMode = .scaleAspectFill
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    func circularShape() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
    }
}
