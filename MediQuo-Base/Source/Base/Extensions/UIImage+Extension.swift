// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

public protocol ImageResourceType {
    var bundle: Bundle { get }
    var name: String { get }
}

public struct ImageResource: ImageResourceType {
    public let bundle: Bundle
    public let name: String

    public init(bundle: Bundle, name: String) {
        self.bundle = bundle
        self.name = name
    }
}

extension UIImage {
    convenience init?(resource: ImageResourceType, compatibleWith traitCollection: UITraitCollection? = nil) {
        self.init(named: resource.name, in: resource.bundle, compatibleWith: traitCollection)
    }

    public func resizedData(maxCount: Int = 512) -> (Data?, CGSize) {
        guard let data: Data = self.compressWith(quality: .lowest) else {
            NSLog("[UIImage] Image JPEG could not be represented")
            return (nil, .zero)
        }

        // or use 0.8 or whatever you want
        let adjustment: CGFloat = 1.0 / sqrt(2.0)
        let size: CGSize = self.size
        var factor: CGFloat = 1.0

        var currentImage: UIImage = self
        var currentSize: CGSize = size
        var currentCount: Int = data.count
        var currentData: Data? = data

        while currentCount >= (maxCount * 1024) {
            factor *= adjustment
            currentSize = CGSize(width: round(Double(size.width * factor)), height: round(Double(size.height * factor)))
            NSLog("[UIImage] Scaling image at [\(currentSize.width) x \(currentSize.height)]...")
            currentImage = self.resizedImage(newSize: currentSize, interpolationQuality: .high)
            if let representation: Data = currentImage.jpegData(compressionQuality: 0.9) {
                currentCount = representation.count
                currentData = representation
            } else {
                NSLog("[UIImage] Image could not be scaled at factor '\(factor)'; Continue with last result...")
                break
            }
        }

        NSLog("[UIImage] Resulting image [\(currentSize.width) x \(currentSize.height)] ('\(currentCount / 1024)' KB)")
        return (currentData, currentSize)
    }

    public func resizedImage(newSize: CGSize, interpolationQuality quality: CGInterpolationQuality) -> UIImage {
        var drawTransposed: Bool

        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            drawTransposed = true
        default:
            drawTransposed = false
        }

        return self.resizedImage(
            newSize: newSize,
            transform: self.transformForOrientation(newSize: newSize),
            drawTransposed: drawTransposed,
            interpolationQuality: quality
        )
    }

    public func resizedImage(newSize: CGSize, transform: CGAffineTransform, drawTransposed transpose: Bool, interpolationQuality quality: CGInterpolationQuality) -> UIImage {
        guard let imageRef = self.cgImage,
            let space = imageRef.colorSpace else {
            return self
        }
        let newRect: CGRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        let transposedRect: CGRect = CGRect(x: 0, y: 0, width: newRect.size.height, height: newRect.size.width)

        // build a context that's the same dimensions as the new size
        guard let bitmap: CGContext = CGContext(
            data: nil,
            width: Int(newRect.size.width),
            height: Int(newRect.size.height),
            bitsPerComponent: imageRef.bitsPerComponent,
            bytesPerRow: 0,
            space: space,
            bitmapInfo: imageRef.bitmapInfo.rawValue
        ) else {
            return self
        }

        // rotate and/or flip the image if required by its orientation
        bitmap.concatenate(transform)

        // set the quality level to use when rescaling
        bitmap.interpolationQuality = quality

        // draw into the context; this scales the image
        bitmap.draw(imageRef, in: transpose ? transposedRect : newRect)

        // get the resized image from the context and a UIImage
        guard let newImageRef: CGImage = bitmap.makeImage() else {
            return self
        }
        return UIImage(cgImage: newImageRef)
    }

    public func transformForOrientation(newSize: CGSize) -> CGAffineTransform {
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch self.imageOrientation {
        case .down, .downMirrored: // EXIF = 4
            transform = transform.translatedBy(x: newSize.width, y: newSize.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        case .left, .leftMirrored: // EXIF = 5
            transform = transform.translatedBy(x: newSize.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
        case .right, .rightMirrored: // EXIF = 7
            transform = transform.translatedBy(x: 0, y: newSize.height)
            transform = transform.rotated(by: -CGFloat(Double.pi / 2))
        default:
            break
        }

        switch self.imageOrientation {
        case .upMirrored, .downMirrored: // EXIF = 4
            transform = transform.translatedBy(x: newSize.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored: // EXIF = 7
            transform = transform.translatedBy(x: newSize.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
        }

        return transform
    }

    public func coloredImage(with color: UIColor) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        UIGraphicsBeginImageContext(self.size)
        let contextRef = UIGraphicsGetCurrentContext()

        contextRef?.translateBy(x: 0, y: self.size.height)
        contextRef?.scaleBy(x: 1.0, y: -1.0)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)

        contextRef?.setBlendMode(CGBlendMode.normal)
        contextRef?.draw(cgImage, in: rect)
        contextRef?.setBlendMode(CGBlendMode.sourceIn)
        color.setFill()
        contextRef?.fill(rect)

        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return coloredImage
    }

    public func compressWith(quality value: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: value.rawValue)
    }
}

public enum JPEGQuality: CGFloat {
    case lowest = 0
    case low = 0.25
    case medium = 0.5
    case high = 0.75
    case highest = 1
}
