// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

extension Array where Element: Comparable {
    func equals(as this: [Element]) -> Bool {
        return self.count == this.count && self.sorted() == this.sorted()
    }
}

extension Array where Element == Bool {
    func sumPositives() -> Int {
        return self.map { (value: Bool) -> Int in return value ? 1 : 0 }.reduce(0, +)
    }
}

extension Array where Element: Hashable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        for value in self where result.contains(value) == false {
            result.append(value)
        }
        return result
    }
}
