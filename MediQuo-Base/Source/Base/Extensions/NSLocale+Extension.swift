// Copyright © 2020 Medipremium S.L. All rights reserved.

public extension NSLocale {
    static var is12hClockFormat: Bool {
        let locale = NSLocale.current
        if let formatter: String = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: locale) {
            return formatter.contains("a")
        }
        return false
    }
}
