// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation
import UIKit

public extension UIAlertAction {
    convenience init(title: String?, style: UIAlertAction.Style, image: UIImage?, handler: ((UIAlertAction) -> Void)? = nil) {
        self.init(title: title, style: style, handler: handler)
        self.image = image
    }

    convenience init?(title: String?, style: UIAlertAction.Style, imageNamed imageName: String, handler: ((UIAlertAction) -> Void)? = nil) {
        if let image = UIImage(named: imageName) {
            self.init(title: title, style: style, image: image, handler: handler)
        } else {
            return nil
        }
    }

    var image: UIImage? {
        get {
            return self.value(forKey: "image") as? UIImage
        }
        set(image) {
            self.setValue(image, forKey: "image")
        }
    }
}
