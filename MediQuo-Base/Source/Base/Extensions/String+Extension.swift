// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

extension String {
    public func computeAgeFromDate(toDate date: Date = Date()) -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let birthDate = formatter.date(from: self) else { return nil }
        let calendar = Calendar(identifier: .gregorian)
        let birthDayComponents = calendar.dateComponents([.year, .month, .day], from: birthDate)
        let todayComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let temptativeAge = (todayComponents.year ?? 0) - (birthDayComponents.year ?? 0)
        if (todayComponents.month ?? 0) < (birthDayComponents.month ?? 0) || ((todayComponents.month ?? 0) == (birthDayComponents.month ?? 0) && (todayComponents.day ?? 0) < (birthDayComponents.day ?? 0)) {
            return temptativeAge - 1
        } else {
            return temptativeAge
        }
    }

    var isValidEmail: Bool {
        let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let testEmail = NSPredicate(format: "SELF MATCHES %@", regularExpressionForEmail)
        return testEmail.evaluate(with: self)
    }

    var isValidPhone: Bool {
        let regularExpressionForPhoneWithoutPrefix = "^[0-9]*$"
        let testPhoneWithoutPrefix = NSPredicate(format: "SELF MATCHES %@", regularExpressionForPhoneWithoutPrefix)
        let regularExpressionForPhoneWithPrefix = "^[+]+[0-9]*$"
        let testPhoneWithPrefix = NSPredicate(format: "SELF MATCHES %@", regularExpressionForPhoneWithPrefix)
        return testPhoneWithoutPrefix.evaluate(with: self) || testPhoneWithPrefix.evaluate(with: self)
    }

    var securedString: String {
        guard let jwt = MediQuoSDK.instance.jwt else { return self }
        return "\(self)&jwt=\(jwt)"
    }
    
    func shorted(to symbols: Int) -> String {
        guard self.count > symbols else {
            return self
        }
        return self.prefix(symbols) + " ..."
    }
}


