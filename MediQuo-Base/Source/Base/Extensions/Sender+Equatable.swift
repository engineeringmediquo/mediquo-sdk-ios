// Copyright © 2020 Medipremium S.L. All rights reserved.

//import MessageKit
//
//extension Sender: Equatable {
//    public static func == (lhs: Sender, rhs: Sender) -> Bool {
//        return lhs.senderId == rhs.senderId && lhs.displayName == rhs.displayName
//    }
//}
//
//extension SenderType {
//    public func equals(_ anotherSender: SenderType) -> Bool {
//        return senderId == anotherSender.senderId && displayName == anotherSender.displayName
//    }
//}
