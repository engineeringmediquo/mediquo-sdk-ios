// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

public extension NSAttributedString {
    convenience init(string: String, customFont: UIFont, color: UIColor = .black) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: customFont,
            .foregroundColor: color
        ]
        self.init(string: string, attributes: attributes)
    }

    func size(maxWidth: CGFloat) -> CGSize {
        return self.boundingRect(with: CGSize(width: maxWidth, height: .greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size
    }
}
