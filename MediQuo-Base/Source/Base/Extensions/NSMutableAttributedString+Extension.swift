// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension NSMutableAttributedString {
    @discardableResult public func bold(_ text: String, color: UIColor? = .black, fontSize size: Int? = 15) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: CGFloat(size ?? 15)),
            NSAttributedString.Key.foregroundColor: color ?? .black
        ]
        let boldString = NSMutableAttributedString(string: text, attributes: attrs)
        append(boldString)
        return self
    }

    @discardableResult public func normal(_ text: String, color: UIColor? = .black, fontSize size: Int? = 15) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: CGFloat(size ?? 15)),
            NSAttributedString.Key.foregroundColor: color ?? .black
        ]
        let normal = NSAttributedString(string: text, attributes: attrs)
        append(normal)
        return self
    }

    @discardableResult public func underline(_ text: String, color: UIColor? = .black, fontSize size: Int? = 15) -> NSMutableAttributedString {
        let underline = NSMutableAttributedString(string: text, attributes: [
            .underlineStyle: true,
            .font: UIFont.systemFont(ofSize: CGFloat(size ?? 15)),
            NSAttributedString.Key.foregroundColor: color ?? .black
        ])
        append(underline)
        return self
    }

    @discardableResult public func italic(_ text: String, color: UIColor? = .black, fontSize size: Int? = 15) -> NSMutableAttributedString {
        let underline = NSMutableAttributedString(string: text, attributes: [
            .font: UIFont.italicSystemFont(ofSize: CGFloat(size ?? 15)),
            NSAttributedString.Key.foregroundColor: color ?? .black
        ])
        append(underline)
        return self
    }
}
