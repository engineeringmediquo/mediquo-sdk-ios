//
//  UIFont+Extension.swift
//  MediQuo-Base
//
//  Created by David Martin on 18/1/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

extension UIFont {
    var customOrDefault: UIFont {
        guard let style = MediQuoSDK.instance.style, style.bookFont != nil, style.mediumFont != nil, style.boldFont != nil else { return self }
        switch self.fontName {
        case FontFamily.GothamRounded.book.name:
            return style.bookFont?.withSize(self.pointSize) ?? self
        case FontFamily.GothamRounded.medium.name:
            return style.mediumFont?.withSize(self.pointSize) ?? self
        case FontFamily.GothamRounded.bold.name:
            return style.boldFont?.withSize(self.pointSize) ?? self
        default:
            return self
        }
    }
}
