// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UINavigationController {
    func back(to viewController: Any) -> UIViewController? {
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: viewController))" {
                self.popToViewController(element, animated: false)
                return element
            }
        }
        return nil
    }
}
