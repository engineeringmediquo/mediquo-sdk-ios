// Copyright © 2020 Medipremium S.L. All rights reserved.

/// Extension to provide some syntactic sugar in Swift 4+
extension Result where Success == Void {
    /// Swift 4 no longer allows us to omit an associated value of type Void, requiring an empty tuple () instead
    /// of the only possible instance of Void
    ///
    /// End result is that we need to write a `.success` in Result<Void, Error> as
    /// * `.success(())`
    ///
    /// The extension below allows us to simply write this instead:
    /// * `.success`

    static var success: Result {
        return .success(())
    }
}
