//
//  Notifications+Extension.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 19/10/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation

extension Notification {
    enum Appointments {
        public static let SeeButtonBannerClick = Notification.Name(rawValue: "com.mediquo.notification.name.appointments.see.button.banner.click")
        public static let AddCreditCardButtonClick = Notification.Name(rawValue: "com.mediquo.notification.name.appointments.add.credit.card.button.click")
        public static let ChangeCreditCardButtonClick = Notification.Name(rawValue: "com.mediquo.notification.name.appointments.change.credit.card.button.click")
    }
    
    enum Key {
        public enum Appointments {
            public static let id = "com.mediquo.notification.key.appointment.id"
        }
    }
}
