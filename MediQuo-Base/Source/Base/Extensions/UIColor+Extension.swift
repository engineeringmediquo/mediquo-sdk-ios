// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xFF00_0000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00FF_0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000_FF00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x0000_00FF) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

extension UIColor {
    class func purpleGroupAlert() -> UIColor {
        return UIColor(red: 140 / 255.0, green: 122 / 255.0, blue: 196 / 255.0, alpha: 1)
    }

    class func turquoise() -> UIColor {
        return UIColor(red: 66 / 255.0, green: 206 / 255.0, blue: 206 / 255.0, alpha: 1)
    }

    class func redCancelGroupAlert() -> UIColor {
        return UIColor(red: 247 / 255.0, green: 0 / 255.0, blue: 78 / 255.0, alpha: 1)
    }

    class func grayTextGroupAlert() -> UIColor {
        return UIColor(red: 91 / 255.0, green: 91 / 255.0, blue: 91 / 255.0, alpha: 1.0)
    }

    class func containerBackground() -> UIColor {
        return UIColor(red: 220 / 255.0, green: 216 / 255.0, blue: 231 / 255.0, alpha: 1)
    }

    class func grayDarkButtonAlertColor() -> UIColor {
        return UIColor(red: 150 / 255.0, green: 147 / 255.0, blue: 158 / 255.0, alpha: 1.0)
    }

    class func backGrundAlertView() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    }

    // MARK: Common style

    class func primary() -> UIColor {
        return UIColor(rgb: 0x4A1EA7)
    }

    class func secondary() -> UIColor {
        return UIColor(rgb: 0x42CECE)
    }

    class func dark() -> UIColor {
        return UIColor(rgb: 0x4E4E4E)
    }

    class func lightGray() -> UIColor {
        return UIColor(rgb: 0x9E9E9E)
    }

    class func gray3() -> UIColor {
        return UIColor(rgb: 0xE0E0E0)
    }

    class func gray4() -> UIColor {
        return UIColor(rgb: 0xECEFF1)
    }

    class func tintColorUserNoPremium() -> UIColor {
        return UIColor(red: 74 / 255.0, green: 30 / 255.0, blue: 167 / 255.0, alpha: 0.4)
    }

    class func profressionalOnline() -> UIColor {
        return UIColor(red: 0 / 255.0, green: 230 / 255.0, blue: 118 / 255.0, alpha: 1.0)
    }

    class func gray2() -> UIColor {
        return UIColor(rgb: 0xBDBDBD)
    }

    class func listProfressionalOnline() -> UIColor {
        return UIColor(red: 71 / 255.0, green: 230 / 255.0, blue: 118 / 255.0, alpha: 1.0)
    }

    class func groupsNavigationBar() -> UIColor {
        return UIColor(red: 84 / 255, green: 24 / 255, blue: 172 / 255, alpha: 1)
    }

    class func groupsBackground() -> UIColor {
        return UIColor(rgb: 0xECECEC)
    }

    class func groupsStatusButtonBackground() -> UIColor {
        return UIColor(rgb: 0xDBD6E5)
    }

    // MARK: Groups group cell

    class func groupsSmallInformationCellIcons() -> UIColor {
        return UIColor(red: 0x8D / 255, green: 0x8D / 255, blue: 0x8D / 255, alpha: 1)
    }

    class func groupsFollowActivated() -> UIColor {
        return UIColor(red: 0xFF / 255, green: 0xB3 / 255, blue: 0x00 / 255, alpha: 1)
    }

    // MARK: Group message item

    class func bubbleModerator() -> UIColor {
        return UIColor(red: 74 / 255.0, green: 30 / 255.0, blue: 167 / 255.0, alpha: 1.0)
    }

    class func bubbleIncoming() -> UIColor {
        return UIColor(rgb: 0xFFFFFF)
    }

    class func bubbleOutgoing() -> UIColor {
        return UIColor(red: 231 / 255.0, green: 227 / 255.0, blue: 241 / 255.0, alpha: 1.0)
    }

    class func speciality() -> UIColor {
        return UIColor(rgb: 0x8252C8)
    }

    class func bubbleHeader() -> UIColor {
        return UIColor(rgb: 0xB4A2D9)
    }

    // swiftlint:disable cyclomatic_complexity
    class func incoming(by id: Int) -> UIColor {
        switch id {
        case 0: return UIColor(rgb: 0xEF5350)
        case 1: return UIColor(rgb: 0xEC407A)
        case 2: return UIColor(rgb: 0xAB47BC)
        case 3: return UIColor(rgb: 0x7E57C2)
        case 4: return UIColor(rgb: 0x5C6BC0)
        case 5: return UIColor(rgb: 0x42A5F5)
        case 6: return UIColor(rgb: 0x29B6F6)
        case 7: return UIColor(rgb: 0x26C6DA)
        case 8: return UIColor(rgb: 0x26A69A)
        case 9: return UIColor(rgb: 0x66BB6A)
        case 10: return UIColor(rgb: 0xFFCA28)
        case 11: return UIColor(rgb: 0xFFA726)
        case 12: return UIColor(rgb: 0xFF7043)
        case 13: return UIColor(rgb: 0x8D6E63)
        case 14: return UIColor(rgb: 0x78909C)
        default: return UIColor.gray
        }
    }

    // swiftlint:enable cyclomatic_complexity

    class func incoming() -> UIColor {
        return UIColor(rgb: 0x4D4E52)
    }

    class func outgoing() -> UIColor {
        return UIColor(rgb: 0x18181B)
    }

    class func userAdmin() -> UIColor {
        return UIColor(rgb: 0xE6E6E6)
    }

    class func valueAdmin() -> UIColor {
        return UIColor(rgb: 0xFFFFFF)
    }

    class func dateIncomingModeratorSender() -> UIColor {
        return UIColor(rgb: 0xF2F2F2)
    }

    class func dateIncomingOtherSender() -> UIColor {
        return UIColor(rgb: 0xA6A6A6)
    }

    class func dateOutgoing() -> UIColor {
        return UIColor(rgb: 0x666666)
    }

    class func deletedMessage() -> UIColor {
        return UIColor(rgb: 0x737373)
    }

    class func systemMessage() -> UIColor {
        return UIColor(rgb: 0xD9D9D9)
    }

    class func systemMessageWithAlpha() -> UIColor {
        return UIColor(red: 217 / 255, green: 217 / 255, blue: 217 / 255, alpha: 0.6)
    }

    // MARK: Groups group detail

    class func groupsBackgroundInfoIcon() -> UIColor {
        return UIColor(red: 0xE4 / 255, green: 0xE0 / 255, blue: 0xEC / 255, alpha: 1)
    }

    // MARK: Groups group detail Image

    class func gradientTop() -> UIColor {
        return UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)
    }

    class func gradientBotton() -> UIColor {
        return UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0)
    }

    // MARK: Groups Chat Kick User Alert

    class func purpleAlertKickUserGroupColor() -> UIColor {
        return UIColor(red: 63 / 255.0, green: 24 / 255.0, blue: 159 / 255.0, alpha: 1)
    }

    class func linePurpleAlertKickUserGroupColor() -> UIColor {
        return UIColor(red: 203 / 255.0, green: 193 / 255.0, blue: 227 / 255.0, alpha: 1)
    }

    // MARK: Groups Chat Member List Requests

    class func lightGrayTableMembersGroupListColor() -> UIColor {
        return UIColor(red: 236 / 255, green: 236 / 255, blue: 236 / 255, alpha: 1)
    }

    // MARK: VideoCall

    class func videoCallDisabledCameraBackgroundColor() -> UIColor {
        return UIColor(red: 239 / 255, green: 35 / 255, blue: 54 / 255, alpha: 1.0)
    }

    class func videoCallEnabledCameraBackgroundColor() -> UIColor {
        return UIColor(red: 3 / 255, green: 243 / 255, blue: 180 / 255, alpha: 1.0)
    }

    class func videoCallDisabledCancelButtonBackgroundColor() -> UIColor {
        return UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 1.0)
    }

    // MARK: Video call reports

    class func doctorNameReportList() -> UIColor {
        return UIColor(red: 84 / 255, green: 24 / 255, blue: 171 / 255, alpha: 1.0)
    }

    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }

    // MARK: Professionals

    class func barTitleColor() -> UIColor {
        return UIColor(rgb: 0x4A1EA7)
    }

    class func groupProfessionalSwipeBackground() -> UIColor {
        return UIColor(red: 56 / 255.0, green: 201 / 255.0, blue: 201 / 255.0, alpha: 1)
    }

    class func bubbleProfessionalsHeader() -> UIColor {
        return UIColor(rgb: 0x8B6EC4)
    }

    class func bubbleProfessionalsOutgoing() -> UIColor {
        return UIColor(rgb: 0xDAD0F3)
    }

    class func bubbleProfessionalsIncoming() -> UIColor {
        return UIColor(rgb: 0xE8E6EC)
    }

    class func bubbleProfessionalsModerator() -> UIColor {
        return UIColor(rgb: 0xDAD0F3)
    }

    class func deletedProfessionalsBubble() -> UIColor {
        return UIColor(rgb: 0xFF8080)
    }

    class func bubbleProfessionalsTextColor() -> UIColor {
        return UIColor(rgb: 0x404040)
    }

    // MARK: New style

    class func chatBackground() -> UIColor {
        return UIColor(rgb: 0xF2F2F2)
    }

    class func chatDateColor() -> UIColor {
        return UIColor(rgb: 0x9E9E9E)
    }

    class func chatOutgoingTextColor() -> UIColor {
        return UIColor(red: 231 / 255, green: 227 / 255, blue: 241 / 255, alpha: 1.0)
    }

    // MARK: Reports Colors

    class func gray1() -> UIColor {
        return UIColor(red: 158 / 255, green: 158 / 255, blue: 158 / 255, alpha: 1.0)
    }

    // MARK: Searcher Inbox

    class func sectionBackground() -> UIColor {
        return UIColor(rgb: 0xFAFAFA)
    }

    // MARK: Appointment banner

    class func darkRed() -> UIColor {
        return UIColor(rgb: 0xFF014D)
    }

    class func lightRed() -> UIColor {
        return UIColor(rgb: 0xFED5E1)
    }

    class func darkGreen() -> UIColor {
        return UIColor(rgb: 0x42CECE)
    }

    class func closeGray() -> UIColor {
        return UIColor(red: 84 / 255.0, green: 110 / 255.0, blue: 122 / 255.0, alpha: 1.0)
    }

    class func darkOrange() -> UIColor {
        return UIColor(rgb: 0xFF8A01)
    }

    class func darkBlue() -> UIColor {
        return UIColor(rgb: 0x3C50EC)
    }

    class func lightBlue() -> UIColor {
        return UIColor(rgb: 0xECEEFD)
    }

    class func disconnectedBackground() -> UIColor {
        return UIColor(rgb: 0x90A4AE)
    }

    // MARK: Appointment list

    class func darkGray() -> UIColor {
        return UIColor(hex: "#201552FF") ?? .darkGray
    }
}


