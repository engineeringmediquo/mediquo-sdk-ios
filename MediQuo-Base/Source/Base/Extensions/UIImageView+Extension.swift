// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

public extension UIImageView {
    func fadeIn(withDuration duration: TimeInterval, alpha: CGFloat) {
        UIView.self.animate(withDuration: duration, animations: {
            self.alpha = alpha
        })
    }

    func fadeOut() {
        UIView.self.animate(withDuration: 0.5, animations: {
            self.alpha = 0
        })
    }

    func moveIt() {
        let duration: TimeInterval = 1.0
        UIView.self.animate(withDuration: duration, animations: { () -> Void in
            self.frame = CGRect(
                x: self.frame.origin.x,
                y: self.frame.origin.y - 10,
                width: self.frame.size.width,
                height: self.frame.size.height
            )
        })
    }
}
