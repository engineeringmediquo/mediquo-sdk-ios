// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

extension UILabel {
    func settingAttributedString(text: String, font: FontConvertible.Font = FontFamily.GothamRounded.book.font(size: 16),
                                 color: UIColor = ColorName.secondary.color,
                                 lineSpacing: CGFloat = 5, alignment: NSTextAlignment = .natural) {
        let attrs: [NSAttributedString.Key: Any] = [.font: font.customOrDefault, .foregroundColor: color]

        let attributedString = NSMutableAttributedString(string: text, attributes: attrs)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = alignment
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = attributedString
    }
}
