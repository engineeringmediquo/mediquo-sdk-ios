//
//  BaseViewModel.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

protocol BaseViewModel: AnyObject {}
