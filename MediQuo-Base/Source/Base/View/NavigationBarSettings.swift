// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit

// TODO: Cane - Modificar para adaptar los estilos con el SDK
public struct NavigationBarSettings {
    public var prefersLargeTitles: Bool = false
    public let backgroundBarColor: UIColor?
    public let barTintColor: UIColor?
    public let titleTextAttributes: [NSAttributedString.Key: Any]?
    public var largeTitleTextAttributes: [NSAttributedString.Key: Any]?

    private let mediQuo = MediQuoSDK.instance

    public init() {
        self.prefersLargeTitles = self.mediQuo.style?.prefersLargeTitles ?? false
        self.backgroundBarColor = self.mediQuo.style?.primaryColor
        self.barTintColor = .black

        let tintColor: UIColor = self.mediQuo.style?.primaryContrastColor ?? ColorName.contrast.color
        let titleAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: tintColor,
            .font: FontFamily.GothamRounded.medium.font(size: 18).customOrDefault
        ]
        self.titleTextAttributes = titleAttributes

        let titleLargeAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: tintColor,
            .font: FontFamily.GothamRounded.medium.font(size: 32).customOrDefault
        ]
        self.largeTitleTextAttributes = titleLargeAttributes
    }
}

extension NavigationBarSettings {
    public init?(navigationBar: UINavigationBar?) {
        guard let navigationBar = navigationBar else { return nil }
        self.prefersLargeTitles = navigationBar.prefersLargeTitles
        self.backgroundBarColor = navigationBar.backgroundColor
        self.barTintColor = navigationBar.barTintColor
        self.titleTextAttributes = navigationBar.titleTextAttributes
        self.largeTitleTextAttributes = navigationBar.largeTitleTextAttributes
    }
}

extension UINavigationBar {
    public func set(settings: NavigationBarSettings) {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = settings.backgroundBarColor
            appearance.titleTextAttributes = settings.titleTextAttributes ?? [:]
            appearance.largeTitleTextAttributes = settings.largeTitleTextAttributes ?? [:]
            self.tintColor = settings.barTintColor
            self.standardAppearance = appearance
            self.compactAppearance = appearance
            self.scrollEdgeAppearance = appearance
        } else {
            self.isOpaque = true
            self.isTranslucent = false
            self.backgroundColor = settings.backgroundBarColor
            self.barTintColor = settings.barTintColor
            self.titleTextAttributes = settings.titleTextAttributes
            self.largeTitleTextAttributes = settings.largeTitleTextAttributes
        }
        self.prefersLargeTitles = settings.prefersLargeTitles
    }
}
