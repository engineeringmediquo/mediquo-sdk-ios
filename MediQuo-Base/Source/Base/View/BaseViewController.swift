// Copyright © 2020 Medipremium S.L. All rights reserved.

import UIKit
import UserNotifications

@objc protocol BaseViewControllerProtocol: AnyObject {
    func didBecomeActiveAction()
    func bindNotifications()
    func unBindNotifications()
    func bindViewModels()
    func unBindViewModels()
    func showGenericError(_ notification: NSNotification)
}

enum LoadingStatus {
    case start, stop
}

enum BaseViewStates {
    case loading(status: LoadingStatus), error(error: Error)
}

open class BaseViewControllerSDK: UIViewController, BaseViewControllerProtocol {
    
    // Private Properties
    private var environmentManager: EnvironmentManager = EnvironmentManager()
    private let navigationBarSettings: NavigationBarSettings = NavigationBarSettings()

    var mainCoordinator: MainSDKCoordinator?
    let loadingHelper: LoadingHelper? = LoadingHelper()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.tintBackground()
        self.registerForPushNotifications()
        self.mainCoordinator = MainSDKCoordinator(navigationController: self.navigationController ?? UINavigationController())
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bindNotifications()
        self.bindViewModels()
        self.setupNavigationBar()
        self.setBackButtonNavigationController()
        print("APPEAR", self.classForCoder)
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.unBindNotifications()
        self.unBindViewModels()
    }

    open override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        self.setupNavigationBar()
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    deinit {
        CoreLog.business.debug("<<< Deinit called successfully >>>")
    }

    func didBecomeActiveAction() {
        CoreLog.ui.debug("Did become active...")
    }

    func bindNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActiveAction), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showGenericError), name: Notification.Name.Error.Remote.generic, object: nil)
    }

    func unBindNotifications() {
        NotificationCenter.default.removeObserver(self) //swiftlint:disable:this notification_center_detachment
    }

    func bindViewModels() {
        CoreLog.ui.debug("Binding view models")
    }

    func unBindViewModels() {
        CoreLog.ui.debug("Unbinding view models")
    }

    func showGenericError(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            CoreLog.ui.error("Generic error received...")
            return
        }
        let error = userInfo[ErrorType.generic].debugDescription
        CoreLog.ui.error("%@", error)
    }
    
    func enablePrefsLargeTitles() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

    func hideNavigationBar(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    func showNavigationBar(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }

    var bottomHeightSafeArea: CGFloat {
        return UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
    }
}

extension BaseViewControllerSDK {
    func showLoader() {
        loadingHelper?.showLoading(onView: view)
    }
    
    func showLoader(onView view: UIView) {
        loadingHelper?.showLoading(onView: view)
    }

    func hideLoader() {
        loadingHelper?.hideLoading()
    }
    
    func showErrorAlert(onView: UIViewController,
                        title: String,
                        message: String,
                        actionText: String = Localizable.string("literal.accept"),
                        style: UIAlertAction.Style,
                        dismissAction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: actionText, style: .default, handler: { _ in
            dismissAction?()
        }))
        
        onView.present(alertController, animated: true, completion: nil)
    }
}

extension BaseViewControllerSDK {
    private func setupNavigationBar() {
        let settings: NavigationBarSettings = self.navigationBarSettings
        self.navigationController?.navigationBar.set(settings: settings)
    }

    private func tintBackground() {
        self.view.backgroundColor = .white
    }

    private func registerForPushNotifications() {
        guard self.environmentManager.get() != .testing else { return }
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge, .alert, .sound]) { (_: Bool, error: Error?) -> Void in
            if let error: Error = error {
                CoreLog.firebase.info("Authorization request error %@", error.localizedDescription)
                return
            }

            center.getNotificationSettings(completionHandler: { (settings: UNNotificationSettings) in
                guard settings.authorizationStatus == .authorized else {
                    CoreLog.firebase.info("Unauthorized remote notifications")
                    return
                }

                DispatchQueue.main.async {
                    CoreLog.firebase.info("Authorized remote notifications")
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
    
    func checkNotificationPermission(onAllow: @escaping () -> Void, onNotAllow: @escaping () -> Void) {
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { _ in
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (_: Bool, error: Error?) in
                if let error: Error = error {
                    CoreLog.firebase.info("Authorization request error %@", error.localizedDescription)
                    return
                }

                center.getNotificationSettings(completionHandler: { (settings: UNNotificationSettings) in
                    guard settings.authorizationStatus == .authorized else {
                        CoreLog.firebase.info("Unauthorized remote notifications")
                        onNotAllow()
                        return
                    }

                    DispatchQueue.main.async {
                        CoreLog.firebase.info("Authorized remote notifications")
                        UIApplication.shared.registerForRemoteNotifications()
                    }

                    onAllow()
                })
            }
        }
    }
}






