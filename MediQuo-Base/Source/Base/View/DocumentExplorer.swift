// Copyright © 2020 Medipremium S.L. All rights reserved.

import MobileCoreServices
import UIKit

public struct SupportedSendFiles {
    public static let pdf: String = kUTTypePDF as String

    // Zip Files
    public static let archives: String = kUTTypeArchive as String

    // txt, html, rtf, xml, etc
    public static let text: String = kUTTypeText as String

    //    base type for anything containing user-viewable document content
    //    *    (documents, pasteboard data, and document packages.) Types describing
    //    *    files or packages must also conform to kUTTypeData or kUTTypePackage
    //    *    in order for the system to bind documents to them.
    public static let content: String = kUTTypeContent as String

    public static let image: String = kUTTypeImage as String

    public static let formatsSupported: [String] = [SupportedSendFiles.pdf, SupportedSendFiles.archives, SupportedSendFiles.text]
}

public class DocumentExplorer: NSObject {
    public private(set) var pickDocClousure: ((UIDocumentPickerViewController, [URL]) -> Void)?
    public private(set) var cancelClousure: ((UIDocumentPickerViewController) -> Void)?
    public weak var delegate: UIDocumentPickerDelegate?
    private let documentPicker: UIDocumentPickerViewController

    public init(_ documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: SupportedSendFiles.formatsSupported, in: UIDocumentPickerMode.import)) {
        self.documentPicker = documentPicker
    }

    @discardableResult public func add(_ pick: @escaping ((UIDocumentPickerViewController, [URL]) -> Void)) -> Self {
        self.pickDocClousure = pick
        return self
    }

    @discardableResult public func add(_ cancel: @escaping ((UIDocumentPickerViewController) -> Void)) -> Self {
        self.cancelClousure = cancel
        return self
    }

    public func present(on viewController: UIViewController, animated: Bool = true, completion: (() -> Swift.Void)? = nil) {
        self.documentPicker.delegate = self
        viewController.modalPresentationStyle = .fullScreen
        viewController.present(self.documentPicker, animated: animated, completion: completion)
    }
}

extension DocumentExplorer: UIDocumentPickerDelegate {
    @available(iOS 11.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let clousure = pickDocClousure else {
            return
        }
        clousure(controller, urls)

        self.delegate?.documentPicker?(controller, didPickDocumentsAt: urls)
    }

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        guard let clousure = pickDocClousure else {
            return
        }
        clousure(controller, [url])
        self.delegate?.documentPicker?(controller, didPickDocumentsAt: [url])
    }

    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        guard let clousure = cancelClousure else {
            return
        }
        clousure(controller)
        self.delegate?.documentPickerWasCancelled?(controller)
    }
}
