//
//  Components.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

class Components {
    static func resolve() {
        self.resolveAuthModule()
        self.resolveInstallationModule()
        self.resolveRegisterFirebaseModule()
        self.resolveProfileModule()
        self.resolveProfessionalsListModule()
        self.resolveRegisterRoomModule()
        self.resolveProfessionalProfileModule()
        self.resolveMedicalHistoryModule()
        self.resolveReportsModule()
        self.resolveRecipesModule()
    }
}

// MARK: - Auth

extension Components {
    internal static func resolveAuthModule() {
        Resolver.shared.add(type: AuthManager.self, { return AuthManager() })
        Resolver.shared.add(type: AuthUseCase.self, { return AuthUseCase() })
        Resolver.shared.add(type: AuthRepository.self, { return AuthRepository() })
    }
}

// MARK: - Setup

extension Components {
    internal static func resolveInstallationModule() {
        Resolver.shared.add(type: InstallationManager.self, { return InstallationManager() })
        Resolver.shared.add(type: InstallationUseCase.self, { return InstallationUseCase() })
        Resolver.shared.add(type: InstallationRepository.self, { return InstallationRepository() })
    }
}

// MARK: - Firebase

extension Components {
    internal static func resolveRegisterFirebaseModule() {
        Resolver.shared.add(type: RegisterFirebaseManager.self, { return RegisterFirebaseManager() })
        Resolver.shared.add(type: RegisterFirebaseUseCase.self, { return RegisterFirebaseUseCase() })
        Resolver.shared.add(type: RegisterFirebaseRepository.self, {
            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
            let manager: RemoteManagerProtocol = RemoteManager(configuration)
            return RegisterFirebaseRepository(remote: manager)
        })
    }
}

// MARK: - Profile

extension Components {
    internal static func resolveProfileModule() {
        Resolver.shared.add(type: ProfileManager.self, { return ProfileManager() })
        Resolver.shared.add(type: ProfileUseCase.self, { return ProfileUseCase() })
        Resolver.shared.add(type: ProfileRepository.self, { return ProfileRepository() })
    }
}

// MARK: - Professionals list

extension Components {
    internal static func resolveProfessionalsListModule() {
        Resolver.shared.add(type: ProfessionalsListManager.self, { return ProfessionalsListManager() })
        Resolver.shared.add(type: ProfessionalsListViewModel.self, { return ProfessionalsListViewModel() })
        Resolver.shared.add(type: ProfessionalsListUseCase.self, { return ProfessionalsListUseCase() })
        Resolver.shared.add(type: ProfessionalsListRepository.self, {
            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
            let manager: RemoteManagerProtocol = RemoteManager(configuration)
            return ProfessionalsListRepository(remote: manager)
        })
    }
}

// MARK: - Room

extension Components {
    internal static func resolveRegisterRoomModule() {
//        Resolver.shared.add(type: ChatRoomViewModel.self, { return ChatRoomViewModel() })
//        Resolver.shared.add(type: RoomUseCase.self, { return RoomUseCase() })
//        Resolver.shared.add(type: RoomRepository.self, {
//            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
//            let manager: RemoteManagerProtocol = RemoteManager(configuration)
//            return RoomRepository(remote: manager)
//        })
    }
}

// MARK: - Professional profile

extension Components {
    internal static func resolveProfessionalProfileModule() {
//        Resolver.shared.add(type: ProfessionalProfileViewModel.self, { return ProfessionalProfileViewModel() })
//        Resolver.shared.add(type: ProfessionalProfileUseCase.self, { return ProfessionalProfileUseCase() })
//        Resolver.shared.add(type: ProfessionalProfileRepository.self, {
//            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
//            let manager: RemoteManagerProtocol = RemoteManager(configuration)
//            return ProfessionalProfileRepository(remote: manager)
//        })
    }
}

// MARK: - Medical history

extension Components {
    internal static func resolveMedicalHistoryModule() {
        Resolver.shared.add(type: MedicalHistoryViewModel.self, { return MedicalHistoryViewModel() })
        Resolver.shared.add(type: MedicalHistoryUseCase.self, { return MedicalHistoryUseCase() })
        Resolver.shared.add(type: MedicalHistoryRepository.self, {
            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
            let manager: RemoteManagerProtocol = RemoteManager(configuration)
            return MedicalHistoryRepository(remote: manager)
        })
    }
}

// MARK: - Reports

extension Components {
    internal static func resolveReportsModule() {
        Resolver.shared.add(type: ReportsViewModel.self, { return ReportsViewModel() })
        Resolver.shared.add(type: ReportsUseCase.self, { return ReportsUseCase() })
        Resolver.shared.add(type: ReportsRepository.self, {
            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
            let manager: RemoteManagerProtocol = RemoteManager(configuration)
            return ReportsRepository(remote: manager)
        })
    }
}

// MARK: - Recipes

extension Components {
    internal static func resolveRecipesModule() {
        Resolver.shared.add(type: RecipesViewModel.self, { return RecipesViewModel() })
        Resolver.shared.add(type: RecipesUseCase.self, { return RecipesUseCase() })
        Resolver.shared.add(type: RecipesRepository.self, {
            let configuration: RemoteConfiguration = RemoteConfiguration(baseUrl: RemoteBasesUrl.sdkApi)
            let manager: RemoteManagerProtocol = RemoteManager(configuration)
            return RecipesRepository(remote: manager)
        })
    }
}
