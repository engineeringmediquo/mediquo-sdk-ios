//
//  Inject.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

@propertyWrapper
struct Inject<Component> {
    var component: Component

    init() {
        self.component = Resolver.shared.resolve(Component.self)
    }

    public var wrappedValue: Component {
        get { return component }
        mutating set { component = newValue }
    }
}
