//
//  Resolver.swift
//  MediQuo-Base
//
//  Created by David Martin on 19/12/20.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import Foundation

protocol InjectableComponent: AnyObject {}

class Resolver {
    static let shared = Resolver()

    var factoryDict: [String: () -> InjectableComponent] = [:]

    func add(type: InjectableComponent.Type, _ factory: @escaping () -> InjectableComponent) {
        self.factoryDict[String(describing: type.self)] = factory
    }

    func resolve<InjectableComponent>(_ type: InjectableComponent.Type) -> InjectableComponent {
        guard let component: InjectableComponent = self.factoryDict[String(describing: InjectableComponent.self)]?() as? InjectableComponent else {
            Components.resolve()
            return self.resolve(type)
        }
        return component
    }
}
