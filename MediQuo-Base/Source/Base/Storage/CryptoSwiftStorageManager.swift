// Copyright © 2020 Medipremium S.L. All rights reserved.

import CryptoSwift

// Only for < iOS 13.0
public class CryptoSwiftStorageManager {
    private let defaultCypherKey = UIDevice.current.identifierForVendor!.uuidString.prefix(16).description
    public init() {}

    public func save<T: Codable>(object: T, by key: String, isSecured: Bool) {
        if isSecured {
            if let base64cipher = try? Rabbit(key: self.defaultCypherKey) {
                let encryptObject = try? PropertyListEncoder().encode(object).encrypt(cipher: base64cipher)
                UserDefaults.standard.set(encryptObject, forKey: key)
            }
        } else {
            let objectEncoded = try? PropertyListEncoder().encode(object)
            UserDefaults.standard.set(objectEncoded, forKey: key)
        }
    }

    public func get<T: Codable>(by key: String, isSecured: Bool) -> T? {
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            if let base64cipher = try? Rabbit(key: self.defaultCypherKey), isSecured {
                return try? PropertyListDecoder().decode(T.self, from: data.decrypt(cipher: base64cipher))
            } else {
                return try? PropertyListDecoder().decode(T.self, from: data)
            }
        }
        return nil
    }
}
