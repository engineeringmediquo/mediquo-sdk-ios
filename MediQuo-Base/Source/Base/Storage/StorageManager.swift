// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

@propertyWrapper
struct StorageDatasource<T: Codable> {
    private let key: String
    private let isSecured: Bool

    init(isSecured: Bool = true) {
        self.key = String(describing: T.self)
        self.isSecured = isSecured
    }

    var wrappedValue: T? {
        get {
            if #available(iOS 13.0, *) {
                return CryptoKitStorageManager().get(by: self.key, isSecured: self.isSecured)
            } else {
                return CryptoSwiftStorageManager().get(by: self.key, isSecured: self.isSecured)
            }
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.removeObject(forKey: self.key)
                return
            }
            if #available(iOS 13.0, *) {
                CryptoKitStorageManager().save(object: newValue, by: self.key, isSecured: self.isSecured)
            } else {
                CryptoSwiftStorageManager().save(object: newValue, by: self.key, isSecured: self.isSecured)
            }
        }
    }
}
