//
//  Notifications+Tracking.swift
//  MediQuo-Base
//
//  Created by David Martin on 23/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

// MARK: - Tracking events

extension Notification.Name {
    public struct Event {
        public struct Chat {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.chat.view")
            public static let messageReceived = Notification.Name(rawValue: "com.mediquo.notification.name.event.chat.message.received")
            public static let messageSend = Notification.Name(rawValue: "com.mediquo.notification.name.event.chat.message.send")
        }
        public struct Call {
            public static let started = Notification.Name(rawValue: "com.mediquo.notification.name.event.call.started")
            public static let ended = Notification.Name(rawValue: "com.mediquo.notification.name.event.chat.call.ended")
        }
        public struct VideoCall {
            public static let started = Notification.Name(rawValue: "com.mediquo.notification.name.event.videocall.started")
            public static let ended = Notification.Name(rawValue: "com.mediquo.notification.name.event.chat.videocall.ended")
        }
        public struct ProfessionalProfile {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.professionalProfile.view")
        }
        public struct MedicalHistory {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.medicalHistory.view")
        }
        public struct Allergies {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.allergies.view")
        }
        public struct Illnesses {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.illnesses.view")
        }
        public struct Medications {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.medications.view")
        }
        public struct Reports {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.reports.view")
        }
        public struct Recipes {
            public static let view = Notification.Name(rawValue: "com.mediquo.notification.name.event.recipes.view")
        }
    }
}

// MARK: - Tracking keys

extension Notification.Name {
    public struct Key {
        public static let professionalHash = "professionalHash"
        public static let specialityId = "specialityId"
    }
}
