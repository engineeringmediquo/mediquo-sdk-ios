// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

extension Notification.Name {
    struct Push {
        static let messageReceived = Notification.Name(rawValue: "com.mediquo.push.messageReceived")
    }
    struct Socket {
        static let ready = Notification.Name(rawValue: "com.mediquo.socket.ready")
    }
}
