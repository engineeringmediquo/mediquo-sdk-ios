// Copyright © 2020 Medipremium S.L. All rights reserved.

import Foundation

extension Notification.Name {
    public struct Error {
        public struct Remote {
            public static let generic = Notification.Name(rawValue: "com.mediquo.notification.name.error.remote.generic")
            public static let maintenance = Notification.Name(rawValue: "com.mediquo.notification.name.error.remote.maintenance")
        }
    }
}
