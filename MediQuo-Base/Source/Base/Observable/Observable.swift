//
//  Observable.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

class Observable<T> {
    typealias Observer = (T?) -> Void

    private var observer: Observer?

    var value: T? {
        didSet {
            self.observer?(self.value)
        }
    }

    init() {
        self.value = nil
    }

    init(_ value: T) {
        self.value = value
    }

    func subscribe(_ observer: Observer?) {
        self.observer = observer
    }

    func unsubscribe() {
        self.observer = nil
    }
}

// TODO: Cane - Migrar a usar este Observable en todos los pods

public class MediQuoObservable<T> {
    public typealias Listener = (T) -> Void
    public var listener: Listener?

    public var value: T {
        didSet {
            listener?(value)
        }
    }

    public init(_ value: T) {
        self.value = value
    }

    public func subscribe(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }

    public func unsubscribe() {
        listener = nil
    }
}
