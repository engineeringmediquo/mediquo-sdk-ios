//
//  MediQuoSocketManager.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import SocketIO
import mediquo_videocall_lib

public protocol MediquoChatManagerInput {
    // Properties
    var isConnected: Bool { get }
    
    // Methods
    func fetch(by roomId: Int)
    func fetch(by roomId: Int,
               pivotId: String,
               mode: MessageModel.PivotMode)
    func send(message: MessageModel, messagesForCache: [MessageModel])
    func update(message: MessageModel,
                status: MessageModel.MessageStatusSDK)
    func leave(by roomId: Int?)
    func closeAll()
    func handleError(error: BaseError)
    
    func typing(by roomId: Int)
    func stopTyping(by roomId: Int)
    
    func join(by request: SocketData..., and channel: String)
    func onEnterRoom()
    func onFetchMessages()
    func onReceive()
    func onUpdate()
    func onTyping()
    func onStopTyping()
    func onAppointment()
    func onRoomAccepted()
    func onRoomRejected()
    func onVideoCallEvents()
}

public protocol MediquoChatManagerOutput {
    // Status
    var socketStatusChanged: MediQuoObservable<SocketIOStatus?> { get set }
    // Room
    var roomId: MediQuoObservable<Int?> { get set }
    // Messages
    var messages: MediQuoObservable<[MessageModel]?> { get set }
    var message: MediQuoObservable<MessageModel?> { get set }
    var messageStatus: MediQuoObservable<MessageStatusModel?> { get set }
    // States
    var isTyping: MediQuoObservable<Bool?> { get set }
    var isRoomAccepted: MediQuoObservable<Bool?> { get set }
    var isRoomRejected: MediQuoObservable<Bool?> { get set }
    // VideoCall
    var videoCallRequested: MediQuoObservable<VideoCallNotificationSchema?> { get set }
    var videoCallRejected: MediQuoObservable<VideoCallNotificationSchema?> { get set }
    var videoCallPickedUp: MediQuoObservable<VideoCallNotificationSchema?> { get set }
    // Appointment
    var appointment: MediQuoObservable<AppointmentSDKModel?> { get set }
    var error: MediQuoObservable<BaseError?> { get set }
}

public protocol MediquoChatManager: MediquoChatManagerInput,
                                    MediquoChatManagerOutput,
                                    MediquoChatCacheManager {}

public protocol VideoCallSocketDelegate {
    func socketReady()
    func onSocketCallRequested(schema: VideoCallNotificationSchema)
    func onSocketCallRejected(schema: VideoCallNotificationSchema)
    func onSocketCallPickedUp(schema: VideoCallNotificationSchema)
    func onRoomCallRequested(schema: VideoCallNotificationSchema)
}


public struct MediQuoSDKChatManagerBuilder {
    let socketURL: URL
    let jwt: String
    let myUserHash: String
    
    public init(socketURL: URL,
                jwt: String,
                myUserHash: String) {
        self.socketURL = socketURL
        self.jwt = jwt
        self.myUserHash = myUserHash
    }
    
    static func getSDKBuilder() -> MediQuoSDKChatManagerBuilder? {
        guard let url = URL(string: RemoteBasesUrl.socketApi),
              let jwt = AuthManager().getJwt(),
              let myUserHash = ProfileManager().getProfile()?.userHash else { return nil }
        return MediQuoSDKChatManagerBuilder(socketURL: url,
                                            jwt: jwt,
                                            myUserHash: myUserHash)
    }
}

// TODO: Cane - Documentar los protocolos 📄
public class MediQuoSDKChatManager: MediquoChatManager {
    
    // Private Properties
    @Inject
    private var authManager: AuthManager

    @Inject
    private var profileManager: ProfileManager
    private var manager: SocketManager!
    private var socket: SocketIOClient?
    
    private var isStarted: Bool = false
    private var isOnEnterRoom: Bool = false
    private var isOnFetchMessages: Bool = false
    private var isOnReceive: Bool = false
    private var isOnReceiveStatus: Bool = false
    private var isOnReady: Bool = false
    private var isOnTyping: Bool = false
    private var isOnStopTyping: Bool = false
    private var isOnCallRequested: Bool = false
    private var isOnCallRejected: Bool = false
    private var isOnCallPickedUp: Bool = false

    private var myUserHash: String = ""
    private var jwt: String?
    private var channel: String = ""
    private var messageIdsDeleted: [String] = []
    private var chatCacheManager: MediquoChatCacheManager?
    private var fetchRoomMessagesUseCase: FetchRoomMessagesUseCase?
    
    // Bindings
    public var socketStatusChanged: MediQuoObservable<SocketIOStatus?> = MediQuoObservable(nil)
    public var roomId: MediQuoObservable<Int?> = MediQuoObservable(nil)
    public var messages: MediQuoObservable<[MessageModel]?> = MediQuoObservable(nil)
    public var message: MediQuoObservable<MessageModel?> = MediQuoObservable(nil)
    public var messageStatus: MediQuoObservable<MessageStatusModel?> = MediQuoObservable(nil)
    public var isTyping: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    public var isRoomAccepted: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    public var isRoomRejected: MediQuoObservable<Bool?> = MediQuoObservable(nil)
    public var videoCallRequested: MediQuoObservable<VideoCallNotificationSchema?> = MediQuoObservable(nil)
    public var videoCallRejected: MediQuoObservable<VideoCallNotificationSchema?> = MediQuoObservable(nil)
    public var videoCallPickedUp: MediQuoObservable<VideoCallNotificationSchema?> = MediQuoObservable(nil)
    public var appointment: MediQuoObservable<AppointmentSDKModel?> = MediQuoObservable(nil)
    public var error: MediQuoObservable<BaseError?> = MediQuoObservable(nil)
    
    // Exposed Properties
    public var isConnected: Bool {
        return self.socket?.status == SocketIOStatus.connected
    }

    public init(builder: MediQuoSDKChatManagerBuilder,
                chatCacheManager: MediquoChatCacheManager? = DefaultMediquoChatCacheManager(),
                fetchRoomMessagesUseCase: FetchRoomMessagesUseCase? = RoomUseCase()) {
        self.chatCacheManager = chatCacheManager
        self.fetchRoomMessagesUseCase = fetchRoomMessagesUseCase
        setupSDKChatManager(builder: builder)
    }
    
    private func setupSDKChatManager(builder: MediQuoSDKChatManagerBuilder) {
        myUserHash = builder.myUserHash
        jwt = builder.jwt
        
        let queue = DispatchQueue(label: "mediquo.socket-thread", qos: .utility, attributes: [], target: nil)
        self.manager = SocketManager(socketURL: builder.socketURL,
                                     config: [.log(true),
                                              .secure(true),
                                              .reconnects(true),
                                              .reconnectAttempts(-1),
                                              .handleQueue(queue), .path("/socket.io/"),
                                              .compress])

        let credentials = SocketCredentials(jwt: builder.jwt)
        guard let data: Data = try? JSONEncoder().encode(credentials),
              let parameters: [String: Any] = try? JSONSerialization.jsonObject(with: data) as? [String: Any] else {
            CoreLog.socketLog.error("Error serialitzing parameters")
            return
        }
        
        self.manager.config.insert(.connectParams(parameters), replacing: true)
        self.socket = self.manager.defaultSocket
        bindEvents()
        connect()
        CoreLog.socketLog.debug("Updating socket credentials", self.myUserHash)
    }

    private func connect() {
        guard let socket = self.socket,
              socket.status != SocketIOStatus.connected else { return }
        socket.connect()
    }

    private func disconnect() {
        guard let socket = self.socket else { return }
        socket.disconnect()
        socket.removeAllHandlers()
    }

    private func bindEvents() {
        self.onConnectionEvent()
        self.onDisconnectionEvent()
        self.onChangeStatusEvent()
        self.onReadyEvent()
    }

    public func closeAll() {
       self.isStarted = false
       self.disconnect()
    }
    
    public func handleError(error: BaseError) {
        self.error.value = error
    }
}

// MARK: - Chat Emits
extension MediQuoSDKChatManager {
    public func join(by request: SocketData...,
                     and channel: String) {
        self.channel = channel
        guard let socket = self.socket,
              socket.status == SocketIOStatus.connected else {
            self.connect()
            self.handleError(error: BaseError.socketError(.socketIsDisconnect))
            return
        }
        self.isStarted = true
        socket.emit("enterRoom", with: request)
    }

    public func leave(by roomId: Int?) {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected, let roomId = roomId else {
            CoreLog.socketLog.debug("Can't leave, socket is already disconnect")
            return
        }
        socket.emit("leaveRoom", ["exitRoom": roomId])
    }

    public func fetch(by roomId: Int) {
        fetchMessages(by: roomId,
                      pivotId: nil,
                      mode: nil,
                      limit: ChatConstants.numberOfMessagesForFirstFetch)
    }

    public func fetch(by roomId: Int,
                      pivotId: String,
                      mode: MessageModel.PivotMode) {
        fetchMessages(by: roomId,
                      pivotId: pivotId,
                      mode: mode,
                      limit: ChatConstants.numberOfMessagesForPagination)
    }
    
    private func fetchMessages(by roomId: Int,
                               pivotId: String?,
                               mode: MessageModel.PivotMode?,
                               limit: Int) {
        guard let jwt = jwt else {
            self.handleError(error: .socketError(.socketIsDisconnect))
            return
        }
        
        self.fetchRoomMessagesUseCase?.fetchMessages(by: roomId,
                                                     pivotId: pivotId,
                                                     mode: mode,
                                                     limit: limit,
                                                     jwt: jwt,
                                                     completion: { result in
            switch result {
                case .success(let messages):
                    var newMessages = self.mapMessageList(from: messages)
                    let messagesPendingToSentFromCache = self.getMessagesPendingToSentFromCache(toUserHash: self.channel,
                                                                                                myUserHash: self.myUserHash)
                    newMessages.append(contentsOf: messagesPendingToSentFromCache)
                    self.messages.value = newMessages
                case .failure(let error):
                    self.handleError(error: .socketError(.wrongDataResponse(error.description)))
            }
        })
    }
    
    public func send(message: MessageModel, messagesForCache: [MessageModel]) {
        guard let socket = self.socket else {
            CoreLog.socketLog.debug("Can't send message, socket is already disconnect")
            self.setMessagesInCache(toUserHash: self.channel,
                                    myUserHash: self.myUserHash,
                                    messages: messagesForCache)
            return
        }

        if let name = message.metadata.name,
               message.metadata.isImage || message.metadata.isFile {
            socket.emit("message", message.description, message.roomId, message.messageId, message.type.rawValue, name)
        } else {
            socket.emit("message", message.description, message.roomId, message.messageId, message.type.rawValue)
        }
    }

    public func update(message: MessageModel, status: MessageModel.MessageStatusSDK) {
        guard let socket = self.socket else {
            CoreLog.socketLog.debug("Can't update message, socket is already disconnect")
            return
        }
        socket.emit("messageStatus", message.messageId, message.roomId, status.rawValue)
    }

    public func typing(by roomId: Int) {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected else {
            CoreLog.socketLog.debug("Can't typing, socket is already disconnect")
            return
        }
        socket.emit("typing", roomId)
    }

    public func stopTyping(by roomId: Int) {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected else {
            CoreLog.socketLog.debug("Can't stop typing, socket is already disconnect")
            return
        }
        socket.emit("stopTyping", roomId)
    }
}

// MARK: - Chat Public Handlers
extension MediQuoSDKChatManager {
    public func onEnterRoom() {
        guard let socket = self.socket,
              !self.isOnEnterRoom else { return }
        self.isOnEnterRoom = true
        socket.on("enterRoom") { data, _ in
             CoreLog.socketLog.debug("On event response with: %@", data)
            guard let data = data.first,
                  let response: Int = data as? Int else { return }
            self.roomId.value = response
        }
    }

    public func onFetchMessages() {}

    public func onReceive() {
        guard let socket = self.socket,
              socket.status == SocketIOStatus.connected,
              !self.isOnReceive else {
            CoreLog.socketLog.debug("Can't receive, socket is already disconnect")
            return
        }
        self.isOnReceive = true
        messageIdsDeleted = []
        onReceiveOnMessage()
        onReceiveOnDeletedMessage()
    }
    
    private func onReceiveOnMessage() {
        guard let socket = self.socket,
                  socket.status == SocketIOStatus.connected else {
            debugPrint("Can't receive, socket is already disconnect")
            return
        }
        socket.on("message") { data, _ in
            guard let data = data.first else { return }
            do {
                 let container: Data = try JSONSerialization.data(withJSONObject: data)
                 var model: MessageModel = try JSONDecoder().decode(MessageModel.self, from: container)
                 model.isCurrentSender = self.isCurrentSender(from: model)
                 model.isDeleted = self.isDeletedMessage(from: model)
                 self.message.value = model
            } catch {
                debugPrint("Error decoding type 'message': %@", error.localizedDescription)
            }
        }
    }
    
    private func onReceiveOnDeletedMessage() {
        guard let socket = self.socket,
                  socket.status == SocketIOStatus.connected else {
            debugPrint("Can't receive, socket is already disconnect")
            self.handleError(error: BaseError.socketError(.socketIsDisconnect))
            return
        }
        socket.on("message_deleted") { data, _ in

        }
    }

    public func onUpdate() {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected, !self.isOnReceiveStatus else {
            CoreLog.socketLog.debug("Can't receive update, socket is already disconnect")
            return
        }
        self.isOnReceiveStatus = true
        socket.on("messageStatus") { data, _ in
            do {
                guard let data = data.first else { return }
                let container: Data = try JSONSerialization.data(withJSONObject: data)
                let model: MessageStatusModel = try JSONDecoder().decode(MessageStatusModel.self, from: container)
                self.messageStatus.value = model
                CoreLog.socketLog.debug("Received update message status from socket: %@", model.messageId)
            } catch {
                CoreLog.socketLog.error("Error decoding type 'message status': %@", error.localizedDescription)
            }
        }
    }

    public func onTyping() {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected, !self.isOnTyping else { return }
        self.isOnTyping = true
        socket.on("typing") { _, _ in
            self.isTyping.value = true
            CoreLog.socketLog.debug("Received typing event")
        }
    }

    public func onStopTyping() {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected, !self.isOnStopTyping else { return }
        self.isOnStopTyping = true
        socket.on("stopTyping") { _, _ in
            self.isTyping.value = false
            CoreLog.socketLog.debug("Received stop typing event")
        }
    }

    public func onRoomAccepted() {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected else { return }
        socket.on("room_accepted") { _, _ in
            self.isRoomAccepted.value = true
            CoreLog.socketLog.debug("Received is room accepted event")
        }
    }

    public func onRoomRejected() {
        guard let socket = self.socket, socket.status == SocketIOStatus.connected else { return }
        socket.on("room_rejected") { _, _ in
            self.isRoomRejected.value = true
            CoreLog.socketLog.debug("Received is room rejected event")
        }
    }
    
    public func onAppointment() {
        guard let socket = self.socket,
              socket.status == SocketIOStatus.connected else { return }
        
        let normalCallback: NormalCallback = { data, _ in
            guard let data = data.first else { return }
            do {
                let container: Data = try JSONSerialization.data(withJSONObject: data)
                let model: AppointmentSDKModel = try JSONDecoder().decode(AppointmentSDKModel.self, from: container)
                self.appointment.value = model
                CoreLog.socketLog.debug("Appointment from socket: %@", model.id)
            } catch {
                CoreLog.socketLog.error("Error decoding type 'appointment': %@", error.localizedDescription)
            }
        }
        socket.on("appointment_created", callback: normalCallback)
        socket.on("appointment_accepted", callback: normalCallback)
        socket.on("appointment_cancelled", callback: normalCallback)
        socket.on("appointment_declined", callback: normalCallback)
        socket.on("appointment_finished", callback: normalCallback)
        socket.on("appointment_expired", callback: normalCallback)
        socket.on("appointment_uncharged", callback: normalCallback)
        socket.on("appointment_owed", callback: normalCallback)
        socket.on("appointment_rescheduled", callback: normalCallback)
        socket.on("appointment_unpaid", callback: normalCallback)
    }
}

// MARK: - VideoCall Public Handlers
extension MediQuoSDKChatManager {
    public func onVideoCallEvents() {
        guard let _ = self.socket else { return }
        self.onCallRequested()
        self.onCallRejected()
        self.onCallPickedUp()
    }

    private func onCallRequested() {
        guard let socket = self.socket,
             !self.isOnCallRequested else { return }
        self.isOnCallRequested = true
        
        socket.on("call_requested") { data, _ in
            do {
                guard let data = data.first else { return }
                let container: Data = try JSONSerialization.data(withJSONObject: data)
                let model: VideoCallNotificationSchema = try JSONDecoder().decode(VideoCallNotificationSchema.self, from: container)
                self.videoCallRequested.value = model
                CoreLog.socketLog.debug("On call requested event response")
            } catch {
                CoreLog.socketLog.error("Error decoding type 'call_requested': %@", error.localizedDescription)
            }
        }
    }

    private func onCallRejected() {
        guard let socket = self.socket,
              !self.isOnCallRejected else { return }
        self.isOnCallRejected = true
        socket.on("call_rejected") { data, _ in
            do {
                guard let data = data.first else { return }
                let container: Data = try JSONSerialization.data(withJSONObject: data)
                let model: VideoCallNotificationSchema = try JSONDecoder().decode(VideoCallNotificationSchema.self, from: container)
                self.videoCallRejected.value = model
                CoreLog.socketLog.debug("On call rejected event response")
            } catch {
                CoreLog.socketLog.error("Error decoding type 'call_rejected': %@", error.localizedDescription)
            }
        }
    }

    private func onCallPickedUp() {
        guard let socket = self.socket,
              !self.isOnCallPickedUp else { return }
        self.isOnCallPickedUp = true
        socket.on("call_picked_up") { data, _ in
            do {
                guard let data = data.first else { return }
                let container: Data = try JSONSerialization.data(withJSONObject: data)
                let model: VideoCallNotificationSchema = try JSONDecoder().decode(VideoCallNotificationSchema.self, from: container)
                self.videoCallPickedUp.value = model
                CoreLog.socketLog.debug("On call picked up event response")
            } catch {
                CoreLog.socketLog.error("Error decoding type 'call_picked_up': %@", error.localizedDescription)
            }
        }
    }
}

// MARK: - Mappers & Helpers
extension MediQuoSDKChatManager {
    private func refreshMessages() {
        guard let socket = self.socket,
              socket.status == SocketIOStatus.connected,
              let roomId = self.roomId.value else {
            debugPrint("Can't receive, socket is already disconnect")
            return
        }
        fetch(by: roomId)
    }
    
    private func mapMessageList(from models: [MessageApiModel]) -> [MessageModel] {
        var messagesList: [MessageModel] = []
        models.forEach { [weak self] msg in
            guard let self = self,
                  let roomId = self.roomId.value else { return }
            
            
            var message: MessageModel = MessageModel(messageApiModel: msg)
            message.isCurrentSender = isCurrentSender(from: message)
            message.isDeleted = isDeletedMessage(from: message)

            // Cuando envíamos en el mensaje a caché lo ponemos con 0,
            // al actualizarlo le ponemos el roomId que le toca
            if message.roomId == 0 {
                message.roomId = roomId
            }
            
            if self.myUserHash != message.fromUserHash,
                message.status != MessageModel.MessageStatusSDK.read {
                self.update(message: message,
                            status: .read)
            }
            messagesList.append(message)
        }
        return messagesList
    }
    
    private func isDeletedMessage(from message: MessageModel) -> Bool {
        guard let deletedAt = message.deletedAt,
              !deletedAt.isZero else { return false }
        return true
    }
    
    private func isCurrentSender(from message: MessageModel) -> Bool {
        return self.myUserHash == message.fromUserHash
    }
}

// MARK: - Private Handlers
extension MediQuoSDKChatManager {
    private func onConnectionEvent() {
        guard let socket = self.socket else { return }
        socket.on(clientEvent: .connect) { _, _ in
            CoreLog.socketLog.debug("Socket connected successfully")
        }
    }

    private func onDisconnectionEvent() {
        guard let socket = self.socket else { return }
        socket.on(clientEvent: .disconnect) { _, _ in
            CoreLog.socketLog.debug("Socket is disconnected")
        }
    }

    private func onChangeStatusEvent() {
        guard let socket = self.socket, !self.isOnReady else { return }
        self.isOnReady = true
        socket.on(clientEvent: .statusChange) { _, _ in
            CoreLog.socketLog.debug("Socket status changed, new status is: %@", socket.status.description)
            self.socketStatusChanged.value = self.socket?.status
        }
    }

    private func onReadyEvent() {
        guard let socket = self.socket else { return }
        socket.on("ready") { _, _ in
            CoreLog.socketLog.debug("Socket is ready!")
            self.socketStatusChanged.value = self.socket?.status
            self.onVideoCallEvents()
        }
    }
}

// MARK: - Cache Methods
extension MediQuoSDKChatManager {
    public func setMessagesInCache(toUserHash: String,
                             myUserHash: String, messages: [MessageModel]) {
         chatCacheManager?.setMessagesInCache(toUserHash: toUserHash,
                                              myUserHash: myUserHash,
                                              messages: messages.suffix(20))
     }
     
    public func getMessagesFromCache(toUserHash: String,
                               myUserHash: String) -> [MessageModel] {
         return chatCacheManager?.getMessagesFromCache(toUserHash: toUserHash,
                                                       myUserHash: myUserHash) ?? []
     }
     
    public func getMessagesPendingToSentFromCache(toUserHash: String, myUserHash: String) -> [MessageModel] {
         return chatCacheManager?.getMessagesPendingToSentFromCache(toUserHash: toUserHash,
                                                                    myUserHash: myUserHash) ?? []
     }
     
    public func getLastMessagesPendingToSentFromCache(toUserHash: String, myUserHash: String) -> [MessageModel] {
         return chatCacheManager?.getLastMessagesPendingToSentFromCache(toUserHash: toUserHash, myUserHash: myUserHash) ?? []
     }
     
    public func setMessageInCache(toUserHash: String,
                           myUserHash: String,
                           message: String) {
        chatCacheManager?.setMessageInCache(toUserHash: toUserHash,
                                            myUserHash: myUserHash,
                                            message: message)
    }
    
    public func getMessageFromCache(toUserHash: String,
                             myUserHash: String) -> String {
        return chatCacheManager?.getMessageFromCache(toUserHash: toUserHash,
                                                     myUserHash: myUserHash) ?? ""
    }
}




