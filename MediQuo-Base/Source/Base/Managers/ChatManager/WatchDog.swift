//
//  WatchDog.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 1/2/23.
//  Copyright © 2023 Mediquo. All rights reserved.
//

import UIKit

public class WatchDogSDK: NSObject {
    public let repeatTime: TimeInterval
    private var timer: Timer?
    public var action: (() -> Void)?
    public private(set) var isRunning: Bool = false

    public var isValid: Bool {
        return self.timer?.isValid ?? false
    }

    override public init() {
        self.repeatTime = 1
    }

    public init(repeatTime: TimeInterval) {
        self.repeatTime = repeatTime
    }

    deinit {
        self.timer?.invalidate()
        self.timer = nil
        self.action = nil
        NotificationCenter.default.removeObserver(self)
    }

    public func start() {
        if self.isRunning {
            return
        }
        if self.timer == nil {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        }
        self.timer = Timer.scheduledTimer(withTimeInterval: self.repeatTime, repeats: true, block: { _ in
            self.action?()
        })

        self.isRunning = true
    }

    public func stop() {
        self.isRunning = false
        self.timer?.invalidate()
        self.timer = nil
    }

    // MARK: Application Notifications

    @objc private func didBecomeActive() {
        self.start()
    }

    @objc private func didEnterBackground() {
        self.stop()
    }
}
