//
//  MediquoSDKChatCache+Manager.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 5/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation
 
public protocol MediquoChatCacheManager {
    func setMessagesInCache(toUserHash: String,
                            myUserHash: String,
                            messages: [MessageModel])
    func setMessageInCache(toUserHash: String,
                           myUserHash: String,
                           message: String)
    
    func getMessagesFromCache(toUserHash: String,
                              myUserHash: String) -> [MessageModel]
    func getMessagesPendingToSentFromCache(toUserHash: String,
                                           myUserHash: String) -> [MessageModel]
    func getLastMessagesPendingToSentFromCache(toUserHash: String,
                                               myUserHash: String) -> [MessageModel]
    func getMessageFromCache(toUserHash: String,
                             myUserHash: String) -> String
}

public class DefaultMediquoChatCacheManager: MediquoChatCacheManager {
    private let chatMessageCacheKey = "MESSAGE-"
    private let chatMessagesCacheKey = "MESSAGES-"
    
    public init() {}
}

// MARK: - Messages in Cache
extension DefaultMediquoChatCacheManager {
    public func setMessagesInCache(toUserHash: String,
                                   myUserHash: String,
                                   messages: [MessageModel]) {
        
        if let messagesEncoded = try? JSONEncoder().encode(messages) {
            UserDefaults.standard.set(messagesEncoded, forKey: chatMessagesCacheKey + toUserHash + myUserHash)
        }
    }
    
    public func getMessagesFromCache(toUserHash: String,
                                     myUserHash: String) -> [MessageModel] {
        if let data = UserDefaults.standard.object(forKey: chatMessagesCacheKey + toUserHash + myUserHash) as? Data,
           let messagesDecoded = try? JSONDecoder().decode([MessageModel].self, from: data) {
            return messagesDecoded
        } else {
            return []
        }
    }
}

// MARK: - Messages Pending in Cache
extension DefaultMediquoChatCacheManager {
    public func getMessagesPendingToSentFromCache(toUserHash: String,
                                                  myUserHash: String) -> [MessageModel] {
        return getMessagesFromCache(toUserHash: toUserHash,
                                    myUserHash: myUserHash).compactMap { message in
            return message.status == .error ? message : nil
        }
    }
    
    public func getLastMessagesPendingToSentFromCache(toUserHash: String,
                                                      myUserHash: String) -> [MessageModel] {
        if getMessagesFromCache(toUserHash: toUserHash,
                                myUserHash: myUserHash).last?.status == .error {
            return getMessagesFromCache(toUserHash: toUserHash,
                                        myUserHash: myUserHash).compactMap { message in
                return message.status == .error ? message : nil
            }
        } else {
            return []
        }
    }
}

// MARK: - Single Message in Cache
extension DefaultMediquoChatCacheManager {
    public func setMessageInCache(toUserHash: String,
                                  myUserHash: String,
                                  message: String) {
        if let messageEncoded = try? JSONEncoder().encode(message) {
            UserDefaults.standard.set(messageEncoded, forKey: chatMessageCacheKey + toUserHash + myUserHash)
        }
    }
    
    public func getMessageFromCache(toUserHash: String,
                                    myUserHash: String) -> String {
        if let data = UserDefaults.standard.object(forKey: chatMessageCacheKey + toUserHash + myUserHash) as? Data,
           let messageDecoded = try? JSONDecoder().decode(String.self, from: data) {
            return messageDecoded
        } else {
            return ""
        }
    }
}

