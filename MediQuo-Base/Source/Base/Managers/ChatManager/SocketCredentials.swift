//
//  SocketCredentials.swift
//  MediQuo-BaseTests
//
//  Created by David Martin on 26/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import Foundation

struct SocketCredentials: Encodable {
    enum CodingKeys: String, CodingKey {
        case jwt
    }

    let jwt: String

    public func encode(to encoder: Encoder) throws {
        var container: KeyedEncodingContainer<CodingKeys> = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.jwt, forKey: .jwt)
    }
}
