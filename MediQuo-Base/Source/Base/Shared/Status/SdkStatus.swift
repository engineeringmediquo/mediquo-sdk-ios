//
//  SdkStatus.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

public enum SdkStatus {
    case ready
    case unauthorized(Error)
}
