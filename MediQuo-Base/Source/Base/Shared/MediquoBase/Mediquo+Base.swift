//
//  Mediquo+Base.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 17/10/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation
import UIKit

public class MediquoBase {
    public static func getProfessionalListView() -> UINavigationController {        
        let navigationController = UINavigationController(rootViewController: UIViewController())
        navigationController.modalPresentationStyle = .overFullScreen
        return navigationController
    }
}
