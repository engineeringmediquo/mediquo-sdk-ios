//
//  MediQuo+Notifications.swift
//  MediQuo-Base
//
//  Created by David Martin on 12/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import mediquo_videocall_lib
import UIKit

// MARK: - Notifications

extension MediQuoSDK {
    public class func didReceiveRemoteNotification(userInfo: [AnyHashable: Any], completion: @escaping () -> Void) {
        guard let jsonStringData: String = userInfo["data"] as? String,
              let data = jsonStringData.data(using: .utf8),
              let notificationSchema: NotificationSchema = try? JSONDecoder().decode(NotificationSchema.self, from: data) else { return }

        switch notificationSchema.notificationType {
        case .messageCreated:
            let topViewController = UIApplication.topViewController()

            if let viewController: ProfessionalsListViewController = topViewController as? ProfessionalsListViewController {
                viewController.execute(deeplink: notificationSchema)
            } else if let viewController: ChatRoomViewController = topViewController as? ChatRoomViewController {
                viewController.execute(deeplink: notificationSchema)
            } else if let viewController: ProfessionalProfileViewController = topViewController as? ProfessionalProfileViewController {
                viewController.execute(deeplink: notificationSchema)
            } else {
                // Mark type of deeplink to execute
                guard let roomId = notificationSchema.roomId else { return }
                MediQuoSDK.instance.deeplinkModel = DeeplinkModel(type: .chat(roomId))

                // Set professionals list as root view controller
                let viewController = MediQuoSDK.getProfessionalsList()
                let navigationController = UINavigationController(rootViewController: viewController)
                UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = navigationController
            }
        case .callRequested, .callRejected:
            self.instance.processVideoCallDidReceiveRemoteNotification(userInfo: userInfo, completion: completion)
        case .unknown:
            completion()
        }
    }

    // Alternative function to get chat ViewController when open a push notification
    public class func didReceiveRemoteNotificationWithViewController(userInfo: [AnyHashable: Any], completion: @escaping (UIViewController?) -> Void) {
        guard let jsonStringData: String = userInfo["data"] as? String,
              let data = jsonStringData.data(using: .utf8),
              let notificationSchema: NotificationSchema = try? JSONDecoder().decode(NotificationSchema.self, from: data) else { return }

        switch notificationSchema.notificationType {
        case .messageCreated:
            let topViewController = UIApplication.topViewController()

            if let viewController: ProfessionalsListViewController = topViewController as? ProfessionalsListViewController {
                viewController.execute(deeplink: notificationSchema)
            } else if let viewController: ChatRoomViewController = topViewController as? ChatRoomViewController {
                viewController.execute(deeplink: notificationSchema)
            } else if let viewController: ProfessionalProfileViewController = topViewController as? ProfessionalProfileViewController {
                viewController.execute(deeplink: notificationSchema)
            } else {
                // Mark type of deeplink to execute
                guard let roomId = notificationSchema.roomId else { return }
                MediQuoSDK.instance.deeplinkModel = DeeplinkModel(type: .chat(roomId))

                let viewController = MediQuoSDK.getProfessionalsList()
                completion(viewController)
            }
        case .callRequested, .callRejected:
            self.instance.processVideoCallDidReceiveRemoteNotification(userInfo: userInfo) {
                completion(nil)
            }
        case .unknown:
            completion(nil)
        }
    }

    public class func willPresentRemoteNotification(userInfo: [AnyHashable: Any], completion: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard let jsonStringData: String = userInfo["data"] as? String,
              let data = jsonStringData.data(using: .utf8),
              let notificationSchema: NotificationSchema = try? JSONDecoder().decode(NotificationSchema.self, from: data) else {
            completion([])
            return
        }

        switch notificationSchema.notificationType {
        case .messageCreated:
            guard let roomId = notificationSchema.roomId else {
                completion([])
                return
            }
            let topViewController = UIApplication.topViewController()

            if let viewController: ProfessionalsListViewController = topViewController as? ProfessionalsListViewController {
                NotificationCenter.default.post(name: Notification.Name.Push.messageReceived, object: nil)
                viewController.setNotificationPresentation(by: roomId, completionHandler: completion)
            } else if let viewController: ChatRoomViewController = topViewController as? ChatRoomViewController {
                viewController.setNotificationPresentation(by: roomId, completionHandler: completion)
            } else if let viewController: ProfessionalProfileViewController = topViewController as? ProfessionalProfileViewController {
                viewController.setNotificationPresentation(by: roomId, completionHandler: completion)
            } else {
                completion([.alert, .badge, .sound])
            }
        case .callRequested, .callRejected:
            self.instance.processVideoCallWillPresentRemoteNotification(userInfo: userInfo, completion: completion)
        case .unknown:
            completion([])
        }
    }

    public class func didReceiveRemoteNotification(userInfo: [AnyHashable: Any], completion: @escaping (UIBackgroundFetchResult) -> Void) {
        guard userInfo["data"] != nil else {
            completion(.failed)
            return
        }
        completion(.noData)
    }
}
