//
//  MediQuo+VideoCall.swift
//  MediQuo-Base
//
//  Created by David Martin on 14/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import mediquo_videocall_lib
import PromiseKit
import UIKit

// MARK: - Init lib

extension MediQuoSDK {
    internal func processVideoCall<T>(by result: PromiseKit.Result<T>) {
        guard result.isFulfilled else { return }
        self.initVideoCallLib()
    }

    private func initVideoCallLib() {
        guard let apiKey = self.authManager.getApiKey(), let jwt = self.authManager.getJwt() else { return }
        let environment: EnvironmentType = EnvironmentManager().get()
        switch environment {
        case .debug, .testing:
            VideoCall.initLibForSdk(to: .develop, apiKey: apiKey, token: jwt, and: localizableVC)
        case .release:
            VideoCall.initLibForSdk(to: .production, apiKey: apiKey, token: jwt, and: localizableVC)
        }
    }
}

// MARK: - Notifications

extension MediQuoSDK {
    internal func processVideoCallDidReceiveRemoteNotification(userInfo: [AnyHashable: Any], completion: @escaping () -> Void) {
        guard let currentViewController = UIApplication.topViewController(), !MediQuoSDK.instance.isVideoCallSocketInitialized else { return }
        self.initVideoCallLib()
        self.videoCallManager?.processTapPushNotification(role: .patient, currentViewController: currentViewController, userInfo: userInfo, completionHandler: completion)
    }

    internal func processVideoCallWillPresentRemoteNotification(userInfo: [AnyHashable: Any], completion: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard let currentViewController = UIApplication.topViewController(), !MediQuoSDK.instance.isVideoCallSocketInitialized else {
            completion([])
            return
        }
        self.initVideoCallLib()
        self.videoCallManager?.processPushNotification(role: .patient, currentViewController: currentViewController, userInfo: userInfo, completion: completion)
    }

    internal func processVideoCallDidReceiveRemoteNotification(schema: VideoCallNotificationSchema, completion: @escaping (UIBackgroundFetchResult) -> Void) {
        completion(schema.type != nil ? .noData : .failed)
    }
}

// MARK: - Room videocall

extension MediQuoSDK {
    internal func showRoomCallRequested(_ model: VideoCallCurrentCallModel) {
        MediQuoSDK.instance.isVideoCallSocketInitialized = true

        var type: CallType? = .none
        switch model.type {
            case .call:
                type = .call
            case .video:
                type = .video
            default:
                break
        }
        let callModel: CallModel = CallModel(uuid: model.uuid, roomId: nil, type: type, sessionId: model.session.sessionId, token: model.session.customerToken)
        let customerModel: CustomerModel = CustomerModel(name: model.name, avatar: model.avatar)
        let schema: VideoCallNotificationSchema = VideoCallNotificationSchema(call: callModel, customer: customerModel)

        self.videoCallManager?.showInProgressCall(by: .patient, by: schema) { viewController in
            if let viewController = viewController {
                viewController.modalPresentationStyle = .fullScreen
                UIApplication.topViewController()?.present(viewController, animated: true)
            }
        }
    }
}
