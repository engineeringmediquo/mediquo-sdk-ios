//
//  MediQuo+MedicalHistory.swift
//  MediQuo-Base
//
//  Created by David Martin on 26/2/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

// MARK: - Medical History

extension MediQuoSDK {
    public class func getMedicalHistory() -> MedicalHistoryListViewController {
        return MedicalHistoryListViewController()
    }

    public class func getAllergies() -> MedicalHistorySectionViewController { 
        let viewController: MedicalHistorySectionViewController = StoryboardScene.MedicalHistorySection.medicalHistorySection.instantiate()
        viewController.type = .allergies
        return viewController
    }

    internal class func getAllergyForm() -> MedicalHistorySectionFormViewController {
        let viewController: MedicalHistorySectionFormViewController = StoryboardScene.MedicalHistorySection.medicalHistorySectionForm.instantiate()
        viewController.type = .allergies
        return viewController
    }

    public class func getDiseases() -> MedicalHistorySectionViewController {
        let viewController: MedicalHistorySectionViewController = StoryboardScene.MedicalHistorySection.medicalHistorySection.instantiate()
        viewController.type = .diseases
        return viewController
    }

    internal class func getDiseaseForm() -> MedicalHistorySectionFormViewController {
        let viewController: MedicalHistorySectionFormViewController = StoryboardScene.MedicalHistorySection.medicalHistorySectionForm.instantiate()
        viewController.type = .diseases
        return viewController
    }

    public class func getMedications() -> MedicalHistorySectionViewController {
        let viewController: MedicalHistorySectionViewController = StoryboardScene.MedicalHistorySection.medicalHistorySection.instantiate()
        viewController.type = .medications
        return viewController
    }

    internal class func getMedicationForm() -> MedicalHistorySectionFormViewController {
        let viewController: MedicalHistorySectionFormViewController = StoryboardScene.MedicalHistorySection.medicalHistorySectionForm.instantiate()
        viewController.type = .medications
        return viewController
    }
}

// MARK: - Reports

extension MediQuoSDK {
    public class func getReports() -> ReportsListViewController {
        return StoryboardScene.ReportsList.initialScene.instantiate()
    }

    internal class func getReportDetail(_ reportUUID: String?) -> ReportDetailViewController {
        let viewController: ReportDetailViewController = StoryboardScene.ReportDetail.initialScene.instantiate()
        viewController.reportUUID = reportUUID
        return viewController
    }

    internal class func getReportsListEmpty() -> ReportListEmptyViewController {
        return StoryboardScene.ReportListEmpty.initialScene.instantiate()
    }

    internal class func getReportDetailPDF(data: Data?, with reportModel: ReportModel?) -> ReportDetailPDFViewController {
        let viewController: ReportDetailPDFViewController = StoryboardScene.ReportDetailPDF.initialScene.instantiate()
        viewController.data = data
        viewController.reportModel = reportModel
        return viewController
    }
}

// MARK: - Recipes

extension MediQuoSDK {
    public class func getRecipes() -> RecipesListViewController {
        return StoryboardScene.RecipesList.initialScene.instantiate()
    }

    internal class func getRecipesListEmpty() -> RecipesEmptyListViewController {
        return StoryboardScene.RecipesEmptyList.initialScene.instantiate()
    }

    internal class func getRecipeDetailPDF(data: Data?, with recipeModel: RecipeModel?) -> RecipesDetailPDFViewController {
        let viewController: RecipesDetailPDFViewController = StoryboardScene.RecipesDetailPDF.initialScene.instantiate()
        viewController.data = data
        viewController.recipeModel = recipeModel
        return viewController
    }
}
