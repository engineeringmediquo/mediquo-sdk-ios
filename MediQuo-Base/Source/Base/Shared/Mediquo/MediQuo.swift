//
//  MediQuo.swift
//  MediQuo-Base
//
//  Created by David Martin on 23/07/2020.
//  Copyright © 2020 Mediquo. All rights reserved.
//

import mediquo_videocall_lib
import PromiseKit
import UIKit

// MARK: - Injections

public final class MediQuoSDK {
    private var _style: MediQuoStyleType?

    private var _isVideoCallSocketInitialized: Bool = false

    @Inject
    private var installationManager: InstallationManager

    @Inject
    internal var authManager: AuthManager

    @Inject
    internal var profileManager: ProfileManager

    @Inject
    private static var registerFirebaseManager: RegisterFirebaseManager

    @Inject
    internal var professionalsListManager: ProfessionalsListManager

    @StorageDatasource
    internal var deeplinkModel: DeeplinkModel?
}

// MARK: - Configure and setup

extension MediQuoSDK {
    public class func _mqInit(apiKey: String, style: MediQuoStyleType? = nil, completion: @escaping (Swift.Result<Void, BaseError>) -> Void) {
        EnvironmentManager().set(environment: .debug)
        MediQuoSDK.instance.initalize(apiKey: apiKey, style: style, completion: completion)
    }

    public class func initialize(apiKey: String, style: MediQuoStyleType? = nil, completion: @escaping (Swift.Result<Void, BaseError>) -> Void) {
        EnvironmentManager().set(environment: .release)
        MediQuoSDK.instance.initalize(apiKey: apiKey, style: style, completion: completion)
    }

    public class func authenticate(clientCode: String, completion: @escaping (SdkStatus) -> Void) {
        MediQuoSDK.instance.authenticate(clientCode: clientCode, completion: completion)
    }

    public class func deauthenticate(completion: @escaping (Bool) -> Void) {
        MediQuoSDK.instance.deauthenticate(completion: completion)
    }

    public class func registerFirebase(token: String, completion: @escaping (Bool) -> Void) {
        let deviceId = self.instance.installationManager.retrieveUUID()
        MediQuoSDK.registerFirebaseManager.register(request: RegisterFirebaseRequest(token: token, deviceId: deviceId)) { result in
            if case .success = result {
                CoreLog.business.info("Firebase token has been registered successfully")
                completion(true)
            }
            if case let .failure(error) = result {
                CoreLog.business.error("Firebase token couldn't registered: %@", error.localizedDescription)
                completion(false)
            }
        }
    }
    
    public class func initialize(style: MediQuoStyleType) {
        CoreLog.business.info("SDK styles has been initialized successfully")
        MediQuoSDK.instance.style = style
    }
}

// MARK: - Internal params

extension MediQuoSDK {
    internal static let instance: MediQuoSDK = MediQuoSDK()

    internal var jwt: String? {
        self.authManager.getJwt()
    }

    internal var videoCallManager: VideoCallManagerProtocol? {
        try? VideoCall.getVideoCallManager()
    }

    internal var isVideoCallSocketInitialized: Bool {
        get {
            return MediQuoSDK.instance._isVideoCallSocketInitialized
        }
        set {
            MediQuoSDK.instance._isVideoCallSocketInitialized = newValue
        }
    }

    internal var style: MediQuoStyleType? {
        get {
            return MediQuoSDK.instance._style
        }
        set {
            MediQuoSDK.instance._style = newValue
        }
    }
}

// MARK: - Initalize Flow

extension MediQuoSDK {
    private func initalize(apiKey: String,
                           style: MediQuoStyleType? = nil,
                           completion: @escaping (Swift.Result<Void, BaseError>) -> Void) {
        firstly {
            self.set(style: style)
        }.then {
            self.injectDependences()
        }.then {
            self.set(apiKey: apiKey)
        }.then {
            self.install()
        }.done {
            CoreLog.business.info("Initialize flow done")
            completion(.success)
        }.catch { error in
            CoreLog.business.error("Initialize flow with errors: %@", error.localizedDescription)
            completion(.failure(.remoteError(.reason(error.localizedDescription))))
        }.finally {
            CoreLog.business.info("Initialize flow has been finished")
        }
    }
    
    private func set(style: MediQuoStyleType?) -> Promise<Void> {
        return Promise<Void> { seal in
            if style == nil {
                self.style = MediQuoStyle()
            } else {
                self.style = style
            }
            seal.fulfill_()
        }
    }

    private func injectDependences() -> Promise<Void> {
        return Promise<Void> { seal in
            Components.resolve()
            seal.fulfill_()
        }
    }

    private func set(apiKey: String) -> Promise<Void> {
        return Promise<Void> { seal in
            self.authManager.set(apiKey: apiKey)
            seal.fulfill_()
        }
    }

    private func install() -> Promise<Void> {
        return Promise<Void> { seal in
            self.installationManager.install { result in
                if case .success = result {
                    seal.fulfill_()
                }
                if case let .failure(error) = result {
                    seal.reject(error)
                }
            }
        }
    }
}

// MARK: - Authenticate Flow

extension MediQuoSDK {
    private func authenticate(clientCode: String, style: MediQuoStyleType? = nil, completion: @escaping (SdkStatus) -> Void) {
        firstly {
            self.set(clientCode: clientCode)
        }.then {
            self.authenticate()
        }.then {
            self.setProfile()
        }.tap { result in
            self.processVideoCall(by: result)
        }.done {
            self.processInitLib(.ready, completion: completion)
        }.catch { error in
            self.processInitLib(.unauthorized(error), completion: completion)
        }.finally {
            CoreLog.business.info("Initialize flow has been finished")
        }
    }

    private func set(clientCode: String) -> Promise<Void> {
        return Promise<Void> { seal in
            self.authManager.set(clientCode: clientCode)
            seal.fulfill_()
        }
    }

    private func authenticate() -> Promise<Void> {
        return Promise<Void> { seal in
            self.authManager.authenticate { result in
                if case .success = result {
                    seal.fulfill_()
                }
                if case let .failure(error) = result {
                    seal.reject(error)
                }
            }
        }
    }

    private func setProfile() -> Promise<Void> {
        return Promise<Void> { seal in
            self.profileManager.fetch { result in
                if case .success = result {
                    seal.fulfill_()
                }
                if case let .failure(error) = result {
                    seal.reject(error)
                }
            }
        }
    }

    private func processInitLib(_ status: SdkStatus, completion: @escaping (SdkStatus) -> Void) {
        switch status {
        case .ready:
            CoreLog.business.info("MediQuo SDK has been initialized successfully")
        case .unauthorized(let error):
            CoreLog.business.error("MediQuo SDK couldn't initialized: %@", error.localizedDescription)
        }
        completion(status)
    }
}

// MARK: - Deauthenticate

extension MediQuoSDK {
    private func deauthenticate(completion: @escaping (Bool) -> Void) {
        firstly {
            self.invalidateSession()
        }.tap { result in
            self.unregisterFirebase(by: result)
        }.done {
            self.processDeInitLib(true, completion: completion)
        }.catch { error in
            self.processDeInitLib(false, completion: completion)
        }.finally {
            CoreLog.business.info("Deauthenticate flow has been finished")
        }
    }

    private func invalidateSession() -> Promise<Void> {
        return Promise<Void> { seal in
            self.authManager.invalidate { result in
                if case .success = result {
                    CoreLog.business.info("MediQuo SDK has been invalidated session successfully")
                    seal.fulfill_()
                }
                if case let .failure(error) = result {
                    CoreLog.business.error("MediQuo SDK couldn't invalidated session: %@", error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }

    private func unregisterFirebase<T>(by result: PromiseKit.Result<T>) {
        guard result.isFulfilled else { return }
        MediQuoSDK.registerFirebaseManager.unregister { result in
            if case .success = result {
                CoreLog.business.info("Firebase token has been unregistered successfully")
            }
            if case let .failure(error) = result {
                CoreLog.business.error("Firebase token couldn't unregistered: %@", error.localizedDescription)
            }
        }
    }

    private func processDeInitLib(_ isDeauthenticated: Bool, error: Error? = nil, completion: @escaping (Bool) -> Void) {
        if isDeauthenticated {
            CoreLog.business.error("MediQuo SDK has been desauthenticated successfully")
            completion(true)
        } else {
            guard let error = error else { completion(false); return }
            CoreLog.business.error("MediQuo SDK couldn't desauthenticated: %@", error.localizedDescription)
            completion(false)
        }
    }
}
