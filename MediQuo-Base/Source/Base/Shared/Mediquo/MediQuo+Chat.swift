//
//  MediQuo+Chat.swift
//  MediQuo-Base
//
//  Created by David Martin on 15/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

// MARK: - Chat

extension MediQuoSDK {
    public class func getProfessionalsList() -> ProfessionalsListViewController {
        return ProfessionalsListViewController()
    }

    public class func getPendingMessages(completion: @escaping (Int) -> Void) {
        self.instance.professionalsListManager.getPendingMessagesCount { result in
            if case let .success(response) = result {
                CoreLog.business.info("There are %@ pending messages", response)
                completion(response)
            }
            if case let .failure(error) = result {
                CoreLog.business.error("Error getting pending messages: %@", error.description)
                completion(0)
            }
        }
    }
}
