//
//  MediQuoStyleType.swift
//  MediQuo-Base
//
//  Created by David Martin on 22/1/21.
//  Copyright © 2021 Mediquo. All rights reserved.
//

import UIKit

public protocol MediQuoStyleType {
    
    // General
    var prefersLargeTitles: Bool { get set }
    var primaryColor: UIColor { get set }
    var primaryContrastColor: UIColor { get set }
    var secondaryColor: UIColor { get set }
    var accentColor: UIColor { get set }
    
    // Chat Room
    var chatBackgroundColor: UIColor { get set }
    var chatTitleViewColor: UIColor { get set }
    var chatSubTitleViewColor: UIColor { get set }
    var messageTextDateColor: UIColor { get set }
    var messageBackgroundDateColor: UIColor { get set }
    var messageTextOutgoingColor: UIColor { get set }
    var messageTextIncomingColor: UIColor { get set }
    var bubbleBackgroundOutgoingColor: UIColor { get set }
    var bubbleBackgroundIncomingColor: UIColor { get set }
    
    // Fonts
    var bookFont: UIFont? { get set }
    var mediumFont: UIFont? { get set }
    var boldFont: UIFont? { get set }
    
    // Delegates
    var professionalsListDelegate: ProfessionalsListDelegate? { get set }
}

public struct MediQuoStyle: MediQuoStyleType {
    
    
    // General
    public var prefersLargeTitles: Bool = true
    public var primaryColor: UIColor = ColorName.primary.color
    public var primaryContrastColor: UIColor = ColorName.contrast.color
    public var secondaryColor: UIColor = ColorName.secondary.color
    public var accentColor: UIColor = ColorName.accent.color
    
    // Chat Room
    public var chatBackgroundColor: UIColor = UIColor(red: 0.969, green: 0.973, blue: 0.98, alpha: 1)
    public var chatTitleViewColor: UIColor = ColorName.contrast.color
    public var chatSubTitleViewColor: UIColor = UIColor(red: 0.565, green: 0.643, blue: 0.682, alpha: 1) 
    public var messageTextDateColor: UIColor = ColorName.dateTextColor.color
    public var messageBackgroundDateColor: UIColor = ColorName.dateBackgroundColor.color
    public var messageTextOutgoingColor: UIColor = ColorName.messageTextOutgoingColor.color
    public var messageTextIncomingColor: UIColor = ColorName.messageTextIncomingColor.color
    public var bubbleBackgroundOutgoingColor: UIColor = ColorName.bubbleBackgroundOutgoingColor.color
    public var bubbleBackgroundIncomingColor: UIColor = ColorName.bubbleBackgroundIncomingColor.color
    
    // Fonts
    public var bookFont: UIFont?
    public var mediumFont: UIFont?
    public var boldFont: UIFont?
    
    // Delegates
    public var professionalsListDelegate: ProfessionalsListDelegate? // swiftlint:disable:this weak_delegate
    public init() {}
}
