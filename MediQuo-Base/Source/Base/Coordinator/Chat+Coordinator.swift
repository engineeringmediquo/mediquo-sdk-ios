//
//  Chat+Coordinator.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 14/11/22.
//

import UIKit

public class ChatSDKCoordinator: NSObject, ChildCoordinator {
    var parentCoordinator: MainSDKCoordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var profileManager: ProfileManager = ProfileManager()
    let chatRoomViewController: ChatRoomViewController
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        chatRoomViewController = ChatRoomViewController()
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}

extension ChatSDKCoordinator {
    // TODO: Cane - Crear un único model aquí
    public func showChatRoomView(professionalUserHash: String,
                                 isFromDeeplink: Bool,
                                 socket: MediquoChatManager?,
                                 fetchRoomUseCase: FetchRoomUseCase,
                                 chatUseCase: ChatControllerUseCase?,
                                 professionalModel: ProfessionalSDKModel?,
                                 currentUserHash: String?,
                                 currentUserName: String?,
                                 chatRoomNavigationDelegate: ChatRoomNavigationDelegate?,
                                 chatRoomAppointmentDelegate: ChatRoomAppointmentDelegate?) {
        chatRoomViewController.viewModel = DefaultChatRoomViewModel(socket: socket,
                                                                    professionalModel: professionalModel,
                                                                    currentUserHash: currentUserHash ?? profileManager.getProfile()?.userHash,
                                                                    currentUserName: currentUserName ?? profileManager.getProfile()?.name,
                                                                    fetchRoomUseCase: fetchRoomUseCase,
                                                                    chatUseCase: chatUseCase,
                                                                    professionalUserHash: professionalUserHash)
     
        chatRoomViewController.chatRoomNavigationDelegate = chatRoomNavigationDelegate
        chatRoomViewController.chatRoomAppointmentDelegate = chatRoomAppointmentDelegate
        let animated = !isFromDeeplink
        navigationController.pushViewController(chatRoomViewController, animated: animated)
    }
    
    func showProfessionalProfile(professionalModel: ProfessionalSDKModel?) {
        let viewController: ProfessionalProfileViewController = ProfessionalProfileViewController()
        viewController.professionalModel = professionalModel
        navigationController.pushViewController(viewController, animated: true)
    }
}

