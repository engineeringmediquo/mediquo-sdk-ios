//
//  Main+Coordinator.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 8/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
}

protocol ChildCoordinator: Coordinator {
    var parentCoordinator: MainSDKCoordinator? { get set }
}

public class MainSDKCoordinator: NSObject, Coordinator  {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    let childCoordinator: ChatSDKCoordinator
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        childCoordinator = ChatSDKCoordinator(navigationController: self.navigationController)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}

// MARK: - Chat Room Navigation
extension MainSDKCoordinator: ChatRoomNavigationDelegate {
    public func navigateOnRoomCallRequested(with model: VideoCallCurrentCallModel) {
        // TODO: Cane - Revisar esta jugada
        MediQuoSDK.instance.showRoomCallRequested(model)
    }
    
    public func navigateToProfile(with professionalModel: ProfessionalSDKModel?) {
        showProfessionalProfile(professionalModel: professionalModel)
    }
    
    public func showChatRoomView(professionalUserHash: String,
                                 isFromDeeplink: Bool,
                                 socket: MediquoChatManager? = nil,
                                 fetchRoomUseCase: FetchRoomUseCase? = nil,
                                 chatUseCase: ChatControllerUseCase? = nil,
                                 professionalModel: ProfessionalSDKModel?,
                                 currentUserHash: String? = nil,
                                 currentUserName: String? = nil,
                                 chatRoomNavigationDelegate: ChatRoomNavigationDelegate?,
                                 chatRoomAppointmentDelegate: ChatRoomAppointmentDelegate?) {
        childCoordinators.append(childCoordinator)
        
        // TODO: Cane - Convertir todos estos argumentos en un solo modelo
        childCoordinator.showChatRoomView(professionalUserHash: professionalUserHash,
                                          isFromDeeplink: isFromDeeplink,
                                          socket: socket,
                                          fetchRoomUseCase: fetchRoomUseCase ?? RoomUseCase(),
                                          chatUseCase: chatUseCase,
                                          professionalModel: professionalModel,
                                          currentUserHash: currentUserHash,
                                          currentUserName: currentUserName,
                                          chatRoomNavigationDelegate: chatRoomNavigationDelegate,
                                          chatRoomAppointmentDelegate: chatRoomAppointmentDelegate)
    }
    
    public func showProfessionalProfile(professionalModel: ProfessionalSDKModel?) {
        let childCoordinator = ChatSDKCoordinator(navigationController: self.navigationController)
        childCoordinators.append(childCoordinator)
        childCoordinator.showProfessionalProfile(professionalModel: professionalModel)
    }
}
