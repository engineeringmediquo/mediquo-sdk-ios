//
//  ProfessionalList+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 17/10/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

public protocol ProfessionalListSDKEmptyViewDelegate {
    func showEmptyView()
    func hideEmptyView()
}

public protocol ProfessionalListSDKViewNavigationDelegate {
     func navigateTo(section: Int, position: Int)
}

public class ProfessionalListSDKView: UIView {
    
    public var emptyViewDelegate: ProfessionalListSDKEmptyViewDelegate?
    public var navigationDelegate: ProfessionalListSDKViewNavigationDelegate?
    
    // Private properties
    private var sections: [Int: String] = [:]
    private var professionals: [ProfessionalSDKModel] = []
    private var isSearched: Bool = false
    
    private(set) var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    // MARK: View lifeCycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // Public Methods
    public func reloadData(from professionals: [ProfessionalSDKModel],
                           and sections:[Int: String],
                           with isSearched: Bool) {
        
        self.professionals = []
        self.sections = [:]
        self.professionals = professionals
        self.sections = sections
        tableView.refreshControl?.endRefreshing()
        tableView.refreshControl?.isEnabled = !isSearched
        tableView.reloadData()
    }
    
    public func setBackgroundViewVisibility(isHidden: Bool) {
        tableView.backgroundView?.isHidden = isHidden
    }
    
    public func getRefreshControl() -> UIRefreshControl? {
        tableView.refreshControl = UIRefreshControl()
        return tableView.refreshControl
    }
    
    public func endRefreshing() {
        tableView.refreshControl?.endRefreshing()
        tableView.refreshControl?.isEnabled = false
    }
}

// MARK: Setup View
extension ProfessionalListSDKView {
    private func setupView() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(ProfessionalsListSDKCell.self,forCellReuseIdentifier: ProfessionalsListSDKCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundView = EmptyListViewController().view
        tableView.backgroundColor = .white
        tableView.keyboardDismissMode = .onDrag
        addSubview(tableView)
        setupTableViewConstraints()
    }
}

// MARK: - TableView DataSource
extension ProfessionalListSDKView: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        navigationDelegate?.navigateTo(section: indexPath.section,
                                       position: indexPath.row)
    }
    
    public func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        toogleTableViewBackground()
        return professionals.count
    }
    
    public func numberOfSections(in _: UITableView) -> Int {
        return sections.count
    }

    public func tableView(_: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: ProfessionalsListSDKCell = tableView.dequeueReusableCell(withIdentifier: ProfessionalsListSDKCell.identifier,
                                                                                 for: indexPath) as? ProfessionalsListSDKCell else { return UITableViewCell() }
        
        cell.configure(professionals[indexPath.row])
        return cell
    }

    public func tableView(_: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return professionals.isEmpty ? CGFloat(0) : CGFloat(64) 
    }

    func toogleTableViewBackground() {
        if !professionals.isEmpty {
            if isSearched {
                emptyViewDelegate?.showEmptyView()
            } else {
                setBackgroundViewVisibility(isHidden: true)
            }
        } else {
            emptyViewDelegate?.showEmptyView()
            setBackgroundViewVisibility(isHidden: false)
        }
    }
}


// MARK: Setup Constraints
extension ProfessionalListSDKView {
    private enum Margins {
        static let zero = CGFloat(0)
    }
    
    private enum Sizes {
        static let zero = CGFloat(0)
    }
    
    private func setupTableViewConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor, constant: Margins.zero),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.zero),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: Margins.zero),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Margins.zero)
        ])
    }
}
