//
//  ProfessionalsListSDKCell+Constraints.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 2/11/22.
//

import UIKit
import SnapKit

// MARK: - Update constraints
extension ProfessionalsListSDKCell {
     func updateGlobalStackViewConstraints() {
        globalHStackView.snp.makeConstraints { make in
            make.center.equalTo(self.contentView)
            make.height.equalTo(self.contentView).offset(-32)
            make.width.equalTo(self.contentView).offset(-32)
        }
    }

    func updateAvatarConstraints() {
        avatarImageView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView)
            make.leading.equalTo(self.globalHStackView)
            make.size.equalTo(CGSize(width: 48, height: 48))
        }
        statusView.snp.makeConstraints { make in
            make.top.equalTo(self.avatarImageView)
            make.trailing.equalTo(self.avatarImageView)
            make.size.equalTo(CGSize(width: 14, height: 14))
        }
        organizationLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        organizationView.snp.makeConstraints { make in
            make.top.equalTo(self.avatarImageView.snp.bottom).offset(-4)
            make.centerX.equalTo(self.avatarImageView)
            make.size.equalTo(CGSize(width: 48, height: 16))
        }
    }

    func updateCenterStackViewConstraints() {
        centerStackView.snp.makeConstraints { make in
            make.centerY.equalTo(self.globalHStackView)
            make.height.equalTo(self.globalHStackView)
        }
        nameLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView)
        }
        specialityLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView)
        }
        lastMessageLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self.centerStackView)
            make.height.equalTo(20)
            make.width.equalTo(self.centerStackView)
        }
    }

    func updateRightStackViewConstraints() {
        rightView.snp.makeConstraints { make in
            make.trailing.equalTo(self.globalHStackView)
            make.height.equalTo(self.globalHStackView)
            make.centerY.equalTo(self.globalHStackView)
            make.width.equalTo(64)
        }
        updateDateLabel.snp.makeConstraints { make in
            make.top.equalTo(self.rightView)
            make.trailing.equalTo(self.rightView)
            make.height.equalTo(20)
        }
        lockImageView.snp.makeConstraints { make in
            make.top.equalTo(self.updateDateLabel.snp.bottom)
            make.centerX.equalTo(self.rightView)
            make.size.equalTo(CGSize(width: 20, height: 20))
        }
        iconsStackView.snp.makeConstraints { make in
            make.top.equalTo(self.lockImageView.snp.bottom)
            make.centerX.equalTo(self.rightView)
            make.height.equalTo(16)
            make.width.equalTo(self.rightView)
        }
        iconsGapView.snp.makeConstraints { make in
            make.centerY.equalTo(self.iconsStackView)
            make.height.equalTo(self.iconsStackView)
            make.width.equalTo(48)
        }
        appointmentsImage.snp.makeConstraints { make in
            make.center.equalTo(self.appointmentsView)
            make.size.equalTo(self.appointmentsView)
        }
        appointmentsView.snp.makeConstraints { make in
            make.centerY.equalTo(self.iconsStackView)
            make.height.equalTo(self.iconsStackView)
            make.width.equalTo(16)
        }
        pendingMessagesLabel.snp.makeConstraints { make in
            make.center.equalTo(self.pendingMessagesView)
        }
        pendingMessagesView.snp.makeConstraints { make in
            make.centerY.equalTo(self.iconsStackView)
            make.height.equalTo(self.iconsStackView)
            make.width.equalTo(16)
        }
        rightGapView.snp.makeConstraints { make in
            make.top.equalTo(self.iconsStackView.snp.bottom)
            make.bottom.equalTo(self.globalHStackView)
            make.centerX.equalTo(self.rightView)
            make.width.equalTo(self.rightView)
        }
    }
}
