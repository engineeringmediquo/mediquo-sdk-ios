//
//  ProfessionalList+Cell.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 31/10/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import Foundation
import SDWebImage
import SnapKit

class ProfessionalsListSDKCell: UITableViewCell {
    private enum LastMessageSentType: String {
        case file
        case image
    }
    
    static let identifier: String = "professionalsList"

    // MARK: Global Stack View

    private(set) var globalHStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 16
        return stackView
    }()

    // MARK: Left Avatar Image View

    private(set) var avatarImageView = UIImageView()
    private(set) var statusView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 2
        return view
    }()

    private(set) var organizationView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        return view
    }()

    private(set) var organizationLabel: UILabel = {
        let label = UILabel()
        label.font = FontFamily.GothamRounded.medium.font(size: 12).customOrDefault 
        return label
    }()

    // MARK: Center Vertical Stack View

    private(set) var centerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()

    private(set) var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .dark()
        label.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        return label
    }()

    private(set) var specialityLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.primary()
        label.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private(set) var lastMessageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .dark()
        label.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        return label
    }()

    // MARK: Right Vertical Stack View

    private(set) var rightView = UIView()
    private(set) var updateDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = FontFamily.GothamRounded.book.font(size: 12).customOrDefault
        label.textAlignment = .right
        return label
    }()

    private(set) var lockImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icLock")
        imageView.isHidden = true
        return imageView
    }()

    private(set) var iconsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .trailing
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 4
        return stackView
    }()

    private(set) var iconsGapView: UIView = {
        let view = UIView()
        view.isHidden = false
        return view
    }()

    private(set) var appointmentsView: UIView = {
        let view = UIView()
        view.isHidden = true
        return view
    }()

    private(set) var appointmentsImage = UIImageView()

    private(set) var pendingMessagesView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = UIColor.secondary()
        return view
    }()

    private(set) var pendingMessagesLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = FontFamily.GothamRounded.medium.font(size: 10).customOrDefault
        label.textAlignment = .center
        return label
    }()

    private(set) var rightGapView: UIView = {
        let view = UIView()
        view.isHidden = false
        return view
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        centerStackView.addArrangedSubview(nameLabel)
        centerStackView.addArrangedSubview(specialityLabel)
        centerStackView.addArrangedSubview(lastMessageLabel)

        pendingMessagesView.addSubview(pendingMessagesLabel)
        appointmentsView.addSubview(appointmentsImage)
        iconsStackView.addArrangedSubview(iconsGapView)
        iconsStackView.addArrangedSubview(appointmentsView)
        iconsStackView.addArrangedSubview(pendingMessagesView)

        rightView.addSubview(updateDateLabel)
        rightView.addSubview(lockImageView)
        rightView.addSubview(iconsStackView)
        rightView.addSubview(rightGapView)

        organizationView.addSubview(organizationLabel)

        globalHStackView.addArrangedSubview(avatarImageView)
        globalHStackView.addSubview(statusView)
        globalHStackView.addSubview(organizationView)
        globalHStackView.addArrangedSubview(centerStackView)
        globalHStackView.addArrangedSubview(rightView)

        contentView.addSubview(globalHStackView)

        setNeedsUpdateConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        avatarImageView.rounded()
        statusView.rounded()
        pendingMessagesView.rounded()
    }

    override func updateConstraints() {
        updateGlobalStackViewConstraints()
        updateAvatarConstraints()
        updateCenterStackViewConstraints()
        updateRightStackViewConstraints()

        super.updateConstraints()
    }

    func configure(_ model: ProfessionalSDKModel) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.selectionStyle = .default
            self.contentView.backgroundColor = .white
            self.statusView.backgroundColor = ContactStatusViewModel(from: model.status ?? .unknown).color
            self.nameLabel.text = model.name
            self.specialityLabel.text = model.title
            self.fillOrganizationInfo(model)
            self.fillLastMessage(model)
            self.fillUpdateTime(model.lastMessageTimestamp)
            self.fillIsAvailable(model.isAvailable ?? false)
            self.fillAppointmentsIcon(model)
            self.fillPendingMessages(model.pending)
            self.fillAvatarImage(image: model.avatarUIImage)
        }
    }
}

// MARK: - Fill Cell Info
extension ProfessionalsListSDKCell {
    private func fillAvatarImage(image: UIImage?) {
        self.avatarImageView.image = image
    }
    
    private func fillOrganizationInfo(_ model: ProfessionalSDKModel) {
        guard let license = model.license,
              let name = license.organizationName,
              let textColor = license.organizationColor,
              let backgroundColor = license.organizationBackgroundColor else {
            organizationLabel.text = nil
            organizationLabel.textColor = nil
            organizationView.backgroundColor = nil
            return
        }
        organizationLabel.text = name
        organizationLabel.textColor = UIColor(hex: textColor + "FF")
        organizationView.backgroundColor = UIColor(hex: backgroundColor + "FF")
    }

    private func fillLastMessage(_ model: ProfessionalSDKModel) {       
        let imageStatus = NSTextAttachment()
        imageStatus.image = getLastMessageStatusImage(model)
        imageStatus.bounds = getLastMessageStatusSize(model) ?? CGRect(x: 0, y: -1.5, width: 20, height: 10)

        let attributedString = NSMutableAttributedString()
        let type = model.typeLastMessage
 
        switch type {
            case LastMessageSentType.image.rawValue:
                let imageMeta = NSTextAttachment()
                imageMeta.image = Asset.cameraIconLastMessage.image
                imageMeta.bounds = CGRect(x: 2, y: 0, width: 12, height: 12)
                
                if let isOwnMessage = model.isOwnMessage,
                       isOwnMessage {
                    attributedString.append(NSAttributedString(attachment: imageStatus))
                }
                attributedString.append(NSAttributedString(attachment: imageMeta))
                attributedString.append(NSAttributedString(string: " \(L10n.Localizable.Mediquo.Messages.LastMessage.Received.image)"))
            case LastMessageSentType.file.rawValue:
                let imageMeta = NSTextAttachment()
                imageMeta.image = Asset.documentIconLastMessage.image
                imageMeta.bounds = CGRect(x: 2, y: 0, width: 12, height: 12)

                if let isOwnMessage = model.isOwnMessage,
                       isOwnMessage {
                    attributedString.append(NSAttributedString(attachment: imageStatus))
                }
                attributedString.append(NSAttributedString(attachment: imageMeta))
                attributedString.append(NSAttributedString(string: " \(L10n.Localizable.Mediquo.Messages.LastMessage.Received.document)"))
            default:
                if let isOwnMessage = model.isOwnMessage,
                       isOwnMessage {
                    attributedString.append(NSAttributedString(attachment: imageStatus))
                    if let stringLastMessage = model.stringLastMessage {
                        attributedString.append(NSAttributedString(string: " \(stringLastMessage)"))
                    }
                } else {
                    if let stringLastMessage = model.stringLastMessage {
                        attributedString.append(NSAttributedString(string: " \(stringLastMessage)"))
                    }
                }
        }
                
        lastMessageLabel.attributedText = attributedString
    }

    private func fillUpdateTime(_ timestamp: Double?) {
        guard let timestamp = timestamp, timestamp != 0 else {
            updateDateLabel.text = nil
            return
        }
        let date = Date(timeIntervalSince1970: timestamp)
        updateDateLabel.text = DateManager.formatColloquial(date)
    }

    private func fillIsAvailable(_ isAvailable: Bool = false) {
        lockImageView.isHidden = isAvailable
    }

    // swiftlint:disable cyclomatic_complexity
    private func fillAppointmentsIcon(_ model: ProfessionalSDKModel) {
        guard let type = model.appointmentType,
              let status = model.appointmentStatus else {
            appointmentsView.isHidden = true
            return
        }

        if type == .past {
            switch status {
            case .pending:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CobroRecibido.image
            case .cancelled:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32Canceled.image
            case .declined:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CobroDeclinado.image
            case .expired:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CobroExpirado.image
            case .finished:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CobroRealizado.image
            default:
                appointmentsView.isHidden = true
            }
        }

        if type == .future {
            switch status {
            case .pending:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CitaRecibida.image
            case .accepted:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32ConsultaAceptada.image
            case .cancelled:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32Canceled.image
            case .declined:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CitaRechazada.image
            case .expired:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CitaExpirada.image
            case .finished, .freeOfCharge:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32CitaRealizada.image
            case .unpaid:
                appointmentsView.isHidden = false
                appointmentsImage.image = Asset.ic32PagoPendiente.image
            default:
                appointmentsView.isHidden = true
            }
        }
    }

    private func fillPendingMessages(_ pendingMessages: Int?) {
        guard let pendingMessages = pendingMessages, pendingMessages > 0 else {
            pendingMessagesView.isHidden = true
            pendingMessagesLabel.text = nil
            updateDateLabel.textColor = .lightGray
            return
        }
        pendingMessagesView.isHidden = false
        pendingMessagesLabel.text = pendingMessages.description
        updateDateLabel.textColor = UIColor.secondary()
    }
}

// MARK: - LastMessageStatusImage
extension ProfessionalsListSDKCell {
    private func getLastMessageStatusImage(_ model: ProfessionalSDKModel) -> UIImage? {
        guard let status = model.statusLastMessage else {
            return nil
        }
        switch status {
            case 0:
                return Asset.messagePendingNew.image // Asset.messageSentNew.image
            case 1:
                return Asset.messageUnreadNew.image
            case 3:
                return Asset.messageReadNew.image
            default:
                return Asset.messageUnreadNew.image
        }
    }
    
    private func getLastMessageStatusSize(_ model: ProfessionalSDKModel) -> CGRect? {
        guard let status = model.statusLastMessage else {
            return nil
        }
        switch status {
            case 0:
                // Estado Pending
                return CGRect(x: 0, y: -2, width: 16, height: 16)
            case 1:
                // Estado enviado
                return CGRect(x: 0, y: -1.5, width: 20, height: 10)
            case 3:
                // Estado recibido
                return CGRect(x: 0, y: -1.5, width: 20, height: 10)
            default:
                return CGRect(x: 0, y: -1.5, width: 20, height: 10)
        }
    }
}


