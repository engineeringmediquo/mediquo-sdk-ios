//
//  ConnectingTitle+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 1/12/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

class ConnectingTitleView: UIView {
   
    private let titleLabel = UILabel(frame: CGRect(x: 0, y: -4, width: 0, height: 0))
    private let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: 0, height: 0))
    
    private var containerVerticalStackView: UIStackView = {
        let containerVerticalStackView = UIStackView(frame: .zero)
        containerVerticalStackView.axis = .vertical
        containerVerticalStackView.distribution = .fill
        containerVerticalStackView.spacing = 0
        return containerVerticalStackView
    }()
    
    private var containerHorizontalStackView: UIStackView = {
        let containerHorizontalStackView = UIStackView(frame: .zero)
        containerHorizontalStackView.axis = .horizontal
        containerHorizontalStackView.distribution = .fill
        containerHorizontalStackView.spacing = 8
        return containerHorizontalStackView
    }()
    
    private var activityIndicatorView: UIActivityIndicatorView = .init(frame: CGRect(x: 0, y: 0, width: 16, height: 16)) 
    
    // MARK: View lifeCycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func configureView(with title: String?,
                       and subtitle: String?) {
        
        titleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        titleLabel.textColor = MediQuoSDK.instance.style?.primaryContrastColor
        titleLabel.font = FontFamily.GothamRounded.medium.font(size: 14).customOrDefault
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        subtitleLabel.backgroundColor = MediQuoSDK.instance.style?.primaryColor
        subtitleLabel.textColor = MediQuoSDK.instance.style?.chatSubTitleViewColor
        subtitleLabel.font = FontFamily.GothamRounded.book.font(size: 14).customOrDefault
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        activityIndicatorView.color = UIColor.disconnectedBackground()
        activityIndicatorView.startAnimating()
        
        containerVerticalStackView.addArrangedSubview(titleLabel)
        containerHorizontalStackView.addArrangedSubview(activityIndicatorView)
        containerHorizontalStackView.addArrangedSubview(subtitleLabel)
        containerVerticalStackView.addArrangedSubview(containerHorizontalStackView)
    }
    
    override func updateConstraints() {
        containerVerticalStackView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.equalTo(max(titleLabel.frame.size.width,
                                   subtitleLabel.frame.size.width))
            make.height.equalTo(Sizes.containerHeight)
        }
        super.updateConstraints()
    }
}

// MARK: Setup View
extension ConnectingTitleView {
    private func setupView() {
        setupContainerView()
    }
    
    private func setupContainerView() {
        addSubview(containerVerticalStackView)
    }
}

// MARK: Setup Constraints
extension ConnectingTitleView {
    private enum Margins {
        static let zero = CGFloat(0)
    }
    
    private enum Sizes {
        static let zero = CGFloat(0)
        static let containerWidth = CGFloat(130)
        static let containerHeight = CGFloat(40)
    }
}
