//
//  Toast+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 11/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import UIKit

class Toast {
    class func show(message: String, backgroundColor: UIColor? = UIColor.gray, textColor: UIColor? = UIColor.white, over view: UIView? = nil) {
        guard let window: UIWindow = UIApplication.shared.keyWindow else {
            return
        }
        let label = UIToastLabel(frame: CGRect.zero)
        label.backgroundColor = backgroundColor
        label.textColor = textColor
        label.textAlignment = .center
        label.font = FontFamily.GothamRounded.light.font(size: 20).customOrDefault  
        label.text = message
        label.alpha = 0.0
        label.layer.cornerRadius = 8
        label.clipsToBounds = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        window.addSubview(label)

        // Constraints
        let leadingConstant = NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: window, attribute: .leading, multiplier: 1, constant: 40)
        leadingConstant.priority = UILayoutPriority(rawValue: 750)
        leadingConstant.isActive = true

        let trailingConstant = NSLayoutConstraint(item: window, attribute: .trailing, relatedBy: .greaterThanOrEqual, toItem: label, attribute: .trailing, multiplier: 1, constant: 40)
        trailingConstant.priority = UILayoutPriority(rawValue: 750)
        trailingConstant.isActive = true

        let centerXConstant = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)
        centerXConstant.priority = UILayoutPriority(rawValue: 750)
        centerXConstant.isActive = true

        if let view = view {
            let originY = window.convert(window.frame, from: view).origin.y
            let leadingConstant = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: window, attribute: .top, multiplier: 1, constant: originY)
            leadingConstant.isActive = true
        } else {
            let centerYConstant = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: window, attribute: .centerY, multiplier: 1.5, constant: 0)
            centerYConstant.isActive = true
        }

        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
            label.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 2.0, delay: 2.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () in
                label.alpha = 0
            }, completion: { _ in
                label.removeFromSuperview()
            })
        })
    }
}

class UIToastLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        super.drawText(in: rect.inset(by: insets))
    }
}
