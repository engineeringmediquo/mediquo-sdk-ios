//
//  Avatar+View.swift
//  MediQuo-Base
//
//  Created by Cane Allesta Martinez on 24/11/22.
//  Copyright © 2022 Mediquo. All rights reserved.
//

import SnapKit

extension AvatarStatusView {
    private enum Margins {
        static let zero = CGFloat(0)
    }
    
    private enum Sizes {
        static let zero = CGFloat(0)
        static let statusSize = CGFloat(14)
        static let containerSize = CGFloat(32)
    }
}

class AvatarStatusView: UIView {
    private var containerView: UIView = {
        let containerView = UIView(frame: .zero)
        containerView.backgroundColor = MediQuoSDK.instance.style?.primaryColor 
        return containerView
    }()
    
    private(set) var avatarImageView: UIImageView = {
        let avatarImageView = UIImageView()
        return avatarImageView
    }()
    
    private(set) var statusView: UIView = {
        let view = UIView(frame: .zero)
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 2
        return view
    }()
    
    // MARK: View lifeCycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func configureView(from status: SDKStatus?, avatarURL: URL?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if let avatarUrl = avatarURL {
                self.avatarImageView.sd_setImage(with: avatarUrl)
            }
            self.statusView.backgroundColor = ContactStatusViewModel(from: status ?? .unknown).color
            self.layoutIfNeeded()
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        avatarImageView.rounded()
        statusView.rounded()
    }

    override func updateConstraints() {
        containerView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.equalTo(Sizes.containerSize)
            make.height.equalTo(Sizes.containerSize)
        }
        
        avatarImageView.snp.makeConstraints { make in
            make.centerY.equalTo(self.containerView)
            make.leading.equalTo(self.containerView).offset(-16)
            make.size.equalTo(CGSize(width: Sizes.containerSize, height:  Sizes.containerSize))
        }
        
        statusView.snp.makeConstraints { make in
            make.top.equalTo(self.avatarImageView)
            make.trailing.equalTo(self.avatarImageView).offset(6)
            make.size.equalTo(CGSize(width: Sizes.statusSize, height: Sizes.statusSize))
        }
        
        super.updateConstraints()
    }
}

// MARK: Setup View
extension AvatarStatusView {
    private func setupView() {
        setupContainerView()
        setupAvatarStatusView()
        setNeedsUpdateConstraints()
    }
    
    private func setupContainerView() {
        addSubview(containerView)
    }
    
    private func setupAvatarStatusView() {
        containerView.addSubview(avatarImageView)
        containerView.addSubview(statusView)
    }
}




